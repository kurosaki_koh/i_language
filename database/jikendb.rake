require 'idef_db.rb'
  puts self
  include ILanguage::UtilsConfig
  desc 'Create a config file.'
  task :create_config , 'config_path' do |x,args|
    config_path = args.config_path || default_config_file_path()
    config_dir = default_appdata_directory_path()
    puts config_path,File.exist?(config_path)
    Dir.mkdir(config_dir) unless File.exist?(config_dir)
    unless File.exist?(config_path)
      config = {}
      config[:db_config] = {
        :adapter => 'sqlite3',
        :dbfile => default_database_file_path() ,
        :encoding => 'UTF8'
      }
      config[:configs] = {:source => File.join(config_dir,'アイドレスデータ.txt')}
      YAML.dump(config , open(config_path,'w') )
    end
  end

namespace :db do
  include ILanguage
  desc 'Create IDefinition database as a SQLite3 file'
  task :create ,'config_path'  => :create_config do |x,args|
    config_path = args.config_path || default_config_file_path()
    config = load_config(config_path)

    IDefinitionDB::init_sqlite_db(config[:db_config][:dbfile])
#    unless File.exist?(config[:db_config][:dbfile])
#      require 'sqlite3'
#      puts "create database #{config[:db_config][:dbfile]}"
#      db=SQLite3::Database.new(config[:db_config][:dbfile])
#    end
  end
end