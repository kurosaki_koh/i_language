# -*- encoding: utf-8 -*-
class IDefinition < ActiveRecord::Base
  serialize :compiled_hash
  serialize :warnings
  
  def create_library
    ILanguageLibraryParser.new.parse(self.definition)
  end

  def lib
    name = self.name
    
    unless name
      lib_parser = ILanguageLibraryParser.new
      @lib = lib_parser.parse(self.definition)
      unless @lib
        @failure_reasons = lib_parser.failure_reason
        raise Exception.new(lib_parser.failure_reason)
      end
      self.name = @lib.node.name
      self.object_type = @lib.node.object_type
    end

    unless @lib
      lib_parser = ILanguageLibraryParser.new unless lib_parser
      @lib = lib_parser.parse(self.definition)
      unless @lib
        @failure_reasons = lib_parser.failure_reason
      end
    end
    @lib
  end

  def specialty_node
    unless @sp_node
      sp_parser = ISpecialtiesParser.new
      @sp_node = sp_parser.parse(self.lib.node.specialty_text)
      unless @sp_node
        @failure_reasons = sp_parser.failure_reason
        @sp_defs = nil
        return nil
      end
    end
    return @sp_node
  end  
  
  def specialty_defs
    unless specialty_node
      return nil
    end
    @sp_defs = specialty_node.specialty_defs
    return @sp_defs
  end
  
#  def specialty_defs
#    node = self.specialty_node
#    if node #!= SpecialtyParser
#      return node.specialty_defs # .collect{|s| s.text_value}
#    else
#      return nil
#    end
#  end
  
#  before_save :update_from_definition

  def evaluation
    if compiled_hash && compiled_hash['評価']
      compiled_hash['評価']
    elsif lib && lib.node
      lib.node.evaluation
    end
  end


  def convert_to_record(record , node)
      record.name = self.name
      evaluation = node.evaluation
      for key in Job::AbilityKanji.keys
        record[key] = evaluation[Job::AbilityKanji[key]]
      end
      record.definition = definition
      record.note = definition
      record
  end
  
  def to_record
    node = lib.node
    case node.object_type
    when /職業/
      record = Job.find_by_name(self.name)
      record = Job.new unless record
    when /(種族|人)$/
      record = Race.find_by_name(self.name)
      record = Race.new unless record
    else
      return nil
    end
    record = convert_to_record(record , node)
    return record
  end

  def before_save
    if compiled_hash.nil? || definition_changed?
      begin
        update_from_definition
        hash = lib.node.to_hash
        hash['特殊定義']= specialty_defs.collect{|sp| sp.to_hash}
        self.compiled_hash = hash
      rescue Exception
        result = {
              '特殊定義' => nil,
              '定義' => self.definition,
              'エラー' =>  $!.message ,
              :backtrace => $!.backtrace ,
              :failure_reason => @failure_reasons
              }
        self.compiled_hash = result
      end
    end
  end

  private
  def update_from_definition
#    lib = create_library()
    self.name = lib.node.name
    self.object_type = lib.node.object_type
  end
  
end

