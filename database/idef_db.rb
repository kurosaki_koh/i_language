$KCODE='u'

require 'rubygems'
require 'active_record'
#p Dependencies.load_paths
#puts $LOAD_PATH.join("\n")

$: << File.dirname(__FILE__)
#require 'parser/lib.rb'
#require 'i_definition.rb'
require 'yaml'
#require 'yaml_waml'
require 'uconv'

module ILanguage
  module UtilsConfig

    module_function
    
    def default_config_file_path
      File.join(default_appdata_directory_path(),'.jikenrc')
    end

    def default_database_file_path
      File.join(default_appdata_directory_path(),'jiken.sqlite3')
    end

    def default_appdata_directory_path
      case RUBY_PLATFORM
      when /win/ , /mingw/
        File.join(ENV['APPDATA'],'Jiken')
      when /linux/
        ENV['HOME']
      else
        ENV['HOME']
      end
    end

#    DefaultConfigFile = default_config_file_path()
#    @@configs = YAML.load_file(DefaultConfigFile)[:configs]
    public
    
    @@configs = {}
    @@configs[:db_config] = {
        :adapter => 'sqlite3',
        :dbfile => default_database_file_path() ,
        :encoding => 'UTF8'
    }

    def load_config(filename)
      @@configs = YAML.load_file(filename)
    end

  end

  module IDefinitionDB
    include ILanguage::UtilsConfig

    class InitialSchema < ActiveRecord::Migration
      def self.up
        create_table :i_definitions do |t|
          t.string :name
          t.string :object_type
          t.text :definition
          t.text :compiled_hash
          t.timestamps
        end
      end

      def self.down
        drop_table :i_definitions
      end
    end

    class IDefinition < ActiveRecord::Base
    end
    def self.init_sqlite_db(db_filename=default_config_file_path())

      if db_filename =~ /^\.\//
        db_filename = File.join(File.dirname(__FILE__),db_filename) 
      end
      @@configs[:db_config][:dbfile] = db_filename if db_filename

      unless File.exist?(@@configs[:db_config][:dbfile])
        require 'sqlite3'
        db=SQLite3::Database.new(@@configs[:db_config][:dbfile])
      end

      ActiveRecord::Base.establish_connection(
        @@configs[:db_config]
      )

      unless IDefinition.table_exists?
        InitialSchema.migrate(:up)
      end
    end
    
#    init_sqlite_db(@@configs[:db_file])
  end
end



