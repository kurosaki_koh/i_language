$KCODE='u'

require 'rubygems'
require 'active_record'
#p Dependencies.load_paths
#puts $LOAD_PATH.join("\n")

$: << File.dirname(__FILE__)
require 'lib.rb'
require 'i_definition.rb'
require 'yaml'
#require 'yaml_waml'
require 'uconv'

DefaultDB=   {
    :adapter => 'sqlite3',
    :dbfile => File.join(File.dirname(__FILE__),'idef.sqlite3') ,
    :encoding => 'UTF8'
  }

module IdefSqlite
  @@initialized = false
  def self.init_sqlite_db(db = DefaultDB)
    return if @@initialized
    ActiveRecord::Base.establish_connection(
      db
    )
    @@initialized = true
  end
end

