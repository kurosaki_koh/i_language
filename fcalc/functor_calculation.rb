module ILanguage
  module FCalc
    class FunctorCalculationContainer
      attr_reader :children

      def initialize(unit , evaluator)
        super(unit , evaluator)
        @children = []
      end

      def record(functor,context)
        calc = FunctorCalculation.new
        calc.function_name = functor.function_name
        calc.args = functor.args.collect{|arg|arg.evaluate(context)}
        calc.total = functor.evaluate(context)
        @children << calc
      end
    end

    class FunctorCalculation
      attr_accessor :expr , :function_name , :args , :result, :functor , :error
      attr_reader :children 

      def initialize
        @args = []
        @children = []
        @functor = nil
        @error = nil
      end

      def make_child(functor)
#        calc.function_name = functor.function_name
#        calc.args = functor.args.collect{|arg|arg.evaluate(context)}
#        calc.total = functor.evaluate(context)
        @children << calc = self.class.new
        calc.functor = functor
        calc.function_name = functor.function_name
        calc
      end

      def last_child
        @children[-1] || self
      end

      def to_s
#        result = "#{@function_name}(#{@args.join(' , ')}) = #{@result}" if @function_name
#        children_result = @children.collect{|c|c.to_s}
        if @children.size > 0
          self_str = @functor.format(self) if @function_name
          children_result = @children.collect{|c|c.functor.format(c)}
          [self_str , children_result].compact.flatten.join("\n")
        else
          @result.to_s
        end
      end
    end
  end
end