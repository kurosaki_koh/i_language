# -*- encoding: utf-8 -*-
#gem "activerecord", "=2.1.2"
require 'active_support'
module ILanguage
  module FCalc

    BuiltinFunctions = {}

    Parser = ILanguage::FCalc::FCalcFormatParser.new
    
    def self.to_rd(val)
      result = (1.2 ** val)
      result >= 0.05 ? result.round(1) : result
    end

    def self.build_function(f_name , opts = {} ,  &proc)
      pf = BuiltinFunctions[f_name] = if opts[:source]
        root = Parser.parse(opts[:source])
        raise Parser.failure_reason if root.nil?
        pf = root.interpret
        pf
      else
        ProcFunctor.new(nil , opts[:params] , opts[:result_type] , &proc)
      end
      pf.function_name = f_name
      pf
    end

    build_function('ＲＤ' ,
      :params => [['評価値',Numeric]] ,
      :result_type => Numeric  ) {|ctx , options , *args|
      to_rd(args[0])
    }

    build_function('合計' ,
      :params => [['項目' , TypedArrayClass[Numeric]]] ,
      :result_type => Numeric ) {|ctx , options , *args|
      args.sum
    }

    build_function('評価変換' ,
      :params => [['ＲＤ値',Numeric]] ,
      :result_type => Numeric ) {|ctx , options , *args|
      val = args[0]
      if val.class == Float
        rounded = val.round(1)
        val = rounded if rounded > 0
      end
      low = (Math.log(val) / Math.log(1.2)).floor
      high_threshold = to_rd(low+1)
      (val >= high_threshold) ?  low + 1 : low
    }
    build_function('ＲＤ合算' ,
      :params => [['評価値' , TypedArrayClass[Numeric]]] ,
      :result_type => Numeric ) {|ctx , options , *args|
      if calc = options[:calculation]
        self_calc = calc.children.pop
      end
      rd_fctr = ctx.variable('ＲＤ')
      sum_fctr = ctx.variable('合計')
      rds = args.collect{|arg|
        rd_fctr.evaluate(ctx, :calculation => calc , :args => [arg])
      }
      rdsum = sum_fctr.evaluate(ctx , :calculation => calc ,  :args => rds)
      result = ctx.variable('評価変換').evaluate(ctx, :calculation => calc , :args => [rdsum])
      if self_calc
        calc.children.push self_calc
      end
      result
    }

    build_function('関数合成' ,:source => %!
関数（Ａ：関数，Ｂ：関数）｛
　関数（引数１：任意）｛
　　＜Ｂ＞（＜Ａ＞（＜引数１＞））
　｝
｝
! )


    build_function('切り捨て' ,
      :params => [['値',Numeric] , ['桁数',Numeric]] ,
      :result_type => Numeric  ) {|ctx , options , *args|

      shift_val = (10**args[1])
      val = args[0].to_f * shift_val
      val.floor.to_f / shift_val
    }

    build_function('乗算関数' ,:source => %!
関数（倍数：数値）｛
　関数（引数：任意）｛
　　＜引数＞＊＜倍数＞
　｝
｝
! )
  end
end