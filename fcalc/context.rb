# -*- encoding: utf-8 -*-
module ILanguage
  module FCalc

    module ContextModule

      def increment_ref(name)
        self.ref_count ||= {}
        self.ref_count[name] ||= 0
        self.ref_count[name] += 1
        raise "変数 '#{name}' が循環参照を起こしています。" if self.ref_count[name] > 10
      end

      def decrement_ref(name)
        self.ref_count ||= {}
        self.ref_count[name] ||= 0
        self.ref_count[name] -= 1
      end

      def reset_ref
        self.ref_count ||= {}
        self.ref_count.clear
      end

      def variable(name)
        self.variables[name] ? self.variables[name] : (self.parent && self.parent.variable(name))
      end
    end
    
    class Context
      include ContextModule
      attr_reader :variables
      attr_accessor  :parent , :statements , :ref_count

      def initialize(parent = nil)
        @variables = {}
        @statements = []
        @parent = parent

        @variables.update(BuiltinFunctions)
      end
    end

  end
end