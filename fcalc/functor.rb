# coding: utf-8

require File.join(File.dirname(__FILE__),'context.rb')
require File.join(File.dirname(__FILE__),'functor_calculation.rb')

require 'delegate'

module ILanguage
  module FCalc
    class Parameter
      attr_reader :name , :type_name
      attr_accessor :value , :p_class

      unless defined?(TypeNameToClass)
        TypeNameToClass = {}
        ClassToTypeName = {}
        ClassList = []
      end

      def self.register_type( klass , name)
        unless ClassList.include?(klass)
          TypeNameToClass[name]=klass
          ClassToTypeName[klass]=name
          ClassList << klass
        end
      end

      def self.find_type(klass)
        distances = {}
        begin
          candidates = ClassList.select{|cand|
            cand >= klass
          }
        rescue => e
          puts cand.to_s
          puts klass.to_s
          raise e
        end

        return nil if candidates.size == 0
        
        candidates.each do |item|
          distances[item] = distance(item , klass)
        end

        candidates.min{|a,b|distances[a] <=> distances[b]}
      end

      def self.distance(super_class , klass)
        n = 0
        target = klass
        until super_class == target
          target = target.superclass
          n += 1
        end
        return n
      end

      def Parameter.to_type_name(klass)
        klass2 = if  klass.class <= TypedArrayClass
          klass.var_class
        else
          klass
        end
        
#        name = if target = ClassList.find{|registered| registered >= klass2 }
#          ClassToTypeName[target]
#        else
#          ''
#        end
        name = if target = find_type(klass2)
          ClassToTypeName[target]
        else
          ''
        end

        if  klass.class <= TypedArrayClass
          name = "配列（#{name}）"
        end
        name
      end
      
      register_type Numeric , '数値'
      register_type Array , '配列'
      register_type String , '文字列'
      register_type Object , '任意'

      def initialize(name , type_name)
        @name = name
        if type_name.class == String
          @type_name = type_name
          @p_class = TypeNameToClass[type_name]
          raise "仮引数'#{name}'の型名が不正です：#{type_name}。" if @p_class.nil?
        elsif type_name.class <= Class || TaigenClass
          @p_class = type_name
          @type_name = Parameter.to_type_name(type_name)
        end
        @value = nil
      end

      def reset
        @value = nil
      end
    end

    module TaigenClass
    end

    class Boolean
      include TaigenClass
      def Boolean.>(other)
        other == TrueClass || other == FalseClass
      end

      def Boolean .>=(other)
        self > other
      end

    end

    class TypedArrayClass
      include TaigenClass
      attr_reader :var_class

      def initialize(var_class)
        @var_class = var_class
      end

      def self.[](klass)
        self.new(klass)
      end

      def check_type(target)
        if target.class <= Array
          target.all?{|item| @var_class >= item.class}
        else
          @var_class >= target
        end
      end

      def new(array)
        TypedArray.new(array)
      end
    end

    unless defined?(TypedArray)
      class TypedArray < DelegateClass(Array)
        def initialize(ary_class , ary=nil)
          super(ary)
          @ary_class = ary_class
        end

        def class
          @ary_class
        end
      end
    end

    class Functor
      attr_accessor :function_name , :arguments , :source
      attr_reader :context , :parameters , :result_type

      def initialize(context )
        @context = context
        @arguments = []
      end

      def calculate(context = @context , arg = {})
        calc = FunctorCalculation.new
        arg[:calculation] = calc
        calc.result = self.evaluate(context , arg)
        calc
      end
      
      def evaluate(context = @context , args = {})
        evaluated_arguments = prepare(context , args)
        result = do_process(evaluated_arguments , context , args)
        post_process(result , context , args)
        result
      end

      def do_process(arguments , context , args={})
        nil
      end
      
      def prepare(context , args)
        result = arguments.collect{|param|
          param.class <= Functor ? param.evaluate(context , args) : param
        }
        check_parameter_types(result , context , args)
        if calc = args[:calculation]
          calc = calc.make_child(self)
          calc.function_name = self.function_name
          calc.args.clear
          calc.args.concat result
        end
        result
      end

      def post_process(result , context,args)
        if calc = args[:calculation]
          calc.last_child.result = result
        end
      end

      def format(calc)
        "#{self.function_name}(#{calc.args.join(' , ')}) = #{calc.result}"
      end

      def self.parameter(name , p_klass)
        @parameters ||= []
        #type check
        raise "#{name.to_s}はStringでなければならない。" unless name.class == String
        raise "#{p_klass.to_s}はClassオブジェクトでなければならない。" unless p_klass.class <= Class

        @parameters << Parameter.new(name,p_klass) unless @parameters.find{|param|param.name == name}
      end

      def self.inherited(subclass)
        subclass.class_eval do
          @parameters = []
        end
      end

      def self.parameters
        @parameters
      end

      def parameters
        self.class.parameters
      end

      def self.result_type(val = nil)
        @result_type = val if val
        @result_type
      end

      def result_type(context)
        self.class.result_type
      end
      
      def check_parameter_types(arguments , context , args)
        normal_params = self.normal_parameters
        if check_normal_parameter_size(arguments , context , args)
          raise "エラー：関数'#{function_name}'の仮引数と実引数の数が合いません。(仮引数#{normal_params.size}に対し#{arguments.size})。"
        end

        result = check_normal_parameter_types(arguments , context , args)
        unless result == true
          raise "エラー：関数 ""#{result[:function_name]}"" の引数 ""#{result[:param].name}"" の型(#{Parameter.to_type_name(result[:arg_class])})が定義（#{result[:param].type_name}）と一致しません。"
        end

        check_variable_parameters(arguments , context , args)
        true
      end

      def check_normal_parameter_types(arguments , context , args)
        for param , arg in self.normal_parameters.zip(arguments)
          break if arg.nil?
          arg_class = arg_class(arg,context)

          flag = if param.p_class == Object
            true
          elsif param.p_class.class == TypedArrayClass
            param.p_class.check(arg_class)
          elsif param.p_class == LamdaFunctor
            if arg.class == ReferenceFunctor && !arg.is_function_application?
              ref = arg
              while ref.class <= ReferenceFunctor
                ref = ref.evaluate(context , args)
                break unless ref.class <= Functor
              end
              ref.class <= LamdaFunctor
            elsif arg.class <= LamdaFunctor
              true
            else
              false
            end
          else
            param.p_class >= arg_class
          end
          unless flag
            return {:function_name => function_name , :param => param , :arg_class => arg_class , }
          end
        end
        return true
      end

      def check_normal_parameter_size(arguments , context , args)
        normal_params = self.normal_parameters
        arguments.size < normal_params.size || ( arguments.size > normal_params.size && !self.variable_parameter)
      end
      
      def variable_parameter
        begin
          case
          when self.parameters.size == 0
            nil
          when self.parameters[-1].nil?
            nil
          when self.parameters[-1].p_class.class != TypedArrayClass
            nil
          else
            self.parameters[-1]
          end
        rescue NoMethodError => e
          raise "variable_parameter#{self.parameters[-1].class}　#{self.function_name }  #{self.class}"
        end
      end

      def arg_class(arg,context)
        arg.class <= Functor ? arg.result_type(context) : arg.class
      end

      def normal_parameters
        @normal_parameters ||= if self.variable_parameter
         self.parameters[0..-2] 
        else
         self.parameters.dup
        end
      end
      
      def check_variable_parameters(arguments , context , args)
        return true unless v_param = self.variable_parameter

        normal_params_size = self.normal_parameters.size
        variable_args = arguments[(- normal_params_size)..-1]
        v_param_class = self.variable_parameter.p_class
        arg_class = nil
        if variable_args.all?{|arg|
            arg_class = self.arg_class(arg , context)
            v_param_class.check_type( arg_class)
        }
          true
        else
          raise "関数 ""#{function_name}"" の可変長引数の型(#{Parameter.to_type_name(arg_class)})が宣言(#{v_param.type_name})と一致しません。"
        end
      end
    end

    class BinaryExprFunctor < Functor
      parameter '引数１' , Numeric
      parameter '引数２' , Numeric
      result_type Numeric
      
      Operators = {
        :add => '+' ,
        :subtract => '-' ,
        :multiply => '*' ,
        :divide => '/'
      }
      def initialize(context , function_name , op1 , op2)
        super(context)
        @function_name = function_name
        @op1 = op1
        @op2 = op2
      end

      def arguments
        [@op1 , @op2]
      end

      def do_process(arguments , context = @context , args ={})
        value1 , value2  = arguments
#        value1 = @op1.evaluate(context,args)
#        value2 = @op2.evaluate(context,args)

        case @function_name
        when :add
          value1 + value2
        when :subtract
          value1 - value2
        when :multiply
          value1 * value2
        when :divide
          value1.to_f / value2
        when :rd_merge
          rd_merge = context.variable('ＲＤ合算')
          if calc = args[:calculation]
            self_calc = calc.children.pop
          end
          result = rd_merge.evaluate(context , :args => [value1 , value2 ] ,  :calculation => calc)
          
          result
        else
          nil
        end
      end

      def format(calc)
        sprintf("#{calc.args[0].to_s} #{Operators[calc.function_name]} #{calc.args[1].to_s} = #{calc.result}")
      end
    end
    
    class NumberFunctor < Functor
      result_type Numeric
      
      def initialize(context , value)
        super(context)
        @function_name = :number
        @value = value
      end

      def arguments
        [@value]
      end
      
#      def do_process(argments , context=@context , args ={})
#        @value
#      end
      def evaluate(context=@context , args ={})
        @value
      end

      def to_s
        "NumberFunctor:" + @value.to_s
      end
    end

    class BooleanFunctor < NumberFunctor
      result_type Boolean
      def initialize(context , value)
        super(context , nil)
        @function_name = :boolean
        @value = value
      end

      def to_s
        "BooleanFunctor:" + @value.to_s
      end
    end

    class ArrayFunctor < Functor

      def initialize(context , value)
        super(context)
        @function_name = :array
        @value = value
      end

      def arguments
        @value
      end

      def evaluate(context=@context , args ={})
        @value.collect{|item| item.class == String ? item : item.evaluate(context , args)}
      end

      def to_s
        "ArrayFunctor:" + @value.to_s
      end
    end

    class VariableFunctor < Functor
      def initialize(context , expr)
        super(context)
        @expr = expr
      end

      def arguments
        []
      end

      def evaluate(context , args ={})
        @value ||= @expr.evaluate(context)
      end

      def result_type(context)
        @value && @value.class
      end

    end

    class ReferenceFunctor < Functor
      attr_reader :identifier
      def initialize(context ,identifier,parameters = nil)
        super(context)
        @identifier = identifier
        @parameters = parameters || []
        @function_name = '参照'
      end

      def evaluate(context = @context , args ={})
        var = context.variable(@identifier)
        raise "識別子 #{@identifier} が定義されていません。" if var.nil?

        hash = {}
        calc = hash[:calculation] = args[:calculation]

        begin
          context.increment_ref(@identifier)
        rescue RuntimeError => e
          if calc
            calc.result = nil
            calc.error = e
          end
          raise e
        end

        if var.class <= LamdaFunctor && !self.is_function_application?
          return var
        end
        if var.class <= LamdaFunctor && self.is_function_application?
          hash[:args] = @parameters
          return var.evaluate(context, hash)
        end

        hash[:args] = @parameters
        result = var.class <= Functor ? var.evaluate(context, hash) : var

        if result.class <= UserDefinedFunctor && self.is_function_application?
          result = result.evaluate(context,hash)
        end
        context.decrement_ref(@identifier)
        result
      end

      def is_function_application?
        @parameters.size > 0
      end

      def result_type(context)
        var = context.variable(@identifier)
        raise "識別子 #{@identifier} が定義されていません。" if var.nil?
        if var.class <= LamdaFunctor && !self.is_function_application?
          var.result_type(context)
        elsif var.class <= Functor
          var.result_type(context)
        else
          var.class
        end
      end

      def is_circular?(context)
        context.reset_ref
        flag = false
        begin
          self.evaluate(context)
        rescue RuntimeError => e
          if e.to_s =~ /循環参照/
            flag = true
          end
        ensure
          context.reset_ref
        end
        flag
      end

      def to_s
        "Reference:#{@identifier} , #{@parameters.collect{|param|param.class.to_s}.join(' ')}"
      end

      def format(calc)
        "#{@identifier} ←　#{calc.result.to_s}"
      end
    end

    class LamdaFunctor < Functor
      attr_accessor :binding
    end
    Parameter.register_type LamdaFunctor , '関数'

    class UserDefinedFunctor < LamdaFunctor
      def initialize(context , parameters , expr)
        super(context)
        @local_context = Context.new
        @expr = expr
        @parameters = parameters
        @value = {}
      end

      def evaluate(context , args={})
#        @value[args[:args]] ||= begin
          if @binding
            @binding.parent = context
            context = @binding
          end
          local_context = Context.new(context)
          check_parameter_types(args[:args] , local_context , args)
          apply(args[:args],local_context)
          hash = args.dup
          hash[:args]=[]
          if @expr.class <= UserDefinedFunctor
            result = @expr.dup
            result.binding = local_context
            result
          else
            @expr.evaluate(local_context , hash)
          end
#          @expr.evaluate(local_context , args)
#        end
      end

      def parameters
        @parameters
      end
      
      def reset_parameters
        @local_context.variables.clear
      end

      def apply(args,context)
        @parameters.zip(args){|param , value|
          context.variables[param.name] = value
        }
      end

      def result_type(context)
#        if @parameters.all?{|param|
#            context.variable(param.name)
#
#            }
#          @expr.result_type(context)
#        else
#          self.class
#        end
        if @expr.class <= UserDefinedFunctor
          UserDefinedFunctor
        else
          @expr.result_type(context)
        end
      end

      def to_s
        @source
      end
    end

    class ProcFunctor < LamdaFunctor
      def initialize(context , parameters ,result_type , &proc)
        super(context)
        @proc = proc

        @parameters = parameters.collect{|param|
          Parameter.new(param[0] , param[1])
        }
        @result_type = result_type
      end

      def prepare(context , args)
        result = args[:args].collect{|param|
          param.class <= Functor ? param.evaluate(context , args) : param
        }
        check_parameter_types(result ,context,args) if @parameters
        if calc = args[:calculation]
          calc = calc.make_child(self)
          calc.function_name = self.function_name
          calc.args.clear
          calc.args.concat result
        end
        result
      end

      def do_process(arguments , context , args)
        @proc.call(context , args , *arguments)
      end

      def result_type
        @result_type
      end

      def parameters
        @parameters
      end
    end

  end

end
