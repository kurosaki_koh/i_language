# coding: utf-8

require 'treetop'
require File.join(File.dirname(__FILE__),'../parser/lib/token.rb')
require File.join(File.dirname(__FILE__),'fcalc.rb')
#Treetop.load File.join(File.dirname(__FILE__),'../parser/lib/token.treetop')
#Treetop.load File.join(File.dirname(__FILE__),'fcalc.treetop')
require File.join(File.dirname(__FILE__),'functor.rb')
require File.join(File.dirname(__FILE__),'../parser/lib/jcode_util')

module ILanguage
  module FCalc
    module BinaryOperatorNode
      def inspect_expr
        self.function_name
      end
    end

    module ExprNode
      def self.extended(mod)
        mod.instance_eval do
          def functor_class=(klass)
            @functor_class = klass
          end

          def functor_class(var = nil)
            @functor_class = var
            @functor_class
          end
        end

        def create_functor(context,*args)
          functor = functor_class.new(context,*args)
          functor.source = self.text_value
          functor
        end
      end
#      def to_ary
#        [self]
#      end
    end

    module ParenthesizedExprNode
      include ExprNode
      def interpret(context)
        expr.interpret(context)
      end
    end

    class NumbersNode < Treetop::Runtime::SyntaxNode
      include ExprNode
      include JcodeUtil

      def interpret(context)
        value = parse_num(text_value)
        value = value.to_f / 100 if self.is_persented?
        NumberFunctor.new(context , value)
      end

      def is_persented?
        persented.text_value != ''
      end

      def is_negative?
        negative.text_value != ''
      end
      
      def inspect_expr
        parse_num(text_value)
      end
    end

    module StringNode
      include ExprNode

      def inner_literal
        if self.text_value =~ /^(["']|”|’)([^\1]*)\1$/m
          $2
        else
          self.text_value
        end
      end
      
      def interpret(context)
        inner_literal
      end
    end

    class BooleanNode <  Treetop::Runtime::SyntaxNode
      include ExprNode
      def interpret(context)
        BooleanFunctor.new(context , %w[true 有効].include?(self.text_value) ? true : false)
      end
    end

    class ArrayNode <  Treetop::Runtime::SyntaxNode
      include ExprNode

      def interpret(context)
        nodes = fc_array_items.to_nodes.collect{|node| node.interpret(context)}
        first_class = nodes[0].class

        ArrayFunctor.new(context , nodes)
      end
    end

    module StatementsNode
      def last_expr
        array = self.to_nodes
        array.reverse.find{|item|item.extension_modules.include?(ExprNode)}
      end

      def to_nodes
        if defined? self.statements
          [statement , self.statements.to_nodes].flatten
        else
          [statement]
        end
      end

      def interpret
        context = ILanguage::FCalc::Context.new
        result = nil
        self.to_nodes.each do |statement|
          begin
            result = statement.interpret(context)
            result.source = statement.text_value if defined?(result.source)
            result
          rescue => e
            raise e
          end
        end
        result
      end
    end

    module BinaryExprNode
      include ExprNode
      def interpret(context)
        rpn = BinaryExprNode.sort_to_rpn(self.to_list)

        stack = []
        for item in rpn
          if item.extension_modules.include?(BinaryOperatorNode)
            node = BinaryExprFunctor.new(context , item.function_name , stack[-2] , stack[-1])
            stack.pop
            stack.pop
            stack.push(node)
          else
            stack.push(item.interpret(context))
          end
        end
        stack[0]
      end

      def to_list
        result = [unit_expr , binary_operator]
        if expr.extension_modules.include?(BinaryExprNode)
          result += expr.to_list
        else
          result << expr
        end
        result
      end

      def inspect_expr
        BinaryExprNode.sort_to_rpn(to_list).collect{|item|item.inspect_expr}.inspect
      end

      def self.inspect_expr(array)
        array.collect{|item|item.inspect_expr}.inspect
      end
      def self.find_first_operator(array)
        loc = -1
        prior = -1
        array.each_with_index do |item,index|
          if item.class <= Treetop::Runtime::SyntaxNode && item.extension_modules.include?(BinaryOperatorNode)
            if prior < item.priority
              loc = index
              prior = item.priority
            end
          end
        end
        loc
      end

      def self.sort_to_rpn(array)
        loc = self.find_first_operator(array)
        return array.flatten if loc == -1
#        if loc == -1
#          puts self.inspect_expr(array.flatten)
#          return array.flatten
#        end

        operator = array[loc]
        left_operand = array[loc-1]
        right_operand = array[loc+1]

#        raise "left operand is nil." if left_operand.nil?
#        if right_operand.nil?
#          raise "right operans is nil. loc = #{loc} , #{self.inspect_expr(array)}"
#        end

        left_array = array[0...(loc-1)] || []
        right_array = array[(loc+2)..-1] || []
        current_expr = [left_operand , right_operand , operator]
        return sort_to_rpn((left_array << current_expr) + right_array)
      end

    end

    module AsignmentNode
      include ExprNode
      def interpret(context)
        variable = right_expr.interpret(context)
        context.variables[name.text_value] = variable
      end
    end

    module ReferenceExprNode
#    class ReferenceExprNode < Treetop::Runtime::SyntaxNode
      include ExprNode
      def interpret(context)
        if parameters.text_value == ''
          ReferenceFunctor.new(context , identifier.text_value)
        else
          params = parameters.parameter_application.to_nodes.collect{|node| node.interpret(context)}
          ReferenceFunctor.new(context , identifier.text_value , params )
        end
      end
    end

    module LamdaExprNode
      include ExprNode
      def interpret(context)
        params = parameter_list.to_nodes.collect{|node|node.interpret(context)}
        inner_expr = right_expr.interpret(context)
        inner_expr.source = right_expr.text_value
        UserDefinedFunctor.new(context , params , inner_expr)
      end
    end

    module ParameterDeclarationNode
      def interpret(context)
        Parameter.new(identifier.text_value , type_name.text_value)
      end
    end

    module ParameterApplicationNode
      def to_nodes
        if defined? self.parameter_application
          [self.expr , self.parameter_application.to_nodes].flatten
        else
          [self]
        end
      end
    end
  end
end

require File.join(File.dirname(__FILE__),'builtin_functions.rb')