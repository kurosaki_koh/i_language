# -*- encoding: utf-8 -*-
require File.dirname(__FILE__) + '/../parser/lib.rb'
require 'jcode'
$KCODE = 'u'

def inspect_library(hash)
  for prop in ['Ｌ：名','名称','要点','周辺環境','→次のアイドレス']
    puts prop + ' ; ' + hash[prop].to_s
  end
end

def inspect_spec(hash)
  std_props =   ['定義','行為名','兵科種別','発動条件','特殊種別']

  props = hash.keys.select{|k| std_props.all?{|s| s != k } }
  for prop in std_props + props
    puts prop + ' ： ' +  hash[prop].inspect
  end
  puts
  
end

if ARGV.size == 0
  puts '引数にテキストファイル名を指定してください。sample_def1.txtとか。'
  exit
end

#外側のＬ：を解釈するパーザを生成
i_language_parser = ILanguageLibraryParser.new

#内側の形式化書式による特殊を解釈するパーザを生成
sp_parser = ISpecialtiesParser.new

#ｉ言語定義を読み込み
source = ARGF.read

#外側のＬ：を解析
parsed_library = i_language_parser.parse(source)

#文法エラーで解析失敗した場合はnilが返る。
if parsed_library.nil?
#文法エラーが発生した箇所を出力して終了
  puts "Syntax Error:" + i_language_parser.failure_reason
  exit 1
end

#構文解析した内容をハッシュに変換
library_hash = parsed_library.node.to_hash

#ハッシュの内容を出力
inspect_library(library_hash)

#Ｌ：の中から特殊定義を取り出す。
specs_source = parsed_library.node.specialty_text

#内側の形式化書式による特殊を
parsed_specs = sp_parser.parse(specs_source)
#文法エラーで解析失敗した場合はnilが返る。
if parsed_specs.nil?
#文法エラーが発生した箇所を出力して終了
  puts "Syntax Error:" + i_language_parser.failure_reason
  exit 1
end

#解析した特殊を取り出す。戻り値は特殊それぞれを示す構文解析木ノードの配列。
specs = parsed_specs.specialty_defs

#ノードそれぞれをハッシュに変換して表示。
for spec in specs
  hash = spec.to_hash
  inspect_spec(hash)
end