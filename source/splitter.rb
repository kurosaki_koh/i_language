# -*- encoding: utf-8 -*-
$KCODE='s'

Boundaries = %w[
|||||||||||||||||||||||||||||||||||||||
`bdAChX
{ÝAChX
ZpAChX
æè¨AChX
R×õAChX
Vbvn
®êìÆ
]


FileNames = %w[
C³ð.txt
lEEÆEEÆSAChX.txt
ACEENPCAChX.txt
{ÝEgDAChX.txt
ZpEâZECxgAChX.txt
æè¨AChX.txt
R×õEACeAChX.txt
VbvnAChX.txt
]

DefaultSourcePath = File.join(File.dirname(__FILE__),'AChXf[^.txt')

source_path = ARGV.shift || DefaultSourcePath

$source = open(source_path,'r')
output_path = File.dirname(source_path)

def split_file(filename,boundary,mode = 'w')
  reg = /^#{boundary}/ unless boundary.nil?
  open(filename,mode){|port|
#    port.puts boundary if boundary
    while !$source.eof?
#    	puts [$line , boundary,reg]
    	
    	if  $line =~ reg
    	  break
    	end
    	port.write($line)
    	$line = $source.readline
    end
    port.flush
  }
end

$line = $source.readline
for i in 0...FileNames.size
  boundary = Boundaries[i]
  filename = File.join(output_path, FileNames[i])

  split_file(filename,boundary)
  i+=1
end

split_file('C³ð.txt','','a')


$source.close