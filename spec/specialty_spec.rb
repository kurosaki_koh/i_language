# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')

#require File.join(File.dirname(__FILE__) , '../calculator/unit')

describe Specialty do
  before(:each) do
    @parser =  EVCCParser.new
    @context = Context.new
  end

  def create_cyborg_specs
    troop = @context.parse_troop <<EOT
soldier:サイボーグ;
EOT
    soldier = troop.children[0]
    specs = soldier.specialties
    return soldier , specs
  end

  def create_soldier_and_specs(idress_name)
    soldier = Soldier.new(@context)
    specs = @context.create_spec(soldier , idress_name )
    return soldier , specs
  end

  def create_soldier_and_spec(idress_name,sp_name)
    soldier = Soldier.new(@context)
    specs = @context.create_spec(soldier , idress_name )
    spec = specs.find{|sp| sp.name == sp_name}
    return soldier , spec
  end

  it "can initialize" do
    soldier , specs = create_cyborg_specs()
    specs.size.should == 5
    bonus_spec = specs.find{|sp| sp.name == 'サイボーグの筋力・耐久力補正'}
    bonus_spec.should_not be_nil
    bonus_spec.owner.object_id.should == soldier.object_id
  end

  it 'returns owner' do
    soldier , specs = create_cyborg_specs()

    bonus_spec = specs.find{|sp| sp.name == 'サイボーグの筋力・耐久力補正'}
    bonus_spec.owner.object_id.should == soldier.object_id
  end
  
  it 'returns correct target' do
    troop = Troop.new(@context)
    
    soldier , specs = create_cyborg_specs()
    troop.add soldier
    
    bonus_spec = specs.find{|sp| sp.name == 'サイボーグの筋力・耐久力補正'}
    bonus_spec.target.object_id.should == soldier.object_id

    vehicle = Vehicle.new(@context)
    troop.add vehicle

    pilot , pilot_specs = create_soldier_and_specs('名パイロット')
    vehicle.add(pilot)

    pilot_bonus = pilot_specs.find{|sp| sp.name == '名パイロットの搭乗戦闘補正'}
    pilot_bonus.target.object_id.should == vehicle.object_id

    troop.remove(soldier)
    vehicle.add(soldier)
    soldier.reflesh
    bonus_spec = soldier.specialties.find{|sp| sp.name == 'サイボーグの筋力・耐久力補正'}
    bonus_spec.target.object_id.should == soldier.object_id

    knight , knight_specs = create_soldier_and_specs('騎士')
    troop.add(knight)
    knight_bonus = knight_specs.find{|sp| sp.name == '騎士の白兵距離戦闘補正'}
    knight_bonus.target.object_id.should == knight.object_id

    troop.remove(knight)
    vehicle.add(knight)
    knight_bonus.target.object_id.should == vehicle.object_id
  end

  it 'returns disabled or not.' do
    troop = parse_troop <<EOT
soldier:サイボーグ;
EOT
    soldier = troop.children[0]
    specs = soldier.specialties
#    troop.add soldier

    bonus_spec = specs.find{|sp| sp.name == 'サイボーグの筋力・耐久力補正'}
    bonus_spec.is_disabled?.should == false

    @context.disabled_sps << 'サイボーグの筋力・耐久力補正'
    @context.reset_situation_caches
#    @context.all_disabled_sps_cashe = nil
#    troop.all_disabled_sps_cashe = nil
#    soldier.all_disabled_sps_cashe = nil
    troop.reset_situation_caches
    soldier.reset_situation_caches
    bonus_spec.is_disabled?.should == true
  end

  it 'returns spec availability.' do
    source = <<EOT
soldier:ハッカー＋高機能ハンドヘルド;
EOT
    troop = parse_troop(source)
    soldier = troop.children[0]
    b_spec = soldier.find_spec('高機能ハンドヘルドの情報系職業補正')
#    soldier , specs = create_soldier_and_specs('高機能ハンドヘルド')
#    b_spec = specs.find{|sp| sp.name == '高機能ハンドヘルドの情報系職業補正'}
    b_spec.is_available?.should == false

    soldier.conditions << '（位置づけ（情報系）の職業による）'
    soldier.reset_situation_caches
#    soldier.all_conditions_cashe = nil
    b_spec = soldier.find_spec('高機能ハンドヘルドの情報系職業補正')
    soldier.all_bonuses.include?(b_spec).should be_truthy
    b_spec.is_available?.should == true

    soldier.disabled_sps << '高機能ハンドヘルドの情報系職業補正'
    soldier.reset_situation_caches
#   soldier.all_disabled_sps_cashe = nil
    soldier.reflesh
    soldier.all_bonuses.include?(b_spec).should be_falsey
#    b_spec.is_available?.should == false

    soldier , specs = create_cyborg_specs()

    bonus_spec = specs.find{|sp| sp.name == 'サイボーグの筋力・耐久力補正'}
    bonus_spec.is_available?.should == true
  end

  it 'judges basic bonus.' do
    soldier , spec = create_soldier_and_spec('サイボーグ','サイボーグの筋力・耐久力補正')
    spec.is_ability_bonus?.should == true
    spec.is_whole_bonus?.should == false
    spec.is_applicable?(soldier , '筋力').should == true
    spec.is_applicable?(soldier , '耐久力').should == true
    spec.is_applicable?(soldier , '知識').should == false
    soldier.disabled_sps << 'サイボーグの筋力・耐久力補正'
    soldier.disabled_sps.should == ['サイボーグの筋力・耐久力補正']
    soldier.reflesh
    soldier.reset_situation_caches
    #all_disabled_sps_cashe = nil
    soldier.all_bonuses.include?(spec).should be_falsey
    #    spec.is_applicable?(soldier , '耐久力').should == false

    source = 'hacker:ハッカー+高機能ハンドヘルド；'
    troop = parse_troop(source)
    soldier = troop.children[0]
    spec = soldier.find_spec('高機能ハンドヘルドの情報系職業補正')
#    soldier , spec = create_soldier_and_spec('高機能ハンドヘルド','高機能ハンドヘルドの情報系職業補正')
    spec.is_ability_bonus?.should == false
    spec.is_judge_bonus?.should == true
    spec.is_whole_bonus?.should == false
    spec.is_applicable?(soldier , '情報戦闘').should == false
    soldier.conditions << '（位置づけ（情報系）の職業による）'
    soldier.reset_situation_caches
    spec = soldier.find_spec('高機能ハンドヘルドの情報系職業補正')
#    soldier.all_conditions_cashe = nil
    spec.is_applicable?(soldier , '情報戦闘').should == true


    soldier , spec = create_soldier_and_spec('ホープ','ホープの帯同補正')
    spec.is_ability_bonus?.should == true
    spec.is_whole_bonus?.should == true

    soldier , spec = create_soldier_and_spec('名パイロット','名パイロットの搭乗戦闘補正')
    vehicle = Vehicle.new(@context)
    vehicle.add soldier
    spec.is_ability_bonus?.should == false
    spec.is_whole_bonus?.should == true
    spec.is_applicable?(soldier , '体格').should == false
    vehicle.conditions << '（｛Ｉ＝Ｄ，ＲＢ，航空機｝に搭乗して、戦闘する場合での）'
    vehicle.reset_situation_caches
#   vehicle.all_conditions_cashe = nil
    vehicle.children.each{|c| c.reset_situation_caches}
    spec.is_applicable?(soldier , '体格').should == false
    spec.is_applicable?(vehicle , '体格').should == true
    spec.is_applicable?(vehicle , '対空戦闘').should == true

    policemen = parse_troop('policeman:警官+天陽;')
    policeman = policemen.children[0]
    spec = policeman.find_spec('天陽の全能力補正')
    spec.is_applicable?(policeman , '体格').should == true
    spec.is_applicable?(policeman,'治安維持').should == false

    fakes = parse_troop <<EOT
//有効条件:｛Ｉ＝Ｄ，ＲＢ，航空機｝に搭乗して戦闘する場合での
_define_I=D:{
fake_t:フェイクトモエリバー+増加燃料装備,
pilot:名パイロット
};
EOT
    fake = fakes.children[0]
    spec = fake.find_spec('増加燃料装備の全能力補正')
    spec.is_applicable?(fake,'耐久').should == true
    spec.is_applicable?(fake,'対空戦闘').should == true
  end

  it '条件付き全能力補正' do
    troop = parse_troop <<EOT
//有効条件：ヒーローに変身することで
（Ｐ）02-00028-01_４４４：南国人＋パイロット＋超薬戦獣＋ヒロイック・パイロット＋ＨＱ感覚＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*感覚+5*知識+1*外見+2*幸運+2;
EOT
    soldier = troop.children[0]
    spec = soldier.find_spec('変身ヒーローの変身能力補正')
    spec.is_applicable?(soldier,'知識').should == true
  end

  it '任意補正・特定能力に対して' do
    source = <<EOT
＃！任意補正：学兵の特殊補正，体格
＃！有効条件：順応性があるため
soldier:学兵;
EOT
    troop = parse_troop(source)
    soldier = troop.children[0]
    soldier.evals['体格'].should == 0
    soldier.evaluate('体格').should == 1

    source = <<EOT
＃！任意補正：生徒会役員の適応力補正，体格
＃！部分有効条件：戦闘において
＃！部分有効条件：ＡＲ７以下の場合
soldier:生徒会役員;
EOT
    troop = parse_troop(source)
    soldier = troop.children[0]
    soldier.evals['体格'].should == 4
    soldier.evaluate('体格').should == 6
  end

  it '任意能力補正・特定の判定に対して' do
    source = <<EOT
＃！任意補正：学兵の特殊補正,白兵戦
＃！有効条件：順応性があるため
soldier:学兵＋剣士;
EOT
    troop = parse_troop(source)
    soldier = troop.children[0]

    soldier.weapon_names.should == ['白兵（剣）']
    
    melee = Evaluator['白兵戦']
    melee_defence = Evaluator['装甲（白兵距離）']
    calc = melee.choice_alternative(soldier)
    calc.total.should == 4
    melee_defence.choice_alternative(soldier).total.should == 3

    source = <<EOT
＃！任意補正：学兵の特殊補正,白兵距離戦闘行為
＃！有効条件：順応性があるため
soldier:学兵＋剣士;
EOT
    troop = parse_troop(source)
    soldier = troop.children[0]

    melee.choice_alternative(soldier).total.should == 4
    melee_defence.choice_alternative(soldier).total.should == 4

    source = <<EOT
＃！任意補正：生徒会役員の適応力補正,白兵戦
＃！部分有効条件：戦闘において
＃！部分有効条件：ＡＲ７以下の場合
_define_I=D:{
ミラー：ミラーコート（本国仕様）,
pilot:生徒会役員;
pilot2:生徒会役員;
};
EOT

    troop = parse_troop(source)
    soldier = troop.children[0]
    melee.choice_alternative(soldier).total.should == 24
  end

  it 'ＡＣＥ評価機能' do
    source = 'みらのちゃん：磯貝みらの;'
    troop = parse_troop(source)
    soldier = troop.children[0]
    for col in Abilities
      soldier.evals[col].should == 3
    end

    specs = soldier.specialties
    puts specs.collect{|sp|sp.source}
    specs.size.should == 24
  end

  it 'is_availableの部分条件対応' do
    source = 'soldier:帝國軍歩兵;'
    troop = parse_troop(source)
    soldier = troop.children[0]
    sp = soldier.find_spec('帝國軍歩兵の近距離戦闘補正')
    sp.is_available?.should be_falsey
    sp.is_available?([/射撃（銃）/,/属性（弾体）/,/近距離(.*)での$/]).should be_truthy

  end

  it 'Ｌ：取得' do
    source ='soldier:帝國軍歩兵;'
    troop = parse_troop(source)
    soldier = troop.children[0]
    sp = soldier.find_spec('帝國軍歩兵の近距離戦闘補正')

    sp.lib.should_not be_nil
    sp.lib.placement.should == ['歩兵系']
  end

  it '未使用補正と使用済み補正の混在' do
    source = <<EOT
＃！個別付与：猫と犬の前足が重なった腕輪，｛本隊｝
_define_I=D:{チップボール・月の３号：チップボール：体格+4,
42-00617-01_助清：北国人＋トップガン＋教導パイロット＋帝國軍歩兵＋ＨＱ感覚＋ＨＱ感覚＋参謀：敏捷+1*知識+1*幸運+1;
};

_define_I=D:{チップボール・雪の１号：チップボール：体格+4,
18-00341-01_九音・詩歌：詩歌の民＋帝國軍歩兵＋パイロット＋星見司：敏捷+1*器用+1*知識+1*幸運+1;
－恩寵の時計：個人所有：外見+2（着用型／首）
－猫と犬の前足が重なった腕輪：個人所有：同調判定+3（着用型／腕）
－恩寵の短剣：個人所有：白兵戦行為可能、白兵戦行為+1（着用型／その他）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00341-01%A1%A7%B6%E5%B2%BB%A1%A6%BB%ED%B2%CE；
};
＃！特殊無効：猫と犬の前足が重なった腕輪の同調補正，チップボール・雪の１号 ＃部隊・分隊単位で効果を反映させているため。
＃！特殊無効：恩寵の短剣の白兵距離戦闘補正，チップボール・雪の１号
EOT
    troop = parse_troop(source)
    synchro = Evaluator['同調']
    details = synchro.details(troop)
    details.total.should == 9
    
    all_sp = troop.children.collect{|c|
      sp_list = c.specialties
      if c.class <= Vehicle
        sp_list += c.children.collect{|pilot|pilot.specialties}.inject([]){|array,item|
          array | item
        }
      end
      sp_list.flatten
    }.flatten | troop.specialties.flatten

    sps = all_sp.select{|sp|sp.name =~ /猫と犬の前足が重なった腕輪の同調補正/}

    sps.all?{|sp|troop.is_sp_used?(sp).should be_truthy}
  end


  def sp_include?(troop,target_sp)
    $sp_include_result = false
    troop.total_cost { |unit , sp|
      $sp_include_result = true if sp.object_id == target_sp.object_id
    }
    $sp_include_result
  end

  it '７５％制限能力とコスト計算' do
    source = <<EOT
soldier1:戦闘工兵;
solseir2:歩兵;
EOT
    troop = parse_troop(source)
    soldier = troop.children[0]
    etc_sp = soldier.find_spec('戦闘工兵の爆破能力')
    etc_sp.should_not be_nil
    sp_oid = etc_sp.object_id
    flag = true

    etc_sp.is_tqr?.should be_truthy
    etc_sp.spec_name.should == '爆破能力'

    sp_include?(troop , etc_sp).should be_falsey

    source = <<EOT
soldier1:戦闘工兵;
soldier2:戦闘工兵;
soldier3:戦闘工兵;
solseir4:歩兵;
EOT
    troop = parse_troop(source)
    soldier = troop.children[0]
    etc_sp = soldier.find_spec('戦闘工兵の爆破能力')
    sp_include?(troop , etc_sp).should be_truthy

    source = <<EOT
_define_I=D:{chip:チップボール,
soldier1:戦闘工兵;
};
EOT
    troop = parse_troop(source)
    chip = troop.children[0]
    soldier = chip.children[0]
    etc_sp = soldier.find_spec('戦闘工兵の爆破能力')
    chip.etc.find{|sp|sp.object_id == etc_sp.object_id}.should_not be_nil
    sp_include?(troop , etc_sp).should be_truthy

    source = <<EOT
_define_I=D:{chip:チップボール,
soldier1:戦闘工兵;
solseir4:歩兵;
};
EOT
    troop = parse_troop(source)
    chip = troop.children[0]
    soldier = chip.children[0]
    etc_sp = soldier.find_spec('戦闘工兵の爆破能力')
    sp_include?(troop , etc_sp).should be_falsey

    troop = parse_troop <<EOT
architect:建築家
EOT
    soldier = troop.children[0]
    etc_sp = soldier.find_spec('建築家の建築時生物資源消費')

    sp_include?(troop , etc_sp).should be_truthy

    troop = parse_troop <<EOT
_define_I=D:{chip:チップボール,
architect:建築家
};
EOT
    soldier = troop.children[0].children[0]
    etc_sp = soldier.find_spec('建築家の建築時生物資源消費')

    sp_include?(troop , etc_sp).should be_truthy
  end

  it '学生の特殊補正' do
    source = <<EOT
student:学生+歩兵;

＃！評価派生：ＡＲ７以下，中距離戦
＃！有効条件：ＡＲ７以下の場合
＃！任意補正：学生の適応力補正，中距離戦
EOT
    troop = parse_troop(source)
    middle = Evaluator['中距離戦']
#    calc = middle.calculate(troop)
#    calc.total.should == 2

    middle2 = @context.particular_evaluators[0]
    middle2.prepare_particular_situation(troop)
    student = troop.children[0]
    spec = student.find_spec('学生の適応力補正')
    middle2.match?(spec).should == true
    calc = middle2.calculate(troop)
    calc.total.should == 4
  end

  it '７５％制限能力' do
    source = <<EOT
＃！有効条件：非戦争行動を行う場合
Ｌ：行政官　＝　｛
　ｔ：名称　＝　行政官（職業）
　ｔ：要点　＝　礼儀正しい，制服，上げた手
　ｔ：周辺環境　＝　都市
　ｔ：評価　＝　体格１，筋力２，耐久力２，外見５，敏捷１，器用１，感覚１，知識５，幸運１
　ｔ：特殊　＝　｛
　　＊行政官の職業カテゴリ　＝　，，，派生職業アイドレス。
　　＊行政官の根源力制限　＝　，，，着用制限（根源力：１５００００以上）。
　　＊行政官の非戦争行動補正　＝　，，条件発動，（非戦争行動を行う場合）全判定、評価＋６。７５％制限能力。
　　＊行政官の非戦争行動時同調補正　＝　，，条件発動，（非戦争行動を行う場合）同調、自動成功。７５％制限能力。　＃ｔ：非戦争行動　＝　冒険および戦争イベントに指定されていない行動のこと。例えば治安維持や戦災復興は非戦争行動である。
　｝
　ｔ：→次のアイドレス　＝　特別行政区（イベント），特別部隊（組織），復興支援（イベント），民衆支援（イベント）
｝

soldier1：歩兵＋行政官；
soldier2：歩兵；
EOT
    troop = parse_troop(source)
    troop.evaluate('体格').should == 4

  end

  it '器用による防御の７５％制限' do
    source = <<EOT
healer1:華麗なる治癒師；
healer2:華麗なる治癒師；
healer3:華麗なる治癒師；
doctor:医師；
EOT

    troop = parse_troop(source)

    healer1 = @context.units['healer1']
    healer1.tqrs.should include('受け流し能力'.intern)
    troop.team_tqrs.should include('受け流し能力'.intern)
    parry = troop.find_evaluator('器用による防御（通常）')
    troop.evaluator_include?(parry).should be_truthy

    source = <<EOT
healer1:華麗なる治癒師；
healer2:華麗なる治癒師；
doctor:医師；
EOT

    troop = parse_troop(source)
    troop.evaluator_include?(parry).should be_falsey

    source = <<EOT
doctor:医師；
EOT
    troop = parse_troop(source)
    troop.evaluator_include?(parry).should be_falsey

  end

  it 'Specialty#tqr_id' do
    source = <<EOT
Ｌ：偵察兵　＝　｛
　ｔ：名称　＝　偵察兵（職業）
　ｔ：要点　＝　偵察機器，野戦服
　ｔ：周辺環境　＝　なし
　ｔ：評価　＝　体格０，筋力０，耐久力１，外見０，敏捷１，器用０，感覚１，知識１，幸運－１
　ｔ：特殊　＝　｛
　　＊偵察兵の職業カテゴリ　＝　，，派生職業アイドレス。
　　＊偵察兵の位置づけ　＝　，，歩兵系。
　　＊偵察兵の白兵距離戦闘行為　＝　歩兵，条件発動，白兵距離戦闘行為が可能。＃白兵距離戦闘評価：可能：（体格＋筋力）÷２
　　＊偵察兵の近距離戦闘行為　＝　，，近距離戦闘行為が可能。＃近距離戦闘評価：可能：（敏捷＋筋力）÷２
　　＊偵察兵の近距離戦闘補正　＝　歩兵，任意発動，（近距離での）攻撃、評価＋２、燃料－１万ｔ。＃近距離戦闘評価：可能：（敏捷＋筋力）／２
　　＊偵察兵の中距離戦闘行為　＝　歩兵，条件発動，中距離戦闘行為が可能。＃中距離戦闘評価：可能：（感覚＋知識）／２
　　＊偵察兵の偵察補正　＝　歩兵，条件発動，（偵察での）感覚、評価＋３、燃料－１万ｔ。７５％制限能力。＃偵察評価：一般：感覚
　｝
　ｔ：→次のアイドレス　＝　吉田遥（ＡＣＥ），特殊部隊員（職業），追跡者（職業）
｝

_define_I=D:{チップ：チップボール,
scout：偵察兵＋犬妖精；
};
EOT

    troop = parse_troop(source)

    chip = troop.children[0]
    scout_soldier = chip.children[0]
    scout_sp = scout_soldier.find_spec('偵察兵の偵察補正')
    scout_sp.methods.should include(:tqr_id)
    scout_sp.tqr_id.should == '偵察補正'.intern

    middle_sp = scout_soldier.find_spec('偵察兵の近距離戦闘行為')
    middle_sp.tqr_id.should == '近距離戦闘行為'.intern

    sp = chip.find_spec('チップボールの陣地構築補正')
    sp.is_tqr?.should be_falsey
    sp.tqr_id.should == '陣地構築補正'.intern
  end

  it '７５％制限識別子' do
    source = <<EOT
Ｌ：偵察兵　＝　｛
　ｔ：名称　＝　偵察兵（職業）
　ｔ：要点　＝　偵察機器，野戦服
　ｔ：周辺環境　＝　なし
　ｔ：評価　＝　体格０，筋力０，耐久力１，外見０，敏捷１，器用０，感覚１，知識１，幸運－１
　ｔ：特殊　＝　｛
　　＊偵察兵の職業カテゴリ　＝　，，派生職業アイドレス。
　　＊偵察兵の位置づけ　＝　，，歩兵系。
　　＊偵察兵の白兵距離戦闘行為　＝　歩兵，条件発動，白兵距離戦闘行為が可能。＃白兵距離戦闘評価：可能：（体格＋筋力）÷２
　　＊偵察兵の近距離戦闘行為　＝　，，近距離戦闘行為が可能。＃近距離戦闘評価：可能：（敏捷＋筋力）÷２
　　＊偵察兵の近距離戦闘補正　＝　歩兵，任意発動，（近距離での）攻撃、評価＋２、燃料－１万ｔ。＃近距離戦闘評価：可能：（敏捷＋筋力）／２
　　＊偵察兵の中距離戦闘行為　＝　歩兵，条件発動，中距離戦闘行為が可能。＃中距離戦闘評価：可能：（感覚＋知識）／２
　　＊偵察兵の偵察補正　＝　歩兵，条件発動，（偵察での）感覚、評価＋３、燃料－１万ｔ。７５％制限能力。＃偵察評価：一般：感覚
　｝
　ｔ：→次のアイドレス　＝　吉田遥（ＡＣＥ），特殊部隊員（職業），追跡者（職業）
｝

_define_I=D:{チップ：チップボール,
scout：偵察兵＋犬妖精；
};
EOT

    troop = parse_troop(source)

    chip = troop.children[0]
    chip.methods.should include(:team_tqrs)
    chip.team_tqrs.should include('偵察補正'.to_sym)

    chip_team_tqrs = chip.team_tqrs

    troop.methods.should include(:team_tqrs)
    troop.team_tqrs.should include('偵察補正'.to_sym)

  end

  it '条件つき可能行為' do
    source = <<EOT
Ｌ：僧侶　＝　｛
　ｔ：名称　＝　僧侶（職業）
　ｔ：要点　＝　僧帽，聖典，祭司服
　ｔ：周辺環境　＝　寺院
　ｔ：評価　＝　体格１，筋力０，耐久力０，外見５，敏捷０，器用４，感覚０，知識４，幸運７
　ｔ：特殊　＝　｛
　　＊僧侶の職業カテゴリ　＝　，，派生職業アイドレス。
　　＊僧侶の位置づけ　＝　，，治癒系。
　　＊僧侶の治療行為　＝　，，治療行為が可能。＃治療評価：可能：（器用＋知識）÷２
　　＊僧侶の治療補正　＝　歩兵，任意発動，（奇跡による）治療、評価＋３、燃料－１万ｔ。＃治療評価：可能：（（器用＋知識）÷２）
　　＊僧侶の蘇生補正　＝　歩兵，条件発動，（蘇生判定に関する）全判定、評価＋８。この効果は重ねがけできない。
　　＊僧侶の白兵距離戦闘行為　＝　，，（アンデッドに対し）白兵距離戦闘行為が可能。＃白兵距離戦闘評価：可能：（体格＋筋力）÷２
　　＊僧侶の白兵距離戦闘補正　＝　歩兵，条件発動，（アンデッドに対し、白兵距離での）｛攻撃，防御｝、評価＋５、燃料－１万ｔ。＃白兵距離戦闘評価：可能：（体格＋筋力）÷２
　　＊僧侶の調伏行為　＝　歩兵，条件発動，調伏行為が可能。ただし、信仰している神々によっては使用することができない。＃調伏評価：可能：外見　または　幸運
　｝
　ｔ：→次のアイドレス　＝　モンド・フレイ（ＡＣＥ），僧正（職業），上級僧侶（職業），僧兵（職業）
｝

priest:僧侶；
EOT

    troop = parse_troop(source)

    priest = @context.units['priest']
    sp = priest.find_spec('僧侶の白兵距離戦闘行為')
    sp.condition_match?.should be_falsey

    troop.action_names.should_not include('白兵距離戦闘行為')

    source = <<EOT
Ｌ：僧侶　＝　｛
　ｔ：名称　＝　僧侶（職業）
　ｔ：要点　＝　僧帽，聖典，祭司服
　ｔ：周辺環境　＝　寺院
　ｔ：評価　＝　体格１，筋力０，耐久力０，外見５，敏捷０，器用４，感覚０，知識４，幸運７
　ｔ：特殊　＝　｛
　　＊僧侶の職業カテゴリ　＝　，，派生職業アイドレス。
　　＊僧侶の位置づけ　＝　，，治癒系。
　　＊僧侶の治療行為　＝　，，治療行為が可能。＃治療評価：可能：（器用＋知識）÷２
　　＊僧侶の治療補正　＝　歩兵，任意発動，（奇跡による）治療、評価＋３、燃料－１万ｔ。＃治療評価：可能：（（器用＋知識）÷２）
　　＊僧侶の蘇生補正　＝　歩兵，条件発動，（蘇生判定に関する）全判定、評価＋８。この効果は重ねがけできない。
　　＊僧侶の白兵距離戦闘行為　＝　，，（アンデッドに対し）白兵距離戦闘行為が可能。＃白兵距離戦闘評価：可能：（体格＋筋力）÷２
　　＊僧侶の白兵距離戦闘補正　＝　歩兵，条件発動，（アンデッドに対し、白兵距離での）｛攻撃，防御｝、評価＋５、燃料－１万ｔ。＃白兵距離戦闘評価：可能：（体格＋筋力）÷２
　　＊僧侶の調伏行為　＝　歩兵，条件発動，調伏行為が可能。ただし、信仰している神々によっては使用することができない。＃調伏評価：可能：外見　または　幸運
　｝
　ｔ：→次のアイドレス　＝　モンド・フレイ（ＡＣＥ），僧正（職業），上級僧侶（職業），僧兵（職業）
｝

＃！部分有効条件：アンデッドに対し

priest:僧侶；
EOT

    troop = parse_troop(source)
    troop.action_names.should include('白兵距離戦闘行為')

  end

  it 'tqr_id仕様変更' do
    source = <<EOT

＃！特殊無効：軌道降下兵の中距離戦闘補正
＃！特殊無効：軌道降下兵の遠距離戦闘行為
soldier1:帝國軍歩兵
soldier2:宇宙軍＋軌道降下兵
EOT
    troop = parse_troop(source)

    sp = troop.children[0].find_spec('帝國軍歩兵の遠距離戦闘補正')
    sp.is_tqr?.should be_truthy
    sp.tqr_id.should be_nil
    sp.tqr_requirement.should == :'遠距離戦闘行為'

    soldier1 = troop.children[0]
    s1tqr = soldier1.tqrs
    s1tqr.size.should == 3

    soldier2 = troop.children[1]
    sp = soldier2.find_spec('宇宙軍の筋力・耐久力補正')
    sp.is_tqr?.should_not be_truthy
    sp.tqr_id.should == :'筋力・耐久力補正'
    sp.tqr_requirement.should == :'筋力・耐久力補正'

    s2tqr = soldier2.tqrs
    s2tqr.size.should == 7
    tqrs = troop.team_tqrs
    tqrs.size.should == 2

    sp2 = troop.children[1].find_spec('軌道降下兵の中距離戦闘補正')
    sp2.should_not be_nil
    sp2.is_disabled?.should be_truthy
    troop.action_evals_str

    troop.is_tqr_qualified?(sp2).should be_truthy
    sp2.is_cost_required?.should be_falsey
  end

  it 'tqr_id仕様変更2' do
      source = <<EOT
36-00512-01_ボロマール：高位東国人＋理力使い＋理力建築士＋式神使い：敏捷+1*知識+2*幸運+1；
36-00689-01_藻女：高位東国人+式神使い+式使い+侍女+法官：外見+6*敏捷+1*器用+1*知識+3*幸運+1；
36-00690-01_水仙堂　雹：高位東国人+理力建築士+建築家+式神使い+護民官：敏捷+1*知識+2；
36-00691-01_七比良　鸚哥：高位東国人＋理力使い＋理力建築士＋式神使い+吏族：敏捷+1*感覚+2*知識+3；
36-00692-01_あすふぃこ：高位東国人＋理力使い＋理力建築士＋式神使い：敏捷+1*知識+3；
36-00693-01_柊　久音：高位東国人＋理力使い＋理力建築士＋式神使い+吏族：敏捷+1*感覚+2*知識+2；
36-00695-01_みぽりん：高位東国人+式神使い+式使い+侍女+護民官：筋力+1*耐久+3*敏捷+1*器用+1*知識+3；
36-00696-01_有馬信乃：高位東国人+理力建築士+建築家+式神使い+参謀：敏捷+1*知識+2*幸運+1；
36-00707-02_ミツキ：高位東国人+理力建築士+建築家+式神使い：筋力+1*敏捷+1*器用+1*知識+2；
36-00787-01_ある：高位東国人＋理力使い＋理力建築士＋式神使い：外見+2*知識+2；
36-00818-01_谷坂　少年：東国人+理力使い+理力使い：知識+2；
EOT
      troop = parse_troop(source)

      unit = @context.units['ボロマール']
      sp = unit.find_spec('式神使いの式神召喚能力')
      sp.is_tqr?.should be_truthy
      sp.tqr_id.should == :'式神召喚能力'

      unit2 = @context.units['藻女']
      sp2 = unit2.find_spec('式使いの式神召喚能力')
      sp2.is_tqr?.should be_truthy
      sp2.tqr_id.should == :'式神召喚能力'

      troop.team_tqrs.should include(:'式神召喚能力')

      sp2.is_cost_required?.should be_truthy
  end

  it 'Specialty#is_cost_required? と多兵科混成' do
    source = <<EOT
_define_I=D:{
チップ01_国有：チップボール：体格+3/* 宰相府大規模工廠T14製品体格+3 */，
（Ｐ）45-00420-01_ｔａｃｔｙ：北国人+歩兵+バトルメード+経済専門家+東方有翼騎士：敏捷+1*器用+1*知識+4*幸運+2*知識+2*外見+2*耐久力+1；
＃－総合大学：満天星国所有：知識＋２
－猫と犬の前足が重なった腕輪：個人所有：同調評価＋３（着用型／腕）
＃－秘書官夏服：個人所有：外見＋２，（宰相府内での非戦闘行為での）全判定評価＋３（着用型／胴体）
－蛇の指輪２：個人所有：耐久力＋１（着用型／手先）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00420-01%A1%A7%A3%F4%A3%E1%A3%E3%A3%F4%A3%F9 [^]

};

_define_I=D:{
チップ02_国有：チップボール：体格+3*筋力+2*耐久力+2，
（Ｐ）00-xx051-xx_猫士：西国人＋宇宙軍＋軌道降下兵＋宇宙の戦士;
};

_define_I=D:{
チップ03_国有：チップボール：体格+3*筋力+2*耐久力+2，
（Ｐ）00-xx061-xx_猫士：西国人＋宇宙軍＋軌道降下兵＋宇宙の戦士;
};


44-00526-01_瀬戸口まつり：高位北国人＋戦闘工兵＋医師＋宰相の娘＋秘書官：外見+3*敏捷+1*器用+1*知識+1*幸運+6;
＃－秘書官正装：個人所有：外見＋３（着用型／胴体）
－家族の指輪：個人所有：（家内安全のために行動する場合）全判定、評価＋２（着用型／手先）
－レーザーピストル：個人所有：近距離戦可能，近距離戦＋３（着用型／その他 ＃武器（片手装備））
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00526-01%A1%A7%C0%A5%B8%CD%B8%FD%A4%DE%A4%C4%A4%EA#j4dfe3c1 [^]

33-00174-01_ダーム：高位西国人＋マシンマイスター＋チューニングマスター＋名整備士＋東方有翼騎士＋ＨＱ知識：知識+1;
－藩国立工部大学の授業：国有：知識＋３（ＨＱで＋２、反映済み）、整備判定＋４

_define_division:{チップボール分隊:チップボールの直接火力支援＋チップボールの直接火力支援：：中距離戦＋２,
チップ01_国有，
チップ02_国有，
チップ03_国有，
};
_define_division:{整備歩兵分隊,
33-00174-01_ダーム
};
_define_division:{戦闘工兵分隊,
44-00526-01_瀬戸口まつり
};

EOT
    troop = parse_troop(source)
    chip = troop.children[0]
    sp = chip.find_spec('チップボールの防御補正')
    sp.is_cost_required?.should be_falsey

    troop.context.variable('コスト計上分隊').should == ['本隊']
    troop.total_cost.should == {'燃料' => -33}

    source = <<EOT
＃！編成種別：重編成，多兵科混成

_define_I=D:{
チップ01_国有：チップボール：体格+3/* 宰相府大規模工廠T14製品体格+3 */，
（Ｐ）45-00420-01_ｔａｃｔｙ：北国人+歩兵+バトルメード+経済専門家+東方有翼騎士：敏捷+1*器用+1*知識+4*幸運+2*知識+2*外見+2*耐久力+1；
＃－総合大学：満天星国所有：知識＋２
－猫と犬の前足が重なった腕輪：個人所有：同調評価＋３（着用型／腕）
＃－秘書官夏服：個人所有：外見＋２，（宰相府内での非戦闘行為での）全判定評価＋３（着用型／胴体）
－蛇の指輪２：個人所有：耐久力＋１（着用型／手先）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00420-01%A1%A7%A3%F4%A3%E1%A3%E3%A3%F4%A3%F9 [^]

};

_define_I=D:{
チップ02_国有：チップボール：体格+3*筋力+2*耐久力+2，
（Ｐ）00-xx051-xx_猫士：西国人＋宇宙軍＋軌道降下兵＋宇宙の戦士;
};

_define_I=D:{
チップ03_国有：チップボール：体格+3*筋力+2*耐久力+2，
（Ｐ）00-xx061-xx_猫士：西国人＋宇宙軍＋軌道降下兵＋宇宙の戦士;
};


44-00526-01_瀬戸口まつり：高位北国人＋戦闘工兵＋医師＋宰相の娘＋秘書官：外見+3*敏捷+1*器用+1*知識+1*幸運+6;
＃－秘書官正装：個人所有：外見＋３（着用型／胴体）
－家族の指輪：個人所有：（家内安全のために行動する場合）全判定、評価＋２（着用型／手先）
－レーザーピストル：個人所有：近距離戦可能，近距離戦＋３（着用型／その他 ＃武器（片手装備））
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00526-01%A1%A7%C0%A5%B8%CD%B8%FD%A4%DE%A4%C4%A4%EA#j4dfe3c1 [^]

33-00174-01_ダーム：高位西国人＋マシンマイスター＋チューニングマスター＋名整備士＋東方有翼騎士＋ＨＱ知識：知識+1;
－藩国立工部大学の授業：国有：知識＋３（ＨＱで＋２、反映済み）、整備判定＋４

_define_division:{チップボール分隊:チップボールの直接火力支援＋チップボールの直接火力支援：：中距離戦＋２,
チップ01_国有，
チップ02_国有，
チップ03_国有，
};
_define_division:{整備歩兵分隊,
33-00174-01_ダーム
};
_define_division:{戦闘工兵分隊,
44-00526-01_瀬戸口まつり
};

EOT
    troop = parse_troop(source)
    troop.should_not be_nil
    troop.context.variable('コスト計上分隊').should == ['本隊','チップボール分隊','整備歩兵分隊','戦闘工兵分隊']
    for div in [troop , troop.divisions].flatten
      div.action_evals_str
    end
    troop.total_cost.should == {'資源'=> -9  , '燃料' => -66}
  end

  it '＃！費用計上' do
    source = <<EOT
＃！費用計上：ダーム分隊

44-00526-01_瀬戸口まつり：高位北国人＋戦闘工兵＋医師＋宰相の娘＋秘書官：外見+3*敏捷+1*器用+1*知識+1*幸運+6;
33-00174-01_ダーム：高位西国人＋マシンマイスター＋チューニングマスター＋名整備士＋東方有翼騎士＋ＨＱ知識：知識+1;

_define_division:{ダーム分隊，ダーム｝；
_define_division:{まつり分隊，瀬戸口まつり｝；
EOT

    troop = parse_troop(source)
    troop.should_not be_nil
    troop.action_evals_str
    div1 = troop.divisions[0]
    div1.action_evals_str

    troop.context.variable('コスト計上分隊').should == ['本隊','ダーム分隊']
    div1.is_cost_required?.should be_truthy
    troop.total_cost.should == {'資源'=> -9  , '燃料' => -18}

    source = <<EOT
＃！費用計上：｛ダーム分隊，まつり分隊｝

44-00526-01_瀬戸口まつり：高位北国人＋戦闘工兵＋医師＋宰相の娘＋秘書官：外見+3*敏捷+1*器用+1*知識+1*幸運+6;
33-00174-01_ダーム：高位西国人＋マシンマイスター＋チューニングマスター＋名整備士＋東方有翼騎士＋ＨＱ知識：知識+1;

_define_division:{ダーム分隊，ダーム｝；
_define_division:{まつり分隊，瀬戸口まつり｝；
EOT

    troop = parse_troop(source)
    troop.should_not be_nil
    troop.action_evals_str
    div1 = troop.divisions[0]
    div1.action_evals_str
    div2 = troop.divisions[1]
    div2.action_evals_str

    troop.context.variable('コスト計上分隊').should == ['本隊','ダーム分隊','まつり分隊']
    div1.is_cost_required?.should be_truthy
    div2.is_cost_required?.should be_truthy
    troop.total_cost.should == {'資源'=> -9  , '燃料' => -33}
  end

  it '多兵科混成での７５％制限能力の燃料消費' do
    troop = parse_troop <<EOT
＃！編成種別：重編成，多兵科混成

soldier1:戦闘工兵；
soldier2：歩兵；
soldier3：歩兵；
soldier4：歩兵；

_define_division:{div1, soldier1};
_define_division:{div2, soldier2,soldier3};
_define_division:{div3, soldier4};
EOT
    troop.action_evals_str
    div1 = troop.divisions[0]
    div1.action_evals_str
    
    costs = troop.organization_cost_hash
    costs['燃料'].should == 36
  end
  
  it '空母搭載時のオプション装備費用免除' do
    source = <<EOT
Ｌ：スティガンデ級軽航空母艦の搭載　＝　｛
　ｔ：名称　＝　スティガンデ級軽航空母艦の搭載（定義）
　ｔ：評価　＝　なし
　ｔ：特殊　＝　｛
　　＊スティガンデ級軽航空母艦の搭載の定義カテゴリ　＝　，，消費削減。
　　＊スティガンデ級軽航空母艦の搭載の燃料消費削減　＝　，，基本オプション装備時の燃料消費を０にする。
　｝
｝

＃！個別付与：スティガンデ級軽航空母艦の搭載，本隊
＃！部分有効条件：戦闘する場合での

_define_I=D: {Ｐ１_騎士団所有：多国籍戦術戦闘機”ペヤーン”,
－多国籍戦術戦闘機”ペヤーン”の増槽：ＡＲ＋５、全評価－３（追加された５ＡＲを使い切るまで）　この装備は重複装備できない。
（Ｐ）18-00345-01_竜宮・司・ヒメリアス・ドラグゥーン：詩歌の民+歩兵+パイロット+法官：敏捷+1*知識+6*幸運+1*外見+2,
（ＣＰ）29-xx001-xx_犬士０１：西国人+スターファイター+スターライナー+スターリフター+ＳＨＱ感覚
}；

_define_I=D:{
ＲＴＲ１_星鋼京所有：ＲＴＲ＋ＳＨＱ敏捷,
－増加燃料装備：藩国所有：装甲+6*ＡＲ+3*全評価-3　増加分ＡＲ３使用後、全評価+3*装甲-6
（P）42-00612-01_御鷹：北国人＋撃墜王＋トップガン＋教導パイロット＋法の司＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*感覚+5*知識+1*幸運+1;
－法の執行者の紋章：個人所有：
（CP）42-00530-02_木曽池春海：北国人＋撃墜王＋トップガン＋教導パイロット＋護民官＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：器用+1*感覚+2*知識+1*幸運+1;
－互尊Ｃ：個人所有：
（CP）42-00610-01_センハ：北国人＋撃墜王＋トップガン＋教導パイロット＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*幸運+1;};

EOT

    troop = parse_troop(source)
    troop.action_evals_str

    p1 = troop.children[0]
    pilot_bonus = p1.children[1].find_spec('スターファイターの搭乗戦闘補正')

    reduction_sp = p1.resource_modifications[0]
    reduction_sp.name.should == 'スティガンデ級軽航空母艦の搭載の燃料消費削減'
    reduction_sp.is_applicable?(pilot_bonus).should be_falsey

    option_cost = p1.find_spec('多国籍戦術戦闘機”ペヤーン”の増槽の装備時燃料消費')
#    option_lib = option_cost.lib
#    categories = option_lib.categories
#    categories.should include('基本オプション')
    reduction_sp.is_applicable?(option_cost).should be_truthy

    troop.organization_cost('燃料').should == 21
  end

  it '航路移動時消費の計算' do
    source = <<EOT
Ｌ：ドッグバッグ（通常型）　＝　｛
　ｔ：名称　＝　ＮＶ－０１Ｂ　ドッグバッグ（通常型）（乗り物）
　ｔ：要点　＝　双発輸送機，ＶＴＯＬ，小型
　ｔ：周辺環境　＝　滑走路
　ｔ：評価　＝　装甲４
　ｔ：特殊　＝　｛
　　＊ドッグバッグ（通常型）の乗り物カテゴリ　＝　，，航空機。
　　＊ドッグバッグ（通常型）の位置づけ　＝　，，輸送機械。
　　＊ドッグバッグ（通常型）の必要パイロット数　＝　，，パイロット１名。
　　＊ドッグバッグ（通常型）の必要コパイロット数　＝　，，コパイロット１名。
　　＊ドッグバッグ（通常型）の搭乗資格　＝　，，搭乗可能（航空機）。
　　＊ドッグバッグ（通常型）の航路移動時燃料消費　＝　，条件発動，（１航路につき）燃料－１万ｔ。
　　＊ドッグバッグ（通常型）の燃料代替消費　＝　，，＜＊ドッグバッグ（通常型）の航路移動時燃料消費＞の代わりにマイル５で代用することができる。
　　＊ドッグバッグ（通常型）の輸送力　＝　，，１０人／機の輸送力を持つ。
　　＊ドッグバッグ（通常型）の航路数　＝　，，１ターンに１航路の往復移動（地上－地上）、航路変更不可。
　　＊ドッグバッグ（通常型）の人機数　＝　，，５人機。
　　＊ドッグバッグ（通常型）の特殊能力　＝　，，積荷が半分以下の時、空港、滑走路などを使用せずに離着陸できる。
　　＊ドッグバッグ（通常型）の整備　＝　，，整備を生活ゲームで行うことができる。
　　＊ドッグバッグ（通常型）の投下能力　＝　，条件発動，積荷に＜ドッグバッグ（通常型）の投下＞を付与する。積荷を降下作戦で使う事ができる。
　｝
　ｔ：→次のアイドレス　＝　航空機・大型輸送機の開発（イベント），航空機・大型ヘリの開発（イベント），航空機・戦術輸送機の開発（イベント），誘導員（職業）
｝

_define_vessel:{
Ｄ１_国有：ドッグバッグ（通常型）,
（Ｐ）44-00220-01_奥羽りんく：西国人＋猫の決戦存在＋スペーススターシップオフィサー＋宰相の娘＋秘書官：外見+2*敏捷+1*器用+1*知識+1*幸運+1；
＃－恩寵の時計：個人所有：外見＋２（着用型／首）
－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／腕）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00220-01%A1%A7%B1%FC%B1%A9%A4%EA%A4%F3%A4%AF
（ＣＰ）00-xx054-xx_猫士：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
};

EOT
    troop = parse_troop(source)
    troop.action_evals_str

    cost = troop.children[0].find_spec('ドッグバッグ（通常型）の航路移動時燃料消費')
    cost.is_consumed_by_hops?.should be_truthy
    cost.is_cost_required?.should be_truthy
    troop.children[0].find_spec('ドッグバッグ（通常型）の必要パイロット数').is_consumed_by_hops?.should be_falsey
    troop.sp_cost_str.should match(/燃料：1万ｔ/)
    
    source = <<EOT
Ｌ：「きゃりっじ」　＝　｛
　ｔ：名称　＝　Ｃ－３４「きゃりっじ」（乗り物）
　ｔ：要点　＝　双発輸送機，コンテナ
　ｔ：周辺環境　＝　滑走路
　ｔ：評価　＝　装甲２
　ｔ：特殊　＝　｛
　　＊「きゃりっじ」の乗り物カテゴリ　＝　，，航空機。
　　＊「きゃりっじ」の位置づけ　＝　，，輸送機械。
　　＊「きゃりっじ」の航路移動時燃料消費　＝　，条件発動，（１航路につき）燃料－３万ｔ。
　　＊「きゃりっじ」の航路移動時資源消費　＝　，条件発動，（１航路につき）資源－１万ｔ。
　　＊「きゃりっじ」の必要パイロット数　＝　，，パイロット２名。
　　＊「きゃりっじ」の必要コパイロット数　＝　，，コパイロット２名。
　　＊「きゃりっじ」の搭乗資格　＝　，，搭乗可能（航空機）。
　　＊「きゃりっじ」の航路数　＝　，，１ターンに２航路の往復移動（地上－地上）、航路変更不可。
　　＊「きゃりっじ」の輸送力　＝　，，｛２０人／機，５万ｔ｝の輸送力を持つ。
　　＊「きゃりっじ」の人機数　＝　，，５人機。
　　＊「きゃりっじ」の投下能力　＝　，条件発動，積荷に＜「きゃりっじ」の投下＞を付与する。積荷を降下作戦で使う事ができる。
　｝
　ｔ：→次のアイドレス　＝　輸送ヘリの開発（イベント），航空機・大型輸送機の開発（イベント），空挺歩兵（職業）
｝

_define_vessel:{
Ｄ１_国有：「きゃりっじ」,
（Ｐ）44-00220-01_奥羽りんく：西国人＋猫の決戦存在＋スペーススターシップオフィサー＋宰相の娘＋秘書官：外見+2*敏捷+1*器用+1*知識+1*幸運+1；
＃－恩寵の時計：個人所有：外見＋２（着用型／首）
－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／腕）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00220-01%A1%A7%B1%FC%B1%A9%A4%EA%A4%F3%A4%AF
（Ｐ）00-xx054-xx_猫士1：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
（ＣＰ）00-xx055-xx_猫士2：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
（ＣＰ）00-xx056-xx_猫士3：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
};

EOT
    troop = parse_troop(source)
    troop.action_evals_str

    troop.sp_cost_str.should match(/燃料：6万ｔ/)
    troop.sp_cost_str.should match(/資源：2万ｔ/)

  end

  it '航路移動時消費の計算' do
    source = <<EOT
33-00650-01_キギ：高位西国人＋マシンマイスター＋チューニングマスター＋名整備士＋ＨＱ知識＋参謀：敏捷+1*感覚+2*知識+1*幸運+2；
33-00665-01_プロメテウス（仮）：高位西国人＋マシンマイスター＋チューニングマスター＋名整備士＋ＨＱ知識：敏捷+1*幸運+1;
33-00733-02_獅進：高位南国人＋学生＋砲手＋戦車エース＋ＨＱ感覚＋ＳＨＱ感覚＋煌月＋ＨＱ;
33-00651-01_空紅：高位南国人＋学生＋砲手＋戦車エース＋ＨＱ感覚＋ＳＨＱ感覚＋煌月＋ＨＱ：敏捷+1*幸運+2;
EOT
    troop = parse_troop(source)
    troop.action_evals_str

    kigi = troop.children[0]
    spec = kigi.find_spec('チューニングマスターの整備補正')

    spec.is_cost_required?.should be_falsey

    troop.sp_cost_str.should match(/燃料：0万ｔ/o)
  end

end
