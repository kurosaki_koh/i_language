# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')

describe Division do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end
  it "create" do
    source =  <<EOT
hacker1:東国人＋ハッカー＋ギーク＋ネットワークエンジニア;
hacker2:東国人＋ハッカー＋ギーク＋ネットワークエンジニア;
engineer：東国人＋ハッカー＋ギーク＋ネットワークエンジニア＋宇宙の賢者+法官：敏捷+1*知識+4*幸運+1;

_define_division:{
hacker_div,
hacker1 ,
hacker2 ;
};

_define_division:｛
kurosaki:大型ＰＣ:筋力+1*体格+1：情報戦闘+2,
－電子妖精軍
engineer
｝;
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    troop.divisions.size.should == 2
    troop.divisions[0].name.should == 'hacker_div'
    troop.divisions[1].name.should == 'kurosaki'

    troop.divisions[0].member_names.should == ['hacker1','hacker2']
    troop.divisions[1].member_names.should == ['engineer']

    troop.divisions[0].children[0].name.should == 'hacker1'
    troop.divisions[1].children[0].name.should == 'engineer'

    troop.divisions[1].idresses.should == ['大型ＰＣ','電子妖精軍']
    troop.divisions[1].personal_bonuses.should == {'筋力'=>1 , '体格'=>1}
    troop.divisions[1].optional_bonuses.should == {'情報戦闘' => 2}

    puts inspect_evals(troop.divisions[0])

    puts inspect_evals(troop.divisions[1])
  end

  it '分隊定義にて搭乗者が歩兵扱いされたら、評価に反映されること。' do
    source = <<EOT
＃！部分有効条件：戦闘する場合での
_define_I=D:{
チップ：灼天,
pilot：名パイロット＋歩兵;
};

_define_division:{
搭乗者分隊,
pilot；
};
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
    division = @context.units['搭乗者分隊']
    division.should_not be_nil
    
    m_range = Evaluator['中距離戦']
    m_range.evaluate(division).should == 3

    calc = m_range.calculate(division)
    calc.total.should == 3
  end

  it '分隊への有効条件' do
    source = <<EOT
＃！有効条件：＜藩王の指揮＞に関する

king：藩王;
soldier1：ネットワークエンジニア;
－藩王の指揮
soldier2：ネットワークエンジニア;
－藩王の指揮

_define_division:{
藩王分隊,
king，
soldier2,
};

_define_division:{
ロンリー分隊,
soldier1;
};
EOT

    troop = parse_troop(source)
    troop.should_not be_nil
    inspect_evals(troop)
    #    division = @context.units['藩王分隊']
    division = troop.divisions[0]
    division.should_not be_nil

    division.all_conditions.include?('（＜藩王の指揮＞に関する）').should be_truthy
    soldier2 = @context.units['soldier2']
    soldier2.all_conditions.include?('（＜藩王の指揮＞に関する）').should be_truthy

    soldier2dup = division.children[1]
    soldier2dup.name.should == 'soldier2'
    soldier2.object_id.should_not == soldier2dup.object_id
    
    lead = soldier2dup.specialties.find{|sp|sp.name == '藩王の指揮の判定補正'}
    lead.should_not be_nil
    lead.is_available?.should be_truthy

    soldier2dup.evaluate('体格').should == 6

    puts inspect_evals(division)
  end

  it '分隊しても同じく評価されること。' do
    source = <<EOT
＃！部隊補正：教導パイロットの指導
＃！部分有効条件：戦闘する場合での

_define_I=D:{
Antares1：Ａｎｔａｒｅｓ,
－Ａｎｔａｒｅｓの軽装鎧
－増加燃料装備
pilot：北国人＋撃墜王＋トップガン＋教導パイロット
}；

_define_division:{
div1,
Antares1
};
EOT

    troop = parse_troop(source)
    troop.should_not be_nil
    inspect_evals(troop)
    #    division = @context.units['藩王分隊']
    division = troop.divisions[0]
    division.should_not be_nil
    vehicle = division.children[0]

    vehicle_org = @context.units['Antares1']
    vehicle.object_id.should_not == vehicle_org.object_id
    puts division.all_partial_conditions
#    division.all_partial_conditions.include?(/戦闘する場合での/).should be_true
    pilot_clone = vehicle.children[0]
#    pilot_clone.all_partial_conditions.include?(/戦闘する場合での/).should be_true

    bonus = pilot_clone.specialties.find{|sp|sp.name == '撃墜王の搭乗戦闘補正'}
    bonus.should_not be_nil
    bonus.is_available?.should be_truthy
    pilot_clone.is_troop_member?.should_not be_truthy
    bonus.owner.object_id.should == pilot_clone.object_id
    pilot_clone.parent.object_id.should == vehicle.object_id
    bonus.troop_member.object_id.should == vehicle.object_id
    bonus.troop_member.object_id.should_not == vehicle_org.object_id
    bonus.target.object_id.should == vehicle.object_id
    spec = vehicle.find_spec('Ａｎｔａｒｅｓの軽装鎧の体格補正')
    spec.is_target?(vehicle,'体格').should be_truthy
    vehicle.is_evaluate_item?(spec).should be_truthy
    eitems = vehicle.evaluate_items
    puts eitems.collect{|e|e.name}.join("\n")
    division.evaluate('体格').should == 50
  end

  it '藩王の指揮と分隊' do
    source = <<EOT
＃！個別付与：藩王の指揮，部隊全員
＃！除外：藩王の指揮，王さま

＃！有効条件：＜藩王の指揮＞に関する，｛本隊，分隊１｝

王さま：藩王；
歩兵１：歩兵；
歩兵２：歩兵；
歩兵３：歩兵；

_define_division:{分隊１,
王さま,
歩兵１,
歩兵２
};

_define_division:{分隊２,
歩兵３
};
EOT

    troop = parse_troop(source)

    puts troop.evals_str()

    div2 = @context.units['分隊２']

    div2.all_conditions.include?('（＜藩王の指揮＞に関する）').should be_falsey
    soldier3_2 = div2.children[0]
    soldier3_0 = troop.children[3]
    soldier3_0.object_id.should_not == soldier3_2.object_id
    soldier3_0.unit_type.object_id.should_not == soldier3_2.unit_type.object_id
    soldier3_2.all_conditions.include?('（＜藩王の指揮＞に関する）').should be_falsey

    puts div2.evals_items()
    soldier3_2.evaluate('体格').should == 0
  end
  it '分隊で基本状況が適用されてない件' do
    source = <<EOT
＃！特殊無効：ムラマサ２の射撃に対する防御補正，本隊 ＃ムラマサ２込みで同調できないケースの為

muramasa：ムラマサ２；
sessyo：摂政；

_define_division:{ムラマサ単独，
muramasa
};

_define_division:{摂政単独，
sessyo
};

＃！状況：対射撃防御
＃！部分有効条件：＜射撃＞での攻撃に対する
EOT
    troop = parse_troop(source)

    m_div = @context.units['ムラマサ単独']

    defense = Evaluator['装甲（通常）']
    defense.evaluate(troop).should == 6

    calc = defense.calculate(troop)
    calc.total.should == 6

    @context.change_situation_to('対射撃防御')
    muramasa = @context.units['muramasa']
    @context.all_disabled_sps.size.should == 0
    troop.all_disabled_sps.size.should == 1
    muramasa.all_disabled_sps.size.should == 1
    defense.evaluate(troop).should == 6

    calc = defense.calculate(troop)
    calc.total.should == 6

    @context.change_situation_to('（基本）')
    defense.evaluate(m_div).should == 4

    calc = defense.calculate(m_div)
    calc.total.should == 4

    @context.change_situation_to('対射撃防御')
    muramasa_org = troop.children[0]
    muramasa = m_div.children[0]
    (muramasa.object_id != muramasa_org.object_id).should be_truthy
#    defense.evaluate(m_div).should == '自動成功'

    calc = defense.calculate(m_div)
    calc.total.should == '自動成功'
    calc.is_string?.should == true

    @context.change_situation_to('対射撃防御')
    troop.all_partial_conditions.should include(/＜射撃＞での攻撃に対する/)
    m_div.all_partial_conditions.should include(/＜射撃＞での攻撃に対する/)
    muramasa.all_partial_conditions.should include(/＜射撃＞での攻撃に対する/)
    muramasa.all_bonuses.size.should == 3
    troop.all_disabled_sps.include?('ムラマサ２の射撃に対する防御補正').should be_truthy
    m_div.all_disabled_sps.include?('ムラマサ２の射撃に対する防御補正').should be_falsey
    muramasa.all_disabled_sps.include?('ムラマサ２の射撃に対する防御補正').should be_falsey

    sp = muramasa.find_spec('ムラマサ２の射撃に対する防御補正')
    sp.is_available?([/装甲での/]).should be_truthy

    defense = Evaluator['装甲（通常）']
    defense.evaluate(troop).should == 6
    defense.evaluate(m_div).should == '自動成功'

  end

  it '藩王の指揮と分隊' do
    source = <<EOT
＃！個別付与：藩王の指揮，全ユニット
＃！除外：藩王の指揮，king

＃！有効条件：＜藩王の指揮＞に関する，｛本隊，歩兵分隊、藩王分隊｝

soldier:歩兵;
soldier2:歩兵;
king:藩王

_define_division:{歩兵分隊,
soldier
};

_define_division:{藩王分隊,
king ,
soldier2
};
EOT
    troop = parse_troop(source)

    dv = @context.units['歩兵分隊']
  end

  it '1227バグ' do
    source = <<EOT
soldier:剣士;
－カトラス

＃！特殊無効：カトラスの白兵距離戦闘補正，soldier

_define_division:{test,
soldier
};
EOT
    troop = parse_troop(source)
    puts result = troop.evals_str
    result.should_not be_nil

    for dv in troop.divisions
      puts dv.evals_str
    end

    soldier_org = troop.children[0]
    soldier_org.disabled_sps.include?('カトラスの白兵距離戦闘補正').should == true

    dv = @context.units['test']
    soldier = dv.children[0]
    disableds = soldier.all_disabled_sps
    disableds.include?('カトラスの白兵距離戦闘補正').should == true
  end

    it '評価派生の中の個別付与が分隊で無効' do
    source = <<EOT
11-00230-01_玄霧弦耶：森国人+藩王+名医+マッドサイエンティスト+参謀：敏捷+1*知識+1*幸運+1；
11-00035-01_東西　天狐：森国人+優しい死神+暗殺者+特務警護官+ＨＱ知識+ＨＱ器用+ＨＱ敏捷+吏族：敏捷+1*幸運+1*知識+6；


_define_division:{div1,玄霧弦耶};
_define_division:{div2,東西　天狐};


＃！評価派生：国歌使用時，｛装甲，近距離戦，中距離戦｝
＃！個別付与：国歌，全隊員
＃！有効条件：この絶技を使用した場合、戦闘での
EOT

    troop = parse_troop(source)
    div1 = @context.units['div1']
    genya = @context.units['玄霧弦耶']
    ev = div1.evaluators.find{|e|e.name == '（国歌使用時）装甲（通常）'}

    calc = ev.calculate(div1)
    calc.total.should == 6
  end
  it '分隊時消費の免除' do
    source = <<EOT
＃00:東方有翼騎士団 T14編成
＃編成表:http://www30.atwiki.jp/idress/pages/417.html

＃！部分有効条件：盾によって
＃！部隊補正：チップボールの直接火力支援＋チップボールの直接火力支援：：中距離戦＋２

＃！補正ＨＱ：藩国立工部大学の授業の知識補正，２

33-00174-01_ダーム：高位西国人＋マシンマイスター＋チューニングマスター＋名整備士＋東方有翼騎士＋ＨＱ知識：知識+1;
－藩国立工部大学の授業：国有：知識＋３（ＨＱで＋２、反映済み）、整備判定＋４

44-00526-01_瀬戸口まつり：高位北国人＋戦闘工兵＋医師＋宰相の娘＋秘書官：外見+3*敏捷+1*器用+1*知識+1*幸運+6;
＃－秘書官正装：個人所有：外見＋３（着用型／胴体）
－家族の指輪：個人所有：（家内安全のために行動する場合）全判定、評価＋２（着用型／手先）
－レーザーピストル：個人所有：近距離戦可能，近距離戦＋３（着用型／その他 ＃武器（片手装備））
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00526-01%A1%A7%C0%A5%B8%CD%B8%FD%A4%DE%A4%C4%A4%EA#j4dfe3c1 [^]

_define_I=D:{
チップ01_国有：チップボール：体格+3/* 宰相府大規模工廠T14製品体格+3 */，
（Ｐ）45-00420-01_ｔａｃｔｙ：北国人+歩兵+バトルメード+経済専門家+東方有翼騎士：敏捷+1*器用+1*知識+4*幸運+2*知識+2*外見+2*耐久力+1；
＃－総合大学：満天星国所有：知識＋２
－猫と犬の前足が重なった腕輪：個人所有：同調評価＋３（着用型／腕）
＃－秘書官夏服：個人所有：外見＋２，（宰相府内での非戦闘行為での）全判定評価＋３（着用型／胴体）
－蛇の指輪２：個人所有：耐久力＋１（着用型／手先）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00420-01%A1%A7%A3%F4%A3%E1%A3%E3%A3%F4%A3%F9 [^]

};

_define_I=D:{
チップ02_国有：チップボール：体格+3*筋力+2*耐久力+2/* 宇宙軍補正適用のチップボール特例 */，
（Ｐ）00-xx051-xx_猫士：西国人＋宇宙軍＋軌道降下兵＋宇宙の戦士;
};

_define_I=D:{
チップ03_国有：チップボール：体格+3*筋力+2*耐久力+2/* 宇宙軍補正適用のチップボール特例 */，
（Ｐ）00-xx061-xx_猫士：西国人＋宇宙軍＋軌道降下兵＋宇宙の戦士;
};

_define_division:{火力担当分隊:チップボールの直接火力支援＋チップボールの直接火力支援：：中距離戦＋２,
44-00526-01_瀬戸口まつり,
チップ01_国有，
チップ02_国有，
チップ03_国有，
};
_define_division:{整備歩兵分隊,
33-00174-01_ダーム
};

_define_division:{チップボール分隊:チップボールの直接火力支援＋チップボールの直接火力支援：：中距離戦＋２,
チップ01_国有，
チップ02_国有，
チップ03_国有，
};

_define_division:{戦闘工兵分隊,
44-00526-01_瀬戸口まつり
};

＃！評価派生：盾放棄時，装甲
＃！特殊無効：チップボールの防御補正
EOT
    troop = parse_troop(source)
    troop.action_evals_str
    sps = []
    result = troop.sp_cost_str{|unit,sp|
      sps << sp
    }
    sps.find{|sp|sp.name =~ /整備補正/}.should be_nil
    puts result
    sps.each{|sp| puts sp.owner.name + sp.source}
  end
  
  it '別部隊での夜間戦闘補正が多兵科混成時にコスト消費に計上されない' do
    source = <<EOT
（騎）44-00761-01_NEKOBITO：西国人+通訳+猫の神様+猫の決戦存在+護民官：外見+2/*恩寵の時計*/*幸運+1/*ちいさなペリドットの指輪*/;
－ＩＤプレート：個人所有：プレートを見ることにより、どこの誰だかわかる。首に付けるアイテムをもう一つ着用できる。ただし、装備可能数には含める。首に着用。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00761-01%A1%A7NEKOBITO
（同）44-00008-01_ゆうみ：西国人+整備士２+整備士２+通訳+護民官：敏捷+1*器用+1*知識+1*幸運+1;
（同）44-00803-01_可西：西国人+整備士２+整備士２+名整備士;

_define_division:{【涼州藩国搬送分隊】，
44-00761-01_NEKOBITO
};

＃！編成種別：重編成，多兵科混成
EOT

    troop = parse_troop(source)
    troop.action_evals_str
    troop.divisions[0].action_evals_str

    troop.sp_cost_str.should match(/燃料：9万ｔ/)
  end

  it 'ユニット定義より先に_define_division' do
    source = <<EOT
_define_division:{【涼州藩国搬送分隊】，
44-00761-01_NEKOBITO
};
（騎）44-00761-01_NEKOBITO：西国人+通訳+猫の神様+猫の決戦存在+護民官：外見+2/*恩寵の時計*/*幸運+1/*ちいさなペリドットの指輪*/;
（同）44-00008-01_ゆうみ：西国人+整備士２+整備士２+通訳+護民官：敏捷+1*器用+1*知識+1*幸運+1;
（同）44-00803-01_可西：西国人+整備士２+整備士２+名整備士;
EOT

    troop = parse_troop(source)
    troop.should_not be_nil
    puts troop.action_evals_str
  end
end
