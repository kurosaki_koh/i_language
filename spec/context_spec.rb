# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__), './calculator_test_helper.rb')

describe Context do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end

  it "＃！状況" do
    source = <<EOT
＃！有効条件：位置づけ（情報系）の職業による
hacker:ハッカー＋高機能ハンドヘルド;

＃！状況：第５世界でマジックアイテム無効
＃！特殊無効：高機能ハンドヘルドの情報戦闘補正
＃！特殊無効：高機能ハンドヘルドの情報系職業補正，hacker
EOT

    troop = parse_troop(source)
    troop.should_not be_nil

    @context.situation_names.sort.should == ['第５世界でマジックアイテム無効', '（基本）']
    @context.current_situation_name.should == '（基本）'

    @context.conditions.should == ['', '（位置づけ（情報系）の職業による）']
    @context.disabled_sps.should == []
    #    @context.all_disabled_sps_cashe.nil?.should be_true
    @context.reset_situation_caches
    #l_disabled_sps.should == []
    @context.units['hacker'].all_disabled_sps.should == []
    #    @context.all_disabled_sps_cashe.should == []

    @context.change_situation_to('第５世界でマジックアイテム無効')
    @context.situation_names.size.should == 2
    @context.current_situation_name.should == '第５世界でマジックアイテム無効'
    @context.disabled_sps('（基本）').should == []
    @context.disabled_sps.should == ['高機能ハンドヘルドの情報戦闘補正']
    @context.disabled_sps('第５世界でマジックアイテム無効').should == ['高機能ハンドヘルドの情報戦闘補正']
    @context.all_disabled_sps.should == ['高機能ハンドヘルドの情報戦闘補正']
    @context.troop.disabled_sps.should == []
    @context.troop.all_disabled_sps.should == ['高機能ハンドヘルドの情報戦闘補正']
    hacker = @context.units['hacker']
    hacker.all_disabled_sps.should == ['高機能ハンドヘルドの情報系職業補正', '高機能ハンドヘルドの情報戦闘補正']
    #    @context.units['hacker'].all_disabled_sps_cashe.should == ['高機能ハンドヘルドの情報系職業補正','高機能ハンドヘルドの情報戦闘補正']
    @context.conditions.should == ['']
    @context.all_conditions.should == ['', '（位置づけ（情報系）の職業による）']
    hacker.all_conditions.should == ['', '（位置づけ（情報系）の職業による）']

    @context.troop.situation_names.size.should == 2
  end

  it '個別付与・除外の状況別指定' do
    source = <<EOT
soldier:歩兵＋偵察兵;

＃！個別付与：追跡者，soldier
＃！状況：藩王付き
＃！個別付与：藩王の指揮，soldier
＃！有効条件：＜藩王の指揮＞に関する
EOT

    troop = parse_troop(source)
    soldier = @context.units['soldier']
    defined?(soldier.all_idresses).should_not be_nil
    defined?(soldier.idress_appendants).should_not be_nil

    soldier.idress_appendants.size.should == 1
    soldier.idress_appendants[0].should == '追跡者'
    soldier.all_idresses.size.should == 3

    soldier.evaluate('敏捷').should == 6

    @context.change_situation_to('藩王付き')
    soldier.idress_appendants.size.should == 1
    all_idresses = soldier.all_idresses
    all_idresses.size.should == 4
    all_idresses.should == %W[歩兵 偵察兵 追跡者 藩王の指揮]

    sp = soldier.find_spec('藩王の指揮の判定補正')
    sp.is_available?.should be_truthy
    soldier.is_evaluate_item?(sp).should be_truthy
    soldier.evaluate('敏捷').should == 8

    source = <<EOT
soldier:歩兵＋偵察兵;

＃！個別付与：追跡者，soldier
＃！状況：藩王付き
＃！個別付与：藩王の指揮，soldier
＃！有効条件：＜藩王の指揮＞に関する
＃！除外：歩兵，soldier
EOT
    troop = parse_troop(source)
    soldier = @context.units['soldier']
    soldier.idress_appendants.size.should == 1
    soldier.idress_appendants[0].should == '追跡者'
    defined?(soldier.idress_exceptions).should_not be_nil
    soldier.idress_exceptions.size.should == 0
    soldier.all_idresses.size.should == 3

    @context.change_situation_to('藩王付き')
    soldier.idress_exceptions.size.should == 1
    all_idresses = soldier.all_idresses
    all_idresses.size.should == 3
    all_idresses.should == %W[偵察兵 追跡者 藩王の指揮]

    soldier.evaluate('敏捷').should == 7

  end

  it '部隊にユニットがない' do
    proc { troop = parse_troop('') }.should raise_error(RuntimeError, /部隊にユニットが存在しません/)

  end

  it '＃！状況（対象分隊指定）' do
    source = <<EOT
soldier1:歩兵；
_define_I=D:{チップ：チップボール,
soldier:歩兵＋名パイロット；
};

＃！状況：戦闘時，｛本隊，チップ分隊｝
＃！部分有効条件：戦闘する場合での

_define_division:{歩兵分隊，soldier};
_define_division:{チップ分隊,チップ};
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
    troop.all_situation_names.size.should == 2
    troop.all_situation_names.should == ['（基本）', '戦闘時']
    @context.change_situation_to('戦闘時', false)
    dummy = troop.evals_str
    soldier_div = troop.divisions[0]
    soldier_div.all_situation_names.size == 0
    soldier_div.all_situation_names.should == ['（基本）']
    chip_div = troop.divisions[1]
    chip_div.all_situation_names.size == 2
    chip_div.all_situation_names.should == ['（基本）', '戦闘時']
  end

  it '【本隊】でもよい' do
    source = <<EOT
34-xx000-xx_王猫タロ・ターマ・ハーナ８世：南国人＋医師＋医師＋名医+敬人;
34-xx000-xx_王猫タロ・ターマ・ハーナ８世：南国人＋医師＋医師＋名医+敬人;
34-xx000-xx_王猫タロ・ターマ・ハーナ８世：南国人＋医師＋医師＋名医+敬人;
34-00675-01_東　恭一郎：高位南国人＋炎の料理人＋歩兵＋緊急展開軍+行政士官+ＨＱ知識+ＨＱ知識+謙者：敏捷+1*知識+1*幸運+1;
－アンデットバスター：個人所有：歩兵武装，条件発動，（アンデッドに対する）攻撃、評価＋５。両手持ち武器。
EOT
    troop = parse_troop(source)

    @context.units['本隊'].object_id.should == troop.object_id
    @context.units['【本隊】'].object_id.should == troop.object_id
  end

  it 'ｌ：変数　←　代入値' do
    source = <<EOT
ｌ：検証中機能　←　true

_define_I=D:{ＲＴ１０：ＲＴＲ,
(P)43-xx027-xx_菊花：西国人＋帝國軍歩兵＋スターライナー＋スターリフター＋ＨＱ感覚；
(CP)43-xx032-xx：聡慎：北国人＋帝國軍歩兵＋はぐれメード＋ナースメード２；
(CP)43-xx033-xx：呂旋：北国人＋帝國軍歩兵＋はぐれメード＋ナースメード２
}；
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
    @context.variable('検証中機能').evaluate.should be_truthy

  end

  describe 'ACEを含む編成でのルールチェック' do
    subject { proc { parse_troop(source) } }
    context '通常のACEを含む' do
      context '歩兵部隊で' do
        context 'ACEしかいない' do
          let(:source) { "ACE：鷹野徹；\n" }
          it { should_not raise_error }
        end
        context 'ACE以外がいる' do
          let(:source) { <<EOT
ACE：鷹野徹；
soldier:歩兵；
EOT
          }
          it { should raise_error }
        end
      end
      context 'I=D部隊で' do
        context 'ACE搭乗ユニットしかいない' do
          let(:source) do
            <<EOT
_define_I=D:{ 
ユーカ_国有：ユーカ＋ＳＨＱ対空戦闘＋ＨＱ対空戦闘, 
（P）あゆみの晋太郎：あゆみの晋太郎（夫婦）＋ＵＨＱ＋ＳＨＱ＋ＳＨＱ：; 
（CP）03-00045-01_久珂あゆみ：高位西国人＋ＷＳＯ＋ナイスバディ＋撃墜王＋ＳＨＱ感覚＋ＵＨＱ感覚＋ＳＨＱ感覚＋ＨＱ感覚＋晋太郎の奥さん（あゆみ版）＋ＨＱ器用＋ＨＱ感覚＋ＳＨＱ幸運＋ＳＨＱ感覚＋ＨＱ外見：敏捷+1*器用+1*知識+1*幸運+2 
}; 
EOT
          end
          it { should_not raise_error }
        end
        context 'ユニットが複数ある' do
          let(:source) do
            <<EOT
_define_I=D:{ 
ユーカ1_国有：ユーカ＋ＳＨＱ対空戦闘＋ＨＱ対空戦闘, 
（P）あゆみの晋太郎：あゆみの晋太郎（夫婦）＋ＵＨＱ＋ＳＨＱ＋ＳＨＱ：; 
（CP）03-00045-01_久珂あゆみ：高位西国人＋ＷＳＯ＋ナイスバディ＋撃墜王＋ＳＨＱ感覚＋ＵＨＱ感覚＋ＳＨＱ感覚＋ＨＱ感覚＋晋太郎の奥さん（あゆみ版）＋ＨＱ器用＋ＨＱ感覚＋ＳＨＱ幸運＋ＳＨＱ感覚＋ＨＱ外見：敏捷+1*器用+1*知識+1*幸運+2 
}; 
_define_I=D:{ 
ユーカ2_国有：ユーカ＋ＳＨＱ対空戦闘＋ＨＱ対空戦闘, 
（P）パイロット：高位西国人＋ＷＳＯ＋ナイスバディ＋撃墜王; 
（CP）コパイロット：高位西国人＋ＷＳＯ＋ナイスバディ＋撃墜王；
};
EOT
          end
          it { should raise_error }
        end

      end
      context '艦船部隊で' do
        context 'ACE搭乗ユニットしかいない' do
          let(:source) do
            <<EOT
_define_vessel:{ 
ＦＥＧボウルドカーン_国有：ＦＥＧボウルドカーン＋ＳＨＱ輸送量＋ＳＨＱ輸送量, 
（Ｐ）03-xx009-xx_つばさ：西国人＋パイロット＋整備士２＋名整備士＋ＨＱ知識; 
（Ｐ）03-xx010-xx_大吉：西国人＋パイロット＋整備士２＋名整備士＋ＨＱ知識; 
（Ｐ）06-00769-01_三園晶：愛の民＋整備士２＋名パイロット＋ホープ＋護民官２； 
（ＣＰ）03-xxx17-01_鷹野徹：鷹野徹＋ＳＨＱ＋ＨＱ； 
（ＣＰ）24-00236-02_朝霧：南国人+猫妖精２+潜水艦乗り+戦争の天才+整備士官； 
（ＣＰ）03-xx011-xx_メイ：西国人+猫妖精２+パイロット+名パイロット; 
};
EOT
          end
          it { should_not raise_error }
        end
        context 'ユニットが複数いる' do
          let(:source) do
            <<EOT
_define_vessel:{ 
ＦＥＧボウルドカーン1_国有：ＦＥＧボウルドカーン＋ＳＨＱ輸送量＋ＳＨＱ輸送量, 
（Ｐ）03-xx009-xx_つばさ：西国人＋パイロット＋整備士２＋名整備士＋ＨＱ知識; 
（Ｐ）03-xx010-xx_大吉：西国人＋パイロット＋整備士２＋名整備士＋ＨＱ知識; 
（Ｐ）06-00769-01_三園晶：愛の民＋整備士２＋名パイロット＋ホープ＋護民官２； 
（ＣＰ）03-xxx17-01_鷹野徹：鷹野徹＋ＳＨＱ＋ＨＱ； 
（ＣＰ）24-00236-02_朝霧：南国人+猫妖精２+潜水艦乗り+戦争の天才+整備士官； 
（ＣＰ）03-xx011-xx_メイ：西国人+猫妖精２+パイロット+名パイロット; 
};
_define_vessel:{
ＦＥＧボウルドカーン2_国有：ＦＥＧボウルドカーン＋ＳＨＱ輸送量＋ＳＨＱ輸送量, 
（Ｐ）p1：西国人＋パイロット＋整備士２＋名整備士＋ＨＱ知識; 
（Ｐ）p2：西国人＋パイロット＋整備士２＋名整備士＋ＨＱ知識; 
（Ｐ）p3：愛の民＋整備士２＋名パイロット＋ホープ＋護民官２； 
（ＣＰ）cp1：西国人+猫妖精２+パイロット+名パイロット; 
（ＣＰ）cp2：南国人+猫妖精２+潜水艦乗り+戦争の天才+整備士官； 
（ＣＰ）cp3：西国人+猫妖精２+パイロット+名パイロット; 
};
EOT
          end
          it { should raise_error }
        end
      end

    end
  end

  describe 'アイドレス別ワーニングと文法ワーニングの抽出' do
#    subject { parse_troop(source)}
    context 'アイドレス別ワーニングを含む。' do
      let(:source) { "test：アウィナイト；\n" }
      it 'warnings' do
        troop = parse_troop(source)
        p @context.warnings
        @context.warnings.should == ['アウィナイト：補足：ＲＢ等のパイロット能力評価を２倍にする乗り物の計算には対応していません。パイロットの個人修正欄を利用して、評価２倍になるよう調整してください。',
                                     'アウィナイト：注意：ＴＬＯは用法・用量を守って正しくお使いください。']
      end
    end

    context 'hash_for_api' do
      let(:source) { "test：アウィナイト+test；\n" }

      it 'hash_for_api' do
        troop = parse_troop(source)
        @context.hash_for_api.should == {
            'warnings' => [
                'アウィナイト：補足：ＲＢ等のパイロット能力評価を２倍にする乗り物の計算には対応していません。パイロットの個人修正欄を利用して、評価２倍になるよう調整してください。',
                'アウィナイト：注意：ＴＬＯは用法・用量を守って正しくお使いください。'
            ],
            'not_found' => ['test'],
            'divisions' => troop.hash_for_api['divisions']

        }
      end
    end
  end
  describe 'Resourcify' do
    describe '通常系' do
      let(:source) do
        <<EOT
32-00635-01：hacker:ハッカー；
－大型ＰＣの計算資源

Ｌ：コマンドリソース化（hacker）　＝　｛
　ｆ：＊ハッカーのナショナルネット接続能力　＝　ハッカーのナショナルネット接続能力：６：＃情報戦
　ｆ：＊大型ＰＣの計算資源の情報戦闘補正　＝　大型ＰＣの計算資源の情報戦闘補正：１２：＃情報戦
｝
EOT
      end
      before (:each) do
        @troop = parse_troop(source)
      end

      it 'コマンドリソース化された補正は無効扱いとする' do
        expect(Evaluator['情報戦'].evaluate(@troop)).to eq 5
        expect(@context.disabled_sps).to eq ['ハッカーのナショナルネット接続能力', '大型ＰＣの計算資源の情報戦闘補正']
      end

      it 'コマンドリソース化された結果を取得できる。' do
        expect(@context.converted_command_resources).to match [
                                                                  a_hash_including(name: 'ハッカーのナショナルネット接続能力', power: 6, tag: '情報戦'),
                                                                  a_hash_including(name: '大型ＰＣの計算資源の情報戦闘補正', power: 12, tag: '情報戦')
                                                              ]

        cs = @context.converted_specialties

        expect(cs['ハッカーのナショナルネット接続能力']).to eq( {name: 'ハッカーのナショナルネット接続能力', power: 6, tag: '情報戦'})
        expect(cs['大型ＰＣの計算資源の情報戦闘補正']).to eq( {name: '大型ＰＣの計算資源の情報戦闘補正', power: 12, tag: '情報戦'})

      end

      it 'API出力に指定したコマンドリソースが含まれる' do
        expect(@context.hash_for_api['divisions'].first['command_resources']).to match [
                                                                                           a_hash_including(name: 'ハッカーのナショナルネット接続能力', power: 6, tag: '情報戦'),
                                                                                           a_hash_including(name: '大型ＰＣの計算資源の情報戦闘補正', power: 12, tag: '情報戦')
                                                                                       ]
      end

      it 'すべてコンバートしたコマンドリソースの一覧が得られる' do
        crs = @context.parse_command_resources
        expect(crs.size).to eq 23

        puts crs.inspect
      end

      describe '行為と補正の不可分扱い' do
        context '基本' do
          let(:source) do
            <<EOT
32-00635-01：hacker:ハッカー＋ギーク＋ネットワークエンジニア；
－大型ＰＣの計算資源

Ｌ：コマンドリソース化（hacker）　＝　｛
　ｆ：＊ネットワークエンジニアの情報戦闘補正　＝　ネットワークエンジニアの情報戦闘補正：６：＃情報戦
　ｆ：＊ギークの白兵距離戦闘行為　＝　ギークの白兵距離戦闘行為：６：＃白兵戦
｝
EOT
          end
          it '補正特殊を指定すると、対応する行為も無効化される' do
            expect(@context.disabled_sps).to include 'ネットワークエンジニアの情報戦闘行為'
          end

          it '行為特殊を指定すると、対応する補正も無効化される' do
            expect(@context.disabled_sps).to include 'ギークの白兵距離戦闘補正'
          end
        end
        context '異常系' do
          let(:source2) do
            <<EOT
32-00635-01：hacker:ハッカー＋ギーク＋ネットワークエンジニア；

Ｌ：コマンドリソース化（hacker）　＝　｛
　ｆ：＊ネットワークエンジニアの情報戦闘行為　＝　ネットワークエンジニアの情報戦闘行為：６：＃情報戦
　ｆ：＊ネットワークエンジニアの情報戦闘補正　＝　ネットワークエンジニアの情報戦闘補正：６：＃情報戦
｝
EOT
          end
          it '行為と補正の両方を同時にコンバートすることはできない' do
            expect{@context.parse_troop(source2)}.to raise_error
          end
        end
      end
    end

    describe '異常系' do
      subject do
        proc{@context.parse_troop(source)}
      end
      describe '存在しない特殊を指定' do
        let(:source) {<<EOT
32-00635-01：hacker:ハッカー；

Ｌ：コマンドリソース化（hacker）　＝　｛
　ｆ：＊ギークのナショナルネット接続能力　＝　ギークのナショナルネット接続能力：６：＃情報戦
｝
EOT
        }

        it { is_expected.to raise_error}
      end
      describe '同名特殊を2度以上指定する' do
        let(:source) {<<EOT
32-00635-01：hacker:ハッカー；

Ｌ：コマンドリソース化（hacker）　＝　｛
　ｆ：＊ハッカーのナショナルネット接続能力　＝　ハッカーのナショナルネット接続能力：６：＃情報戦
　ｆ：＊ハッカーのナショナルネット接続能力　＝　ハッカーのナショナルネット接続能力：１０：＃情報戦
｝
EOT
        }

        it { is_expected.to raise_error}
      end
    end
  end
end

