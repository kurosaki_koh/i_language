# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')


describe 'Integration' do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end

  it 'test' do
    source = <<EOT
＃！有効条件：契約した王と距離１０ｍ以内で一緒に行動する間の
＃！部分有効条件：盾によって

＃！個別付与：大型ＰＣ，全ユニット
＃！除外：大型ＰＣ，｛32-00644-01_鴻屋　心太，ザ・グレート・エビゾー｝

＃！個別付与：藩王の指揮，全ユニット
＃！除外：藩王の指揮，32-00622-01_セントラル越前

＃！有効条件：＜藩王の指揮＞に関する，｛本隊，越前情報部（降車），藩王分隊｝

32-00622-01_セントラル越前：東国人＋藩王＋ギーク＋ネットワークエンジニア：知識+1*幸運+1;
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00622-01%A1%A7%A5%BB%A5%F3%A5%C8%A5%E9%A5%EB%B1%DB%C1%B0

32-00644-01_鴻屋　心太：東国人＋サイボーグ＋大剣士＋王：敏捷+1*知識+1*幸運+1;

_define_I=D:{
ザ・グレート・エビゾー：チップボール：体格+4/* Ｔ１５巨大工廠生産品につき */,
32-00643-01_夜薙当麻：東国人＋軌道降下兵＋大剣士＋剣：敏捷+1*知識+1*幸運+1;
－カトラス：個人所有：白兵距離戦闘行為，歩兵，条件発動，（白兵距離での）｛攻撃，防御，移動｝、評価＋２。片手持ち武器。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00643-01%A1%A7%CC%EB%C6%E5%C5%F6%CB%E3
－王の契約による強化：契約した王と距離１０ｍ以内で一緒に行動する間の）全判定、評価＋３。
－鴻屋王の特産品による強化：（＜王の特産品による強化の特殊能力＞で選ばれた）全判定、評価＋２。＃「白兵戦の全判定に＋２」とする。
};

＃！特殊無効：カトラスの白兵距離戦闘補正，ザ・グレート・エビゾー


＃元々の「王の特産品による強化」特殊に基づき、太元で白兵戦補正が付くよう、専用の定義パッチを作成。
Ｌ：鴻屋王の特産品による強化　＝　｛
　ｔ：名称　＝　鴻屋王の契約による強化（定義）
　ｔ：評価　＝　なし
　ｔ：特殊　＝　｛
　　＊鴻屋王の特産品による強化の定義カテゴリ　＝　，，判定補正。
　　＊鴻屋王の特産品による強化の特殊能力　＝　，，魂の故郷の特産品に由来する力を契約した剣に与えることができ、その力に応じた判定を選ぶことができる。
　　＊鴻屋王の特産品による強化の特殊補正　＝　，条件発動，（白兵距離での）全判定、評価＋２。
　　＊鴻屋王の特産品による強化の効果範囲　＝　，，契約した王と距離１０ｍ以内で一緒に行動する間。
　｝
｝

_define_division:{越前情報部（降車）：電子妖精軍,
32-00622-01_セントラル越前，
32-00644-01_鴻屋　心太,
32-00643-01_夜薙当麻
};

_define_division:{越前情報部・剣王分隊,
32-00644-01_鴻屋　心太,
ザ・グレート・エビゾー
};

_define_division:{越前情報部・剣王分隊（降車）,
32-00644-01_鴻屋　心太,
32-00643-01_夜薙当麻
};
EOT
    context = Context.new
    troop = context.parse_troop(source)
    puts result = troop.evals_str
    result.should_not be_nil

    for dv in troop.divisions
      puts dv.evals_str
    end

    ebizo_original = troop.children[2]
    ebizo_original.disabled_sps.include?('カトラスの白兵距離戦闘補正').should == true
    dvken = context.units['越前情報部・剣王分隊']
    ebizo = dvken.children[1]
    puts ebizo.children[0].idresses
    disableds = ebizo.current_situation.disabled_sps
    disableds.include?('カトラスの白兵距離戦闘補正').should == true
    melee = Evaluator['白兵戦']
#    ebizo.children[0].evaluate('知識').should == -1
    cal = melee.calculate(ebizo)

    operands = cal.arguments.values.inject(Set.new) do |set , value|
      set |= value.operands_no_dup
    end

    puts operands.collect{|sp|sp.source}
    melee.evaluate(ebizo).should == 30
  end

  it 'test2' do
    source = <<EOT
soldier:剣士;
－カトラス

＃！特殊無効：カトラスの白兵距離戦闘補正，soldier

_define_division:{test,
soldier
};
EOT
    troop = parse_troop(source)
    puts result = troop.evals_str
    result.should_not be_nil

    for dv in troop.divisions
      puts dv.evals_str
    end

    soldier_org = troop.children[0]
    soldier_org.disabled_sps.include?('カトラスの白兵距離戦闘補正').should == true

    dv = @context.units['test']
    soldier = dv.children[0]
    disableds = soldier.all_disabled_sps
    disableds.include?('カトラスの白兵距離戦闘補正').should == true
  end
end

