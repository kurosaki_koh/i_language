# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')


describe Evaluator do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end

  it "情報戦と高機能ハンドヘルド" do
    source = 'soldier:ハッカー+高機能ハンドヘルド;'
    soldier = parse_troop(source).children[0]
    hacking = Evaluator['情報戦']
    hacking.evaluate(soldier).should == 8

    source2 = <<EOT
//有効条件：位置づけ（情報系）の職業による
soldier:ハッカー+高機能ハンドヘルド;
EOT
    troop = parse_troop(source2)
    soldier2 = troop.children[0]
    hacking.evaluate(soldier2).should == 9

    #    puts inspect_evals(troop)
  end

  it "偵察兵" do
    source = 'soldier:偵察兵;'
    troop = parse_troop(source)
    soldier = troop.children[0]
    scout = troop.find_evaluator('偵察')
    scout.evaluate(soldier).should == 4
    troop.evaluators.include?(scout).should be_truthy

    source = <<EOT
soldier1:歩兵;
soldier2:歩兵;
soldier3:偵察兵;
EOT

    troop = parse_troop(source)
    troop.evaluators.include?(scout).should be_truthy

  end

  it '外交戦' do
    source = <<EOT
soldier1:外戚;
soldier2:歩兵;
EOT
    troop = parse_troop(source)
    diplomacy = Evaluator['外交戦']
    #    puts inspect_evals(troop)
    diplomacy.evaluate(troop).should == 10

    #    pending
    troop.is_action_available?('外交戦行為')
  end

  it '警官' do
    troop = parse_troop('police:警官;')
    security = Evaluator['治安維持']
    security.evaluate(troop).should == 7

    troop = parse_troop('police:警官+天陽;')
    security = Evaluator['治安維持']
    security.evaluate(troop).should == 7
  end

  it '侵入' do
    troop = parse_troop('ninja:世界忍者;')
    intrusion_luck = Evaluator['侵入（幸運）']
    ninja = troop.children[0]
    spec = ninja.find_spec('世界忍者の侵入補正')
    ninja.is_evaluate_item?(spec).should == false
    ninja.evaluate('幸運').should == 0
    intrusion_luck.evaluate(troop).should == 3

    intrusion_agidex = Evaluator['侵入（敏捷・器用）']
    intrusion_agidex.evaluate(troop).should == 0

    intrusions = troop.evaluators.select{|ev|ev.name =~ /侵入/}
    intrusions.size.should == 2

    cats = parse_troop('cat:猫;')
    intrusion_luck.evaluate(cats).should == 3
    intrusion_agidex.evaluate(cats).should == 4
  end


  it '追跡' do
    troop = parse_troop('detective:探偵;')
    trace_sen = Evaluator['追跡（感覚）']
    trace_sen.evaluate(troop).should == 7

    trace_luc = Evaluator['追跡（幸運）']
    trace_luc.evaluate(troop).should == 8

    trace_evs = troop.evaluators.select{|ev|ev.name =~ /追跡/}
    trace_evs.size == 2
  end

  it '隠蔽' do
    troop = parse_troop('detective:探偵;')
    hide = troop.find_evaluator('隠蔽')
    hide.evaluate(troop).should == 8
    troop.evaluators.include?(hide).should be_truthy

    troop = parse_troop('detective:優しい死神;')
    troop.evaluators.include?(hide)
    hide.evaluate(troop).should == '自動成功'

    troop = parse_troop <<EOT
detective:探偵;
soldier:歩兵;
EOT
    troop.evaluators.include?(hide).should be_truthy
  end

  it '対空＋全能力' do
    troop = parse_troop <<EOT
_define_I=D:{
fake_t:フェイクトモエリバー,
pilot:名パイロット
};
EOT
    aab = troop.evaluators.find{|ev|ev.name =~ /対空/}
    aab.name.should == '対空戦闘'
    fake = troop.children[0]
    spec = fake.find_spec('名パイロットの搭乗戦闘補正')
    spec.is_applicable?(troop.children[0],'対空戦闘').should == false
    aab.evaluate(troop).should == 13

    troop2 = parse_troop <<EOT
//有効条件:｛Ｉ＝Ｄ，ＲＢ，航空機｝に搭乗して、戦闘する場合での
_define_I=D:{
fake_t:フェイクトモエリバー,
pilot:名パイロット
};
EOT
    aab.evaluate(troop2).should == 14

    troop3 = parse_troop <<EOT
//有効条件:｛Ｉ＝Ｄ，ＲＢ，航空機｝に搭乗して、戦闘する場合での
_define_I=D:{
fake_t:フェイクトモエリバー+増加燃料装備,
pilot:名パイロット
};
EOT
    aab.evaluate(troop3).should == 11
  end

  it 'ＡＣＥ航空機の対空' do
    source = <<EOT
_define_I=D:{決戦号_星鋼京所有：決戦号＋ＨＱ,
pilot:パイロット;
}；
EOT
    troop = parse_troop(source)
    aab = troop.evaluators.find{|ev|ev.name =~ /対空/}
    aab.name.should == '対空戦闘'
    vehicle = troop.children[0]
    vehicle.evals['対空戦闘'].should == 23
    vehicle.evaluate('対空戦闘').should == 23
    aab.evaluate(vehicle).should == 26
    aab.evaluate(troop).should == 26
  end

  it 'デカショーの対歩兵限定中距離補正' do
    troop = parse_troop <<EOT
Ｌ：普通の人　＝　｛
　ｔ：名称　＝　普通の人（人）
　ｔ：評価　＝　体格０，筋力０，耐久力０，外見０，敏捷０，器用０，感覚０，知識０，幸運０
　ｔ：特殊　＝　｛
　　＊普通の人の人カテゴリ　＝　，，基本人アイドレス。
　　＊普通の人の位置づけ　＝　，，デバッグ用定義。
　｝
｝

_define_I=D:{
鍋デカショー０１_国有：デカショー，
soldier:普通の人;
};
EOT
    middle = Evaluator['中距離戦']
    middle.evaluate(troop).should == 13

    troop = parse_troop <<EOT
Ｌ：普通の人　＝　｛
　ｔ：名称　＝　普通の人（人）
　ｔ：評価　＝　体格０，筋力０，耐久力０，外見０，敏捷０，器用０，感覚０，知識０，幸運０
　ｔ：特殊　＝　｛
　　＊普通の人の人カテゴリ　＝　，，基本人アイドレス。
　　＊普通の人の位置づけ　＝　，，デバッグ用定義。
　｝
｝

＃！部分有効条件：敵が歩兵の場合のみ
_define_I=D:{
鍋デカショー０１_国有：デカショー，
soldier:普通の人;
};
EOT
    middle.evaluate(troop).should == 16
  end

  it '白兵戦' do
    troop = parse_troop('soldier:剣士')
    melee = Evaluator['白兵戦']
    melee.evaluate(troop).should == 3
  end

  it '近距離戦' do
    troop = parse_troop('soldier:帝國軍歩兵;')
    short_range = Evaluator['近距離戦']
    short_range.choice_alternative(troop).total.should == 6
    
    troop = parse_troop('policeman:警官')
    short_range.evaluate(troop).should == 6

    #    source = <<EOT
    #//部分有効条件：射撃戦として
    #policeman:警官;
    #EOT
    #    troop = parse_troop(source)
    #    soldier = troop.children[0]
    #    sp = soldier.find_spec('警官の近距離戦闘補正')
    #    sp.is_available?([/近距離.*での/]).should be_truthy
    #    short_range.evaluate(troop).should == 6

    source = <<EOT
policeman:ＦＥＧの騎士;
EOT
    troop = parse_troop(source)
    soldier = troop.children[0]
    short_range.evaluate(troop).should == 3
    @context.partial_conditions << /ロケットパンチによる/
    @context.reset_situation_caches
    @context.partial_conditions.should == [/ロケットパンチによる/]
    @context.all_partial_conditions.should == [/ロケットパンチによる/]
    troop.reset_situation_caches
    @context.units['policeman'].reset_situation_caches
    troop.all_partial_conditions.should == [/ロケットパンチによる/]
    short_range.evaluate(troop).should == 6

    source = <<EOT
policeman:ＦＥＧの騎士;
＃！状況：ロケットパンチ解禁
＃！部分有効条件：ロケットパンチによる
EOT
    troop = parse_troop(source)
    soldier = troop.children[0]
    short_range.evaluate(troop).should == 3
    @context.partial_conditions.should == []
    @context.all_partial_conditions.should == []
    #    troop.all_partial_conditions_cashe.should == []
    @context.change_situation_to('ロケットパンチ解禁')
    @context.partial_conditions.should == [/ロケットパンチによる/]
    @context.all_partial_conditions.should == [/ロケットパンチによる/]
    troop.all_partial_conditions.should == [/ロケットパンチによる/]
    @context.units['policeman'].all_partial_conditions.should == [/ロケットパンチによる/]
    short_range.evaluate(troop).should == 6

    @context.change_situation_to('（基本）')
    short_range.evaluate(troop).should == 3
  end

  it '同調' do
    source = <<EOT
soldier:歩兵;
leader:宇宙の賢者;
EOT
    troop = parse_troop(source)

    synchro = Evaluator['同調']

    leader = troop.children[1]
    synchro.evaluate(troop).should == 5
    details = synchro.details(troop)

    details.children.size.should == 1
    details.children[0].unit.object_id.should == leader.object_id
    details.total.should == 5

    source = <<EOT
leader1:宇宙の賢者;
leader2:宇宙の賢者;
EOT
    troop = parse_troop(source)


    leader1 = troop.children[0].object_id
    leader2 = troop.children[1].object_id
    synchro.evaluate(troop).should == 5
    details = synchro.details(troop)

    details.children.find{|c| c.unit.object_id == leader1}.should_not be_nil
    details.children.find{|c| c.unit.object_id == leader2}.should_not be_nil
    details.total.should == 5

    source = <<EOT
＃！部隊補正：：全能力+2
soldier:歩兵;
leader:宇宙の賢者;
EOT

    troop = parse_troop(source)
    synchro.evaluate(troop).should == 7

    source = <<EOT
＃！部隊補正：：外見+1
soldier:歩兵;
leader:宇宙の賢者;
EOT

    troop = parse_troop(source)
    synchro.evaluate(troop).should == 6

    source = <<EOT
＃！部隊補正：：：同調+3
soldier:歩兵;
leader:宇宙の賢者;
EOT

    troop = parse_troop(source)
    synchro.evaluate(troop).should == 8

    source = <<EOT
＃！部隊補正：：：同調+3
soldier:歩兵+摂政;
leader:宇宙の賢者;
EOT
    troop = parse_troop(source)
    synchro.evaluate(troop).should == '自動成功'

    source = <<EOT
＃！部隊補正：猫と犬の前足が重なった腕輪
soldier:歩兵;
leader:宇宙の賢者;
EOT
    troop = parse_troop(source)
    synchro.evaluate(troop).should == 8

    source = <<EOT
＃！部隊補正：恩寵の時計
soldier:歩兵;
leader:宇宙の賢者;
EOT
    troop = parse_troop(source)
    synchro.evaluate(troop).should == 7
  end

  it '同調担当者を算出' do
    source = <<EOT
32-00635-01_黒埼紘：東国人＋ギーク＋スペーススペルキャスター＋宇宙の賢者+法官：敏捷+1*知識+4*幸運+1;
－高機能ハンドヘルド：個人所有：情報戦闘行為，歩兵，条件発動，情報戦闘、評価＋３。 , ，歩兵，条件発動，（元から情報戦闘行為ができる職業が行う場合での）情報戦闘、評価＋１。片手持ち武器。
－猫と犬の前足が重なった腕輪：個人所有：，，条件発動，同調、評価＋３。同能力重複適用不可。腕に着用。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00635-01%A1%A7%B9%F5%BA%EB%B9%C9
32-00625-02_青柳竜峻：東国人＋ハッカー＋ギーク＋理力使い;
－高機能ハンドヘルド：個人所有：情報戦闘行為，歩兵，条件発動，情報戦闘、評価＋３。 , ，歩兵，条件発動，（元から情報戦闘行為ができる職業が行う場合での）情報戦闘、評価＋１。片手持ち武器。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00625-02%A1%A7%C0%C4%CC%F8%CE%B5%BD%D4
32-00640-01_朱居まりあ：東国人＋ハッカー＋ギーク＋理力使い：敏捷+1*知識+1*幸運+1;
EOT

    troop = parse_troop(source)
    sync = Evaluator['同調']
    details = sync.details(troop)

    details.children.size.should == 1
    leader = details.children[0].unit
    leader.name.should == '黒埼紘'
    details.total.should == 14

    source = <<EOT
32-00625-02_青柳竜峻：東国人＋ハッカー＋ギーク＋理力使い;
－高機能ハンドヘルド：個人所有：情報戦闘行為，歩兵，条件発動，情報戦闘、評価＋３。 , ，歩兵，条件発動，（元から情報戦闘行為ができる職業が行う場合での）情報戦闘、評価＋１。片手持ち武器。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00625-02%A1%A7%C0%C4%CC%F8%CE%B5%BD%D4
32-00640-01_朱居まりあ：東国人＋ハッカー＋ギーク＋理力使い：敏捷+1*知識+1*幸運+1;
EOT
    troop = parse_troop(source)
    details = sync.details(troop)
    details.children.size.should == 2
    details.total.should == -2

    source = <<EOT
32-00635-01_黒埼紘：東国人＋ギーク＋スペーススペルキャスター＋宇宙の賢者+法官：敏捷+1*知識+4*幸運+1;
－高機能ハンドヘルド：個人所有：情報戦闘行為，歩兵，条件発動，情報戦闘、評価＋３。 , ，歩兵，条件発動，（元から情報戦闘行為ができる職業が行う場合での）情報戦闘、評価＋１。片手持ち武器。
－猫と犬の前足が重なった腕輪：個人所有：，，条件発動，同調、評価＋３。同能力重複適用不可。腕に着用。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00635-01%A1%A7%B9%F5%BA%EB%B9%C9
32-00625-02_青柳竜峻：東国人＋ハッカー＋摂政＋理力使い;
－高機能ハンドヘルド：個人所有：情報戦闘行為，歩兵，条件発動，情報戦闘、評価＋３。 , ，歩兵，条件発動，（元から情報戦闘行為ができる職業が行う場合での）情報戦闘、評価＋１。片手持ち武器。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00625-02%A1%A7%C0%C4%CC%F8%CE%B5%BD%D4
32-00640-01_朱居まりあ：東国人＋ハッカー＋ギーク＋理力使い：敏捷+1*知識+1*幸運+1;
EOT

    troop = parse_troop(source)
    details = sync.details(troop)
    details.children.size.should == 1
    details.children[0].unit.name.should == '青柳竜峻'
    details.total.should == '自動成功'

    source = <<EOT
32-00635-01_黒埼紘：東国人＋ギーク＋スペーススペルキャスター＋宇宙の賢者+法官：敏捷+1*知識+4*幸運+1;
32-00625-02_青柳竜峻：東国人＋ハッカー＋ギーク＋理力使い;
32-00640-01_朱居まりあ：東国人＋ハッカー＋ギーク＋理力使い：敏捷+1*知識+1*幸運+1;

_define_I=D:{
ぬりかべ：チップボール,
32-00643-01_夜薙当麻：東国人＋軌道降下兵＋摂政＋剣：敏捷+1*知識+1*幸運+1;
};
EOT

    troop = parse_troop(source)
    details = sync.details(troop)
    details.children.size.should == 1
    details.children[0].unit.name.should == '夜薙当麻'
    details.total.should == '自動成功'

    #犬士は同調算出に含めない。
    source = <<EOT
18-00348-01_崎戸剣二：詩歌の民＋犬妖精＋整備士２＋バトルメード：敏捷+1；
18-xx005-xx_犬士：詩歌の民＋犬妖精＋吟遊詩人＋ドラゴンシンパシー＋ＨＱ知識＋ＨＱ知識；
EOT
    troop = parse_troop(source)
    details = sync.details(troop)
    details.children.size.should == 1
    details.children[0].unit.name.should == '崎戸剣二'
    details.total.should == 11

  end

  it 'ネットワークエンジニア' do
    source = <<EOT
engineer:ネットワークエンジニア;
EOT
    troop = parse_troop(source)
    net_support = Evaluator['ネットワーク維持']
    net_support.should_not be_nil
    net_trace = Evaluator['ネットワーク追跡']
    net_trace.should_not be_nil

    net_support.evaluate(troop).should == 17
    net_trace.evaluate(troop).should == 17
  end

  it 'Evaluator#tags と装甲' do
    source = <<EOT
fencer:剣士::装甲+1;
EOT
    troop = parse_troop(source)
    %w[装甲（通常） 装甲（白兵距離） 装甲（近距離）].each{|n|
      Evaluator[n].evaluate(troop).should == 4
    }
  end

  it '魅力の個別評価算出' do
    pending '魅力の算出ルールが不明確なため。個別なのか部隊単位なのか。'
    source = <<EOT
32-00635-01_黒埼紘：東国人＋ギーク＋スペーススペルキャスター＋宇宙の賢者+法官：敏捷+1*知識+4*幸運+1;
32-00625-02_青柳竜峻：東国人＋ハッカー＋ギーク＋理力使い;
32-00640-01_朱居まりあ：東国人＋ハッカー＋ギーク＋理力使い：敏捷+1*知識+1*幸運+1;

_define_I=D:{
ぬりかべ：チップボール,
32-00643-01_夜薙当麻：東国人＋軌道降下兵＋摂政＋剣：敏捷+1*知識+1*幸運+1;
};
EOT
    troop = parse_troop(source)
    charisma = Evaluator['魅力']

    details = charisma.details(troop)
    details.member.size.should == 3
    {'黒埼紘' => 18, '青柳竜峻' => 5 ,  '朱居まりあ' => 6 }.each do |key , value|
      unit= details.member.find{|m|m.name == key}
      unit.should_not be_nil
      charisma.evaluate(unit).should == value
      #      unit = details.members.keys.find{|k| k.name == key}
      #      unit.should_not be_nil
      #      details.members[unit].should == value
    end
  end

  it '歌唱行為' do
    source = <<EOT
18-00344-01_星月　典子：詩歌の民＋蛇神の僧侶＋吟遊詩人＋摂政＋大神官＋ＨＱ知識：敏捷＋1*器用＋1*知識＋1*幸運＋1*耐久力+1*耐久力+3；
－蛇の指輪２：個人所有：耐久力+1（着用型／手先）
－大健康の腕輪：個人所有：耐久力+3（着用型／腕）
－カトラス：個人所有：白兵行為可能、白兵＋２（着用型／その他）＃片手装備→その他

18-00347-01_駒地真子：詩歌の民＋看護士＋吟遊詩人＋ドラゴンシンパシー＋参謀：敏捷+1*器用+1*知識+1*幸運+2*外見+2；
－恩寵の時計：個人所有：外見+2（着用型／首）
－猫と犬の前足が重なった腕輪：個人所有：同調判定+3（着用型／腕）

18-00356-01_経：高位北国人＋蛇神の僧侶＋吟遊詩人＋ドラゴンシンパシー＋大神官＋ＨＱ知識：敏捷＋1*幸運＋1*耐久力+3；
－大健康の腕輪：個人所有：耐久力+3（着用型／腕）

_define_I=D:{
ぬりかべ：チップボール,
32-00643-01_夜薙当麻：東国人＋軌道降下兵＋摂政＋剣：敏捷+1*知識+1*幸運+1;
};
EOT
    troop = parse_troop(source)
    singing = Evaluator['歌唱']
    troop.evaluators.find{|e|e.object_id == singing.object_id}.should be_nil

    source = <<EOT
18-00344-01_星月　典子：詩歌の民＋蛇神の僧侶＋吟遊詩人＋摂政＋大神官＋ＨＱ知識：敏捷＋1*器用＋1*知識＋1*幸運＋1*耐久力+1*耐久力+3；
－蛇の指輪２：個人所有：耐久力+1（着用型／手先）
－大健康の腕輪：個人所有：耐久力+3（着用型／腕）
－カトラス：個人所有：白兵行為可能、白兵＋２（着用型／その他）＃片手装備→その他

18-00347-01_駒地真子：詩歌の民＋看護士＋吟遊詩人＋ドラゴンシンパシー＋参謀：敏捷+1*器用+1*知識+1*幸運+2*外見+2；
－恩寵の時計：個人所有：外見+2（着用型／首）
－猫と犬の前足が重なった腕輪：個人所有：同調判定+3（着用型／腕）

18-00356-01_経：高位北国人＋蛇神の僧侶＋吟遊詩人＋ドラゴンシンパシー＋大神官＋ＨＱ知識：敏捷＋1*幸運＋1*耐久力+3；
－大健康の腕輪：個人所有：耐久力+3（着用型／腕）

EOT
    troop = parse_troop(source)
    singing = troop.find_evaluator('歌唱')
    troop.evaluators.find{|e|e.object_id == singing.object_id}.should_not be_nil

    singing.evaluate(troop).should == 29
  end

  it 'ヘンな治療' do
    source = <<EOT
priest:医師+蛇神の僧侶;
EOT
    troop = parse_troop(source)
    shamanism = Evaluator['ヘンな治療']
    shamanism.evaluate(troop).should == 13
  end

  it '陣地構築' do
    #一般行為としての陣地構築
    source = <<EOT
歩兵：帝國軍歩兵;
EOT
    troop = parse_troop(source)
    positioning_m = troop.find_evaluator('陣地構築（筋力）')
    positioning_i = troop.find_evaluator('陣地構築（知識）')

    m_oid = positioning_m.object_id
    i_oid = positioning_i.object_id
    evs = troop.evaluators
    e_oids = evs.collect{|ev|ev.object_id}
    e_oids.include?(m_oid).should be_truthy
    e_oids.include?(i_oid).should be_truthy

    positioning_m.evaluate(troop).should == 4
    positioning_i.evaluate(troop).should == 5

    #チップつき
    source = <<EOT
_define_I=D:{チップ：チップボール，
歩兵：帝國軍歩兵;
};
EOT
    troop = parse_troop(source)
    evs = troop.evaluators
    e_oids = evs.collect{|ev|ev.object_id}
    e_oids.include?(m_oid).should be_truthy
    e_oids.include?(i_oid).should be_falsey

    positioning_m.evaluate(troop).should == 24

    #チップ＆工兵
    source = <<EOT
＃！部分有効条件：工兵が搭乗している場合，チップ
_define_I=D:{チップ：チップボール，
歩兵：戦闘工兵;
};
EOT
    troop = parse_troop(source)
    positioning_m.evaluate(troop).should == 28
  end

  it '夢の中での攻撃' do
    source = <<EOT
dreamhunter:夢使い;
EOT
    troop = parse_troop(source)
    evs = troop.evaluators
    e_oids = evs.collect{|ev|ev.object_id}

    dream_hunt = Evaluator['夢の中での攻撃']
    e_oids.include?(dream_hunt.object_id)
    dream_hunt.evaluate(troop).should == 4
  end

  it '商人' do
    source = <<EOT
marchant:商人；
EOT
    troop = parse_troop(source)
    evs = troop.evaluators
    e_oids = evs.collect{|ev|ev.object_id}

    appraisal = Evaluator['目利き']
    e_oids.include?(appraisal.object_id)
    appraisal.evaluate(troop).should == 9

    business_talk = Evaluator['商談']
    e_oids.include?(business_talk.object_id)
    business_talk.evaluate(troop).should == 9
  end


  it '同調評価の派生' do
    source = <<EOT
Ｌ：地上戦闘情報共有システム　＝　｛
　ｔ：名称　＝　地上戦闘情報共有システム（技術）
　ｔ：要点　＝　機器の導入，情報共有，無駄のない行動
　ｔ：周辺環境　＝　地上
　ｔ：評価　＝　なし
　ｔ：特殊　＝　｛
　　＊地上戦闘情報共有システムの技術カテゴリ　＝　，，組織技術。
　　＊地上戦闘情報共有システムの使用制限１　＝　，，機械が動作しない環境では、作動しない。
　　＊地上戦闘情報共有システムの使用制限２　＝　，，フィーブル藩国の人間が同じ戦場にいる場合に効果を発揮する。
　　＊地上戦闘情報共有システムの戦闘補正　＝　，，（機械が動作する環境において）｛攻撃，防御｝、評価＋４。
　　＊地上戦闘情報共有システムの同調補正　＝　，，（機械が動作する環境において）同調、評価＋８。
　　＊地上戦闘情報共有システムの特殊効果２　＝　，，同じ戦場にいる味方の部隊に秘匿通信でメッセージを送ることができる。
　　＊地上戦闘情報共有システムの索敵補正　＝　，，（機械が動作する環境において）索敵、評価＋４。
　｝
｝
＃！部隊補正：地上戦闘情報共有システム
soldier:歩兵；
－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／腕）
＃！評価派生：機械動作環境，同調
＃！有効条件：機械が動作する環境において
EOT

    troop = parse_troop(source)

    synchro = Evaluator['同調']
    synchro2 = @context.particular_evaluators[0]
    synchro2.should_not be_nil
    synchro.evaluate(troop).should == 3
    synchro2.evaluate(troop).should == 11

    defined?(synchro.is_predefined?).should be_truthy
    synchro.is_predefined?.should be_truthy
    synchro2.is_predefined?.should be_falsey

    str = troop.leaders_str
    str.should =~ /機械動作環境/

  end
  it 'ツーマンセル詠唱バグ' do
    source = <<EOT
＃！部分有効条件：二人一組で運用する場合
10-00040-01_ソーニャ／ヴァラ／モウン艦氏族／スターチス：高位森国人+突撃アイドル+ハンターキラウイッチ+突撃リポーター+ＨＱ外見：敏捷+1*知識+1*幸運+3；
＃－クローバーのしおり：個人所有：幸運＋１（非着用型）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00040-01%A1%A7%A5%BD%A1%BC%A5%CB%A5%E3%A1%BF%A5%F4%A5%A1%A5%E9%A1%BF%A5%E2%A5%A6%A5%F3%B4%CF%BB%E1%C2%B2%A1%BF%A5%B9%A5%BF%A1%BC%A5%C1%A5%B9 [^]
10-00217-01_久堂尋軌：高位森国人+医師+ハンターキラウイッチ+テストパイロット+ＨＱ器用+法官：敏捷+1*知識+3*幸運+2；
－カトラス：個人所有：白兵行為可能になり、評価＋２（着用型／その他）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00217-01%A1%A7%B5%D7%C6%B2%BF%D2%B5%B0 [^]
EOT

    troop = parse_troop(source)
    Evaluator['詠唱'].evaluate(troop).should == 20
  end

  it 'is_omitted?' do
    source = <<EOT
soldier:歩兵；
EOT

    troop = parse_troop(source)
    armor = Evaluator['装甲（通常）']
    armor_melee = Evaluator['装甲（白兵距離）']

    armor_culc = armor.calculate(troop)
    armor_melee_culc = armor_melee.calculate(troop)
    armor_culc.to_digest == armor_melee_culc.to_digest
    armor_melee.is_omitted?(troop).should be_truthy

    troop = parse_troop('dog:犬妖精；')
    armor_culc = armor.calculate(troop)
    armor_melee_culc = armor_melee.calculate(troop)
    armor_culc.to_digest != armor_melee_culc.to_digest
    armor_melee.is_omitted?(troop).should be_falsey

  end

  it '治療（医学）' do
    source = <<EOT
doctor:医師；
EOT
    troop = parse_troop(source)
    cure = Evaluator['治療']

    calc = cure.calculate(troop)
    calc.total.should == 4
  end

  it '治療（詠唱）' do
    source = <<EOT
doctor:医師＋魔法医；
EOT
    troop = parse_troop(source)
    cure = Evaluator['治療']

    calc = cure.calculate(troop)
    #    cure.is_omitted?(troop).should be_false
    calc.total.should == 17
  end


  it '僧侶の装甲（白兵距離）' do
    source = <<EOT
＃　評価無効：｛白兵戦，装甲（白兵距離）｝

僧侶ａ：はてない国人＋僧侶＋僧正＋上級僧侶；
剣士c：東国人＋剣士＋大剣士＋剣；

＃！評価派生：対アンデッド限定，｛白兵戦，装甲（白兵距離）｝
＃！部分有効条件：アンデッドに対して
＃！部分有効条件：アンデッドに対する

＃！評価派生：対アンデッド以外，白兵戦
EOT
    troop = parse_troop(source)
    armor_melee = Evaluator['装甲（白兵距離）']

    armor_melee.is_omitted?(troop).should be_truthy
    troop_evs = troop.evaluators
    exocism = troop.evaluators.find{|ev| ev.name == '（対アンデッド限定）装甲（白兵距離）'}
    exocism.is_omitted?(troop).should be_falsey
    actions_str = troop.action_evals_str
    actions_str.should =~ /（対アンデッド限定）装甲（白兵距離）/
  end

  it '評価派生からの評価派生' do
    source = <<EOT
＃！個別付与：一人は皆の為に皆は一人の為に，全ユニット

＃！評価派生：絶技使用，装甲
＃！有効条件：この絶技を使用した場合

＃！評価派生：絶技使用＆盾不使用，｛（絶技使用）装甲（通常），（絶技使用）装甲（白兵距離）｝
＃！有効条件：この絶技を使用した場合

_define_I=D:{chip:チップボール,
soldier:歩兵；
};
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
  end

  it '評価定義機能' do
    source = <<EOT
Ｌ：評価定義（遠隔整備時装甲）　＝　｛
　ｆ：引数　＝　ジルコニア装甲
　ｆ：補正対象　＝　防御
　ｆ：補正使用条件　＝　なし
｝

Ｌ：遠隔動作補助システム　＝　｛
　ｔ：名称　＝　遠隔動作補助システム（アイテム）
　ｔ：要点　＝　小型メカ，ヘッドセットと手袋，整備士
　ｔ：周辺環境　＝　戦場
　ｔ：評価　＝　ジルコニア装甲５
　ｔ：特殊　＝　｛
　　＊遠隔動作補助システムのアイテムカテゴリ　＝　，，着用型アイテム。
　　＊遠隔動作補助システムの着用箇所　＝　，，｛頭に着用するもの，腕に着用するもの｝。
　　＊遠隔動作補助システムの形状　＝　，，｛ヘッドセット，手袋｝。
　　＊遠隔動作補助システムの使用制限１　＝　，，羅幻王国のＩ＝Ｄ工場以外での再生産はできない。
　　＊遠隔動作補助システムの使用制限２　＝　，，機械が動作しない環境では、遠隔動作補助システムは機能しない。
　　＊遠隔動作補助システムの使用制限３　＝　，，整備行為ができ、かつ、整備評価１０以上でなければ使用する事ができない。
　　＊遠隔動作補助システムの特殊能力１　＝　，，ＡＲ３以内なら離れていても整備が可能となる。ＡＲ１距離なら整備判定に－１修正を与える。ＡＲ２距離なら－２修正、ＡＲ３距離なら－３修正を与える。
　　＊遠隔動作補助システムの特殊能力２　＝　，，宇宙や海中など生身の整備士が行動できない場所へも整備行為が可能となる。
　　＊遠隔動作補助システムの遠隔整備補正１　＝　歩兵，，（ＡＲ１距離での）整備、評価－１。
　　＊遠隔動作補助システムの遠隔整備補正２　＝　歩兵，，（ＡＲ２距離での）整備、評価－２。
　　＊遠隔動作補助システムの遠隔整備補正３　＝　歩兵，，（ＡＲ３距離での）整備、評価－３。
　｝
　ｔ：→次のアイドレス　＝　整備士官（職業４），整備技術養成学校（施設），海底資源の探索（イベント），兵員輸送ホバー車両の開発（イベント）
｝

soldier:整備士２；
－遠隔動作補助システム

soldier2:整備士２；
－遠隔動作補助システム
EOT

    root = @parser.parse(source)
    puts @parser.failure_reason unless root
    root.should_not be_nil

    troop = parse_troop(source)
    evs = troop.evaluators
    zil_armor = evs.find{|ev|ev.name == '遠隔整備時装甲'}
    zil_armor.should_not be_nil

    zil_armor.args.should == ['ジルコニア装甲']
    zil_armor.bonus_target.should == '防御'
    zil_armor.conditions.should == [/(白兵|射撃)（.+）/o]

    zil_armor.evaluate(troop).should == 8

    source =<<EOT
Ｌ：評価定義（ネットワーク追跡もどき）　＝　｛
　ｆ：引数　＝　｛知識，器用｝
　ｆ：補正対象　＝　情報戦闘
　ｆ：補正使用条件　＝　ネットワーク追跡での
｝

engineer:ネットワークエンジニア＋ギーク；
EOT
    troop = parse_troop(source)

    modoki = troop.evaluators.find{|ev|ev.name == 'ネットワーク追跡もどき'}
    modoki.args.should == %w[知識 器用]
    modoki.bonus_target.should == '情報戦闘'
    modoki.conditions.should == [/ネットワーク追跡での/]
    modoki.class_name.should == 'ネットワーク追跡もどき'
    modoki.action_name.should be_nil
    modoki.evaluate(troop).should == 24

    source = <<EOT
Ｌ：評価定義（ネットワーク追跡もどき３）　＝　｛
　ｆ：引数　＝　｛知識，器用｝
　ｆ：補正対象　＝　情報戦闘
　ｆ：補正使用条件　＝　ネットワーク追跡での
　ｆ：評価分類　＝　ネットワーク追跡
　ｆ：行為分類　＝　ネットワーク追跡行為
｝

geek：ギーク；
EOT
    root = @parser.parse(source)
    root.should_not be_nil

    troop = parse_troop(source)

    evs =  @context.particular_evaluators
    modoki = evs.find{|ev|ev.name == 'ネットワーク追跡もどき３'}
    modoki.class_name.should == 'ネットワーク追跡'
    modoki.action_name.should == 'ネットワーク追跡行為'

    troop.evaluators.find{|ev| ev.name == 'ネットワーク追跡もどき３'}.should be_nil

    troop.action_evals_str.should_not =~ /ネットワーク追跡もどき/
  end
  it '治療での幸運' do
    source = <<EOT
doctor1:名医；
doctor2:医師；
EOT

    troop = parse_troop(source)
    destiny = Evaluator['治療での幸運']
    destiny.is_omitted?(troop).should be_falsey

    source = <<EOT
doctor1:医師；
doctor2:医師；
EOT

    troop = parse_troop(source)
    destiny.is_omitted?(troop).should be_truthy
  end

  it 'AlternativeEvaluator' do
    source = <<EOT
soldier1:歩兵＋騎士；
soldier2:帝國軍歩兵＋剣士；
EOT
    troop = parse_troop(source)
    s_range = Evaluator['近距離戦']
    alts = s_range.alternatives
    alts.size.should > 1

    s_range.choice_alternative(troop.children[0]).total.should == 3
    #    s_range.choice_alternative(troop.children[1]).total.should == 7

    calc = s_range.calculate(troop)
    calc.total.should == 9

    #    melees = Evaluator['白兵戦'].alternatives
    #    melees.size.should == 11

    sp1 = @context.units['soldier1'].find_spec('歩兵の近距離戦闘補正')
    sp2 = @context.units['soldier2'].find_spec('剣士の近距離戦闘補正')
    sp3 = @context.units['soldier1'].find_spec('騎士の近距離戦闘補正')

    sp1.methods.should include(:weapon_names)
    sp1.weapon_names.should == ['射撃（銃）']
    sp1.weapon_names.should == ['射撃（銃）']
    sp2.weapon_names.should == ['白兵（剣）']

    @context.units['soldier1'].weapon_names.should == ['射撃（銃）','白兵（剣）']
    @context.units['soldier1'].weapon_classes.should == ['射撃','白兵']
    sp3.is_used?.should be_truthy
    troop.is_sp_used?(sp3).should be_truthy

    for sp in [sp1,sp2]
      sp.is_used?.should be_falsey
      troop.is_sp_used?(sp).should be_falsey
    end

    source = <<EOT
Ｌ：警官　＝　｛
　ｔ：名称　＝　警官（職業）
　ｔ：要点　＝　警官制服，拳銃，バトン
　ｔ：周辺環境　＝　交番
　ｔ：評価　＝　体格３，筋力３，耐久力２，外見４，敏捷３，器用２，感覚３，知識２，幸運０，治安維持３
　ｔ：特殊　＝　｛
　　＊警官の職業カテゴリ　＝　，，派生職業アイドレス。
　　＊警官の位置づけ　＝　，，｛警察系，歩兵系｝。
　　＊警官の治安維持補正　＝　，条件発動，（治安維持での）全判定、評価＋４。
　　＊警官の白兵距離戦闘行為　＝　，，白兵距離戦闘行為が可能。＃白兵距離戦闘評価：可能：（体格＋筋力）÷２
　　＊警官の白兵距離戦闘補正　＝　，条件発動，（｛白兵（打撃武器），白兵（格闘）｝、白兵距離での）攻撃、評価＋３、燃料－１万ｔ。
　　＊警官の近距離戦闘行為　＝　，，近距離戦闘行為が可能。＃近距離戦闘評価：可能：（敏捷＋筋力）÷２
　　＊警官の近距離戦闘補正　＝　，条件発動，（射撃（銃）、近距離での）攻撃、評価＋３、燃料－１万ｔ。属性（弾体）。
　　＊警官の中距離戦闘行為　＝　，，中距離戦闘行為が可能。＃中距離戦闘評価：可能：（感覚＋知識）÷２
　　＊警官の中距離戦闘補正　＝　，条件発動，（射撃（銃）、中距離での）攻撃、評価＋３。属性（弾体）。
　　＊警官の不殺能力　＝　，条件発動，（敵を倒した時）殺さずに取り押さえるだけになる。
　｝
　ｔ：→次のアイドレス　＝　巡査（職業），私服警官（職業），特殊警官（職業），警察用Ｉ＝Ｄの開発（イベント）
｝

soldier1:警官；
EOT
    troop = parse_troop(source)
    sp1 = @context.units['soldier1'].find_spec('警官の白兵距離戦闘補正')
    sp1.weapon_names.should == ['白兵（打撃武器）','白兵（格闘）']
  end

  it '評価派生での属性対応' do
    source = <<EOT
Ｌ：評価定義（三角飛びによる対空）　＝　｛
　ｌ：引数　＝　｛体格，筋力｝
　ｌ：補正対象　＝　攻撃
　ｌ：補正使用条件　＝　白兵距離での
　ｌ：行為分類　＝　白兵距離戦闘行為
｝

38-00050-02：さるき：暁の民＋ドラゴンスレイヤー＋剣＋ソードマスター;
EOT

    troop = parse_troop(source)
    melee = @context.particular_evaluators.find{|ev|ev.name == '三角飛びによる対空'}
    melee.evaluate(troop).should == 25

  end

  
  it '治安維持評価の判定補正' do
    source = <<EOT
policeman:警官::治安維持+3;
EOT
    troop = parse_troop(source)
    Evaluator['治安維持'].evaluate(troop).should == 10
  end
  it '武器の複合種別対応' do
    source = <<EOT
Ｌ：略奪系考古学者　＝　｛
　ｔ：名称　＝　略奪系考古学者（職業）
　ｔ：要点　＝　古びた帽子，スコップ，悪そうな目
　ｔ：周辺環境　＝　遺跡
　ｔ：評価　＝　体格０，筋力２，耐久力２，外見－１，敏捷２，器用－１，感覚１，知識２，幸運０
　ｔ：特殊　＝　｛
　　＊略奪系考古学者の職業カテゴリ　＝　，，派生職業アイドレス。
　　＊略奪系考古学者の白兵距離戦闘行為　＝　，，白兵距離戦闘行為が可能。＃白兵距離戦闘評価：可能：（体格＋筋力）÷２
　　＊略奪系考古学者の白兵距離戦闘補正　＝　，条件発動，（｛白兵（鞭），白兵（スコップ）｝、白兵距離での）｛攻撃，防御，移動｝、評価＋２、燃料－２万ｔ。
　　＊略奪系考古学者の侵入補正　＝　，条件発動，（幸運での）侵入、評価＋３、燃料－２万ｔ。７５％制限。＃侵入評価：一般：幸運
　　＊略奪系考古学者の罠解除行為　＝　，条件発動，罠解除行為が可能。＃罠解除評価：種別確認？：評価算出法確認？
　　＊略奪系考古学者の罠解除補正　＝　，条件発動，罠解除、評価＋４、燃料－１万ｔ。
　　＊略奪系考古学者の地下・遺跡補正　＝　，条件発動，（地下と遺跡での）全判定、評価＋２。
　｝
　ｔ：→次のアイドレス　＝　徳河舞蔵（ＡＣＥ），労働一号かれんちゃん（ＡＣＥ），ダンジョンエクスプローラー（職業），鞭の達人（職業），Ｉ＝Ｄ・発掘兵器（イベント）
｝
＃！部分有効条件：白兵（格闘）

soldier:南国人＋猫妖精２＋歩兵＋略奪系考古学者

EOT

    troop = parse_troop(source)
    soldier = troop.children[0]
    sp = soldier.find_spec('略奪系考古学者の白兵距離戦闘補正')
    melee = Evaluator['白兵戦']
    sp.is_available?([/白兵（鞭）/,/白兵距離での/]).should be_truthy
    calc = melee.choice_alternative(soldier)

    #    calc = melee.calculate(troop)
    calc.total.should == 10
  end

  it '白兵（格闘）・白兵（全般）・射撃（全般）はいずれとも足せる' do
    source = <<EOT
king:東国人＋藩王＋犬妖精２＋剣士；
EOT
    troop = parse_troop(source)
    melee = Evaluator['白兵戦']
    calc = melee.choice_alternative(troop)

    #    calc = melee.calculate(troop)
    calc.total.should == 15
  end

  it '水中中距離戦闘行為' do
    source = <<EOT
_define_I=D:{そっとちゃん：水竜ソットヴォーチェ２,
soldier:北国人＋犬妖精２＋銃士隊＋竜士隊;
};
EOT
    troop = parse_troop(source)
    Evaluator['水中中距離戦'].evaluate(troop).should == 22
  end
  
  it '評価対象限定' do
    source = <<EOT
Ｌ：評価定義（外交戦２）　＝　｛
　ｌ：引数　＝　外見
　ｌ：補正対象　＝　外交戦
　ｌ：補正使用条件　＝　なし
　ｌ：評価対象条件　＝　外交戦能力
｝

＃！特殊無効：摂政の外交戦能力，soldier
soldier:北国人＋歩兵＋摂政；
minister:北国人＋摂政；
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    ev = @context.particular_evaluators.find{|e|e.class_name == '外交戦２'}
    ev.target_filters[0].condition.should == /外交戦能力/o
    sessyo = troop.children[1]
    sessyo.sp_include?(/外交戦能力/o).should be_truthy
    soldier = troop.children[0]
    soldier.sp_include?(/外交戦能力/o).should be_falsey
    ev.do_target?(sessyo).should be_truthy
    calc = ev.calculate(troop)
    calc.total.should == 9

    source = <<EOT
Ｌ：評価定義（外交戦２）　＝　｛
　ｌ：引数　＝　外見
　ｌ：補正対象　＝　外交戦
　ｌ：補正使用条件　＝　なし
　ｌ：評価対象条件　＝　minister
｝

soldier:北国人＋歩兵；
minister:北国人＋摂政；
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    ev = @context.particular_evaluators.find{|e|e.class_name == '外交戦２'}
    ev.target_filters[0].condition.should == 'minister'
    ev.evaluate(troop).should == 9
  end
  it '0000016: ＲＴＲの対空ミサイルによる遠距離評価を使った対空攻撃' do
    source = <<EOT
Ｌ：ＲＴＲの爆弾　＝　｛
　ｔ：名称　＝　ＲＴＲの爆弾（定義）
　ｔ：評価　＝　なし
　ｔ：特殊　＝　｛
　　＊ＲＴＲの爆弾の定義カテゴリ　＝　，，基本オプション。
　　＊ＲＴＲの爆弾のイベント時燃料消費　＝　，，（戦闘イベント参加時）燃料－１万ｔ。
　　＊ＲＴＲの爆弾の遠距離戦闘行為　＝　，条件発動，遠距離戦闘行為が可能。敵が射程に入ると自動で１回の遠距離戦闘行為ができる。
　　＊ＲＴＲの爆弾の遠距離戦闘補正　＝　，条件発動，（射撃（兵器）、遠距離での）攻撃、評価＋３。属性（爆弾）。
　　＊ＲＴＲの爆弾の自動爆撃能力　＝　，条件発動，敵が射程に入ると自動で１回の遠距離戦闘行為ができる。１００％制限。
　　＊ＲＴＲの爆弾の初期ＡＲ修正　＝　，，ＡＲ－１。
　　＊ＲＴＲの爆弾の使用回数　＝　，，使用回数（１回）。
　｝
｝

_define_I=D:{ＲＴＲさん：ＲＴＲ，
－ＲＴＲの対空ミサイル
－ＲＴＲの爆弾
(P)pilot:パイロット
(CP)copilot1:犬妖精２
(CP)copilot2:犬妖精２
};
EOT

    troop = parse_troop(source)
    evlrs = troop.evaluators
    aaev =  evlrs.find{|ev|ev.name == '遠距離戦闘評価による対空戦闘'}
    aaev.should_not be_nil
    evlrs.find{|ev| ev.name == '対空戦闘'}.should be_nil

    aaev_s = aaev.team_alternatives[2]
    rtr = troop.children[0]

    calc = aaev_s.calculate(rtr)
    calc.total.should == 38

  end

  it 'アトモスの水中白兵行為' do
    pending
    source = <<EOT
_【任意の機体呼称】_組織所有：アトモス+HQ敏捷+HQ感覚+SHQ体格；
_【任意の機体呼称】_組織所有：アトモス/*宰相府大規模工廠T16製品感覚+4評価*/+HQ敏捷+HQ感覚+SHQ体格：感覚+4；

＃！状況：水中１
＃！部分有効条件：水中

＃！状況：水中２（絶対物理防壁不使用）
＃！部分有効条件：水中
＃！特殊無効：アトモスの水中での白兵距離戦闘補正
＃！特殊無効：アトモスの水中での防御補正
EOT

    troop = parse_troop(source)
    evlrs = troop.evaluators
    evlrs.find{|ev|ev.name == '白兵戦'}.should be_nil
    puts troop.evals_str
    puts troop.common_abilities_evals_str
    troop_evals = troop.action_evals_str
    puts troop_evals
    @context.change_situation_to('水中１',false)

    evlrs = troop.evaluators
    evlrs.find{|ev|ev.name == '白兵戦'}.should_not be_nil

    puts troop.evals_str
    puts troop.common_abilities_evals_str
    troop_evals = troop.action_evals_str
    puts troop_evals
  end

  it 'ACEの歩兵の場合は、白兵・射撃区分でしか区別しない。' do
    source = <<EOT
ＡＣＥ：砂漠の騎士サウド+ＨＱ+ＨＱ；
－恩寵の短剣

＃！評価派生：対アンデッド，｛装甲，白兵戦｝
＃！部分有効条件：アンデッドに対する
＃！部分有効条件：アンデッドに対して

＃！評価派生：蘇生での，治療
＃！有効条件：蘇生判定に関する
EOT

    troop = parse_troop(source)
    ev = troop.evaluators.find{|e|e.name == '（対アンデッド）白兵戦'}
    ev.should_not be_nil

    calc = ev.calculate(troop)
    calc.total.should == 32

  end
  it '直接評価提出と一般評価算出提出の混在' do
    source = <<EOT
＃！部隊補正：電子妖精（レンジャー版）

hacker1:愛の民+猫妖精２+ハッカー+スターファイター
hacker2:愛の民+猫妖精２+ハッカー+スターファイター
（随行）firewall:電子妖精（レンジャー版）の情報防壁;
EOT
    troop = parse_troop(source)
    ev = troop.find_evaluator('情報戦防御')
    ev.should_not be_nil
    calc = ev.calculate(troop)
    calc.arguments_avg.should == 60.0
    calc.total.should == 60
  end
  
  it '防御・移動は武器種別の影響を受けない。' do
    source = <<EOT
＃質疑・http://cwtg.jp/qabbs/bbs2.cgi?action=article&id=9565 に基づく修正

soldier:スプリンター＋黒騎士；
EOT
    troop = parse_troop(source)

    melee = Evaluator['白兵戦'].calculate(troop)
    melee.total.should == 11
    armor_melee = Evaluator['装甲（白兵距離）'].calculate(troop)
    armor_melee.total.should == 13

    s_range = Evaluator['近距離戦'].calculate(troop)
    s_range.total.should == 12
    armor_s_range = Evaluator['装甲（近距離）'].calculate(troop)
    armor_s_range.total.should == 14
  end

  it '一般行為評価の自動成功' do
    source = <<EOT
＃！有効条件：郵便配達に関する
postman:郵便配達人＋スプリンター＋追跡者;
EOT
    troop = parse_troop(source)

    manuva = troop.evaluators.find{|ev|ev.name == '移動（通常）'}
    manuva.should_not be_nil
    manuva.evaluate(troop).should == '自動成功'
    common_str = troop.common_abilities_evals_str

    actions_str = troop.action_evals_str
    common_str.should match(/移動：自動成功/)
    actions_str.should_not match(/移動（白兵距離）：自動成功/)
    actions_str.should_not match(/追跡（幸運）：自動成功/)
    actions_str.should_not match(/追跡（感覚）：自動成功/)
    actions_str.should match(/追跡：自動成功/)

    source = <<EOT
postman:郵便配達人＋スプリンター＋追跡者;
other1:歩兵；
EOT
    troop = parse_troop(source)
    actions_str = troop.action_evals_str
    actions_str.should_not match(/追跡：/)
  end

  it 'T16後ほね' do
    source = <<EOT
＃14:後ほねっこ男爵領 T16編成
＃編成表：http://www7.atwiki.jp/atohone/pages/41.html

＃！特殊無効：スプリンターの白兵距離戦闘補正，磯貝みらの２

＃！個別付与：剣技の手ほどきを受ける＋魔法の手ほどきを受ける，｛14-00287-01_瑛の南天，14-00288-01_深夜，14-00796-01_いも子｝
＃！個別付与：保育園の授業，全隊員
＃！除外：保育園の授業，14-ace02-xx_磯貝みらの２
＃！補正ＨＱ：保育園の授業の知識補正，４
＃ ！部分有効条件：白兵武器（剣）
＃！評価派生：迷宮内防御，装甲
＃！有効条件：迷宮での
＃！評価派生：迷宮内移動，移動
＃！有効条件：迷宮での
＃！評価派生：迷宮内偵察，偵察
＃！部分有効条件：迷宮内における

14-00287-01_瑛の南天：北国人＋帝國軍歩兵＋帝國軍歩兵＋迷宮巡視員+ＨＱ筋力+秘書官２：敏捷+1*器用+1*感覚+4*知識+1*幸運+2;
－アンデットバスター：個人所有：歩兵武装，条件発動，（アンデッドに対する）攻撃、評価＋５。（着用型／両手持ち武器）
－大健康の腕輪：個人所有：歩兵，，耐久力、評価＋３。（着用型／腕）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00287-01%A1%A7%B1%CD%A4%CE%C6%EE%C5%B7

14-00284-01_火足水極：北国人＋帝國軍歩兵＋帝國軍歩兵＋迷宮案内犬+ＳＨＱ筋力+参謀：筋力+1*感覚+2*幸運+1;
－高機能ハンドヘルド：個人所有：歩兵武装，，情報戦闘行為が可能。 , 歩兵武装，条件発動，情報戦闘、評価＋３。（着用型／片手持ち武器）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00284-01%A1%A7%B2%D0%C2%AD%BF%E5%B6%CB
14-ace01-xx_後藤亜細亜２：北国人＋帝國軍歩兵＋帝國軍歩兵＋迷宮案内犬＋ＳＨＱ筋力＋護民官;
＃－詳細：http://cwtg.jp/qabbs/bbs2.cgi?action=article&id=9327／護民官所有根拠
14-ace02-xx_磯貝みらの２：磯貝みらの２＋ＨＱ;

＃！状況：法執行時
＃！部分有効条件：法執行時での

＃！状況：後藤亜細亜２を守ろうとするとき
＃！補正ＨＱ：後藤亜細亜２の加護の特殊補正，１
＃！個別付与：後藤亜細亜２の加護，全隊員
＃！除外：後藤亜細亜２の加護，14-ace01-xx_後藤亜細亜２

＃！状況：法執行時＋後藤亜細亜２を守ろうとするとき
＃！部分有効条件：法執行時での
＃！個別付与：後藤亜細亜２の加護，全隊員
＃！除外：後藤亜細亜２の加護，14-ace01-xx_後藤亜細亜２
EOT
    troop = parse_troop(source)

    ev = troop.evaluators.find{|ev|ev.name == '（迷宮内移動）移動（白兵距離）'}
    ev.should_not be_nil
    ev.is_omitted?(troop).should be_truthy
  end


  it '三角とび' do
    source = <<EOT
soldier：大剣士；
－三角とび
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
    evs = troop.evaluators
    ev = evs.find{|ev|ev.name == '三角とびによる対空戦闘'}
    ev.should_not be_nil

  end
  it '同値・別状況の評価がどちらも出力される件' do
    source = <<EOT
＃！個別付与：一人は皆の為に皆は一人の為に，全ユニット

18-00344-01_星月　典子：詩歌の民＋蛇神の僧侶＋吟遊詩人＋摂政＋大神官＋ＨＱ知識：敏捷＋1*器用＋1*知識＋1*幸運＋1；
－蛇の指輪２：個人所有：耐久力+1（着用型／手先）
－大健康の腕輪：個人所有：耐久力+3（着用型／腕）
－カトラス：個人所有：白兵行為可能、白兵＋２（着用型／その他）＃片手装備→その他
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00344-01%A1%A7%C0%B1%B7%EE%A1%A1%C5%B5%BB%D2 [^]

＃！評価派生：絶技使用，装甲
＃！有効条件：この絶技を使用した場合

＃！評価派生：絶技使用＆盾不使用，｛（絶技使用）装甲（通常），（絶技使用）装甲（白兵距離）｝
＃！特殊無効：チップボールの防御補正
＃！有効条件：この絶技を使用した場合

＃！評価派生：対詠唱・魔法，装甲（通常）
＃！部分有効条件：詠唱戦闘行為や魔法に対する

＃！評価派生：対詠唱・魔法＆絶技，（絶技使用）装甲（通常）
＃！有効条件：この絶技を使用した場合
＃！部分有効条件：詠唱戦闘行為や魔法に対する

＃！評価派生：対詠唱・魔法＆絶技＆盾不使用，（絶技使用＆盾不使用）装甲（通常）
＃！特殊無効：チップボールの防御補正
＃！有効条件：この絶技を使用した場合
＃！部分有効条件：詠唱戦闘行為や魔法に対する
EOT
    troop = parse_troop(source)

    ev1 = troop.evaluators.find{|ev|ev.name == '（対詠唱・魔法＆絶技）装甲（通常）'}
    ev2 = troop.evaluators.find{|ev|ev.name == '（対詠唱・魔法＆絶技＆盾不使用）装甲（通常）'}

    calc1 = ev1.calculate(troop)
    calc2 = ev2.calculate(troop)
    calc1.total.should == calc2.total

    flag = (calc1.total.class == String ? ev2.particular_situation_name == ev.particular_situation_name : calc1.to_digest == calc2.to_digest)
    flag.should be_truthy
    ev1.represent_order.should < ev2.represent_order
    ev2.is_omitted?(troop).should be_truthy

  end

  it 'Evaluator新仕様（T16版） 一般的な例' do
    source = <<EOT
soldier:歩兵
_define_I=D:{チップ：チップボール，
pilot:パイロット＋歩兵；
};
EOT
    troop = parse_troop(source)
    scout = Evaluator['偵察']
    m_range = Evaluator['中距離戦']
    diplomacy = Evaluator['外交戦']
    d_sympathy = Evaluator['ドラゴンシンパシー']

    scout.target_set_type.should == :default

    units = scout.target_set(troop)
    [Soldier,Vehicle].zip(units.collect{|u|u.class}){|a,b|
      a.should == b
    }

    [diplomacy , d_sympathy ].all?{|ev|
      units_d = ev.target_set(troop)
      units_d.size.should == 2
      units_d.all?{|u|u.class == Soldier}
    }

    soldier = units[0]
    chip = units[1]
    scout.chained_evaluator(soldier).object_id.should == scout.object_id
    m_range.chained_evaluator(soldier).name.should == '中距離戦（射撃・銃）'
    m_range.chained_evaluator(chip).name.should == '中距離戦（射撃）'

    sp1 = soldier.find_spec('歩兵の中距離戦闘補正')
    m_range.sp_target(sp1).object_id.should == soldier.object_id

    sp2 = chip.specialties.find{|sp|sp.name == '歩兵の中距離戦闘補正'}
    m_range.sp_target(sp2).object_id.should == chip.object_id

    d_sympathy.sp_target(sp2).object_id.should == chip.children[0].object_id

    [scout,m_range].zip([soldier,chip]){|ev,unit| ev.is_branch?(unit).should_not be_truthy}
    [scout,m_range].all?{|ev|ev.is_branch?(troop).should be_truthy}

    tc = scout.create_calculation(troop)
    tc.class.should == TeamCalculation

    calc = scout.chained_evaluator(soldier).create_calculation(soldier)
    calc.class.should == Calculation

    [scout,m_range].all?{|ev|ev.is_parent_eval_enabled?.should be_truthy}
    [scout,m_range].all?{|ev|ev.is_parent_bonus_enabled?.should be_truthy}

    calc = scout.calculate(troop)
    calc.total.should == 6

  end

  it 'Evaluator新仕様（T16版） 操縦' do
    source = <<EOT
_define_I=D:{deca1：デカショー，
pilot1:南国人＋パイロット＋歩兵；
};

_define_I=D:{deca2：デカショー，
pilot2:南国人＋パイロット＋歩兵；
};
EOT
    troop = parse_troop(source)
    ctrl = troop.find_evaluator('操縦')
    ctrl.should_not be_nil
    
    units = ctrl.target_set(troop)
    units.size.should == 2
    units[0].class.should == Vehicle
    calc = ctrl.calculate(troop)
    calc.total.should be_nil

    source = <<EOT
_define_I=D:{deca1：デカショー，
pilot1:南国人＋パイロット＋歩兵；
};

_define_I=D:{deca2：デカショー，
－ラビプラス
pilot2:南国人＋パイロット＋歩兵＋ハイ・パイロット；
};
EOT

    troop = parse_troop(source)
    ctrl = troop.find_evaluator('操縦')
    calc = ctrl.calculate(troop)
    calc.children[1].total.should == 24

  end

  it '操縦評価で割烹着（器用＋１）あり' do
    source = <<EOT
_define_vessel:{
ＦＥＧボウルドカーン_国有：ＦＥＧボウルドカーン＋ＳＨＱ輸送量＋ＳＨＱ輸送量,
（Ｐ）03-00875-01_Ｃａｓｓａｄｙ：西国人＋パイロット＋整備士２＋名整備士＋ＨＱ知識;
（Ｐ）03-xx010-xx_大吉：西国人＋パイロット＋整備士２＋名整備士＋ＨＱ知識;
（Ｐ）03-xx011-xx_メイ：西国人＋パイロット＋整備士２＋名整備士＋ＨＱ知識;
（ＣＰ）03-00069-01_小鳥遊敦：高位西国人+ＷＳＯ+航海士+名パイロット+吏族：外見+5*敏捷+1*器用+1*知識+1;
－割烹着：個人所有：歩兵，条件発動，（調理での）全判定、評価＋１。（弁当が和風になる）。 , 歩兵，，外見、評価＋１。母性を感じる笑顔のため魅力的に感じる。 , 歩兵，，器用、評価＋１。袖を押さえるため器用に振舞える。重ね着。（着用型／重ね着）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00069-01%A1%A7%BE%AE%C4%BB%CD%B7%C6%D8
（ＣＰ）03-xx012-xx_真夜：西国人+猫妖精２+パイロット+名パイロット;
（ＣＰ）03-xx013-xx_リー：西国人+猫妖精２+パイロット+名パイロット;
};
EOT
    troop = parse_troop(source)

    ctrl = Evaluator['操縦（艦隊）']
    calc = ctrl.calculate(troop)
    calc.total.should == 18
  end
  
  it 'ハイ・パイロットの同調自動成功' do
    source = <<EOT
_define_I=D:{
ターキッシュバン２_国有：ターキッシュバン２＋ＨＱ感覚＋ＨＱ感覚,
（Ｐ）33-00665-01_プロメテウス（仮）：高位西国人＋ホープ＋ハイ・パイロット＋名パイロット：知識+3*敏捷+1*幸運+1
－藩国立工部大学：無名騎士藩国所有：知識＋３（ＨＱで＋２、反映済み）、整備判定＋４
（ＣＰ）03-00565-01_那限・ソーマ＝キユウ・逢真：高位西国人＋ＷＳＯ＋パイロット＋名パイロット＋ＨＱ幸運＋補給士官：敏捷+1*知識+1*幸運+1*幸運+20
＃－ピクシーＱ・ソーマ＝キユウ：幸運＋２０（反映済み）（根拠http://p.ag.etr.ac/cwtg.jp/syousyo/1357）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00565-01%A1%A7%C6%E1%B8%C2%A1%A6%A5%BD%A1%BC%A5%DE%A1%E1%A5%AD%A5%E6%A5%A6%A1%A6%B0%A9%BF%BF
（ＣＰ）33-00651-01_空紅：高位西国人＋マシンマイスター＋チューニングマスター＋名整備士＋ＨＱ知識：知識+3*敏捷+1*幸運+2
－藩国立工部大学：無名騎士藩国所有：知識＋３（ＨＱで＋２、反映済み）、整備判定＋４
};
EOT
    troop = parse_troop(source)

    sync = troop.evaluators.find{|ev|ev.name == '同調（個別）'}
    calc = sync.calculate(troop)
    calc.children[0].total.should == '自動成功'
  end

  it '個人能力評価各種' do
    source = <<EOT
soldier1:精霊使い＋式神使い＋悪魔使い；
soldier2:管理番長＋大管理番長；
soldier3：植物使い＋調和者＋動物使い；
soldier4:Ｉ＝Ｄデザイナー＋サイコキノ;
EOT
    troop = parse_troop(source)

    summon = troop.evaluators.find{|ev|ev.name == '召喚'}
    summon.should_not be_nil
    calc = summon.calculate(troop)
    calc.class.should == CalculationSet
    calc.children[0].total.should == 7

    menchi =  troop.evaluators.find{|ev|ev.name == 'メンチ'}
    menchi.should_not be_nil
    calc = menchi.calculate(troop)
    calc.class.should == CalculationSet
    calc.children[1].total.should == -27

    plantmancer = troop.evaluators.find{|ev|ev.name == '植物操作判定'}
    plantmancer.should_not be_nil
    calc = plantmancer.calculate(troop)
    calc.class.should == CalculationSet
    calc.children[2].total.should == 12

    psychokino = troop.evaluators.find{|ev|ev.name == 'サイコキネシス'}
    psychokino.should_not be_nil
    calc = psychokino.calculate(troop)
    calc.class.should == CalculationSet
    calc.children[3].total.should == 9

    analyze = troop.evaluators.find{|ev|ev.name == 'Ｉ＝Ｄデザイナーの解析'}
    analyze.should_not be_nil
    calc = analyze.calculate(troop)
    calc.class.should == CalculationSet
    calc.children[3].total.should == 9

    tame = troop.evaluators.find{|ev|ev.name == '動物操作判定'}
    tame.should_not be_nil
    calc = tame.calculate(troop)
    calc.class.should == CalculationSet
    calc.children[2].total.should == 13

  end

  it '外交戦評価は常に機体から降りているものと扱う' do
    source = <<EOT
_define_I=D：｛
ラグドール－エイレネ_藩国所有：ラグドール＋ＳＨＱ敏捷＋ＳＨＱ敏捷＋ＨＱ感覚,
（P）15-00303-01_イズナ：高位西国人＋名パイロット＋ホープ＋強化新型ホープ＋ＨＱ感覚：敏捷＋1*幸運＋2；
（CP）15-00296-01_守上藤丸：高位西国人＋猫妖精２＋スペーススターキャット＋摂政＋法の司：敏捷＋1*知識＋1*幸運＋2；
－蛇の指輪２：個人所有：（着用型／手先）
－詳細：蛇の指輪２はマジックアイテムのため、高物理域にシフトする際は装備から外します。
－恩寵の時計：個人所有：（着用型／首）
－ちいさなオパールの指輪：個人所有：（着用型／手先）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00296-01%A1%A7%BC%E9%BE%E5%C6%A3%B4%DD
（CP）15-00304-01_シュウマイ：高位西国人＋猫妖精２＋パイロット＋名パイロット：敏捷+1*幸運+3；
｝；
EOT

    troop = parse_troop(source)
    diplomacy = troop.evaluators.find{|ev|ev.name == '外交戦'}
    diplomacy.should_not be_nil
    calc = diplomacy.calculate(troop)
    sub_calc = calc.children[1]
    sub_calc.total.should == 28
    sub_calc.operands.find{|op|op.name =~ /搭乗補正/o}.should be_nil
    sub_calc.operands.find{|op|op.name =~ /恩寵の時計の外見補正/o}.should be_nil

  end

  it '補正除外条件' do
    source = <<EOT
Ｌ：評価定義（情報戦もどき）　＝　｛
　ｌ：引数　＝　｛知識，器用｝
　ｌ：補正対象　＝　情報戦闘
　ｌ：補正使用条件　＝　なし
　ｌ：補正除外条件　＝　｛知識・器用補正｝
｝

hacker:ハッカー；
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
    hack2 = troop.find_evaluator('情報戦もどき')
    hack2.should_not be_nil
    hack2.disabled_sps.should == [/知識・器用補正/]
    sp = troop.children[0].find_spec('ハッカーの知識・器用補正')
    hack2.match?(sp).should be_falsey
    hack2.evaluate(troop).should == 3
  end

  it 'マジカルステッキと白魔法使い' do
    source = <<EOT
magi:森国人＋白魔法使い；
－マジカルステッキ

EOT
    troop = parse_troop(source)

    evaluators = %w[治療 詠唱での探査 流れを戻す 詠唱 詠唱による防御（通常）].collect{|name|troop.find_evaluator(name)}

    for ev in evaluators
      puts ev.name
      ev.evaluate(troop).should == 14
    end
  end

  it '白魔法使いの詠唱防御能力は７５％制限である。' do
    source = <<EOT
magi1:森国人＋白魔法使い；
－マジカルステッキ
magi2:森国人＋白魔法使い；
magi3:森国人＋理力使い
soldier:北国人＋歩兵；

EOT
    troop = parse_troop(source)
    puts troop.tqrs.inspect
    troop.tqrs.should_not include(:'詠唱での防御能力')
    troop.find_evaluator('詠唱による防御（通常）').should be_nil

    source = <<EOT
magi1:森国人＋白魔法使い；
－マジカルステッキ
magi2:森国人＋白魔法使い；
magi3:森国人＋白魔法使い
soldier:北国人＋歩兵；

EOT
    troop = parse_troop(source)
    puts troop.tqrs.inspect
    troop.tqrs.should include(:'詠唱での防御能力')
    troop.find_evaluator('詠唱による防御（通常）').should_not be_nil
  end

  it '交渉術' do
    source = <<EOT
＃！個別付与：交渉術，全隊員

magi1:森国人＋白魔法使い；
－マジカルステッキ
magi2:森国人＋白魔法使い；
magi3:森国人＋白魔法使い
soldier:北国人＋歩兵；

EOT

    troop = parse_troop(source)
    (nego = troop.find_evaluator('交渉')).should_not be_nil
    (democracy = troop.find_evaluator('外交戦')).should_not be_nil

    nego.evaluate(troop).should == 24
    democracy.evaluate(troop).should == 24

    source = <<EOT
＃！個別付与：交渉術，全隊員
＃！除外：交渉術，｛レオ１｝

ゴロネコ１：森国人+妖精に愛されしもの+マジカルポリス+白魔法使い；
ゴロネコ２：森国人+妖精に愛されしもの+マジカルポリス+白魔法使い；
ゴロネコ３：森国人+妖精に愛されしもの+マジカルポリス+白魔法使い；
レオ１：レオドール；
EOT

    troop = parse_troop(source)
    troop.action_evals_str
    (nego = troop.find_evaluator('交渉')).should_not be_nil
  end

  it '音楽家の歌魔法' do
    source = <<EOT
singer1:音楽家；
singer2:音楽家；
singer3:音楽家；
soldier:詩歌の民＋ドラゴンシンパシー＋竜士隊＋法の司；
soldier:詩歌の民＋ドラゴンシンパシー＋竜士隊＋法の司；
EOT
    troop = parse_troop(source)
    troop.action_evals_str
    (sing = troop.find_evaluator('歌唱（歌魔法）')).should_not be_nil

    singer1 = troop.children[0]
    sp = singer1.find_spec('音楽家の歌唱補正')

    troop.is_tqr_qualified?(sp).should be_falsey

    singers = troop.members_without_attendants(sing)
    singers.size.should == 3
    troop.team_tqrs(sing).should include(:'歌唱補正')
    troop.is_tqr_qualified?(sp,sing).should be_truthy
    
    sing.evaluate(troop).should == 21

  end

  it '糸使い' do
    source = <<EOT
Ｌ：糸使い　＝　｛
　ｔ：名称　＝　糸使い（職業）
　ｔ：要点　＝　奇怪な動き，糸,見えない
　ｔ：周辺環境　＝　地獄
　ｔ：評価　＝　体格６，筋力６，耐久力４，外見４，敏捷７，器用９，感覚２，知識２，幸運２
　ｔ：特殊　＝　｛
　　＊糸使いの職業カテゴリ　＝　，，派生職業アイドレス。
　　＊糸使いの糸操作行為　＝　，，糸操作行為が可能。１０ｍまで自在に糸を操る事が出来る。この判定では器用を用いる
　　＊糸使いの糸戦闘行為　＝　，，糸戦闘行為が可能。白兵距離及び近距離で戦闘する事ができる。この判定では器用を用いる。
　　＊糸使いの糸操作補正　＝　，任意発動，（糸操作での）全判定、評価＋６、燃料－２万ｔ。
　｝
　ｔ：→次のアイドレス　＝　糸刃（ＡＣＥ），くねくね（絶技），酢（アイテム），空中浮遊（絶技）
｝

soldier:糸使い；
EOT

    troop = parse_troop(source)
    troop.action_evals_str

    troop.action_names.should include('糸操作行為')
    troop.action_names.should include('糸戦闘行為')

    melee = troop.find_evaluator('糸による白兵戦')
    melee.should_not be_nil
    melee.evaluate(troop).should == 15

    s_range = troop.find_evaluator('糸による近距離戦')
    s_range.should_not be_nil
    s_range.evaluate(troop).should == 15

    spininng = troop.find_evaluator('糸操作')
    spininng.should_not be_nil
    spininng.evaluate(troop).should == 15

  end

  it '体術' do
    source = 'fencer:剣士＋体術；'

    troop = parse_troop(source)
    troop.action_evals_str

    sp = troop.children[0].find_spec('体術の体格補正')

    melee = troop.find_evaluator('白兵戦')
    melee.match?(sp).should be_truthy
    melee.evaluate(troop).should == 5

    armor = troop.find_evaluator('装甲（白兵距離）')
    armor.match?(sp).should be_truthy
    armor.evaluate(troop).should == 5

    armor_normal = troop.find_evaluator('装甲（通常）')
    armor_normal.match?(sp).should be_falsey
    armor_normal.evaluate(troop).should == 3
  end

  it '変化の術' do
    #外見で判定する。
    #部隊内に所持者１００％でなければ評価を出力しない。

    source = <<EOT
ninja:南国人＋世界忍者；
－変化の術
EOT

    troop = parse_troop(source)
    troop.action_evals_str

    ev = troop.find_evaluator('侵入（技術＆特殊使用）')
    ev.should_not be_nil

    ev.evaluate(troop).should == 12


    source = <<EOT
ninja:南国人＋世界忍者；
－変化の術

engineer:北国人＋整備士２；
EOT

    troop = parse_troop(source)
    troop.action_evals_str

    ev = troop.find_evaluator('侵入（技術＆特殊使用）')
    ev.should be_nil
  end
  
  it '治療とヘンな治療の合算対応' do
    #音楽家が部隊に一人以上いれば評価出力
    #音楽家以外のRDは合算しない。

    source = <<EOT
とある詩人：詩歌の民＋蛇神の僧侶＋吟遊詩人＋摂政＋大神官＋ＨＱ知識；
とあるナース１：詩歌の民＋看護士＋吟遊詩人＋ドラゴンシンパシー；
とあるナース２：詩歌の民＋看護士＋吟遊詩人＋ドラゴンシンパシー；
EOT

    troop = parse_troop(source)
    troop.action_evals_str

    ev = troop.find_evaluator('ヘンな治療・蛇神の僧侶のみ')
    ev.should_not be_nil
    ev.evaluate(troop).should == 22

    ev = troop.find_evaluator('ヘンじゃない治療')
    ev.should_not be_nil
    
    ev.are_applicable_between(troop.children[0]).should be_falsey
    ev.are_applicable_between(troop.children[1]).should be_truthy
    
    troop.members_without_attendants(ev).size.should == 2
    
    ev.evaluate(troop).should == 18

    ev = troop.find_evaluator('治療（合算）')
    ev.should_not be_nil

    ev.evaluate(troop).should == 24
    
  end
 
  it '衛生兵のみ部隊の中距離戦' do
    source = <<EOT
medic1:衛生兵；
medic2:衛生兵；
EOT
    troop = parse_troop(source)
    
    medic_m = troop.find_evaluator('中距離戦・衛生兵のみ')
    medic_m.should_not be_nil
    
    medic_m.evaluate(troop).should == 10
    
    except_medics_m = troop.find_evaluator('中距離戦・衛生兵以外')
    except_medics_m.should be_nil
    
    merged_m = troop.find_evaluator('中距離戦（合算）')
    merged_m.should be_nil
       
  end
  
  
  it '衛生兵と通常中距離の合算' do
    source = <<EOT
medic:衛生兵；
soldier:帝國軍歩兵＋偵察兵；
EOT
    troop = parse_troop(source)
    
    medic_m = troop.find_evaluator('中距離戦・衛生兵のみ')
    medic_m.should_not be_nil
    
    medic_m.evaluate(troop).should == 7
    
    except_medics_m = troop.find_evaluator('中距離戦・衛生兵以外')
    except_medics_m.should_not be_nil
    
    except_medics_m.evaluate(troop).should == 8
    
    merged_m = troop.find_evaluator('中距離戦（合算）')
    merged_m.should_not be_nil
    
    merged_m.evaluate(troop).should == 11

       
  end
  
  it '絶技・琴弓の一撃' do
    source = <<EOT
spelunker:白オーマ＋帝國軍歩兵＋琴弓の一撃；
medic:帝國軍歩兵＋偵察兵＋衛生兵：全能力＋５；
soldier:帝國軍歩兵＋偵察兵：全能力＋５；
EOT
    troop = parse_troop(source)
    troop.children[0].idresses.should include('琴弓の一撃')
    
    ev = troop.find_evaluator('琴弓の一撃による対空戦闘')
    ev.should_not be_nil
    calc = ev.calculate(troop)
    calc.children[0].total.should == 21
    calc.children[1].total.should be_nil
  end

  it '絶技音速剣使用時' do
    source = <<EOT
ｌ：＊絶技音速剣の白兵距離戦闘補正　＝　，，（絶技音速剣使用時、白兵距離での）攻撃、評価＋１６。

03-00041-01_是空とおる：高位西国人＋共和国大統領＋ブルータイツマン＋ワンダーダガー＋月詠＋ＳＨＱ＋ＨＱ：敏捷＋１*器用＋１*知識＋１*幸運＋１；
－絶技音速剣：個人所有：（白兵距離での）攻撃、評価＋１６。，，（絶技戦において）白兵距離攻撃を行うことができる。この時評価は”使用者の白兵評価＋１６”を用いる。（絶技）　＃絶技戦時の評価は第１分隊の評価値を参照の事

03-xx001-xx_ラッキー・オーレ：高位西国人＋緊急投擲展開軍＋ウォードレスダンサー＋舞踏体＋ＳＨＱ体格＋ＨＱ体格＋補給士官＋ＨＱ知識＋ＨＱ知識＋ＨＱ知識＋ＨＱ知識：：白兵戦＋１２；
EOT
    troop = parse_troop(source)

    ev = troop.find_evaluator('絶技による白兵戦')
    ev.should_not be_nil
    
    result = ev.calculate(troop)
    result.children[0].total.should == 48
    result.children[1].total.should be_nil

  end
  
  it '遠隔動作補助システム' do
    source = <<EOT
Ｌ：遠隔動作補助システム　＝　｛
　ｔ：名称　＝　遠隔動作補助システム（アイテム）
　ｔ：要点　＝　小型メカ，ヘッドセットと手袋，整備士
　ｔ：周辺環境　＝　戦場
　ｔ：評価　＝　小型メカ装甲５
　ｔ：特殊　＝　｛
　　＊遠隔動作補助システムのアイテムカテゴリ　＝　，，着用型アイテム。
　　＊遠隔動作補助システムの着用箇所　＝　，，｛頭に着用するもの，腕に着用するもの｝。
　　＊遠隔動作補助システムの形状　＝　，，｛ヘッドセット，手袋｝。
　　＊遠隔動作補助システムの使用制限１　＝　，，羅幻王国のＩ＝Ｄ工場以外での再生産はできない。
　　＊遠隔動作補助システムの使用制限２　＝　，，機械が動作しない環境では、遠隔動作補助システムは機能しない。
　　＊遠隔動作補助システムの使用制限３　＝　，，整備行為ができ、かつ、整備評価１０以上でなければ使用する事ができない。
　　＊遠隔動作補助システムのＡＲ１距離整備補正　＝　歩兵武装，，（ＡＲ１距離での）整備、評価－１。
　　＊遠隔動作補助システムのＡＲ２距離整備補正　＝　歩兵武装，，（ＡＲ２距離での）整備、評価－２。
　　＊遠隔動作補助システムのＡＲ３距離整備補正　＝　歩兵武装，，（ＡＲ３距離での）整備、評価－３。
　　＊遠隔動作補助システムの特殊能力１　＝　歩兵武装，，ＡＲ３以内なら離れていても整備が可能となる。ＡＲ１距離なら整備判定に－１修正を与える。ＡＲ２距離なら－２修正、ＡＲ３距離なら－３修正を与える。
　　＊遠隔動作補助システムの特殊能力２　＝　歩兵武装，，宇宙や海中など生身の整備士が行動できない場所へも整備行為が可能となる。
　｝
　ｔ：→次のアイドレス　＝　整備士官（職業４），整備技術養成学校（施設），海底資源の探索（イベント），兵員輸送ホバー車両の開発（イベント）
｝

16-00319-01_刻生・Ｆ・悠也：高位西国人＋ＲＢパイロット＋整備士２＋ホープ＋情報士官＋ＨＱ器用：敏捷+1*知識+1*幸運+3；
－カトラス：個人所有：白兵行為可能。白兵での攻撃・防御・移動＋２（着用型／片手持ち武器）
－きれいなクラゲ：個人所有：無限に時間を消費可能。１ターンに１度、１／６でいいアイデアを教えてもらえるかも知れない（携帯型）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00319-01%A1%A7%B9%EF%C0%B8%A1%A6%A3%C6%A1%A6%CD%AA%CC%E9

25-00472-01_かちゅーしゃ：商業の民＋名整備士＋整備の神様＋マッドサイエンティスト＋整備士官＋ＨＱ器用：敏捷+1*幸運+1；
－恩寵の時計：個人所有：外見＋２（着用型／首に着用するもの）
－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／腕に着用するもの）
－遠隔動作補助システム：羅幻王国保有：生身の整備士が行動できない場所や、ＡＲ３以内の距離で整備可能（着用型／頭に着用するもの，腕に着用するもの）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00472-01%A1%A7%A4%AB%A4%C1%A4%E5%A1%BC%A4%B7%A4%E3
EOT
    troop = parse_troop(source)
    
    maintenance = troop.find_evaluator('整備')
    maintenance.should_not be_nil
    ar3 = troop.find_evaluator('遠隔整備（ＡＲ３距離）')
    ar3.should_not be_nil

    ar3.evaluate(troop).should == 29

    ar2 = troop.find_evaluator('遠隔整備（ＡＲ２距離）')
    ar2.should_not be_nil
    ar2.evaluate(troop).should == 30
    
    ar1 = troop.find_evaluator('遠隔整備（ＡＲ１距離）')
    ar1.should_not be_nil
    ar1.evaluate(troop).should == 31
    
    robo_armor = troop.find_evaluator('装甲（小型メカのみ）')
    robo_armor.should_not be_nil
    robo_armor.evaluate(troop).should == 5
    
  end
  
  it '調伏行為のために補正対象を正規表現に対応' do
    troop = parse_troop <<EOT
＃ｌ：＊僧侶の調伏補正　＝　歩兵，条件発動，調伏、評価＋１０。
＃ｌ：＊僧正の調伏補正　＝　歩兵，条件発動，調伏、評価＋１０。
＃ｌ：＊僧侶の調伏行為　＝　歩兵，条件発動，（信仰している神々によっては）調伏行為が可能。＃調伏評価：可能：外見　または　幸運

＃！部分有効条件：信仰している神々によっては

43-00399-01_花井柾之：高位はてない国人+僧侶+僧正+行政官+護民官２：敏捷+1*器用+1*知識+1*幸運+2；
－聖水：個人所有：アンデットや邪悪な魔法を寄せ付けない。この効果は評価３０として扱う。効果は１０分続く（２個所有）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00399-01%A1%A7%B2%D6%B0%E6%CB%EF%C7%B7；
EOT
    turn_undead = troop.find_evaluator('調伏（幸運）')
    turn_undead.should_not be_nil
    
    turn_undead.evaluate(troop).should == 59

    turn_undead2 = troop.find_evaluator('調伏（外見）')
    turn_undead2.should_not be_nil
    turn_undead2.evaluate(troop).should == 60
  end
  
  it 'create_optional な評価子を派生した場合、本来使えない分隊でも出力されてしまうバグ' do
    troop = parse_troop <<EOT
35-00096-01_夜國涼華：森国人＋妖精に愛されしもの＋マジカルポリス＋白魔法使い+ＨＱ知識+護民官２：敏捷+1*知識+1*幸運+1：治安維持+1/*ＨＱ補正*/;
－クローバーのしおり：個人所有：歩兵，，幸運、評価＋１。

35-00681-01_ＹＯＴ：森国人＋妖精に愛されしもの＋マジカルポリス＋白魔法使い+ＨＱ知識＋シュークリームナイト：敏捷+1*知識+1*幸運+5：治安維持+1/*ＨＱ補正*/;
－恩寵の時計：個人所有：歩兵，，外見、評価＋２。首に着用。
－猫と犬の前足が重なった腕輪：個人所有：，条件発動，同調、評価＋３。同能力重複適用不可。腕に着用。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00681-01%A1%A7%A3%D9%A3%CF%A3%D4

35-00680-01_わたどり：森国人＋妖精に愛されしもの＋マジカルポリス＋白魔法使い＋ＨＱ知識＋吏族２：耐久力+2：治安維持+1/*ＨＱ補正*/;
－マジカルステッキ：藩国所有：歩兵武装，条件発動，詠唱、評価＋3(HQ+1×3+3=6)。詠唱の燃料消費-3、片手持ち武器。


11-00248-01_イク：森国人+優しい死神+暗殺者+特務警護官：敏捷+1*器用+1*知識+1*幸運+2*知識+6/*保育園*/;
－アンデットバスター：個人所有：歩兵武装，条件発動，（アンデッドに対する）攻撃、評価＋５。両手持ち武器。
－クローバーのしおり：個人所有：歩兵，，幸運、評価＋１。
－大健康の腕輪：個人所有：歩兵，，耐久力、評価＋３。腕に着用。
－保育園：玄霧藩国所有：施設／藩国所属のアイドレスの知識＋6　＃ＨＱ込み

_define_division:{分隊，11-00248-01_イク｝；

＃！評価派生：山岳、森林での,{詠唱,詠唱による防御（通常）,治療,詠唱での探査,流れを戻す}
＃！有効条件：｛山岳，森林｝での

EOT
    troop.action_evals_str
    div = troop.divisions[0]
    
    deffence_by_spell = div.find_evaluator('（山岳、森林での）詠唱による防御（通常）')
    deffence_by_spell.should be_nil
  end
  
  it 'AbilitiesEvaluator 1' do
    source =  <<EOT
32-00622-01_セントラル越前：東国人＋ハイギーク＋藩王＋ネットワークエンジニア＋星見司２：知識+1*幸運+1; 

32-00635-01_黒埼紘：東国人＋スペーススペルキャスター＋文殊開発者＋ＨＱ知識＋ネットワークエンジニア＋法の司：敏捷+1*知識+4*幸運+1;
－法の執行者の紋章：個人所有：，条件発動，（法執行時での）全判定、評価＋２。手先に着用。 
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00635-01%A1%A7%B9%F5%BA%EB%B9%C9

＃！評価派生：調べ物時，一般評価
＃！部分有効条件：調べ物での

＃！状況：調べてみた
＃！部分有効条件：調べ物での
EOT
    troop = parse_troop(source)

    troop.should_not be_nil
    troop.context.abilities_evaluators.size.should == 1
    
    result1 = troop.evals_str
    result1.should include "体格：筋力：耐久力：外見：敏捷：器用：感覚：知識：幸運\n25：20：29：22：24：44：25：55：24"
    
    result2 = troop.derived_evals_str
    result2.should include <<EOT
（調べ物時）
体格：筋力：耐久力：外見：敏捷：器用：感覚：知識：幸運
37：36：47：39：33：62：29：73：42
EOT

    calc1 = troop.context.default_abilities_evaluator.calculate(troop)
    calc2 = troop.context.abilities_evaluators[0].calculate(troop)
#    puts calc1.class
    (calc1 != calc2).should be_truthy
    
    troop.context.change_situation_to('調べてみた')
    calc1 = troop.context.default_abilities_evaluator.calculate(troop)
    calc2 = troop.context.abilities_evaluators[0].calculate(troop)
#    puts calc1.class
    (calc1 != calc2).should be_falsey
    
    result3 = troop.derived_evals_str
    result3.should_not include <<EOT
（調べ物時）
体格：筋力：耐久力：外見：敏捷：器用：感覚：知識：幸運
EOT
    
    source2 = "ｌ：一般評価派生での差分値表示　←　有効\n" + source
    troop = parse_troop(source2)
    
    result3 = troop.derived_evals_str
    
    result3.should include '(+12：+16：+18：+17：+9：+18：+4：+18：+18)'
  end
  
  it 'AbilitiesEvaluator 2' do
    troop = parse_troop <<EOT
32-00635-01_黒埼紘：東国人＋スペーススペルキャスター＋文殊開発者＋ＨＱ知識＋ネットワークエンジニア＋法の司：敏捷+1*知識+4*幸運+1;
－法の執行者の紋章：個人所有：，条件発動，（法執行時での）全判定、評価＋２。手先に着用。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00635-01%A1%A7%B9%F5%BA%EB%B9%C9

＃！評価派生：調べ物時，一般評価
＃！部分有効条件：調べ物での

＃！状況：調べてみた
＃！部分有効条件：調べ物での
EOT
    result = troop.derived_evals_str
    result.should include "※調べ物時：全評価+18\n"
    
  end
  
  it '一般評価派生と評価無効' do
    troop = parse_troop <<EOT
32-00635-01_黒埼紘：東国人＋スペーススペルキャスター＋文殊開発者＋ＨＱ知識＋ネットワークエンジニア＋法の司：敏捷+1*知識+4*幸運+1;
－法の執行者の紋章：個人所有：，条件発動，（法執行時での）全判定、評価＋２。手先に着用。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00635-01%A1%A7%B9%F5%BA%EB%B9%C9

＃！評価無効：（法執行時）一般評価，【本隊】

＃！評価派生：調べ物時，一般評価
＃！部分有効条件：調べ物での

＃！評価派生：法執行時，一般評価
＃！部分有効条件：法執行時での

＃！状況：調べてみた
＃！部分有効条件：調べ物での
EOT
    result = troop.derived_evals_str
    result.should_not include "※法執行時：全評価"
    puts result
    
  end

  it '遠距離戦と超遠距離戦補正' do
     troop = parse_troop <<EOT
＃！有効条件：砲兵１０人セットの場合
＃！個別付与：砲兵の支援砲撃，全ユニット
＃！個別付与：ＦＯの砲兵支援，全ユニット

15-xxACE-01_守上藤丸２：守上藤丸２＋咆月＋ＨＱ＋ＳＨＱ；
EOT

    long_range = troop.find_evaluator('遠距離戦')
    long_range.should_not be_nil
    long_range.evaluate(troop).should == 57
  end
end
