# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')

describe Soldier do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end

  it '国民番号；名称：職業' do
    source = <<EOT
32-00123-01:soldier:歩兵;
32-00123-02_soldier2:歩兵;
soldier3:歩兵;
EOT

    root = @parser.parse(source)
    root.should_not be_nil

    troop = parse_troop(source)
    soldier = troop.children[0]
    soldier.name.should == 'soldier'
    defined?(soldier.character_no).should_not be_falsey
    soldier.character_no == '32-00123-01'
    oid = soldier.object_id

    @context.units['32-00123-01_soldier'].object_id.should == oid
    @context.units['32-00123-01:soldier'].object_id.should == oid
    @context.units['soldier'].object_id.should == oid

    soldier2 = troop.children[1]
    oid2 = soldier2.object_id

    soldier2.name.should == 'soldier2'
    soldier2.character_no.should == '32-00123-02'

    @context.units['32-00123-02_soldier2'].object_id.should == oid2
    @context.units['32-00123-02:soldier2'].object_id.should == oid2
    @context.units['soldier2'].object_id.should == oid2

    soldier3 = troop.children[2]
    soldier3.name.should == 'soldier3'

    @context.units['soldier3'].object_id.should == soldier3.object_id
  end

  it '(乗員種別)国民番号；名称：職業' do
    source = <<EOT
(P)32-00123-01:soldier:歩兵;
(CP)32-00123-02_soldier2:歩兵;
(OP)soldier3:歩兵;
(-)soldier4:歩兵;

(Ｐ)32-00123-01:pilot1:歩兵;
(ＣＰ)32-00123-02_pilot2:歩兵;
（ＯＰ）pilot3:歩兵;
(-)pilot4:歩兵;
EOT

    root = @parser.parse(source)
    root.should_not be_nil

    character_nodes = root.select_nodes(CharacterDef)

    character_nodes[0].make_tags(character_nodes[0].character_line).should == ['P']
    character_nodes[1].make_tags(character_nodes[1].character_line).should == ['CP']
    character_nodes[2].make_tags(character_nodes[2].character_line).should == ['OP']
    character_nodes[3].make_tags(character_nodes[3].character_line).should ==  ['-']

    troop = parse_troop(source)
    soldier1 = @context.units['soldier']
    soldier1.should_not be_nil
    soldier1.passenger_type.should == :pilot

    soldier2 = @context.units['soldier2']
    soldier2.should_not be_nil
    soldier2.passenger_type.should == :copilot

    soldier3 = @context.units['soldier3']
    soldier3.should_not be_nil
    soldier3.passenger_type.should == :operator

    soldier4 = @context.units['soldier4']
    soldier4.should_not be_nil
    soldier4.passenger_type.should be_nil

    pilots = ['pilot1','pilot2','pilot3','pilot4'].collect{|n| @context.units[n]}
    pilots.each{|pilot|pilot.should_not be_nil}
    pilots.zip([:pilot , :copilot , :operator , nil]){|pilot,type|
      pilot.passenger_type.should == type
    }
  end

  it '猫士が着たWDの食料消費は半分にしない' do
    source = <<EOT
00-xx001-xx_cat1:南国人＋天陽
00-xx002-xx_cat2:南国人＋天陽
EOT

    troop = parse_troop(source)
    troop.event_cost.should == {'食料' => -3.0}
  end

  it 'ACEが着たWDの食料消費はそのまま' do
    source = <<EOT
タフト：ジョージ・タフト＋天陽；
月光：月光ほろほろ＋天陽；
EOT

    troop = parse_troop(source)
    troop.event_cost.should == {'食料' => -2}
  end

  it '行為制約' do
    source = <<EOT
mechanic:北国人＋整備士＋歩兵；
EOT
    troop = parse_troop(source)
    mechanic = troop.children[0]
    mechanic.act_restrictions.should == [] #通常は何も制約を受けない。

    source = <<EOT
mechanic:北国人＋整備士＋歩兵；
－幻燈
EOT
    troop = parse_troop(source)
    mechanic = troop.children[0]
    mechanic.act_restrictions.should == [:'整備行為'] #幻燈の行為制約により

    source = <<EOT
mechanic:北国人＋整備士＋歩兵；
－幻燈

＃！特殊無効：幻燈の行為制約
EOT
    troop = parse_troop(source)
    mechanic = troop.children[0]
    mechanic.act_restrictions.should == [] #行為制約は無効化できる。

  end

  it '0000045: WD着用時は整備できない(敬人除く）' do
    source = <<EOT
mechanic:北国人＋整備士＋歩兵；
－天陽

ｌ：＊敬人の位置づけ　＝　，，植物型ウォードレス。

＃！状況：ＷＤなし
＃！除外：天陽，mechanic

＃！状況：敬人
＃！除外：天陽，mechanic
＃！個別付与：敬人，mechanic
EOT

    troop = parse_troop(source)
    mecha = troop.children[0]
    mecha.is_wardressed?.should be_truthy
    mecha.act_restrictions.should == [:'整備行為']
    
    troop.action_names.should_not include('整備行為')
    troop.tqrs.should_not include('整備行為')

    @context.change_situation_to('ＷＤなし')
    troop.change_situation_to('ＷＤなし')
    troop.children[0].all_idresses.should_not include('天陽')
    puts troop.children[0].placements.inspect

    troop.children[0].is_wardressed?.should be_falsey
    troop.action_names.should include('整備行為')

    @context.change_situation_to('敬人')
    troop.change_situation_to('敬人')
    troop.children[0].is_wardressed?.should be_falsey
    troop.action_names.should include('整備行為')
  end

  it '戦車兵ウォードレス着用時は整備できる' do
    source = <<EOT
mechanic:北国人＋整備士＋歩兵；
－久遠

ｌ：＊久遠の位置づけ　＝　，，｛戦車兵用ウォードレス，兵器｝。

EOT

    troop = parse_troop(source)
    troop.children[0].is_wardressed?.should_not be_truthy
    troop.action_names.should include('整備行為')
    troop.tqrs.should include(:'整備行為')

  end

  it '幻燈着用時は整備できない' do
    source = <<EOT
mechanic:北国人＋整備士＋歩兵；
－幻燈

ｌ：＊幻燈の位置づけ　＝　，，｛戦車兵用ウォードレス，兵器｝。


＃！状況：ＷＤなし
＃！除外：幻燈，mechanic
EOT

    troop = parse_troop(source)
    mecha = troop.children[0]
    mecha.is_wardressed?.should_not be_truthy
    mecha.act_restrictions.should == [:'整備行為']
    troop.action_names.should_not include('整備行為')
    mecha.self_tqrs.should_not include(:'整備行為')

    @context.change_situation_to('ＷＤなし')
    troop.change_situation_to('ＷＤなし')
    troop.children[0].all_idresses.should_not include('幻燈')
    puts troop.children[0].placements.inspect

    troop.children[0].is_wardressed?.should be_falsey
    troop.action_names.should include('整備行為')

  end

  
  it '消費修正無しでの計算時、氷の料理人の料理強化によるイベント参加消費は計算に含めない' do
    source = <<EOT
＃！個別付与：氷の料理人の料理強化，全隊員
soldier:東国人＋歩兵＋氷の料理人；
00-xx000-xx_dog:越前犬＋歩兵＋氷の料理人；
EOT
    troop = parse_troop(source)
    troop.action_evals_str
    
    food_cost = troop.event_cost(false)
    food_cost.should == {'食料'=> -1.5}
    
    food_cost = troop.event_cost(true)
    food_cost.should == {'食料'=> -3.5}

  end
end
