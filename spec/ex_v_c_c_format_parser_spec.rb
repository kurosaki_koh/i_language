# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')

describe EVCCParser do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end

  it "parses character line (with personal_bonuses)" do
    source = "32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官：敏捷+1*知識+4*幸運+1;"
    puts source
    root = @parser.parse(source)
    puts @parser.failure_reason
    root.should_not be_nil

    characters = root.select_nodes(CharacterDef)
    characters.should_not be_nil
    characters.size.should == 1
    ch = characters[0]

    puts ch.name.text_value
    puts ch.idresses.text_value
    ch.p_part.personal_bonuses.bonuses.each{|b| puts b.text_value}

  end

  it "parses character line (full spec)" do
    source = "32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官：敏捷+1*知識+4*幸運+1：情報戦闘+1*全判定+1;"
    puts source
    root = @parser.parse(source)
 #   puts @parser.failure_reason
    root.should_not be_nil

    characters = root.select_nodes(CharacterDef)
    characters.should_not be_nil
    characters.size.should == 1
    ch = characters[0]

    puts ch.name.text_value
    puts ch.idresses.text_value
    ch.p_part.personal_bonuses.bonuses.each{|b| puts [b.bonus_name , b.sign , b.numbers].collect{|c|c.text_value}.inspect}
    puts "/*/"
    ch.e_part.extra_settings.bonuses.each{|b| puts b.text_value}

  end
  it "parses character line (contains null bonus)" do
    source = "32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官：：情報戦闘+1*全判定+1;"
    puts source
    root = @parser.parse(source)
#    puts @parser.failure_reason
    root.should_not be_nil

    characters = root.select_nodes(CharacterDef)
    characters.should_not be_nil
    characters.size.should == 1
    ch = characters[0]

    puts ch.name.text_value
    puts ch.idresses.text_value
    ch.p_part.personal_bonuses.bonuses.each{|b| puts [b.name , b.sign , b.numbers].collect{|c|c.text_value}.inspect}
    puts "/*/"
    ch.e_part.extra_settings.bonuses.each{|b| puts b.text_value}

  end

  it "parses character line (simple)" do
    source = "32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官;"
    puts source
    root = @parser.parse(source)
#    puts @parser.failure_reason
    root.should_not be_nil

    characters = root.select_nodes(CharacterDef)
    characters.should_not be_nil
    characters.size.should == 1
    ch = characters[0]

    puts ch.name.text_value
    puts ch.idresses.text_value
    ch.p_part.text_value.should == ''
    ch.e_part.text_value.should == ''
  end

  it "parses character line (plural)" do
    source = <<EOT
32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官：：情報戦闘+1*全判定+1;
32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官：敏捷+1*知識+4*幸運+1;
EOT
    puts source
    root = @parser.parse(source)
    puts @parser.failure_reason unless root
    root.should_not be_nil


    characters = root.select_nodes(CharacterDef)
    characters.should_not be_nil
    characters.size.should == 2
    ch = characters[0]

    for ch in characters
      puts ch.name.text_value
      puts ch.idresses.text_value
      if ch.p_part.text_value != ''
        ch.p_part.personal_bonuses.bonuses.each{|b| puts [b.bonus_name , b.sign , b.numbers].collect{|c|c.text_value}.inspect}
      end
      puts "/*/"
      if ch.e_part.text_value != ''
        ch.e_part.extra_settings.bonuses.each{|b| puts b.text_value}
      end
      puts "/* */"
    end
  end

  def print_character(ch)
      puts ch.name.text_value
      puts ch.idresses.text_value
      if ch.p_part.text_value != ''
        ch.p_part.personal_bonuses.bonuses.each{|b| puts [b.name , b.sign , b.numbers].collect{|c|c.text_value}.inspect}
      end
      puts "/*/"
      if ch.e_part.text_value != ''
        ch.e_part.extra_settings.bonuses.each{|b| puts b.text_value}
      end
      puts "/* */"
  end

  it "parses I=D" do
    source = <<EOT
_define_I=D{
I=D_name1：ミラーコート,
32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官：：情報戦闘+1*全判定+1
};
EOT
    root = @parser.parse(source)
    puts @parser.failure_reason unless root
    root.should_not be_nil
    vehicles = root.select_nodes(VehicleDef)
    vehicle = vehicles[0]
    vehicle.name.text_value.should == 'I=D_name1'
    vehicle.idress_part.idresses.text_value.should == 'ミラーコート'
    pilots = vehicle.select_nodes(CharacterDef)
    pilots.size.should == 1
    pilots.each{|ch| print_character(ch)}
  end

  it "parses character and I=D" do
    source = <<EOT
32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者;
_define_I=D{
I=D_name1：ミラーコート,
32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官：：情報戦闘+1*全判定+1
};
EOT
    root = @parser.parse(source)
    puts @parser.failure_reason unless root
    root.should_not be_nil
    units = root.to_array
    for unit in units
      puts unit.class
      puts unit.name.text_value
      if unit.class == VehicleDef

         for child in unit.children
          puts '  -' + child.name.text_value
        end
      end
    end
  end

  it "parses character line with terminal comment 1" do
    source = "32-00635-01　黒埼紘：東国人+ギーク+スペース　スペルキャスター+宇宙の賢者+法官：敏捷+1*知識+4*幸運+1：情報戦闘+1*全判定+1;　＃comment"
    puts source
    root = @parser.parse(source)
 #   puts @parser.failure_reason
    root.should_not be_nil

    characters = root.select_nodes(CharacterDef)
    characters.should_not be_nil
    characters.size.should == 1
    ch = characters[0]

    puts ch.name.text_value
    puts ch.idresses.text_value
    ch.p_part.personal_bonuses.bonuses.each{|b| puts [b.bonus_name , b.sign , b.numbers].collect{|c|c.text_value}.inspect}
    puts "/*/"
    ch.e_part.extra_settings.bonuses.each{|b| puts b.text_value}
  end

  it "parses character line with terminal comment 2" do
    source = "＃！有効条件：＜藩王の指揮＞に関する　 ＃藩王の指揮\n"
    puts source
    root = @parser.parse(source)
 #   puts @parser.failure_reason
    root.should_not be_nil

    conditions = root.select_nodes(ConditionDef)
    conditions.should_not be_nil
    conditions.size.should == 1
    condition = conditions[0]

    puts condition.text_value
    puts '##'+condition.content.text_value+'##'
  end

  it "全角数字" do
    source = "32-00635-01　黒埼紘：東国人+ギーク+スペース　スペルキャスター+宇宙の賢者+法官：敏捷+１*知識+４*幸運+１：情報戦闘+２*全判定+２;　＃comment"
    puts source
    root = @parser.parse(source)

    root.should_not be_nil
    characters = root.select_nodes(CharacterDef)
    characters.should_not be_nil
    characters.size.should == 1
    ch = characters[0]

    hash = parse_bonuses(ch.p_part.text_value.strip.gsub(/(：|:)/,''))
    hash.should == {"幸運"=>1, "敏捷"=>1, "知識"=>4}

    hash = parse_bonuses(ch.e_part.text_value.strip.gsub(/(：|:)/,''))
    hash.should == {"全判定"=>2, "情報戦闘"=>2}
  end

  it '編成表個人所有アイテム欄への対応1' do
    source = <<EOT
32-00635-01　黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官：敏捷+１*知識+４*幸運+１：情報戦闘+２*全判定+２;　＃comment
　－高機能ハンドヘルド：なんたらかんたら
－猫と犬の前足が重なった腕輪：
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00635-01%A1%A7%B9%F5%BA%EB%B9

32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官：敏捷+1*知識+4*幸運+1;
EOT
    root = @parser.parse(source)
    root.should_not be_nil
    characters = root.select_nodes(CharacterDef)
    characters.size.should == 2
    ch_node = characters[0]
    ch_node.class.should == CharacterDef
    ch_node.items.size.should == 3
    ch_node.items[0].idresses.text_value.should == '高機能ハンドヘルド'
    ch_node.items[1].idresses.text_value.should == '猫と犬の前足が重なった腕輪'
    ch_node.items[2].idresses.text_value.should == '個人取得ＨＱ根拠ＵＲＬ'

    ch_node.item_names.should == ['高機能ハンドヘルド' , '猫と犬の前足が重なった腕輪']

    character = ch_node.parse(@context)
    character.idresses.size.should == 7
    character.items.size.should == 2
    character.all_idresses.size.should == 7
    character.all_idresses[-2..-1].should == ['高機能ハンドヘルド' , '猫と犬の前足が重なった腕輪']
    puts character.all_idresses
  end

  it '乗り物へのアイテム追加' do
    source = <<EOT
_define_I=D:{
Ｕ４_国有：多国籍要撃機”ユーフォー”,
－増槽：開始ＡＲ＋５、このＡＲを使い切るまで全評価－３
（Ｐ）06-00147-01_霰矢蝶子：愛の民＋整備士２＋魔術的舞踏子＋やわらか舞踏子＋ＳＨＱ知識＋ＨＱ感覚＋霰矢惣一郎の恋人：敏捷+1*器用+1*知識+1*幸運+2*外見+2*幸運+1;
　－恩寵の時計：個人所有：外見＋２（着用型／首）
　－クローバーのしおり：個人所有：幸運＋１
　－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／手首）
　－アンデットバスター：個人所有：対アンデット、攻撃修正＋５（着用型／その他）
　－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00147-01%A1%A7%F0%C7%CC%F0%C4%B3%BB%D2
（ＣＰ）06-00161-01_矢神サク：愛の民＋整備士２＋魔術的舞踏子＋やわらか舞踏子＋ＳＨＱ知識＋ＨＱ感覚＋ヤガミの恋人＋ＨＱ幸運＋ＨＱ幸運＋ＨＱ外見：敏捷＋1*知識＋1*幸運＋2;
};
EOT
    root = @parser.parse(source)
    root.should_not be_nil
    vehicle_node = root.select_nodes(VehicleDef)[0]
    vehicle_node.items.size.should == 1
    vehicle_node.item_names[0].should == '増槽'

    vehicle = vehicle_node.parse(@context)
    vehicle.all_idresses.size.should == 2
  end

  it '編成表個人所有アイテム欄への対応2' do
    #最後に改行が含まれない場合に対応していること。
    source = <<EOT
歩兵：北国人＋帝國軍歩兵＋偵察兵＋整備士;
－保育園の授業：国有：知識、評価＋２。
EOT
    source << '－個人取得ＨＱ根拠ＵＲＬ：xxx'
    root = @parser.parse(source)
#    puts @parser.failure_reason
    root.should_not be_nil
    characters = root.select_nodes(CharacterDef)
    characters.size.should == 1
    ch_node = characters[0]
    ch_node.class.should == CharacterDef
    ch_node.items.size.should == 2
    ch_node.items[0].idresses.text_value.should == '保育園の授業'
    ch_node.items[1].idresses.text_value.should == '個人取得ＨＱ根拠ＵＲＬ'

    item_names = ch_node.item_names
    item_names.size.should == 1

    item_names[0].should== '保育園の授業'

    troop = parse_troop(source)
    troop.children.size.should == 1

  end

  it '部隊補正' do
    source = <<EOT
//部隊補正：チップボールの直接火力支援:体格＋２:中距離戦闘+3
soldier:歩兵;
EOT
    root = @parser.parse(source)
    root.should_not be_nil
    troop_bonus_node = root.select_nodes(TroopBonusDef)[0]
    troop_bonus_node.should_not be_nil

    troop_bonus_node.p_part.text_value == '体格＋２'
    troop_bonus_node.e_part.text_value == '中距離戦闘+3'

    source = <<EOT
#!部隊補正：チップボールの直接火力支援:体格＋２:中距離戦闘+3
soldier:歩兵;
EOT
    root = @parser.parse(source)
    root.should_not be_nil
    troop_bonus_node = root.select_nodes(TroopBonusDef)[0]
    troop_bonus_node.should_not be_nil

    troop_bonus_node.p_part.text_value == '体格＋２'
    troop_bonus_node.e_part.text_value == '中距離戦闘+3'

    source = <<EOT
＃！部隊補正：チップボールの直接火力支援:体格＋２:中距離戦闘+3
soldier:歩兵;
EOT
    root = @parser.parse(source)
    root.should_not be_nil
    troop_bonus_node = root.select_nodes(TroopBonusDef)[0]
    troop_bonus_node.should_not be_nil

    troop_bonus_node.p_part.text_value == '体格＋２'
    troop_bonus_node.e_part.text_value == '中距離戦闘+3'
  end

  it 'コメントのみの入力' do
    source = <<EOT
/* コメント */
EOT
    root = @parser.parse(source)
    root.should_not be_nil

    source = <<EOT
■行コメント
□行コメント
・行コメント
●コメント
○コメント
EOT
    root = @parser.parse(source)
    puts @parser.failure_reason
    root.should_not be_nil

  end

  it '＃！ないし#!はディレクティブとして、行末コメントと見なさずパーザに渡す' do
    source = <<EOT
#!部隊補正：チップボールの直接火力支援:体格＋２:中距離戦闘+3
soldier:歩兵;
EOT
    result = EVCCParser.remove_comment(source)
    result.should match(/^\#\!/)
  end

  it '定義パッチ' do
    source = <<EOT
soldier:歩兵;
－カトラス２

Ｌ：カトラス２　＝　｛
　ｔ：名称　＝　カトラス（アイテム）
　ｔ：要点　＝　長い，曲がった，格好いい
　ｔ：周辺環境　＝　手
　ｔ：評価　＝　なし
　ｔ：特殊　＝　｛
　　＊カトラスのアイテムカテゴリ　＝　，，着用型アイテム。
　　＊カトラスの位置づけ　＝　，，｛武器，兵器｝。
　　＊カトラスの着用箇所　＝　，，片手持ち武器。
　　＊カトラスの形状　＝　，，剣。
　　＊カトラスの白兵距離戦闘行為　＝　歩兵，条件発動，白兵距離戦闘行為が可能。
　　＊カトラスの白兵距離戦闘補正　＝　歩兵，条件発動，（白兵距離での）攻撃、評価＋２。
　｝
　ｔ：→次のアイドレス　＝　シーアックス（アイテム），剣技（技術），海の戦士（職業），装飾カトラス（アイテム）
｝
EOT

    root = @parser.parse(source)
#    puts @parser.failure_reason
    root.should_not be_nil
    arry = root.to_array
    libs = root.libraries
    libs.size.should == 1
    troop = parse_troop(source)

    @context.definitions['カトラス２'].should_not be_nil

    melee = Evaluator['白兵戦']
    melee.evaluate(troop).should == 2
  end

  it '任意能力補正の選択' do
    source = <<EOT
＃！任意補正：学兵の特殊補正,白兵戦
＃！有効条件：順応性があるため
soldier:学兵＋剣士;
EOT
    root = @parser.parse(source)
    root.should_not be_nil
  end

  it '部分有効条件' do
      source = <<EOT
//部分有効条件：射撃戦による
policeman:警官
EOT
    root = @parser.parse(source)
    puts @parser.failure_reason if root.nil?
    root.should_not be_nil
  end

  it '特殊無効' do
    troop = parse_troop <<EOT
＃！特殊無効：ＦＯの偵察補正
fo:ＦＯ;
EOT
    scout = Evaluator['偵察']
    scout.evaluate(troop).should == 4

    troop = parse_troop <<EOT
＃！部分有効条件：３人セット
＃！特殊無効：ＦＯの偵察補正，fo2
fo1:ＦＯ;
fo2:ＦＯ;
EOT

    @context.disabled_sps.size.should == 0
    @context.units['fo1'].disabled_sps.size == 0
    @context.units['fo2'].disabled_sps.size == 1
    scout.evaluate(troop).should == 7

    troop = parse_troop <<EOT
＃！特殊無効：ＦＯの偵察補正，｛fo1，fo2｝
fo1:ＦＯ;
fo2:ＦＯ;
EOT
    scout.evaluate(troop).should == 7
    @context.units['fo1'].disabled_sps.size == 1
    @context.units['fo2'].disabled_sps.size == 1

    troop = parse_troop <<EOT
＃！特殊無効：ＦＯの偵察補正，｛ fo1 ， fo2 ｝
fo1:ＦＯ;
fo2:ＦＯ;
EOT
    scout.evaluate(troop).should == 7
    @context.units['fo1'].disabled_sps.size == 1
    @context.units['fo2'].disabled_sps.size == 1

    troop = parse_troop <<EOT
＃！特殊無効：ＦＯの偵察補正，｛
 fo1 ,
 fo2
｝
fo1:ＦＯ;
fo2:ＦＯ;
EOT
    scout.evaluate(troop).should == 7
    @context.units['fo1'].disabled_sps.size == 1
    @context.units['fo2'].disabled_sps.size == 1

    troop = parse_troop <<EOT
＃！特殊無効：ＦＯの偵察補正，｛
 fo1 ,
 fo2 ，
｝
fo1:ＦＯ;
fo2:ＦＯ;
EOT
    scout.evaluate(troop).should == 7
    @context.units['fo1'].disabled_sps.size == 1
    @context.units['fo2'].disabled_sps.size == 1
  end


  it '分隊' do
    source =  <<EOT
hacker1:東国人＋ハッカー＋ギーク＋ネットワークエンジニア;
hacker2:東国人＋ハッカー＋ギーク＋ネットワークエンジニア;
engineer：東国人＋ハッカー＋ギーク＋ネットワークエンジニア＋宇宙の賢者+法官：敏捷+1*知識+4*幸運+1;

_define_division:{
hacker_div,
hacker1 ,
hacker2 ;
};

_define_division:｛
kurosaki:大型ＰＣ:筋力+1*体格+1：情報戦闘+2,
－電子妖精軍
engineer
｝;
EOT
    root = @parser.parse(source)
    root.should_not be_nil

    divisions = root.select_nodes(DivisionDef)
    divisions.size.should == 2
    divisions[0].name.text_value.should == 'hacker_div'
    divisions[0].member_names.should == ['hacker1','hacker2']

    divisions[1].name.text_value.should == 'kurosaki'
    divisions[1].member_names.should == ['engineer']
    divisions[1].idress_part.idresses.text_value.should == '大型ＰＣ'
    divisions[1].items.size.should == 1
    divisions[1].items[0].text_value.should == "－電子妖精軍\n"
    divisions[1].p_part.text_value.should == ':筋力+1*体格+1'
    divisions[1].e_part.text_value.should == '：情報戦闘+2'
  end

  it '＃！状況' do
    source = <<EOT
＃！有効条件：位置づけ（情報系）の職業による
hacker:ハッカー＋高機能ハンドヘルド;

＃！状況：第５世界でマジックアイテム無効
＃！特殊無効：高機能ハンドヘルドの情報戦闘補正
＃！特殊無効：高機能ハンドヘルドの情報系職業補正
EOT

    root = @parser.parse(source)
    root.should_not be_nil

    situations = root.select_nodes(SituationDef)
    situations.size.should == 1
    situations[0].name.should == '第５世界でマジックアイテム無効'
  end

  it 'ユニット指定つき有効条件' do
    source = <<EOT
＃！有効条件：位置づけ（情報系）の職業による， hacker
＃！有効条件：｛年齢のことを言われる，結婚が絡む｝場合， ｛hacker ，  maid ｝
hacker:ハッカー＋ハイパーメードお局さん＋高機能ハンドヘルド;
maid:ハイパーメードお局さん;
EOT

    root = @parser.parse(source)
    if root.nil?
      puts @parser.failure_reason
    end
    root.should_not be_nil

    conditions = root.select_nodes(ConditionDef)
    conditions.size.should == 2
    conditions[0].content.text_value.should == '位置づけ（情報系）の職業による'
    conditions[1].content.text_value.should == '｛年齢のことを言われる，結婚が絡む｝場合'
    targets = conditions[0].targetable.select_nodes(LiteralsDef)
    targets.size.should == 1
    targets[0].text_value.should == 'hacker'
    targets = conditions[1].targetable.select_nodes(LiteralsDef)
    targets.size.should == 2
    targets[0].text_value.should == 'hacker'
    targets[1].text_value.should == 'maid'

    troop = parse_troop(source)
    hacker = @context.units['hacker']
    hacker.conditions.include?('（位置づけ（情報系）の職業による）').should be_truthy
    hacker.conditions.include?('（｛年齢のことを言われる，結婚が絡む｝場合）').should be_truthy

    maid = @context.units['maid']
    maid.conditions.include?('（位置づけ（情報系）の職業による）').should be_falsey
    maid.conditions.include?('（｛年齢のことを言われる，結婚が絡む｝場合）').should be_truthy
  end

  it 'ユニット指定つき部分有効条件' do
    source = <<EOT
＃！部分有効条件：位置づけ（情報系）の職業による， hacker
＃！部分有効条件：｛年齢のことを言われる，結婚が絡む｝場合， ｛hacker ，  maid ｝
hacker:ハッカー＋ハイパーメードお局さん＋高機能ハンドヘルド;
maid:ハイパーメードお局さん;
EOT

    root = @parser.parse(source)
    if root.nil?
      puts @parser.failure_reason
    end
    root.should_not be_nil

    pconditions = root.select_nodes(PartialConditionDef)
    pconditions.size.should == 2
    pconditions[0].content.text_value.should == '位置づけ（情報系）の職業による'
    pconditions[1].content.text_value.should == '｛年齢のことを言われる，結婚が絡む｝場合'
    targets = pconditions[0].targetable.select_nodes(LiteralsDef)
    targets.size.should == 1
    targets[0].text_value.should == 'hacker'
    targets = pconditions[1].targetable.select_nodes(LiteralsDef)
    targets.size.should == 2
    targets[0].text_value.should == 'hacker'
    targets[1].text_value.should == 'maid'

    troop = parse_troop(source)
    hacker = @context.units['hacker']
    hacker.partial_conditions.include?(/位置づけ（情報系）の職業による/).should be_truthy
    hacker.partial_conditions.include?(/｛年齢のことを言われる，結婚が絡む｝場合/).should be_truthy

    maid = @context.units['maid']
    maid.partial_conditions.include?(/位置づけ（情報系）の職業による/).should be_falsey
    maid.partial_conditions.include?(/｛年齢のことを言われる，結婚が絡む｝場合/).should be_truthy

  end

  it '個別付与' do
    source = <<EOT
＃！個別付与：大型ＰＣ，32-00635-01_黒埼紘
32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官：敏捷+1*知識+4*幸運+1：情報戦+3;
32-00645-02_栢橋　柚樹：東国人+ハッカー＋ギーク＋理力使い；
EOT
    root = @parser.parse(source)
    root.should_not be_nil
    iad = root.select_nodes(IdressAppendantDef)[0]
    iad.idress_part.text_value.should == '大型ＰＣ'
    iad.targetable.target.text_value.should == '32-00635-01_黒埼紘'

    troop = parse_troop(source)
    kurosaki = @context.units['32-00635-01_黒埼紘']
    kurosaki.all_idresses.size.should == 6
    kurosaki.all_idresses.include?('大型ＰＣ').should be_truthy
#    kurosaki.optional_bonuses['情報戦'].should == 6

    yuzuki =  @context.units['32-00645-02_栢橋　柚樹']
    yuzuki.all_idresses.include?('大型ＰＣ').should be_falsey

    source2 = <<EOT
＃！個別付与：大型ＰＣ＋電子妖精軍，｛32-00635-01_黒埼紘 ， hacker｝
32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官：敏捷+1*知識+4*幸運+1：情報戦+3;
32-00645-02_栢橋　柚樹：東国人+ハッカー＋ギーク＋理力使い；
hacker：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官;
EOT
    troop = parse_troop(source2)

    units = ['32-00635-01_黒埼紘','hacker'].collect{|name|@context.units[name]}
    for unit in units
      unit.all_idresses.size.should == 7
      unit.all_idresses.include?('大型ＰＣ').should be_truthy
      unit.all_idresses.include?('電子妖精軍').should be_truthy
    end

    yuzuki =  @context.units['32-00645-02_栢橋　柚樹']
    yuzuki.all_idresses.include?('大型ＰＣ').should be_falsey

    source3 = <<EOT
＃！個別付与：電子妖精軍，部隊全員
＃！補正ＨＱ：電子妖精軍の情報戦闘補正，３
hacker：ハッカー;
engineer：ネットワークエンジニア；
EOT
    troop = parse_troop(source3)
    units = ['hacker','engineer'].collect{|name|@context.units[name]}
    for unit in units
      unit.all_idresses.size.should == 2
      unit.all_idresses.include?('電子妖精軍').should be_truthy
    end
  end

  it '補正ＨＱ' do
    source = <<EOT
＃！個別付与：電子妖精軍，hacker
＃！補正ＨＱ：電子妖精軍の情報戦闘補正，３
hacker：ハッカー;
engineer：ネットワークエンジニア；
EOT
    root = @parser.parse(source)
    puts @parser.failure_reason if root.nil?
    root.should_not be_nil
    hq_node = root.select_nodes(BonusHQDef)[0]
    hq_node.should_not be_nil

    hq_node.sp_name.text_value.should == '電子妖精軍の情報戦闘補正'
    hq_node.hq.text_value.should == '３'
    hq_node.hq_value.should == 3

    troop = parse_troop(source)
    hacking = Evaluator['情報戦']
    hacker = troop.children[0]
    hacking.evaluate(hacker).should == 28
  end

  it '除外' do
    source = <<EOT
＃！個別付与：藩王の指揮，全ユニット
＃！除外：藩王の指揮，king
king：藩王;
soldier：歩兵；
soldier2:歩兵;
EOT
    root = @parser.parse(source)
    puts @parser.failure_reason if root.nil?
    root.should_not be_nil

    exception_node = root.select_nodes(ExceptionDef)[0]
    exception_node.should_not be_nil

    exception_node.idress_part.text_value.should == '藩王の指揮'
    exception_node.targetable.target.text_value.should == 'king'

    troop = parse_troop(source)
    king = @context.units['king']
    soldier = @context.units['soldier']
    king.all_idresses.size.should == 1
    king.all_idresses.include?('藩王の指揮').should be_falsy

    soldier.all_idresses.size.should == 2
    soldier.all_idresses.include?('藩王の指揮').should be_truthy

    source2 = <<EOT
＃！個別付与：藩王の指揮，全隊員
＃！除外：藩王の指揮，｛king,soldier2｝
king：藩王;
soldier：歩兵；
soldier2:歩兵;
EOT
    troop = parse_troop(source2)
    @context.units['king'].all_idresses.size.should == 1
    @context.units['soldier'].all_idresses.size.should == 2
    @context.units['soldier'].all_idresses.include?('藩王の指揮').should be_truthy
    @context.units['soldier2'].all_idresses.size.should == 1

  end

  it 'コンパイル警告' do
    source = <<EOT
＃！個別付与：藩王の指揮，部隊全員
＃！除外：藩王の指揮，king
king：藩王;
soldier：歩兵；
soldier2:歩兵;
EOT
    root = @parser.parse(source)
    puts @parser.failure_reason if root.nil?
    root.should_not be_nil

    warnings = root.check_warning()
    warnings.size.should == 1
    warnings[0].should =~ /部隊全員/
  end

  it '全隊員・全ユニット' do
    source = <<EOT
＃！個別付与：保育園の授業，全隊員
＃！個別付与：藩王の指揮，全ユニット
＃！有効条件：＜藩王の指揮＞に関する

king：藩王;
soldier：歩兵；
soldier2:歩兵;

_define_I=D:{ダンボー：ダンボール２,
pilot1:歩兵＋パイロット;
pilot2:歩兵＋パイロット;
pilot3:歩兵＋パイロット;
};
EOT

    troop = parse_troop(source)
    troop.children.all?{|c|c.all_idresses.include?('藩王の指揮')}.should be_truthy

    ['king','soldier','soldier2'].each{|n|@context.units[n].all_idresses.include?('保育園の授業').should be_truthy}
    danboh = @context.units['ダンボー']
    danboh.all_idresses.include?('保育園の授業').should be_falsey
    spec = danboh.find_spec('藩王の指揮の判定補正')
    spec.is_target?(danboh).should == true
    danboh.evaluate('体格').should == 20
    ['pilot1','pilot2','pilot3'].each{|n|@context.units[n].all_idresses.include?('保育園の授業').should be_truthy}

  end

  it "_define_vessel" do
    source = <<EOT
_define_vessel:{艦船：ムーン級パトロールフリゲート,
船員：船乗り；
船員：船乗り；
};
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    troop.children[0].class.should == Vessel
  end

  it "_define_career" do
    source = <<EOT
_define_career:{輸送車：兵員輸送車,
兵員１：北国人＋帝國軍歩兵；
兵員２：北国人＋帝國軍歩兵；
};
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    troop.children[0].class.should == Career
  end


  it "＃！評価派生" do
    source = <<EOT
＃！部分有効条件：盾によって
_define_I=D:{チップ：チップボール,
soldier:歩兵;
};

_define_division:{チップ分隊降車,soldier;};

＃！評価派生：チップ盾無し，装甲（通常）
＃！特殊無効：チップボールの防御補正
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    @context.situation_names.include?('チップ盾無し').should_not == true
    evs = troop.evaluators
    armor_original = Evaluator['装甲（通常）']
    armor_clone = @context.particular_evaluators[0]
    armor_clone.should_not be_nil
    @context.particular_evaluators.size.should == 1
    armor_clone.object_id.should_not == armor_original.object_id

    armors = evs.select{|e|e.name =~ /装甲（通常）/}
    armors.size.should == 2
    armors[1].name.should == "（チップ盾無し）装甲（通常）"

    disableds = @context.disabled_sps
    disableds.size.should == 0
    chip = @context.units['チップ']
    disableds = chip.all_disabled_sps
    disableds.size.should == 0
    sp = chip.find_spec('チップボールの防御補正')
    sp.should_not be_nil
    conds0 = @context.all_conditions
    conds = troop.all_conditions
    sp.is_available?().should be_falsy
    sp.is_available?([/装甲での/o]).should == true
    armor_clone.evaluate(troop).should ==17
    armor_original.evaluate(troop).should ==25

    div1 = troop.divisions[0]
    div1.should_not be_nil
    div1.evaluators.find{|item|item.object_id == armor_clone.object_id}
  end

  it '＃！評価派生：（異常系）' do
    source = <<EOT
＃！部分有効条件：盾によって
_define_I=D:{チップ：チップボール,
soldier:歩兵;
};

_define_division:{チップ分隊降車,soldier;};

＃！評価派生：チップ盾無し，でたらめ
＃！特殊無効：チップボールの防御補正
EOT
    troop = nil
    proc{ troop = parse_troop(source) }.should raise_error(RuntimeError , /評価名.+?が存在しません。/)

    source = <<EOT
＃！部分有効条件：盾によって
_define_I=D:{チップ：チップボール,
soldier:歩兵;
};

_define_division:{チップ分隊降車,soldier;};

＃！評価派生：チップ盾無し，装甲
＃！特殊無効：チップボールの防御補正
EOT
    proc{ troop = parse_troop(source) }.should_not raise_error
    troop.should_not be_nil
    names = troop.evaluators.collect{|ev|ev.name}
    puts names
    troop.evaluators.collect{|ev|ev.name}.size.should == 25
  end

  it '＃！評価派生：配列' do
    source =<<EOT
29-00550-01_瑠璃:はてない国人＋黒騎士＋赤髪の乗り手＋警官：敏捷+1*知識+1

＃！評価派生：射撃無効，｛白兵戦，近距離戦｝
＃！特殊無効：警官の白兵距離戦闘補正
EOT

    root = @parser.parse(source)
    #puts @parser.failure_reason
    root.should_not be_nil
    nodes = root.select_nodes(ParticularSituationDef)

    nodes.size.should == 1
    nodes[0].evaluation_names.should == ['白兵戦','近距離戦']
    troop = parse_troop(source)

    names = troop.evaluators.collect{|ev|ev.name}
    names.size.should == 24

  end

  it 'アイテム欄に複数のアイドレスを列挙できる' do
    source = <<EOT
soldier：歩兵；
－保育園の授業＋ＨＱ知識
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
    troop.evaluate('知識').should == 4
  end

  it 'Targetable' do
    source = <<EOT
＃！補正ＨＱ：電子妖精軍の情報戦闘補正，３
＃！個別付与：電子妖精軍，全ユニット

ハッカー１：ハッカー；
ハッカー２：ハッカー；

＃！有効条件：てきとー，全ユニット
＃！特殊無効：電子妖精軍の情報戦闘補正，全隊員
＃！特殊無効：ハッカーの知識・器用補正，ハッカー１
＃！部分有効条件：部分的にてきとー，全ユニット
EOT

    root = @parser.parse(source)
    desable_nodes = root.select_nodes(DisableDef)
    desable_nodes.size.should == 2

    troop = parse_troop(source)
    hacker1 = @context.units['ハッカー１']
    hacker1.all_disabled_sps.include?('電子妖精軍の情報戦闘補正').should == true
    hacker1.disabled_sps.include?('ハッカーの知識・器用補正').should == true
    troop.disabled_sps.include?('ハッカーの知識・器用補正').should_not == true
    spec = hacker1.judge_bonuses.find{|sp|sp.name == '電子妖精軍の情報戦闘補正'}
    spec.nil?.should == true

    hacker1.conditions.include?('てきとー')
    hacker1.partial_conditions.include?('部分的にてきとー')
  end

  it '＃！任意補正の評価子クラス対応' do
    source = <<EOT
学生:学兵＋生徒会役員;
学生:学兵＋生徒会役員;

＃！状況：白兵重視
＃！任意補正：生徒会役員の適応力補正,体格
＃！部分有効条件：戦闘において
＃！部分有効条件：ＡＲ７以下の場合
＃！任意補正：学兵の特殊補正,白兵距離戦闘行為
＃！有効条件：順応性があるため

＃！状況：装甲重視
＃！任意補正：生徒会役員の適応力補正,装甲
＃！部分有効条件：戦闘において
＃！部分有効条件：ＡＲ７以下の場合
＃！任意補正：学兵の特殊補正,装甲
＃！有効条件：順応性があるため
EOT

    troop = parse_troop(source)
    armor = Evaluator['装甲（通常）']

 #   armor.evaluate(troop).should == 7

    @context.change_situation_to('装甲重視')
    armor.evaluate(troop).should == 10
  end
  it '定義パッチの異常系' do
    source = <<EOT
soldier:歩兵;
－カトラス

Ｌ：カトラス　＝　｛
　ｔ：特殊　＝　｛
　　＊カトラスのアイテムカテゴリ　＝　，，着用型アイテム。
　　＊カトラスの位置づけ　＝　，，｛武器，兵器｝。
　　＊カトラスの着用箇所　＝　，，片手持ち武器。
　　＊カトラスの形状　＝　，，剣。
　　＊カトラスの白兵距離戦闘行為　＝　歩兵，条件発動，白兵距離戦闘行為が可能。
　　＊カトラスの白兵距離戦闘補正　＝　歩兵，条件発動，（白兵距離での）攻撃、評価＋２。
　｝
　ｔ：→次のアイドレス　＝　シーアックス（アイテム），剣技（技術），海の戦士（職業），装飾カトラス（アイテム）
｝

EOT
    proc{troop = parse_troop(source)}.should raise_error(RuntimeError , /’ｔ：名称’が解釈できません。/)

    source = <<EOT
soldier:歩兵;
－カトラス

Ｌ：カトラス　＝　｛
　ｔ：名称　＝　カトラス（アイテム）
｝

EOT
    proc{troop = parse_troop(source)}.should raise_error(RuntimeError , /定義パッチ内のｔ：特殊を解析できません。/o)

    source = <<EOT
soldier:歩兵;
－カトラス

Ｌ：カトラス　＝　｛
　ｔ：名称　＝　カトラス（アイテム）
　ｔ：特殊　＝　｛
ひ・み・つ
　｝
｝

EOT
#    proc{troop = parse_troop(source)}.should raise_error(RuntimeError , /定義パッチ内のｔ：特殊が正しく記述されていません。/)
    troop = parse_troop(source)
    troop.should_not == true
  end
  it '＃！評価無効' do
    source = <<EOT
＃！評価無効：追跡（幸運）
soldier:歩兵；
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    @context.all_containments.include?('追跡（幸運）').should == true
    troop.all_containments.include?('追跡（幸運）').should == true
    result = troop.action_evals_str
    puts result
    (result =~ /追跡（幸運）/).should be_nil
  end
  
  it '分隊名とユニット名に重複があった場合、例外を出して処理しない。' do
    source =<<EOT
soldier1:歩兵；
soldier2:歩兵；
_define_division:{soldier1,
soldier
};
EOT
    proc{troop = parse_troop(source)}.should raise_error(StandardError,/分隊と個々のユニットに同じ名前を付ける事はできません。（/)

  end
  it '評価子の上書き' do
    source = <<EOT
Ｌ：評価定義（歌唱）　＝　｛
　ｌ：引数　＝　｛外見，知識｝
　ｌ：補正対象　＝　魅力
　ｌ：補正使用条件　＝　音楽に関わる
　ｌ：行為分類　＝　歌唱行為
｝

singer:詩歌の民＋吟遊詩人＋楽器；
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    singing = Evaluator['歌唱']
    singing2 = @context.evaluators['歌唱']

    singing.object_id.should_not == singing2.object_id
    singing2.evaluate(troop).should == 14
  end
  
  it 'ｌ：評価除外条件' do
    source = <<EOT
Ｌ：評価定義（治療（ヘンじゃない））　＝　｛
　ｌ：引数　＝　｛器用，知識｝
　ｌ：補正対象　＝　治療
　ｌ：補正使用条件　＝　｛医学，奇跡｝
　ｌ：評価分類　＝　ヘンな治療
　ｌ：評価除外条件　＝　蛇神の僧侶
｝

snake:詩歌の民＋蛇神の僧侶；
priest:詩歌の民＋僧侶；
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    ev = troop.find_evaluator('治療（ヘンじゃない）')
    ev.should_not be_nil

    ev.evaluate(troop).should == 11
  end

  it '＃！状況：【分隊名】' do
    source = <<EOT
＃！個別付与：天陽，全隊員

＃！状況：通常
＃！個別付与：高機能ハンドヘルド，全隊員

＃！状況：本隊
＃！個別付与：カトラス,soldier

＃！状況：【分隊１】
＃！除外：天陽，soldier

＃！状況：【分隊２】
＃！有効条件：テスト，soldier2
＃！有効条件：テスト２
＃！部分有効条件：テスト３
＃！特殊無効：テスト４


soldier:歩兵;
soldier2:歩兵；

_define_division:｛【分隊１】,
soldier
};

_define_division:{【分隊２】,
soldier2
};

EOT
    troop = parse_troop(source)
#    @context.situations.size.should == 5
    @context.situation_names.should_not include('本隊')
    troop.situation_names.should_not include('本隊')
#    troop.situations.size.should == 3
#    troop.situation_stack.size.should == 1
    troop.current_situation_name.should == '（基本）'
    soldier = troop.children[0]
#    soldier.situations.size.should == 5
#    soldier.situation_names.should include('本隊')
    soldier.current_situation_name.should == '（基本）'
    soldier.all_idresses.should include('カトラス')

    div1 = @context.units['【分隊１】']
    soldier1_0 = div1.children[0]
    soldier.object_id.should_not == soldier1_0.object_id
    soldier1_0.situation_stack.should == ['【分隊１】','（基本）']
    soldier1_0.all_idresses.should_not include('天陽')
    soldier1_0.all_idresses.should_not include('カトラス')

    div1 = @context.units['【分隊２】']
    soldier = div1.children[0]
    soldier.situations.size.should == 3
    soldier.all_conditions.should include('（テスト）')
    result = div1.all_conditions
    div1.all_conditions.should include('（テスト２）')
    soldier.all_conditions.should include('（テスト２）')

    @context.change_situation_to('通常')
    soldier = troop.children[0]
#    soldier.situations.size.should == 5
#    soldier.situation_names.should include('本隊')
#    soldier.current_situation_name.should == '通常'
    soldier.all_idresses.should include('カトラス')
    soldier.all_idresses.should include('高機能ハンドヘルド')
  end

  it '＃！状況：【本隊】' do
    source = <<EOT
＃！状況：【本隊】
＃！個別付与：カトラス,soldier

soldier:歩兵；
EOT
    troop = parse_troop(source)
    soldier = troop.children[0]
    soldier.all_idresses.should include('カトラス')
  end

  it '特殊定義パッチ' do
    source = <<EOT
ｌ：＊剣士の白兵距離戦闘補正　＝　，条件発動，（白兵（剣）、白兵距離での）攻撃、評価＋５、燃料－１万ｔ。
ｌ：＊剣士の耐久力補正　＝　，条件発動，耐久力、評価＋３。

soldier:剣士；
EOT

    root = @parser.parse(source)
    #puts @parser.failure_reason
    root.should_not be_nil

    spdefs = root.specialties
    spdefs.size.should == 2

    spdef = spdefs[0]
    spdef.text_value.should == 'ｌ：＊剣士の白兵距離戦闘補正　＝　，条件発動，（白兵（剣）、白兵距離での）攻撃、評価＋５、燃料－１万ｔ。'

    spdef.sp_name.text_value .should == '＊剣士の白兵距離戦闘補正'

    spdef.library_name.should == '剣士'
    defs = @context.definitions['剣士']

    spdef_hash = @context.parse_specialty_patch(spdef)
    troop = parse_troop(source)
    ev = troop.find_evaluator('白兵戦')
    ev.evaluate(troop).should == 6
    troop.evaluate('耐久力').should == 4

    source = <<EOT
ｌ：＊謎の白兵距離戦闘補正　＝　，条件発動，（白兵（剣）、白兵距離での）攻撃、評価＋５、燃料－１万ｔ。

soldier:剣士；
EOT
    proc{troop = parse_troop(source)}.should raise_error(RuntimeError , "ライブラリ定義 '謎' が存在しません。")

    source = <<EOT
ｌ：＊剣士のよくわからない特殊　＝　，，なんだかよくわからない。

soldier:剣士；
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
  end
  it '＃！消費削減ＨＱ' do
    source = <<EOT
＃！個別付与：資源低減技術の開発＋資源削減計画，本隊
＃！消費削減ＨＱ：資源削減計画の資源消費削減，７５％＊７５％ ＃リワマヒ国ＨＱ１適用
＃！消費削減ＨＱ：資源低減技術の開発の資源消費削減，＊９０％＊９０％ ＃リワマヒ国ＨＱ１*ＨＱ１適用

_define_I=D: {Ｐ１_騎士団所有：多国籍戦術戦闘機”ペヤーン”,
－多国籍戦術戦闘機”ペヤーン”の増槽：ＡＲ＋５、全評価－３（追加された５ＡＲを使い切るまで）　この装備は重複装備できない。
（Ｐ）18-00345-01_竜宮・司・ヒメリアス・ドラグゥーン：詩歌の民+歩兵+パイロット+整備士官：敏捷+1*知識+6*幸運+1*外見+2,
（ＣＰ）29-xx001-xx_犬士０１：西国人+スターファイター+スターライナー+スターリフター+ＳＨＱ感覚
}；

_define_I=D:{
ＲＴＲ１_星鋼京所有：ＲＴＲ＋ＳＨＱ敏捷,
－増加燃料装備：藩国所有：装甲+6*ＡＲ+3*全評価-3　増加分ＡＲ３使用後、全評価+3*装甲-6
（P）42-00612-01_御鷹：北国人＋撃墜王＋トップガン＋教導パイロット＋法の司＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*感覚+5*知識+1*幸運+1;
－法の執行者の紋章：個人所有：
（CP）42-00530-02_木曽池春海：北国人＋撃墜王＋トップガン＋教導パイロット＋護民官＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：器用+1*感覚+2*知識+1*幸運+1;
－互尊Ｃ：個人所有：
（CP）42-00610-01_センハ：北国人＋撃墜王＋トップガン＋教導パイロット＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚＋補給士官：敏捷+1*幸運+1;};

EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    troop.action_evals_str

    expected = 5 * floor2(0.75*0.75) * floor2(0.9*0.9)
    sp1 = troop.find_spec('資源削減計画の資源消費削減')
    sp1.should_not be_nil
    sp1.class.should == ResourceModificationSpec
    sp1.coefficient.should == floor2(0.75 * 0.75)

    sp2 = troop.find_spec('資源低減技術の開発の資源消費削減')
    sp2.should_not be_nil
    sp2.class.should == ResourceModificationSpec
    sp2.coefficient.should == floor2(0.9 * 0.9 * 0.9)

    troop.organization_cost('資源').should == expected.floor

  end

  it '＃！消費削減ＨＱの異常系' do
    source = <<EOT
＃！消費削減ＨＱ：でたらめ，７５％＊７５％ ＃リワマヒ国ＨＱ１適用
EOT
    proc{@context.parse_troop(source)}.should raise_error(RuntimeError , "消費削減ＨＱで指定された特殊名称\"でたらめ\"は存在しません。")

    source = <<EOT
＃！消費削減ＨＱ：資源削減計画の食料消費削減，７５％＊７５％ ＃リワマヒ国ＨＱ１適用
EOT
    proc{@context.parse_troop(source)}.should raise_error(RuntimeError , "消費削減ＨＱで指定された特殊名称\"資源削減計画の食料消費削減\"は存在しません。")

    source = <<EOT
＃！消費削減ＨＱ：資源削減計画のイベントカテゴリ，７５％＊７５％ ＃リワマヒ国ＨＱ１適用
EOT
    proc{@context.parse_troop(source)}.should raise_error(RuntimeError , '指定された特殊名称"資源削減計画のイベントカテゴリ"は消費修正ではありません。')

    source = <<EOT
＃！消費削減ＨＱ：資源削減計画のイベントカテゴリ，"test" ＃リワマヒ国ＨＱ１適用
EOT
     proc{
       @context.parse_troop(source)
     }.should raise_error(RuntimeError , "＃！消費削減ＨＱにおいて正しい数式が指定されていません。（＃！消費削減ＨＱ：資源削減計画のイベントカテゴリ，\"test\"）")

    source = <<EOT
＃！消費削減ＨＱ：資源削減計画のイベントカテゴリ，2+"test" ＃リワマヒ国ＨＱ１適用
EOT
     proc{@context.parse_troop(source)}.should raise_error(RuntimeError)
  end


  describe 'resourcify' do
    before :each do
      @source = <<EOT
32-00635-01：hacker:ハッカー；

Ｌ：コマンドリソース化（hacker）　＝　｛
　ｆ：＊ハッカーのナショナルネット接続能力　＝　ハッカーのナショナルネット接続能力：６：＃情報戦
｝

EOT
      @root = @parser.parse(@source)
    end

    it 'コンパイルが通る' do
      expect(@root).to_not be_nil
    end

    it 'resourcify_specialty ノードを得られる' do
      expect(@root.resourcifies.size).to eq 1
      expect(@root.resourcifies[0].name.text_value).to eq 'hacker'
      expect(@root.resourcifies[0].resourcify_definitions.size).to eq 1
    end

    it 'resourcify_definition ノードを得られる' do
      expect(@root.resourcifies[0].resourcify_definitions.size).to eq 1
      cr_def = @root.resourcifies[0].resourcify_definitions[0]

      expect(cr_def.sp_name).to eq 'ハッカーのナショナルネット接続能力'

      expect(cr_def.cr_name).to eq 'ハッカーのナショナルネット接続能力'
      expect(cr_def.cr_power).to eq 6
      expect(cr_def.cr_tag).to eq '情報戦'

     expect(cr_def.command_resource_definition).to eq({
       name: 'ハッカーのナショナルネット接続能力'  ,
       power: 6 ,
       tag: '情報戦'
     } )

    end

  end
end

