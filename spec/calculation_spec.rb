# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')

describe Calculation do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end

  it "＃！状況" do
    source = <<EOT
soldier:歩兵；
EOT
    troop = parse_troop(source)
    m_range = Evaluator['中距離戦']

    soldier = @context.units['soldier']
    calc = m_range.choice_alternative(soldier)
    calc.should_not be_nil
    calc.total.should == 2

    tcalc = m_range.choice_alternative(troop)
    tcalc.should_not be_nil
    tcalc.total.should ==2

    #２度目以降のcalculateの戻り値は、キャッシュされた値を返す。
    calc2 = m_range.choice_alternative(soldier)
    calc2.object_id.should == calc.object_id

    tcalc2 = m_range.choice_alternative(troop)
    tcalc2.object_id.should == tcalc.object_id

  end

  it '0000026: ”同能力重複不可”への対応。' do
    source = <<EOT
soldier:歩兵；
－猫と犬の前足が重なった腕輪
－猫と犬の前足が重なった腕輪
soldier2:歩兵；
EOT
    troop = parse_troop(source)

    synchro = Evaluator['同調']
    calc = synchro.calculate(troop)
    calc.children[0].arguments['外見'].operands.size.should == 2
    calc.children[0].arguments['外見'].operands_no_dup.size.should == 1
    calc.total.should == 3

    source = <<EOT
soldier:歩兵；
－猫と犬の前足が重なった腕輪
soldier2:歩兵；
EOT
    troop = parse_troop(source)

    synchro = Evaluator['同調']
    calc = synchro.calculate(troop)
    calc.children[0].arguments['外見'].operands.size.should == 1
    calc.children[0].arguments['外見'].operands_no_dup.size.should == 1
    calc.total.should == 3
  end
  
  it '操縦評価での比較用ハッシュ生成' do
    source = <<EOT
_define_I=D: {ＲＴＲ１_騎士団所有：ＲＴＲ：感覚+4,
－増加燃料装備
（Ｐ）29-00542-01_になし：はてない国人＋騎士＋黒騎士＋赤髪の乗り手＋ぽちの騎士：知識+1*幸運＋2;
（ＣＰ）12-00259-02_ＫＢＮ：北国人＋犬妖精２＋犬妖精２＋近衛騎士＋ＨＱ体格＋星見司２：知識+2/* 保育園（土場） */;
（ＣＰ）（ＣＰ）13-00277-01_辻木　志朗：北国人＋犬妖精２＋犬＋犬の神様＋ガンマン：敏捷+1*知識+1*幸運+2*知識+2/* 保育園（よんた） */;
};
_define_I=D: {ＲＴＲ２_騎士団所有：ＲＴＲ：感覚+4,
－増加燃料装備
（Ｐ）42-00593-01_うのり：北国人＋近衛兵＋トップガン＋帝國軍歩兵：敏捷+1*知識+1*幸運+1;
（ＣＰ）32-00638-01_佐倉　真：東国人+犬妖精２+ドラッガー+ドラッグマジシャン：感覚+4*知識+1*幸運+1;
（ＣＰ）42-00854-01_スノウ・アー：北国人＋教導パイロット＋テストパイロット＋帝國軍歩兵;
};
EOT
    troop = parse_troop(source)
    m = troop.find_evaluator('操縦')
    
    calc = m.calculate(troop)
    calc.hash_for_diff.should == {
      '操縦＠ＲＴＲ１_騎士団所有' => 41 ,
      '操縦＠ＲＴＲ２_騎士団所有' => 34
    }
    
    m = troop.find_evaluator('同調（個別）')
    
    calc = m.calculate(troop)
    puts [calc.class , calc.children[0].class]
    
    m.hash_for_diff(troop).should == {
      '同調（個別）＠ＫＢＮ' => 16 ,
      '同調（個別）＠うのり' => 13
    }
  end
  
  describe Calculation , 'ダイジェスト値の生成' do
    before(:each) do
      source = <<EOT
soldier:歩兵；
EOT
      @troop = parse_troop(source)
      @middle = Evaluator['中距離戦']
      @scout = Evaluator['偵察']
    end

    it '各キャラの各引数Calculatorのto_code' do
      calc = @middle.calculate(@troop)
      defined?(calc.to_digest).should be_truthy
      defined?(calc.to_code).should be_truthy

      #引数１,補正名１,補正名2,補正名2の数という文字列のハッシュを求める。
      calc0 = calc.children[0]
      calc0a = calc0.arguments['感覚']

      calc0a.to_code.should == ['感覚',0,'歩兵の中距離戦闘補正'].to_s
      scout_calc = @scout.calculate(@troop)
      scout_calc.to_code.should == ['感覚',0].to_s
    end

    it 'Calculation が 補正に用いられた特殊の名称と数に基づくダイジェスト値を返す。' do
      calc = @middle.calculate(@troop)
      defined?(calc.to_digest).should be_truthy
      defined?(calc.to_code).should be_truthy      #引数１,補正名１,補正名2,という文字列のハッシュを求める。
      calc0 = calc.children[0]
      calc0a = calc0.arguments['感覚']

      code = calc0.to_code
      code_a = ['感覚',0,'歩兵の中距離戦闘補正'].to_s
      code_b = ['知識',1,'歩兵の中距離戦闘補正'].to_s
      code.should == (code_a + code_b)
      calc.to_digest.should == Digest::MD5.hexdigest(code+['感覚','知識',2].to_s)

      scout_calc = @scout.calculate(@troop)
      scout_calc.to_digest.should == Digest::MD5.hexdigest(['感覚',0].to_s)

      Evaluator['追跡（感覚）'].calculate(@troop).to_digest.should_not == Evaluator['追跡（幸運）'].calculate(@troop).to_digest
    end
    
    it '比較用ハッシュの生成' do
      hash = @middle.calculate(@troop).hash_for_diff
      hash.should == {'中距離戦' => 2}
      
      sync = @troop.context.evaluators['同調']
      hash2 = sync.hash_for_diff(@troop)
      hash2.should == {'同調＠soldier' => 0}
    end
  end
  
  it '判定ひとつで使われた特殊名称の列挙' do
    troop = parse_troop <<EOT
＃！個別付与：電子妖精軍，｛【本隊】｝

32-00622-01_セントラル越前：東国人＋ハイギーク＋藩王＋ネットワークエンジニア＋星見司２：知識+1*幸運+1;

32-00635-01_黒埼紘：東国人＋スペーススペルキャスター＋文殊開発者＋ＨＱ知識＋ネットワークエンジニア＋法の司：敏捷+1*知識+4*幸運+1;
－高機能ハンドヘルド：個人所有：情報戦闘行為，歩兵，条件発動，情報戦闘、評価＋３。 , ，歩兵，条件発動，（元から情報戦闘行為ができる職業が行う場合での）情報戦闘、評価＋１。片手持ち武器。
EOT
    hack = troop.find_evaluator('情報戦')
    calc = hack.calculate(troop)
    ops = calc.distinct_operands
    ops.size.should == 5
    sp_names = ops.collect{|op|op.name}
    %w!ハイギークの知識・感覚補正
       ネットワークエンジニアの情報戦闘補正
       高機能ハンドヘルドの情報戦闘補正
       文殊開発者の知識・器用補正
       電子妖精軍の情報戦闘補正!.all?{|expect|
       sp_names.include?(expect)
    }.should be_truthy

    puts sp_names
  end

end
