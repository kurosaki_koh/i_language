# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')


describe Troop do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end

  it "consists all by airplane" do
    source = <<EOT
_define_I=D:{
fake：フェイクトモエリバー,
pilot:名パイロット
};
EOT

    troop = parse_troop(source)
    troop.evaluators.find{|ev|ev.name == '敏捷による防御（通常）'}.should_not be_nil

    source2 = <<EOT
soldier1:歩兵;
_define_I=D:{
fake：フェイクトモエリバー,
pilot:名パイロット
};
EOT
    troop2 = parse_troop(source2)
    troop2.evaluators.find{|ev|ev.name == '敏捷による防御（通常）'}.should be_nil

    source3 = <<EOT
soldier1:拳法家;
soldier2:拳法家;
EOT
    troop3 = parse_troop(source3)
    evs = troop3.evaluators
    evs.find{|ev|ev.name == '敏捷による防御（通常）'}.should_not be_nil
    puts evs.collect{|ev|ev.name}

    source4 = <<EOT
soldier1:拳法家;
_define_I=D:{
fake：ジャバニーズ,
pilot:名パイロット
};
EOT
    troop4 = parse_troop(source4)
    troop4.evaluators.find{|ev|ev.name == '敏捷による防御（通常）'}.should_not be_nil
  end

  it '部隊補正' do
    source = <<EOT
//部隊補正：チップボールの直接火力支援:感覚+2:中距離戦+3
soldier:歩兵;
EOT
    troop = parse_troop(source)
    #    puts @parser.failure_reason
    troop.idresses.size.should == 1
    troop.idresses[0].should == 'チップボールの直接火力支援'

    troop.evaluations.size.should == 1
    troop.evals_items.size.should == 1
    middle_range = Evaluator['中距離戦']
    troop.evals['感覚'].should == 2
    middle_range.evaluate(troop).should == 9

  end

  it '炎の料理人の料理強化' do
    source = <<EOT
＃！有効条件：炎の料理人と同じ部隊にいる場合
＃！部隊補正：炎の料理人の料理強化
Ｌ：普通の人　＝　｛
　ｔ：名称　＝　普通の人（職業）
　ｔ：評価　＝　全能力０
　ｔ：特殊　＝　｛
　　＊普通の人の職業カテゴリ　＝　，，派生職業アイドレス。
　｝
｝
soldier:普通の人;
EOT
    troop = parse_troop(source)
    StandardAbilities.each{|c|
      troop.evaluate(c).should == 4
    }
    armor = Evaluator['装甲（通常）']
    armor.evaluate(troop).should == 4
    
    #    source = <<EOT
    #＃！有効条件：炎の料理人と同じ部隊にいる場合
    #＃！部隊補正：炎の料理人の料理強化
    #EOT
    #    troop = parse_troop(source)
    #    StandardAbilities.each{|c|
    #      troop.evaluate(c).should == 4
    #    }
  end

  it '行為可能者数の数え上げ' do
    source = <<EOT
hacker1:ハッカー+理力使い;
hacker2:ハッカー+サイボーグ;
EOT
    troop =parse_troop(source)

    troop.num_of_action_owner('情報戦闘行為').should == 2
    troop.num_of_action_owner('詠唱戦闘行為').should == 1

  end

  it '任意能力補正の選択がTroopオブジェクトのプロパティに設定されている' do
    source = <<EOT
＃！任意補正：学兵の特殊補正,白兵戦
＃！任意補正：学兵の特殊補正,体格
＃！有効条件：順応性があるため
soldier:学兵＋剣士;
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    bonuses = @context.flexible_bonus['学兵の特殊補正']
    bonuses.should == '体格'
  end

  it '部隊補正での全能力＋ｎ' do
    source = <<EOT
//部隊補正：：全能力+3
soldier:歩兵;
EOT
    
    troop = parse_troop(source)
    
    m_range = Evaluator['中距離戦']
    m_range.evaluate(troop).should == 5
  end

  it '部隊補正で行為を追加' do
    source = <<EOT
//部隊補正：ピケ・サイドカーの専用重機関銃
soldier:ネットワークエンジニア;
EOT

    troop = parse_troop(source)
    troop.evaluators.find{|ev|ev.name == '中距離戦'}.should_not be_nil

  end

  it '治安維持' do
    source = <<EOT
policeman:東国人＋軌道降下兵＋警官＋特殊警官;
soldier1:ネットワークエンジニア;
soldier2:ネットワークエンジニア;
EOT
    troop = parse_troop(source)
    troop.evaluators.find{|ev|ev.name == '治安維持'}.should_not be_nil

    security = Evaluator['治安維持']
    security.evaluate(troop).should == 17
  end


  it 'チップに乗った偵察兵' do
    source = <<EOT
_define_I=D:{
チップ：チップボール,
scout:偵察兵;
};
EOT
    troop = parse_troop(source)
    troop.evaluators.find{|ev|ev.name == '偵察'}.should_not be_nil
  end

  it '乗り物混合編成では魅力評価子を返さない' do
    source = <<EOT
_define_I=D:{
チップ：チップボール,
scout:偵察兵;
};
EOT
    troop = parse_troop(source)
    troop.evaluators.find{|ev|ev.name == '魅力'}.should be_nil

    source = <<EOT
soldier:歩兵;
_define_I=D:{
チップ：チップボール,
scout:偵察兵;
};
EOT
    troop = parse_troop(source)
    troop.evaluators.find{|ev|ev.name == '魅力'}.should be_nil

    source = <<EOT
soldier:歩兵;
EOT
    troop = parse_troop(source)
    troop.evaluators.find{|ev|ev.name == '魅力'}.should_not be_nil

  end

  it '隠蔽看破' do
    source = <<EOT
_define_I=D:{チップボール・よの１：チップボール：体格+4,
13-00276-01_坂下真砂：北国人＋わんわん偵察兵＋追跡者＋特殊部隊員＋ガンマン＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*知識+6*幸運+2；
};
EOT
    troop = parse_troop(source)
    evs = troop.evaluators
    ev_oids = evs.collect{|ev|ev.object_id}

    reveal = troop.find_evaluator('隠蔽看破')
    r_oid = reveal.object_id
    ev_oids.include?(r_oid).should eq true

    reveal.evaluate(troop).should == 17

    source = <<EOT
soldier:歩兵；
scout:追跡者；
EOT
    troop = parse_troop(source)
    evs = troop.evaluators
    ev_oids = evs.collect{|ev|ev.object_id}

    ev_oids.include?(r_oid).should == false

    source = <<EOT
soldier:歩兵；
scout1:追跡者；
scout2:追跡者；
scout3:追跡者；
EOT
    troop = parse_troop(source)
    evs = troop.evaluators
    ev_oids = evs.collect{|ev|ev.object_id}

    ev_oids.include?(r_oid).should == true

  end

  it '７５％に満たない一般行為の補正は使わない' do
    source = <<EOT
scout:偵察兵＋追跡者
soldier:歩兵
EOT
    troop = parse_troop(source)

    scout = Evaluator['偵察']
    scout.is_standard.should == true
    #    troop.team_action_names.include?('偵察行為').should == false

    soldier = troop.children[0]
    spec = soldier.find_spec('偵察兵の偵察補正')
    troop.is_tqr_qualified?(spec).should eq false
    troop.no_sp?(scout).should  eq true
    scout.evaluate(troop).should == 6

    source = <<EOT
scout:偵察兵＋追跡者
scout:偵察兵＋追跡者
scout:偵察兵＋追跡者
soldier:歩兵
EOT
    troop = parse_troop(source)
    #        if evaluator.is_standard && !team_action_names.include?(evaluator.action_name)
    #    troop.team_action_names.include?('偵察行為').should == true
    scout.evaluate(troop).should == 14

  end

  it '部隊へのevaluate_itemsを有効にする' do
    source = <<EOT
＃！有効条件：非戦争行動を行う場合
Ｌ：行政士官の非戦争行動支援　＝　｛
　ｔ：名称　＝　行政士官の非戦争行動支援（定義）
　ｔ：評価　＝　なし
　ｔ：特殊　＝　｛
　　＊行政士官の部隊支援の定義カテゴリ　＝　，，判定補正。
　　＊行政士官の非戦争行動支援の判定補正　＝　，条件発動，（非戦争行動を行う場合）全判定、評価＋６。同能力重複適用不可。
　｝
｝

＃！部隊補正：行政士官の非戦争行動支援
soldier：歩兵；

_define_division:{ダミー分隊：行政士官の非戦争行動支援,
soldier
};
EOT
    troop = parse_troop(source)
    items = troop.evaluate_items
    result = troop.evals_str

    troop.evals['体格'].should == 0
    items.size.should == 1
    result.should include "体格：筋力：耐久力：外見：敏捷：器用：感覚：知識：幸運\n6：6：7：6：7：5：6：7：5"

    short_range = Evaluator['近距離戦']
    short_range.evaluate(troop).should == 7

    div1 = troop.divisions[0]
    div1.evals_str.should == result
    short_range.evaluate(div1).should == 7
  end
  it '部隊への偵察補正' do
    source = <<EOT
＃！部隊補正：偵察戦術の偵察
soldier:歩兵；
EOT
    troop = parse_troop(source)
    scout = Evaluator['偵察']
    cal = scout.calculate(troop)
    cal.total.should == 5
  end

  describe 'Troopオブジェクトが補正使用判定を持つ' do
    before(:each) do
      source = <<EOT
＃！部分有効条件：二人一組で運用する場合
10-00040-01_ソーニャ／ヴァラ／モウン艦氏族／スターチス：高位森国人+突撃アイドル+ハンターキラウイッチ+突撃リポーター+ＨＱ外見：敏捷+1*知識+1*幸運+3；
＃－クローバーのしおり：個人所有：幸運＋１（非着用型）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00040-01%A1%A7%A5%BD%A1%BC%A5%CB%A5%E3%A1%BF%A5%F4%A5%A1%A5%E9%A1%BF%A5%E2%A5%A6%A5%F3%B4%CF%BB%E1%C2%B2%A1%BF%A5%B9%A5%BF%A1%BC%A5%C1%A5%B9 [^]
10-00217-01_久堂尋軌：高位森国人+医師+ハンターキラウイッチ+テストパイロット+ＨＱ器用+法官：敏捷+1*知識+3*幸運+2；
－カトラス：個人所有：白兵行為可能になり、評価＋２（着用型／その他）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00217-01%A1%A7%B5%D7%C6%B2%BF%D2%B5%B0 [^]
doctor1:高位森国人+医師;
doctor2:高位森国人+医師;


_define_division:{別分隊,
10-00040-01_ソーニャ／ヴァラ／モウン艦氏族／スターチス
10-00217-01_久堂尋軌
};
EOT
      @troop = parse_troop(source)
      puts @troop.action_evals_str
    end

    it '補正が使われたら、troopのused_sp_casheが呼ばれる。' do
      pending #実は使ってない？？
      @troop.current_situation.should_receive(:used_sp_cashe=).any_number_of_times.with(['医師の治療補正'.intern])
      defined?(@troop.is_sp_used?).should == 'method'
    end

    it 'Troopオブジェクトで補正が使われたかどうかを判定できる。' do
      div1 = @troop.divisions[0]
      defined?(@troop.is_sp_used?).should == 'method'

#      puts @troop.action_evals_str
      spec = @troop.children[1].find_spec('医師の治療補正')
      @troop.is_sp_used?(spec).should == true

      spec = div1.children[1].find_spec('医師の治療補正')
      div1.action_evals_str
      div1.is_sp_used?(spec).should == false
    end
  end

  it '部隊で有効な７５％制限能力識別子' do
    source = <<EOT
healer1:華麗なる治癒師；
healer2:華麗なる治癒師；
healer3:華麗なる治癒師；
doctor:医師；
EOT

    troop = parse_troop(source)

    healer1 = @context.units['healer1']

    sp = healer1.find_spec('華麗なる治癒師の受け流し能力')
    sp.tqr_id.should == :'受け流し能力'
    healer1.tqrs.should include(:'受け流し能力')
    troop.team_tqrs.should include(:'受け流し能力')
    parry = troop.find_evaluator('器用による防御（通常）')
    troop.evaluator_include?(parry).should  eq true
  end

  it '無人機' do
    source = <<EOT
＃！評価派生：反撃での，対空戦闘
＃！部分有効条件：反撃する場合
【任意の機体呼称】_組織所有：連結ミアキス（空母型）；
【任意の機体呼称】_組織所有：フラワー級宇宙駆逐艦；
【任意の機体呼称】_組織所有：フラワー級宇宙駆逐艦；
【任意の機体呼称】_組織所有：フラワー級宇宙駆逐艦；
EOT
    troop = parse_troop(source)
    troop.evals.keys.should_not include('体格')

    @context.particular_situation_names.should == ['反撃での']
    troop.all_action_names.should == ['艦隊戦闘行為','（反撃での）対空戦闘行為']
  end

  it '銘入り猫士は同調評価提出者の対象となる。' do
    source = <<EOT
03-x0001-xx_オーレ：高位西国人＋緊急投擲展開軍＋ウォードレスダンサー＋舞踏体＋ＳＨＱ体格＋ＨＱ体格＋補給士官＋ＨＱ知識＋ＨＱ知識＋ＨＱ知識＋ＨＱ知識＋煌月＋ＨＱ；
－帰還用定期券：ＦＥＧ所有：ＦＥＧにゲーム内時間２日で帰還できる。障害がある場合、その報がＦＥＧに届く（携帯型）
－オーレ＋ＨＱ：個人所有：全能力、評価＋５　＃ＨＱでさらに＋１
－成長＋ＨＱ：個人所有：一回り大きくなり、全評価は＋１(＋HQ累積で＋1)される。(イベント)
＃－個人取得ＨＱ根拠ＵＲＬ（川原）：http://farem.s101.xrea.com/idresswiki /index.php?00046-01%A1%A7%C0%EE%B8%B6%B2%ED

03-00046-01_川原雅：高位西国人＋緊急投擲展開軍＋ウォードレスダンサー＋舞踏体＋ＳＨＱ体格＋ＨＱ体格＋補給士官＋ＨＱ知識＋ＨＱ知識＋ＨＱ知識＋ＨＱ知識＋煌月＋ＨＱ：敏捷＋1*器用＋1*知識＋1*幸運＋1；
－レーザーピストル：個人所有：近距離戦闘行為，歩兵，条件発動，（射撃としての近距離での）攻撃、評価＋３（片手持ち武器）
－帰還用定期券：ＦＥＧ所有：ＦＥＧにゲーム内時間２日で帰還できる。障害がある場合、その報がＦＥＧに届く（携帯型）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00046-01%A1 %A7%C0%EE%B8%B6%B2%ED
EOT
    troop = parse_troop(source)

    troop.leaders_str.should =~ /オーレ/
  end
  it '災害対応活動' do
    source = <<EOT
05-00127-01_藤村　早紀乃：南国人＋名医＋治癒師＋消防士＋星見司２：敏捷+1*器用+6*知識+1*幸運+1;
EOT
    troop = parse_troop(source)
    ev = Evaluator['災害対応活動']
    ev.should_not be_nil
    ev.evaluate(troop).should == 18
    troop.evaluators.find{|ev|ev.name == '災害対応活動'}.should_not be_nil
  end

  it '搭乗者が２名以上いる機体があれば、同調（個別）を出す。' do
    source = <<EOT
ｌ：検証中機能　←　有効
＃00：東方有翼第二騎士団 T14編成
＃編成表:http://www30.atwiki.jp/idress/pages/415.html

＃！有効条件：宰相府にある機体に搭乗している場合での
＃！有効条件：＜＊服に着られるの特殊能力＞を使用して行う行為の

_define_I=D:{
Ｓ１__国有：灼天＋ＨＱ体格＋ＳＨＱ体格＋ＳＨＱ,
（Ｐ）00-00758-01_やひろ：高位西国人＋名パイロット＋宰相の娘＋服に着られる＋東方有翼騎士：外見+3;
－秘書官正装：個人所有：外見+3（着用型／体）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00758-01%A1%A7%A4%E4%A4%D2%A4%ED
（ＣＰ）00-xx052-xx_猫士：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
};
_define_I=D:{
Ｓ２__国有：灼天＋ＨＱ体格＋ＳＨＱ体格＋ＳＨＱ,
（Ｐ）14-00287-01_南天：高位北国人+宰相の娘+黒い舞踏子+ブリザードパイロット+秘書官：敏捷+1*器用+1*感覚+4*知識+1*幸運+2;
－スペーススーツ：個人所有：宇宙空間にいる間、全能力＋５（着用型／全身）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00287-01%A1%A7%C6%EE%C5%B7
（ＣＰ）00-xx053-xx_猫士：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
};


＃！状況：戦闘時
＃！部分有効条件：戦闘する場合での
EOT
    troop = parse_troop(source)

    ev = Evaluator['同調（個別）']
    calc = ev.calculate(troop)
    taget_set = ev.target_set(troop)
    s1 = troop.children[0]
    ev.do_target?(s1).should  eq true
    calc.children.size.should == 2

  end

  it '夜間戦闘行為のコスト消費' do
    source = <<EOT
cat:西国人＋猫の神様；
EOT
    troop = parse_troop(source)
    puts troop.action_evals_str

    evs = troop.context.find_evaluator_by_action_name('夜間戦闘行為')
    evs.size.should == 1
    ev = evs[0]
    ev.should_not be_nil
    ev.is_action_bonus?.should  eq true
    #    calc = ev.calculate(troop)

    sp = troop.children[0].find_spec('猫の神様の夜間戦闘補正')
    sp.should_not be_nil

    sp.is_action?.should  eq true
    sp.is_cost_required?.should  eq true

    cost = troop.total_cost
    cost.should == {'燃料' => -3}

    source = <<EOT
_define_I=D:{
ＦＦ１__国有：灼天＋ＨＱ体格＋ＳＨＱ体格＋ＳＨＱ,
（Ｐ）14-00287-01_南天：高位北国人＋宰相の娘＋黒い舞踏子＋ブリザードパイロット＋秘書官：耐久力+3*敏捷+1*器用+1*感覚+4*知識+1*幸運+2；
＃－大健康の腕輪：個人所有：耐久力＋３（着用型／腕）
－高機能ハンドヘルド：個人所有：情報戦＋３（着用型／その他）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00287-01%A1%A7%C6%EE%C5%B7
（ＣＰ）00-00758-01_やひろ：高位西国人＋名パイロット＋宰相の娘＋服に着られる＋東方有翼騎士：外見+2；
－秘書官夏服：個人所有：外見＋２、（宰相府内での非戦闘行為での）全判定、評価＋３（着用型／体）
－高機能ハンドヘルド：個人所有：情報戦＋３（着用型／その他）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00758-01%A1%A7%A4%E4%A4%D2%A4%ED
};

_define_I=D:{
ＦＦ２__国有：灼天＋ＨＱ体格＋ＳＨＱ体格＋ＳＨＱ,
（Ｐ）01-00216-01_緋乃江戌人：高位森国人＋長弓兵＋弓兵＋魔法弓手＋ＨＱ感覚＋東方有翼騎士：体格+10*筋力+10*耐久力+13*外見+10*敏捷+11*器用+10*感覚+10*知識+11*幸運+12；
＃－大健康の腕輪：個人所有：耐久力＋３（着用型／腕）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00216-01%A1%A7%C8%EC%C7%B5%B9%BE%D8%FC%BF%CD
（ＣＰ）44-00220-01_奥羽りんく：西国人＋猫の決戦存在＋スペーススターシップオフィサー＋宰相の娘＋秘書官：敏捷＋１*器用＋１*知識＋１*幸運＋１*外見＋２；
－夢のまどろみ：個人所有：（評価補正無し）（着用型／手先）
＃－恩寵の時計：個人所有：外見＋２（着用型／首）
－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／腕）
－高機能ハンドヘルド：個人所有：情報戦＋３（着用型／その他）
－子守唄：個人所有：（評価補正無し）（技術）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00220-01%A1%A7%B1%FC%B1%A9%A4%EA%A4%F3%A4%AF
};
EOT

    troop = parse_troop(source)
    puts troop.action_evals_str

    rtr2 = troop.children[1]
    sp = rtr2.find_spec('猫の決戦存在の夜間戦闘補正')

    sp_array = []
    cost = troop.total_cost{|unit , sp|
      sp_array << sp
    }
    found = sp_array.find{|sp|sp.name == '猫の決戦存在の夜間戦闘補正'}
    found.should_not be_nil
    cost.should == {'燃料' => -51}
  end

  it 'コストに生物資源が含まれる場合' do
    source = <<EOT
36-00696-01_有馬信乃：高位東国人+理力建築士+建築家+式神使い；
EOT
    troop = parse_troop(source)

    troop.action_evals_str

    result = troop.sp_cost_str
    result.should =~ /生物資源/o
  end

  it 'Troop#unit_type が部隊の構成に応じて変化する。' do
    source = <<EOT
ｌ：＊教導パイロットの指導の特殊補正　＝　，条件発動，全判定、評価＋２。部隊対象能力。

＃！個別付与：教導パイロットの指導，【本隊】

36-00696-01_有馬信乃：高位東国人+理力建築士+建築家+式神使い；
EOT
    troop = parse_troop(source)

    calc = Evaluator['装甲（通常）'].calculate(troop)
    calc.unit_type.should == ['歩兵']
    calc.total.should == 9

    source = <<EOT
ｌ：＊教導パイロットの指導の特殊補正　＝　搭乗，条件発動，全判定、評価＋２。部隊対象能力。

＃！個別付与：教導パイロットの指導，【本隊】

36-00696-01_有馬信乃：高位東国人+理力建築士+建築家+式神使い；
EOT
    troop = parse_troop(source)

    calc = Evaluator['装甲（通常）'].calculate(troop)
    calc.unit_type.should == ['歩兵']
    calc.total.should == 7
    
    source = <<EOT
ｌ：＊教導パイロットの指導の特殊補正　＝　搭乗，条件発動，全判定、評価＋２。部隊対象能力。

＃！個別付与：教導パイロットの指導，【本隊】

_define_I=D:{
Ｓ１__国有：灼天＋ＨＱ体格＋ＳＨＱ体格＋ＳＨＱ,
（Ｐ）00-00758-01_やひろ：高位西国人＋名パイロット＋摂政＋服に着られる＋東方有翼騎士：外見+3;
－秘書官正装：個人所有：外見+3（着用型／体）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00758-01%A1%A7%A4%E4%A4%D2%A4%ED
（ＣＰ）00-xx052-xx_猫士：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
};
EOT

    troop = parse_troop(source)
    calc = Evaluator['装甲（通常）'].calculate(troop)
    calc.unit_type.should == ['搭乗','乗り物']
    calc.total.should == 44

    calc = Evaluator['外交戦'].calculate(troop)
    calc.unit_type.should == ['歩兵']
    calc.total.should == 21

    source = <<EOT
ｌ：＊教導パイロットの指導の特殊補正　＝　搭乗，条件発動，全判定、評価＋２。部隊対象能力。
＃！個別付与：教導パイロットの指導，【本隊】

36-00696-01_有馬信乃：高位東国人+理力建築士+建築家+式神使い；

_define_I=D:{
Ｓ１__国有：灼天＋ＨＱ体格＋ＳＨＱ体格＋ＳＨＱ,
（Ｐ）00-00758-01_やひろ：高位西国人＋名パイロット＋宰相の娘＋服に着られる＋東方有翼騎士：外見+3;
－秘書官正装：個人所有：外見+3（着用型／体）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00758-01%A1%A7%A4%E4%A4%D2%A4%ED
（ＣＰ）00-xx052-xx_猫士：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
};
EOT

    troop = parse_troop(source)
    calc = Evaluator['装甲（通常）'].calculate(troop)
    calc.unit_type.should == []
    calc.total.should == 42

    source = <<EOT
＃42:星鋼京 T16編成
＃編成表：http://www40.atwiki.jp/meteorit/pages/237.html

/*開始*/
ｌ：＊教導パイロットの指導の特殊補正　＝　歩兵，条件発動，全判定、評価＋２。部隊対象能力。
＃！個別付与：教導パイロットの指導，｛【本隊】｝
＃！部分有効条件：燃料増加装備を着用している場合


_define_I=D:{
ＲＴＲ１_星鋼京所有：ＲＴＲ＋ＳＨＱ敏捷,
－増加燃料装備：藩国所有：装甲+6*ＡＲ+3*全評価-3　増加分ＡＲ３使用後、全評価+3*装甲-6
（P）42-00612-01_御鷹：北国人＋撃墜王＋トップガン＋教導パイロット＋法の司＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*感覚+5*知識+1*幸運+1;
－法の執行者の紋章：個人所有：
（CP）42-00530-02_木曽池春海：北国人＋撃墜王＋トップガン＋教導パイロット＋護民官＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：器用+1*感覚+2*知識+1*幸運+1;
－互尊Ｃ：個人所有：
（CP）42-00610-01_センハ：北国人＋撃墜王＋トップガン＋教導パイロット＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*幸運+1;};


_define_I=D:{
ＲＴＲ４_星鋼京所有：ＲＴＲ＋ＳＨＱ敏捷,
－増加燃料装備：藩国所有：装甲+6*ＡＲ+3*全評価-3　増加分ＡＲ３使用後、全評価+3*装甲-6
（P）42-00590-01_セタ・ロスティフンケ・フシミ：北国人＋教導パイロット＋テストパイロット＋外戚：敏捷+1;
（CP）42-00572-01_双海　環：高位西国人＋フライトオフィサー＋整備士２＋スペーススターキャット+護民官：敏捷+1*器用+3*知識+1;
－猫と犬の前足が重なった腕輪：個人所有：
（CP）42-00843-01_サカキ：北国人＋職人見習い＋教導パイロット＋帝國軍歩兵+法の司;
－法の執行者の紋章：個人所有：
};
EOT
    troop = parse_troop(source)
    calc = Evaluator['操縦'].calculate(troop)
    calc.children[0].total.should == 48

    calc = Evaluator['外交戦'].calculate(troop)
    calc.unit_type.should == ['歩兵']
    calc.total.should == 17

    calc = Evaluator['装甲（通常）'].calculate(troop)
    calc.total.should == 36
  end

  it '部隊の航路数' do
    source = <<EOT
_define_vessel:{
Ｄ１_国有：「きゃりっじ」,
（Ｐ）44-00220-01_奥羽りんく：西国人＋猫の決戦存在＋スペーススターシップオフィサー＋宰相の娘＋秘書官：外見+2*敏捷+1*器用+1*知識+1*幸運+1；
＃－恩寵の時計：個人所有：外見＋２（着用型／首）
－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／腕）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00220-01%A1%A7%B1%FC%B1%A9%A4%EA%A4%F3%A4%AF
（Ｐ）00-xx054-xx_猫士1：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
（ＣＰ）00-xx055-xx_猫士2：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
（ＣＰ）00-xx056-xx_猫士3：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
};

_define_vessel:{
Ｄ２_国有：「きゃりっじ」,
（Ｐ）44-00220-02_test：西国人＋猫の決戦存在＋スペーススターシップオフィサー＋宰相の娘＋秘書官：外見+2*敏捷+1*器用+1*知識+1*幸運+1；
};

EOT
    troop = parse_troop(source)
    troop.hops.should == 2

    source = <<EOT
_define_I=D:{ＲＴＲ１：ＲＴＲ，
（Ｐ）44-00220-01_奥羽りんく：西国人＋猫の決戦存在＋スペーススターシップオフィサー＋宰相の娘＋秘書官：外見+2*敏捷+1*器用+1*知識+1*幸運+1；
＃－恩寵の時計：個人所有：外見＋２（着用型／首）
－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／腕）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00220-01%A1%A7%B1%FC%B1%A9%A4%EA%A4%F3%A4%AF
（Ｐ）00-xx054-xx_猫士1：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
（ＣＰ）00-xx055-xx_猫士2：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
（ＣＰ）00-xx056-xx_猫士3：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
};

hacker:東国人+ハッカー＋ギーク＋ネットワークエンジニア；
EOT
    troop = parse_troop(source)
    troop.hops.should be_nil

    source = <<EOT
_define_vessel:{
Ｄ１_国有：「きゃりっじ」,
（Ｐ）44-00220-01_奥羽りんく：西国人＋猫の決戦存在＋スペーススターシップオフィサー＋宰相の娘＋秘書官：外見+2*敏捷+1*器用+1*知識+1*幸運+1；
＃－恩寵の時計：個人所有：外見＋２（着用型／首）
－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／腕）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00220-01%A1%A7%B1%FC%B1%A9%A4%EA%A4%F3%A4%AF
（Ｐ）00-xx054-xx_猫士1：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
（ＣＰ）00-xx055-xx_猫士2：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
（ＣＰ）00-xx056-xx_猫士3：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
};

_define_vessel:{
Ｄ２_国有：ドッグバッグ（医療型）,
（Ｐ）44-00220-02_test：西国人＋猫の決戦存在＋スペーススターシップオフィサー＋宰相の娘＋秘書官：外見+2*敏捷+1*器用+1*知識+1*幸運+1；
};

EOT
    troop = parse_troop(source)
    troop.hops.should == 1

    source = <<EOT
hacker1:東国人＋ハッカー＋ギーク＋ネットワークエンジニア；
hacker2:東国人＋ハッカー＋ギーク＋ネットワークエンジニア；
EOT
    troop = parse_troop(source)
    troop.hops.should be_nil

  end


  it '部隊への偵察補正付与' do
    source = <<EOT
＃！個別付与：地上戦闘情報共有システム，本隊
＃！部分有効条件：フィーブル藩の人間と同じ戦場にいる全地上部隊に
＃！部分有効条件：機械が動作する環境において

16-00306-01_フィーブル：高位西国人＋ハッカー＋ギーク＋藩王：敏捷+1*知識+1*幸運+1；
EOT

    troop = parse_troop(source)

    Evaluator['偵察'].evaluate(troop).should == 11
  end

  it 'バンドタグ' do
    source = <<EOT
Ｌ：スカールド　＝　｛
　ｔ：名称　＝　スカールド（乗り物）
　ｔ：要点　＝　いかにも安そう，展開式の楯，鈍重な
　ｔ：周辺環境　＝　地下
　ｔ：評価　＝　体格３５，筋力２２，耐久力３５，外見２，敏捷９，器用７，感覚４，知識５，幸運１５
　ｔ：特殊　＝　｛
　　＊スカールドの乗り物カテゴリ　＝　，，Ｉ＝Ｄ。
　　＊スカールドのイベント時燃料消費　＝　，，（戦闘イベント参加時）燃料－５万ｔ。
　　＊スカールドのイベント時資源消費　＝　，，（戦闘イベント参加時）資源－２万ｔ。
　　＊スカールドの必要パイロット数　＝　，，パイロット０名。＃無人機
　　＊スカールドの局地活動能力　＝　，，宇宙。
　　＊スカールドの防御補正　＝　，条件発動，防御、評価＋１６。
　　＊スカールドの人機数　＝　，，１０人機。
　　＊スカールドのアタックランク　＝　，，ＡＲ１５。
　　＊スカールドの出撃制限　＝　，，出撃の際にバンドしなければならず、以後、そのバンド先についていく。分割はできない。
　　＊スカールドの防御特性　＝　，，スカールドは大型の楯であり、防御判定時にはバンド対象の部隊を覆うように展開される。
　　＊スカールドの防御性能　＝　，，スカールドは使い捨ての盾として設計されており、防御判定に１回失敗したとしても本体が破損するだけでバンド対象の部隊は無傷である。
　　＊スカールドのその他　＝　，，テスト用特殊。燃料－２万ｔ。　｝
　ｔ：→次のアイドレス　＝　Ｉ＝Ｄ・ソードマンの開発（イベント）
｝

soldier1:東国人＋ハイギーク＋ギーク＋ネットワークエンジニア；
soldier2:東国人＋ハイギーク＋ギーク＋ネットワークエンジニア；

（バンド）スカールド01：スカールド；
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    troop.action_evals_str

    troop.children.size.should == 2
    troop.band.size.should == 1
    troop.band[0].name.should == 'スカールド01'

    troop.action_names.should include('白兵距離戦闘行為')

    ev = troop.find_evaluator('装甲（スカールドのみ）')
    ev.should_not be_nil

    ev.target_set(troop).size.should == 1
    ev.evaluate(troop).should == 51

    cost_result = troop.cost_result_str
    cost_result.should_not be_nil
    puts cost_result
    
    troop.organization_cost('資源').should == 2
    troop.organization_cost('燃料').should == 41
  end

  it '行為補正以外の７５％制限がかかる補正を列挙する' do
    source = <<EOT
scout:偵察兵＋レコン＋工兵；
EOT
    troop = parse_troop(source)
    troop.non_action_bonus_tqrs.should == ['偵察補正','施設破壊能力','塹壕作成能力']
    
  end
  
  it '比較用ハッシュの生成' do
    troop = parse_troop('fencer:大剣士；')
    
    e_hash = {
      '体格' =>  1,
      '筋力' => 3 ,
      '耐久力' => 3 , 
      '外見' =>  1,
      '敏捷' =>  0,
      '器用' =>  -1,
      '感覚' =>  0,
      '知識' =>  -1,
      '幸運' => 0
    }
    
    troop.evals_hash.should == e_hash
    
    action_hash = {
      '装甲（通常）'=> 4 ,
      '魅力'=> 0,
      '白兵戦'=> 4,
      '侵入（敏捷・器用）'=> 0 ,
      '侵入（幸運）'=> 0,
      '追跡（感覚）'=> 0 ,
      '追跡（幸運）'=> 0,
      '偵察'=> 0,
      '隠蔽'=> 0,
      '陣地構築（筋力）'=> 3 ,
      '陣地構築（知識）'=> -1,
      '歌唱'=> 0
    }
    troop.action_evals_hash.should == action_hash
    
    leader_hash = { '同調＠fencer'=> 1 }
    troop.leader_evals_hash.should == leader_hash
    
    costs_hash = {
      '出撃費用/資源'=> 0,
      '出撃費用/食料'=> 0, 
      '出撃費用/燃料'=> 0, 
      '特殊消費/資源'=> 0, 
      '特殊消費/食料'=> 0, 
      '特殊消費/燃料'=> 6, 
      '合計消費/資源'=> 0, 
      '合計消費/食料'=> 0, 
      '合計消費/燃料'=> 6
    }
    troop.costs_hash.should == costs_hash
    
    total_hash = e_hash.merge(action_hash).merge(leader_hash).merge(costs_hash)
    
    troop.hash_for_diff.should == total_hash
    
    names = troop.evaluation_names_order
    puts names.keys.sort_by{|key| names[key]}
  end


  describe 'AR' do
    subject {parse_troop(source)}

    context '歩兵部隊' do
      let(:source) {<<EOT }
soldier:東国人；
soldier2：東国人＋摂政；
EOT
      it '最小のＡＲが採用される。' do

        expect(subject.ar).to eq 9
      end
    end

    context '騎乗部隊' do
      let(:source) {<<EOT }
□汗血馬弐号_暁の円卓所有：汗血馬（ヲチ版）
（騎）38-00125-02_まさきち：暁の民２＋剣＋ソードマスター＋大戦士＋ＳＨＱ耐久力＋ＨＱ体格：
－強靭刀：暁の円卓藩国所有：白兵距離での攻撃判定＋６（着用型／両手持ち武器）
－ますらおの鎧：暁の円卓藩国所有：装甲判定＋７（着用型／全身）
－王の契約による強化:時雨野椿との契約
－汗血馬（ヲチ版）
（同）38-00334-02_時雨野　椿：暁の民２＋ドラゴンスレイヤー＋王＋ライオンハート＋歌い手＋ＨＱ外見＋ＨＱ体格：
－強靭刀：暁の円卓藩国所有：白兵距離での攻撃判定＋６（着用型／両手持ち武器）
－ますらおの鎧：暁の円卓藩国所有：装甲判定＋７（着用型／全身）
EOT

      it '（同）タグが付いたユニットのＡＲは考慮しない。' do
        expect(subject.ar).to eq 14
      end
    end
  end
  describe 'API用ハッシュの生成' do

    subject  { parse_troop(source)}
    context '正常系' do
      let(:source) {"test：大剣士；\n"}
      it 'タプルの配列を返す' do
        hash = subject.hash_for_api
        hash.keys.should include ('divisions')
        hash['divisions'][0].keys.should include('name' , 'evaluations')
        hash['divisions'][0]['evaluations'].should == subject.all_evals_tuples
        hash['divisions'][0]['members'].should == [{'name' => 'test'}]
      end
    end
    context '異常系' do
      let(:source) {"test：アウィナイト+test；\n"}
      it 'タプルの配列を返す' do
        hash = subject.hash_for_api
        hash.keys.should include ('divisions')
        hash['divisions'][0].keys.should include('name' , 'evaluations')
        hash['divisions'][0]['evaluations'].should == subject.all_evals_tuples
        hash['divisions'][0]['members'].should == [{'name' => 'test'}]
      end

    end

    context '摂政の外交戦' do
      let(:source) {"test：摂政；\n"}
      it '外交戦評価のタプルを含む' do
        subject.hash_for_api['divisions'][0]['evaluations'].should include(['外交戦' , 8])
      end
    end

    context '行為・７５％制限' do
      let(:source) {"test：猫妖精＋追跡者;\n"}
      it '行為' do
        subject.hash_for_api['divisions'][0]['tqrs'].should include('夜間戦闘行為')
      end
      it '７５％制限' do
        subject.hash_for_api['divisions'][0]['tqrs'].should include('追跡補正')
      end
    end

    context 'アイドレス名' do
      let(:source) {"test：猫妖精＋追跡者;\n"}
      it '使われているアイドレス名が全て出てくる' do
        subject.hash_for_api['divisions'][0]['idresses'].should include('猫妖精')
        subject.hash_for_api['divisions'][0]['idresses'].should include('追跡者')
        subject.hash_for_api['divisions'][0]['idresses'].size.should == 2
      end
    end


  end


  describe 'Resourcify' do
    before :each do
      source = <<EOT
32-00635-01：hacker:ハッカー；
－大型ＰＣの計算資源

Ｌ：コマンドリソース化（hacker）　＝　｛
　ｆ：＊ハッカーのナショナルネット接続能力　＝　ハッカーのナショナルネット接続能力：６：＃情報戦
　ｆ：＊大型ＰＣの計算資源の情報戦闘補正　＝　大型ＰＣの計算資源の情報戦闘補正：１２：＃情報戦
｝
EOT
      @troop = parse_troop(source)
    end


    it 'API出力に指定したコマンドリソースが含まれる' do
      expect(@troop.hash_for_api['divisions'][0]['command_resources']).to match [
        a_hash_including(name: 'ハッカーのナショナルネット接続能力' , power: 6 , tag: '情報戦') ,
        a_hash_including(name: '大型ＰＣの計算資源の情報戦闘補正' , power: 12 , tag: '情報戦')
      ]
    end
  end
end

#class Object ; def inspect ; 'TEST' ; end ; end