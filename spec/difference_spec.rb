# -*- encoding: utf-8 -*-
require 'rubygems'
require 'rspec'
require  File.join(File.dirname(__FILE__) , '../calculator/difference')

include ILanguage::Calculator

describe Difference do
  let(:left) { {:A => 1 , :B=>2 , :C => 3 } }
  let(:right) { {:B=> 2 , :C=> 4 , :D=> 5} }
  
  context '.new' do
    it 'makes from two hashes' do
      diff = Difference.new(left , right)
      diff.left.should == left
      diff.right.should == right
    end
  end
  
  context '#instance_methods' do
    subject {Difference.new(left , right)}
#    before(:each) do
#      @difference = Difference.new(left , right)
#    end

    it "has 1 deleted items" do
      subject.should have(1).deleted_items
      item = subject.deleted_items[0]
      item.name.should == :A
      item.lvalue.should == 1
      item.rvalue.should be_nil
      item.delta.should be_nil
      
      item.to_s.should == 'A：1'
    end

    it "has 1 added items" do
      subject.should have(1).added_items
      item = subject.added_items[0]
      item.name.should == :D
      item.lvalue.should be_nil
      item.rvalue.should == 5
      item.delta.should be_nil
      item.to_s.should == 'D：5'
    end
    
    it "has 1 modified items" do
      subject.should have(1).modified_items
      item = subject.modified_items[0]
      item.name.should == :C
      item.lvalue.should == 3
      item.rvalue.should == 4
      item.delta.should == 1
      item.to_s.should == 'C：3 < 4 (+1)'
    end
  end
end

