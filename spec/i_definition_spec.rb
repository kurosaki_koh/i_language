# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')

require_relative '../parser/i_definition.rb'

class IDefinition < ActiveRecord::Base
  establish_connection( DatabaseCconfig["test"])
end

describe IDefinition do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end

  def prepare_defs
    south = IDefinition.new
    south.definition = <<EOT
Ｌ：南国人　＝　｛
　ｔ：名称　＝　南国人（人）
　ｔ：要点　＝　布地の少ない服装，小麦色の肌で健康的な人材，金色の髪
　ｔ：周辺環境　＝　密林，古代遺跡，洪水対策された家，沼沢，豊かな動植物相
　ｔ：評価　＝　体格－１，筋力１，耐久力１，外見０，敏捷１，器用０，感覚０，知識０，幸運０
　ｔ：特殊　＝　｛
　　＊南国人の人カテゴリ　＝　，，基本人アイドレス。
　　＊南国人の食料変換　＝　，，（生産フェイズごとに）食料＋１万ｔ。
　　＊南国人の生物資源消費　＝　，，（生産フェイズごとに）生物資源－１万ｔ。
　　＊南国人のイベント時食料消費　＝　，条件発動，（一般行為判定を伴うイベントに参加するごとに、食事として）食料－１万ｔ。
　｝
　ｔ：→次のアイドレス　＝　猫妖精（職業），ドラッガー（職業），歩兵（職業），パイロット（職業），医師（職業），学生（職業），観光地（施設），国歌（絶技），アイドレス工場（施設），食糧生産地（施設），高位南国人（人）
｝
EOT
    south.save
    
    s16 = IDefinition.new
    s16.definition = <<EOT
Ｌ：南国人　＝　｛
　ｔ：名称　＝　南国人（人）
　ｔ：要点　＝　布地の少ない服装，小麦色の肌で健康的な人材，金色の髪
　ｔ：周辺環境　＝　密林，古代遺跡，洪水対策された家，沼沢，豊かな動植物相
　ｔ：評価　＝　体格１６，筋力１，耐久力１，外見０，敏捷１，器用０，感覚０，知識０，幸運０
　ｔ：特殊　＝　｛
　　＊南国人の人カテゴリ　＝　，，基本人アイドレス。
　　＊南国人の食料変換　＝　，，（生産フェイズごとに）食料＋１万ｔ。
　　＊南国人の生物資源消費　＝　，，（生産フェイズごとに）生物資源－１万ｔ。
　　＊南国人のイベント時食料消費　＝　，条件発動，（一般行為判定を伴うイベントに参加するごとに、食事として）食料－１万ｔ。
　｝
　ｔ：→次のアイドレス　＝　猫妖精（職業），ドラッガー（職業），歩兵（職業），パイロット（職業），医師（職業），学生（職業），観光地（施設），国歌（絶技），アイドレス工場（施設），食糧生産地（施設），高位南国人（人）
｝
EOT
    s16.revision = 16
    s16.save
  end

  it 'ターン別定義の検索' do
    prepare_defs()
    south = Definitions['南国人']
    puts south.compiled_hash['評価'].inspect
    south.compiled_hash['評価']['体格'].should == -1

    south16 = Definitions['南国人',16]
    south16.compiled_hash['評価']['体格'].should == 16
  end

  it 'Context によるターン別評価計算' do
    prepare_defs()
    troop = parse_troop <<EOT
test:南国人
EOT
    troop.evaluate('体格').should == -1

    ctx = Context.new(16)
    troop = ctx.parse_troop <<EOT
test:南国人
EOT
    troop.evaluate('体格').should == 16
  end

  it 'make_revision' do
    idef = IDefinition.new
    idef.definition = <<EOT
Ｌ：北国人　＝　｛
　ｔ：名称　＝　北国人（人）
　ｔ：要点　＝　暖かい服装，白い肌で美しい人材，白い髪
　ｔ：周辺環境　＝　針葉樹林，木もないような雪原，豊かな小麦畑，豪雪対策された家，高い山
　ｔ：評価　＝　体格１，筋力０，耐久力－１，外見１，敏捷０，器用０，感覚０，知識１，幸運０
　ｔ：特殊　＝　｛
　　＊北国人の人カテゴリ　＝　，，基本人アイドレス。
　　＊北国人の食料変換　＝　，，（生産フェイズごとに）食料＋１万ｔ。
　　＊北国人の生物資源消費　＝　，，（生産フェイズごとに）生物資源－１万ｔ。
　　＊北国人のイベント時食料消費　＝　，条件発動，（一般行為判定を伴うイベントに参加するごとに、食事として）食料－１万ｔ。
　｝
　ｔ：→次のアイドレス　＝　犬妖精（職業），魔法使い（職業），歩兵（職業），パイロット（職業），整備士（職業），国歌（絶技），アイドレス工場（施設），寮（施設），食糧生産地（施設），バトルメード（職業），高位北国人（人）
｝
EOT
    
    idef.save

    Definitions['北国人'].should_not be_nil
    Definitions['北国人',30].should be_nil

    IDefinition.make_revision(30)

    idef30= Definitions['北国人',30]
    idef30.should_not be_nil
    idef30.revision.should == 30

  end

end

