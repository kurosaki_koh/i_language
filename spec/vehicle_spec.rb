# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')


describe Vehicle do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end

  it "能力修正に誤って判定修正" do
    source = <<EOT
_define_I=D:{
チップ：チップボール：体格+3*白兵距離戦闘+2,
soldier:東国人+軌道降下兵+大剣士+剣
};
EOT

    troop = parse_troop(source)
    str = troop.evals_str
    #    troop.evaluators.find{|ev|ev.name == '敏捷による防御（通常）'}.should == evade
    puts str
  end

  it "判定修正付きの乗り物定義" do
    source = <<EOT
_define_I=D:{
チップ：チップボール：体格+3：白兵距離戦闘+2,
soldier:東国人+軌道降下兵+大剣士+剣
};
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
    str = troop.evals_str
    #    troop.evaluators.find{|ev|ev.name == '敏捷による防御（通常）'}.should == evade
    puts str
  end

  it "搭乗補正の有効判定の自動化" do
    source = <<EOT
_define_I=D:{
チップ：チップボール,
(P)soldier:歩兵＋名パイロット
};
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
    str = troop.evals_str

    soldier = @context.units['soldier']
    chip = @context.units['チップ']
    chip.all_partial_conditions.size.should == 2
    soldier.all_partial_conditions.size.should == 3
    soldier.find_spec('名パイロットの搭乗戦闘補正').is_available?.should eq false

    source = <<EOT
＃！部分有効条件：戦闘する場合
_define_I=D:{
チップ：チップボール,
soldier:歩兵＋名パイロット
};
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
    soldier = @context.units['soldier']
    chip = @context.units['チップ']

    chip_p_conds = chip.all_partial_conditions
    chip_p_conds.size.should == 3

    soldier_p_cond = soldier.all_partial_conditions
    soldier_p_cond.size.should == 3
    soldier.find_spec('名パイロットの搭乗戦闘補正').is_available?.should eq true

    source = <<EOT
_define_I=D:{
ツンユーフォー１_国有：多国籍要撃機”ユーフォー”,
pilot：スペーススターファイター;
};
EOT
    troop = parse_troop(source)
    ufo = troop.children[0]

    p_conds = ufo.all_partial_conditions
    p_conds.any?{|reg| '航空機に搭乗している場合での' =~ reg}.should_not be_nil
    sp = @context.units['pilot'].find_spec('スペーススターファイターの搭乗補正')
    sp.is_available?.should == true
 
  end

  it '偵察兵 in チップボール' do
    source = <<EOT
Ｌ：偵察兵　＝　｛
　ｔ：名称　＝　偵察兵（職業）
　ｔ：要点　＝　偵察機器，野戦服
　ｔ：周辺環境　＝　なし
　ｔ：評価　＝　体格０，筋力０，耐久力１，外見０，敏捷１，器用０，感覚１，知識１，幸運－１
　ｔ：特殊　＝　｛
　　＊偵察兵の職業カテゴリ　＝　，，派生職業アイドレス。
　　＊偵察兵の位置づけ　＝　，，歩兵系。
　　＊偵察兵の白兵距離戦闘行為　＝　歩兵，条件発動，白兵距離戦闘行為が可能。＃白兵距離戦闘評価：可能：（体格＋筋力）÷２
　　＊偵察兵の近距離戦闘行為　＝　，，近距離戦闘行為が可能。＃近距離戦闘評価：可能：（敏捷＋筋力）÷２
　　＊偵察兵の近距離戦闘補正　＝　歩兵，任意発動，（近距離での）攻撃、評価＋２、燃料－１万ｔ。＃近距離戦闘評価：可能：（敏捷＋筋力）／２
　　＊偵察兵の中距離戦闘行為　＝　歩兵，条件発動，中距離戦闘行為が可能。＃中距離戦闘評価：可能：（感覚＋知識）／２
　　＊偵察兵の偵察補正　＝　歩兵，条件発動，（偵察での）感覚、評価＋３、燃料－１万ｔ。７５％制限能力。＃偵察評価：一般：感覚
　｝
　ｔ：→次のアイドレス　＝　吉田遥（ＡＣＥ），特殊部隊員（職業），追跡者（職業）
｝

_define_I=D:{チップ：チップボール,
scout：偵察兵＋犬妖精；
};
EOT

    troop = parse_troop(source)
    scout = Evaluator['偵察']
    scouter = @context.units['scout']
    chip = @context.units['チップ']
    sym_scout = :'偵察補正'
    trace_sym = :'追跡補正'
    scouter.tqrs.include?(sym_scout).should == true
    chip.tqrs.include?(sym_scout).should == true
    troop.tqrs.include?(sym_scout).should == true

    troop.tqrs.include?(trace_sym).should eq true

    all_bonuses_cache = troop.all_bonuses_cache
    scout_sp = all_bonuses_cache.find{|sp|sp.name == '偵察兵の偵察補正'}

    troop.is_tqr_qualified?(scout_sp).should eq true
    bonus = all_bonuses_cache.find{|sp|
      scout.match?(sp)
    }
    bonus.should_not be_nil

    items = scouter.sub_evals_items(scout)
    items.size.should == 1

    #    troop.no_sp?(scout).should == false
    calc = scout.calculate(chip)
    calc.total.should == 9
    scout.evaluate(troop).should == 9
    Evaluator['追跡（感覚）'].evaluate(troop).should == 9
    Evaluator['追跡（幸運）'].evaluate(troop).should == 9

    source = <<EOT
Ｌ：偵察兵　＝　｛
　ｔ：名称　＝　偵察兵（職業）
　ｔ：要点　＝　偵察機器，野戦服
　ｔ：周辺環境　＝　なし
　ｔ：評価　＝　体格０，筋力０，耐久力１，外見０，敏捷１，器用０，感覚１，知識１，幸運－１
　ｔ：特殊　＝　｛
　　＊偵察兵の職業カテゴリ　＝　，，派生職業アイドレス。
　　＊偵察兵の位置づけ　＝　，，歩兵系。
　　＊偵察兵の白兵距離戦闘行為　＝　歩兵，条件発動，白兵距離戦闘行為が可能。＃白兵距離戦闘評価：可能：（体格＋筋力）÷２
　　＊偵察兵の近距離戦闘行為　＝　，，近距離戦闘行為が可能。＃近距離戦闘評価：可能：（敏捷＋筋力）÷２
　　＊偵察兵の近距離戦闘補正　＝　歩兵，任意発動，（近距離での）攻撃、評価＋２、燃料－１万ｔ。＃近距離戦闘評価：可能：（敏捷＋筋力）／２
　　＊偵察兵の中距離戦闘行為　＝　歩兵，条件発動，中距離戦闘行為が可能。＃中距離戦闘評価：可能：（感覚＋知識）／２
　　＊偵察兵の偵察補正　＝　歩兵，条件発動，（偵察での）感覚、評価＋３、燃料－１万ｔ。７５％制限能力。＃偵察評価：一般：感覚
　｝
　ｔ：→次のアイドレス　＝　吉田遥（ＡＣＥ），特殊部隊員（職業），追跡者（職業）
｝

_define_I=D:{チップ：チップボール,
scout：偵察兵＋犬妖精；
soldier:歩兵:感覚+3*幸運+3;
};
EOT
    troop = parse_troop(source)
    scouter = @context.units['scout']
    chip = @context.units['チップ']
    scouter.tqrs.include?(:'偵察補正').should == true
    chip.tqrs.include?(:'偵察補正').should == false
    troop.team_tqrs.include?(:'偵察補正').should == false

    scouter.tqrs.include?(:'追跡補正').should == true
    chip.team_tqrs.should_not include(:'追跡補正')
    chip.self_tqrs.should_not include(:'追跡補正')
    chip.tqrs.should_not include(:'追跡補正')
    troop.tqrs.should_not include(:'追跡補正')

    scout.evaluate(troop).should == 7
    Evaluator['追跡（感覚）'].evaluate(troop).should == 7
    Evaluator['追跡（幸運）'].evaluate(troop).should == 8
    
  end

  it 'Vehicle#specialties' do
    source =<<EOT
_define_I=D: {
チップ２_国有：チップボール,
（Ｐ）18-00426-01_タルク：詩歌の民＋犬妖精＋銃士隊＋竜士隊＋補給士官：敏捷+1*知識+1*幸運+1
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00426-01%A1%A7%A5%BF%A5%EB%A5%AF [^]
}；
EOT
    context = Context.new
    troop = context.parse_troop(source)

    chip = troop.children[0]
    (defined? chip.specialties).should_not be_nil
    puts (defined? chip.specialties)
    chip.class.should == Vehicle
    chip.is_regarded_as_soldier?.should eq true
    puts chip.methods.sort
    chip.specialties.should_not be_nil

  end

  it 'チップボール搭乗者に自動成功持ち' do
    source = <<EOT
_define_I=D:{
チップボール・月の１号：チップボール：体格+4,
13-00276-01_坂下真砂：北国人＋わんわん偵察兵＋追跡者＋摂政＋ガンマン＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*知識+6*幸運+2；
};
EOT
    context = Context.new
    troop = context.parse_troop(source)
    scout = Evaluator['追跡（感覚）']
    scout_cal = scout.calculate(troop)
    scout_cal.total.should == '自動成功'
    scout_cal.arguments['感覚'].should_not be_nil
    scout_cal.arguments['感覚'].total.should == '自動成功'
    
  end
  it '陣地構築' do
    source = <<EOT
32-00644-01_鴻屋　心太：東国人＋サイボーグ＋大剣士＋王：敏捷+1*知識+1*幸運+1;

_define_I=D:{
ザ・グレート・エビゾー：チップボール：体格+4/* Ｔ１５巨大工廠生産品につき */,
32-00643-01_夜薙当麻：東国人＋軌道降下兵＋大剣士＋剣：敏捷+1*知識+1*幸運+1;
－カトラス：個人所有：白兵距離戦闘行為，歩兵，条件発動，（白兵距離での）｛攻撃，防御，移動｝、評価＋２。片手持ち武器。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00643-01%A1%A7%CC%EB%C6%E5%C5%F6%CB%E3
－王の契約による強化：契約した王と距離１０ｍ以内で一緒に行動する間の）全判定、評価＋３。
－鴻屋王の特産品による強化：（＜王の特産品による強化の特殊能力＞で選ばれた）全判定、評価＋２。＃「白兵戦の全判定に＋２」とする。
};

＃！特殊無効：カトラスの白兵距離戦闘補正，ザ・グレート・エビゾー
EOT
    troop = parse_troop(source)
    chip = troop.children[1]

    sp = chip.find_spec('チップボールの陣地構築補正')
    trench = Evaluator['陣地構築（筋力）']
    troop.no_sp?(trench).should eq false
    trench.evaluate(troop).should == 24
  end
  it '乗り物・歩兵混合編成での評価抑止' do
    source =<<EOT
soldier:歩兵；
_define_I=D:{
チップ：チップボール,
soldier:歩兵＋名パイロット
};
EOT
    troop = parse_troop(source)
    evs = troop.evaluators

    evs.find{|ev|ev.name == '魅力'}.should be_nil
    evs.find{|ev|ev.name == '歌唱'}.should be_nil
    evs.find{|ev|ev.name == '隠蔽'}.should be_nil
    evs.find{|ev|ev.name == '陣地構築（知識）'}.should be_nil

  end

  it '搭乗補正条件の自動判定' do
    source =<<EOT
＃！有効条件：位置づけ（戦車）の乗り物に乗っている場合

_define_I=D:{
３３００：玲瓏＋ＨＱ敏捷＋ＨＱ敏捷, ＃国有
（Ｐ）33-00647-01_ＧＥＮＺ：高位西国人+バーニングパイロット+スペーススターシップオフィサー+ＧＥＮＺの騎士+ＳＨＱ感覚+参謀：敏捷+1*感覚+5*知識+1*幸運+2*知識+3；
－個人ＨＱＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00647-01%A1%A7%A3%C7%A3%C5%A3%CE%A3%DA
（ＣＰ）33-00663-01_ジン：高位西国人+マシンマイスター+ＧＥＮＺの騎士+戦車兵+ＨＱ知識：敏捷+1*知識+3；
};
EOT
    troop = parse_troop(source)

    id3300 = troop.children[0]
    genz = id3300.children[0]
    jin = id3300.children[1]

    burning = genz.find_spec('バーニングパイロットの搭乗補正')
    genz_apc = genz.all_partial_conditions
    jin_apc =  jin.all_partial_conditions
    meister = jin.find_spec('マシンマイスターのコパイロット補正')
    genz_apc.find{|re|'乗り物に搭乗している場合での' =~ re}.should_not be_nil
    burning.is_available?.should eq true
    meister.is_available?.should eq true
    gk1 = genz.find_spec('ＧＥＮＺの騎士の搭乗補正')
    gk1.is_available?.should eq true
    gk2 = jin.find_spec('ＧＥＮＺの騎士の搭乗補正')
    gk2.is_available?.should eq true

    tk = jin.find_spec('戦車兵の感覚・知識補正')
    tk.is_available?.should eq true
    tk.is_applicable?(jin,'知識').should eq true
  end

  it 'パイロット・コパイロットでの搭乗が条件となる特殊の判定自動化' do
    source = <<EOT
＃！部分有効条件：乗り物に搭乗して

_define_I=D:{Ｍ１_騎士団所有：ミラーコート（帝國軍仕様）+HQ感覚+SHQ感覚+SHQ体格,
（Ｐ）44-00229-01_悪童屋　四季：西国人+帝國軍元帥+旅人”悪童版”+テストパイロット+参謀：外見+2*敏捷+1*知識+1*幸運+1,
－恩寵の短剣：個人所有：白兵＋１（着用型／その他）
－恩寵の時計：個人所有：外見＋２（着用型／首）
－個人ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00229-01%A1%A7%B0%AD%C6%B8%B2%B0%A1%A1%BB%CD%B5%A8
（Ｐ）36-xx038-xx_実葉：西国人＋舞踏子＋魔術的舞踏子＋魔法少女+HQ感覚+HQ感覚,
（ＣＰ）14-00798-01_たらすじ：高位北国人+名パイロット+黒い舞踏子+ブリザードパイロット+ＳＨＱ感覚,
（ＣＰ）36-xx040-xx_紫苑：西国人＋舞踏子＋魔術的舞踏子＋魔法少女+HQ感覚+HQ感覚
};
EOT

    troop = parse_troop(source)
    vehicle = troop.children[0]

    expect(vehicle).to respond_to :default_partial_conditions

    pilot2 = @context.units['36-xx038-xx_実葉']
    expect(pilot2).to respond_to :default_partial_conditions
    expect(pilot2.default_partial_conditions.find{|re| 'パイロットとして搭乗している場合での' =~ re}).to  be_truthy

    pilot_sp = pilot2.find_spec('魔術的舞踏子の搭乗補正')
    pilot_sp.is_available?.should eq true
    cop2 = @context.units['36-xx040-xx_紫苑']
    cop2_conds = cop2.all_conditions
    cop2_conds.include?('（パイロットとして搭乗している場合での）').should_not be_truthy
    copilot_sp = cop2.find_spec('魔術的舞踏子の搭乗補正')
    copilot_sp.is_available?.should be_falsey
    (true & true  & copilot_sp.is_available? & copilot_sp.is_ability_match?('体格')).should be_falsey
    copilot_sp.is_applicable?(vehicle,'体格').should be_falsey
    vehicle.is_evaluate_item?(copilot_sp).should be_falsey
    vehicle.is_regarded_as_soldier?.should be_falsey

    e_items = vehicle.evaluate_items()
    e_items.include?(copilot_sp).should be_falsey
  end

  it '航空機編成での禁止行為' do
    source =<<EOT
_define_I=D:{
Ｕ４_国有：多国籍要撃機”ユーフォー”，
－多国籍要撃機”ユーフォー”の増槽
（Ｐ）06-00147-01_霰矢蝶子：愛の民＋整備士２＋魔術的舞踏子＋やわらか舞踏子＋ＳＨＱ知識＋ＨＱ感覚＋霰矢惣一郎の恋人：敏捷+1*器用+1*知識+1*幸運+2*外見+2*幸運+1;
＃　－恩寵の時計：個人所有：外見＋２（着用型／首）
＃　－クローバーのしおり：個人所有：幸運＋１
　－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／手首）
　－アンデットバスター：個人所有：対アンデット、攻撃修正＋５（着用型／その他）
　－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00147-01%A1%A7%F0%C7%CC%F0%C4%B3%BB%D2
（ＣＰ）06-00161-01_矢神サク：愛の民＋整備士２＋魔術的舞踏子＋やわらか舞踏子＋ＳＨＱ知識＋ＨＱ感覚＋ヤガミの恋人＋ＨＱ幸運＋ＨＱ幸運＋ＨＱ外見：敏捷＋1*知識＋1*幸運＋2;
};
EOT
    troop = parse_troop(source)
    actions = troop.action_names
    actions.include?('歌唱行為').should eq false
    actions.include?('陣地構築行為').should eq false

    ufo_actions = troop.children[0].action_names
    ufo_actions.include?('オペレート行為').should eq false
    ufo_actions.include?('整備行為').should eq false

  end
  it 'I＝Dに載ったら、兵科種別が一致する限り、中の人の可能行為も可能とみなす。' do
    source = <<EOT
_define_I=D:{ブルーデカショー：デカショー,
（Ｐ）24-xx001-xx_ルーフ：南国人+騎士+歩兵+パイロット；
};
EOT
    troop = parse_troop(source)
    actions = troop.action_names
    actions.should include('白兵距離戦闘行為')
    actions.should_not include('遠距離戦闘行為')
  end
  it '乗り物での攻撃補正属性は「白兵」「射撃」のカテゴリレベルでしか排他処理を行わない' do
    source = <<EOT
_define_I=D:{ダンさん：ダンボール２，
dog:犬妖精,
fencer:剣士,
};
EOT
    troop = parse_troop(source)
    melee = troop.find_evaluator('白兵戦')
    calc = melee.calculate(troop)
    calc.total.should == 26

    source = <<EOT
_define_I=D:{ダンさん：ダンボール２，
soldier:帝國軍歩兵,
fencer:剣士,
};
EOT
    troop = parse_troop(source)
    melee = Evaluator['近距離戦']
    melee.evaluate(troop).should == 15
  end

  it 'ＡＣＥ搭乗編成' do
    source = <<EOT
_define_I=D:{
ユーカ_国有：ユーカ＋ＳＨＱ対空戦闘＋ＨＱ対空戦闘,
（P）あゆみの晋太郎：あゆみの晋太郎（夫婦）＋ＵＨＱ＋ＳＨＱ＋ＳＨＱ：;
（CP）03-00045-01_久珂あゆみ：高位西国人＋ＷＳＯ＋ナイスバディ＋撃墜王＋ＳＨＱ感覚＋ＵＨＱ感覚＋ＳＨＱ感覚＋ＨＱ感覚＋晋太郎の奥さん（あゆみ版）＋ＨＱ器用＋ＨＱ感覚＋ＳＨＱ幸運＋ＳＨＱ感覚＋ＨＱ外見：敏捷+1*器用+1*知識+1*幸運+2
};
EOT
    troop = parse_troop(source)
    troop.evaluate('体格').should == 41
    troop.evaluate('感覚').should == 60
  end

  it '乗り物の操縦評価' do
    source = <<EOT
_define_I=D:{
三号機_になし藩国所有：ＲＴＲ,
－増加燃料装備：全能力、評価－３。ＡＲ＋３。（装甲による）防御、評価＋６。＋されているＡＲを使うと評価、装甲を元に戻す選択を行うことができる。
－ＲＴＲの対空ミサイル：対空を遠距離評価＋２で撃てる（2発装備）
（Ｐ）29-00548-01_下丁：はてない国人+騎士+黒騎士+赤髪の乗り手＋ぽちの騎士：敏捷+1*幸運+2;
－カトラス：歩兵武装，条件発動，（白兵（剣）、白兵距離での）｛攻撃，防御，移動｝、評価＋２。（着用型／片手）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00548-01%A1%A7%B2%BC%C3%FA
（ＣＰ）29-xx010-xx_ゆず：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）29-xx011-xx_ショウタ：はてない国人＋犬妖精２＋黒騎士＋騎士;
};
_define_I=D:{
二号機_になし藩国所有：ＲＴＲ,
－増加燃料装備：全能力、評価－３。ＡＲ＋３。（装甲による）防御、評価＋６。＋されているＡＲを使うと評価、装甲を元に戻す選択を行うことができる。
－ＲＴＲの対空ミサイル：対空を遠距離評価＋２で撃てる（2発装備）
（Ｐ）29-00559-01_九重　千景：はてない国人+警官+黒騎士+赤髪の乗り手＋ぽちの騎士：敏捷+1*器用+5*知識+1*幸運+2;
－カトラス：歩兵武装，条件発動，（白兵（剣）、白兵距離での）｛攻撃，防御，移動｝、評価＋２。（着用型／片手）
－恩寵の時計：外見、評価＋２。（着用型／首）
－猫と犬の前足が重なった腕輪：同調、評価＋３。同能力重複適用不可。（着用型／腕）
－レトロライフ：個人所有：，，，（レトロライフを着用する者の）｛感覚，白兵距離戦闘，詠唱戦闘，防御｝、評価＋２。（組織技術）
＃－詳細：http://cwtg.jp/bbs2/wforum.cgi?pastlog=0010&no=28338&act=past&mode=allread#28458
＃－http://syaku003.appspot.com/entry/show/47021
＃になし藩のレトロライフは性能更新されておらず、評価＋２のままです。ただし機体搭乗時は機能しません。
－念力：　，，，筋力、評価＋２
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00559-01%A1%A7%B6%E5%BD%C5%A1%A1%C0%E9%B7%CA
（ＣＰ）29-xx008-xx_ラック：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）29-xx009-xx_ルウ：はてない国人＋犬妖精２＋黒騎士＋騎士;
};

_define_I=D:{
一号機_になし藩国所有：ＲＴＲ,
－増加燃料装備：全能力、評価－３。ＡＲ＋３。（装甲による）防御、評価＋６。＋されているＡＲを使うと評価、装甲を元に戻す選択を行うことができる。
－ＲＴＲの対空ミサイル：対空を遠距離評価＋２で撃てる（2発装備）
（Ｐ）TEST：はてない国人+ハイ・パイロット;
（ＣＰ）TEST2：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）TEST3：はてない国人＋犬妖精２＋黒騎士＋騎士;
};

_define_I=D:{
４号機_になし藩国所有：ＲＴＲ,
－増加燃料装備：全能力、評価－３。ＡＲ＋３。（装甲による）防御、評価＋６。＋されているＡＲを使うと評価、装甲を元に戻す選択を行うことができる。
－ＲＴＲの対空ミサイル：対空を遠距離評価＋２で撃てる（2発装備）
（Ｐ）TEST4：はてない国人+超撃墜王;
（ＣＰ）TEST5：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）TEST6：はてない国人＋犬妖精２＋黒騎士＋騎士;
};

EOT
    troop = parse_troop(source)
    control_i = troop.find_evaluator('操縦')
    control_i.should_not be_nil
    rtr3 = troop.children[0]

    rtr3_calc = control_i.calculate(rtr3)
    calc = control_i.calculate(rtr3)
    team_calc = control_i.calculate(troop)

    sub_calc = team_calc.children[0]
    sub_calc.class.should == Calculation
    sub_calc.unit.name == '三号機_になし藩国所有'
    sub_calc.total.should == 21

    team_calc.children.size.should == 4
    team_calc.children[1].total.should == 24
    team_calc.children[2].total.should == 23
    team_calc.children[3].total.should == '自動成功'

    result = troop.action_evals_str
    puts result

  end


  it '同調（機内）' do
    source = <<EOT
＃！個別付与：猫と犬の前足が重なった腕輪，本隊
＃！個別付与：教導パイロットの指導，本隊

_define_I=D:{
一号機_になし藩国所有：ＲＴＲ,
－増加燃料装備：全能力、評価－３。ＡＲ＋３。（装甲による）防御、評価＋６。＋されているＡＲを使うと評価、装甲を元に戻す選択を行うことができる。
－ＲＴＲの対空ミサイル：対空を遠距離評価＋２で撃てる（2発装備）
（Ｐ）29-00550-01_瑠璃:はてない国人＋赤髪の乗り手＋黒騎士＋警官＋ぽちの騎士：敏捷+1*知識+1;
－法の執行者の紋章：個人所有：（法執行時での）全判定、評価＋２。（着用型／指先）
－きれいなクラゲ：個人所有：見ていると無限に時間を消費できる。ついでに１／６でいいアイデアを教えてもらえるかも知れない。この特殊は１ターンに１度使うことができる。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00550-01%A1%A7%CE%DC%CD%FE
（ＣＰ）29-00596-01_ポレポレ・キブルゥ：はてない国人＋犬妖精２＋黒騎士＋騎士＋ギャグ畑：筋力+1*敏捷+1*器用+1*知識+1*幸運+1;
－法の執行者の紋章：個人所有：（法執行時での）全判定、評価＋２。（着用型／指先）
－ボディピストル：個人所有：近距離戦闘行為が可能。（射撃（銃）、近距離での）攻撃、評価＋２。（着用型／片手もち）
－多目的ナイフ：個人所有：（戦闘以外での）器用、評価＋１。（着用型／その他）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00596-01%A1%A7%A5%DD%A5%EC%A5%DD%A5%EC%A1%A6%A5%AD%A5%D6%A5%EB%A5%A5
（ＣＰ）42-00606-01_ＧＴ：北国人＋撃墜王＋トップガン＋教導パイロット：敏捷+1*知識+1*幸運+1;
};

_define_I=D:{
二号機_になし藩国所有：ＲＴＲ,
－増加燃料装備：全能力、評価－３。ＡＲ＋３。（装甲による）防御、評価＋６。＋されているＡＲを使うと評価、装甲を元に戻す選択を行うことができる。
－ＲＴＲの対空ミサイル：対空を遠距離評価＋２で撃てる（2発装備）
（Ｐ）29-00559-01_九重　千景：はてない国人+警官+黒騎士+赤髪の乗り手＋ぽちの騎士：敏捷+1*器用+5*知識+1*幸運+2;
－カトラス：歩兵武装，条件発動，（白兵（剣）、白兵距離での）｛攻撃，防御，移動｝、評価＋２。（着用型／片手）
－恩寵の時計：外見、評価＋２。（着用型／首）
－猫と犬の前足が重なった腕輪：同調、評価＋３。同能力重複適用不可。（着用型／腕）
－レトロライフ：個人所有：，，，（レトロライフを着用する者の）｛感覚，白兵距離戦闘，詠唱戦闘，防御｝、評価＋２。（組織技術）
－念力：　，，，筋力、評価＋２
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00559-01%A1%A7%B6%E5%BD%C5%A1%A1%C0%E9%B7%CA
（ＣＰ）29-xx008-xx_ラック：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）29-xx009-xx_ルウ：はてない国人＋犬妖精２＋黒騎士＋騎士;
};

_define_I=D:{
三号機_になし藩国所有：ＲＴＲ,
－増加燃料装備：全能力、評価－３。ＡＲ＋３。（装甲による）防御、評価＋６。＋されているＡＲを使うと評価、装甲を元に戻す選択を行うことができる。
－ＲＴＲの対空ミサイル：対空を遠距離評価＋２で撃てる（2発装備）
（Ｐ）29-00548-01_下丁：はてない国人+騎士+黒騎士+赤髪の乗り手＋ぽちの騎士：敏捷+1*幸運+2;
－カトラス：歩兵武装，条件発動，（白兵（剣）、白兵距離での）｛攻撃，防御，移動｝、評価＋２。（着用型／片手）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00548-01%A1%A7%B2%BC%C3%FA
（ＣＰ）29-xx010-xx_ゆず：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）29-xx011-xx_ショウタ：はてない国人＋犬妖精２＋黒騎士＋騎士;
};

EOT

    troop = parse_troop(source)
    troop.tags << :ninashi

    sync = troop.evaluators.find{|ev|ev.name == '同調（個別）'}
    sync.should_not be_nil

    s_calcset = sync.calculate(troop)
    s_calcset.children.size.should == 3
    s_calcset.class.should == CalculationSet
    s_calcset.children[0].children.size.should == 3
    sub_calc = s_calcset.children[0]
    sub_calc.class.should == SynchronizeCalculation
    p_bonuses = sub_calc.parent_bonuses
    p_bonuses.size.should == 2
    p_bonuses.collect{|sp|sp.bonus}.sum.should == 5
    sub_calc.base_value.should == 15
    sub_calc.total.should == 17
    sub_calc.inspect_result.should match(/ＧＴ：17/)

    s_calcset.children[1].total.should == 14
  end

  it '機内同調者の選別（一部が犬・猫士）' do
    source = <<EOT
_define_I=D:{
RTR-I：ＲＴＲ,
－ＲＴＲの対空ミサイル：対空を遠距離評価＋２で撃てる（2発装備）
（Ｐ）29-00548-01_下丁：はてない国人+騎士+黒騎士+赤髪の乗り手＋ぽちの騎士：敏捷+1*幸運+2;
（ＣＰ）29-xx010-xx_ゆず：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）29-xx011-xx_ショウタ：はてない国人＋犬妖精２＋黒騎士＋騎士：外見+3;
};
EOT

    troop = parse_troop(source)
    rtr = troop.children[0]
    rtr.children[0].is_qualified_as_leader?.should eq true
    rtr.children[1].is_qualified_as_leader?.should eq false

    rtr.crew_division.are_all_members_npc?.should eq false
    sync = troop.evaluators.find{|ev|ev.name == '同調（個別）'}
    s_calcset = sync.calculate(troop)
    sub_calc = s_calcset.children[0]
    sub_calc.inspect_result.should match(/下丁：5/)
  end

  it '機内同調者の選別（全員が犬・猫士）' do
    source = <<EOT
_define_I=D:{ＲＴ１０：ＲＴＲ,
(P)43-xx027-xx_菊花：西国人＋帝國軍歩兵＋スターライナー＋スターリフター＋ＨＱ感覚；
(CP)43-xx032-xx：聡慎：北国人＋帝國軍歩兵＋はぐれメード＋ナースメード２；
(CP)43-xx033-xx：呂旋：北国人＋帝國軍歩兵＋はぐれメード＋ナースメード２
}；
EOT

    troop = parse_troop(source)
    rtr = troop.children[0]
    rtr.children[0].is_qualified_as_leader?.should eq false
    rtr.children[1].is_qualified_as_leader?.should eq false

    rtr.crew_division.are_all_members_npc?.should eq true
    sync = troop.evaluators.find{|ev|ev.name == '同調（個別）'}
    s_calcset = sync.calculate(troop)
    sub_calc = s_calcset.children[0]
    sub_calc.inspect_result.should match(/聡慎：12 \/ 呂旋：12/)
  end

  it 'I=D・航空機入り編成での一般行為禁則' do
    #質疑 http://cwtg.jp/qabbs/bbs2.cgi?action=article&id=10320 の回答待ち。
    source = <<EOT
soldier:歩兵＋暗殺者；
_define_I=D:{チップ：チップボール,
pilot：南国人+歩兵+パイロット+名パイロット；
};
_define_I=D:{ブルーデカショー：デカショー,
（Ｐ）24-xx001-xx_ルーフ：南国人+歩兵+パイロット+名パイロット；
};
EOT
    troop = parse_troop(source)

    actions = troop.action_names
    actions.should_not include('侵入行為')
    actions.should_not include('隠蔽行為')
    actions.should_not include('陣地構築行為')
    actions.should_not include('歌唱行為')

    result = troop.action_evals_str
    result.should_not match(/侵入/)
    result.should_not match(/陣地構築/)
    result.should_not match(/隠蔽/)

    source2 = <<EOT
soldier:歩兵＋暗殺者；
_define_I=D:{ブルドッグ１：ブルドック,
pilot1：南国人+歩兵+パイロット+名パイロット；
pilot2：南国人+歩兵+パイロット+名パイロット；
pilot3：南国人+歩兵+パイロット+名パイロット；
};
EOT

    troop = parse_troop(source2)

    actions = troop.action_names
    actions.should_not include('侵入行為')
    actions.should_not include('隠蔽行為')
    actions.should include('陣地構築行為')
    actions.should_not include('歌唱行為')

    result = troop.action_evals_str
    result.should_not match(/侵入/)
    result.should match(/陣地構築/)
    result.should_not match(/隠蔽/)
  end

  it 'チップボール等の歩兵用I=Dは一般行為禁則の対象とならない（歩兵とみなす）' do
    #質疑 http://cwtg.jp/qabbs/bbs2.cgi?action=article&id=10320 の回答待ち。
    source = <<EOT
soldier:歩兵；
_define_I=D:{チップ：チップボール,
（Ｐ）24-xx001-xx_ルーフ：南国人+歩兵+パイロット+名パイロット；
};
EOT
    troop = parse_troop(source)

    actions = troop.action_names
    actions.should include('侵入行為')
    actions.should include('隠蔽行為')
    actions.should include('陣地構築行為')
    result = troop.action_evals_str
    result.should match(/侵入/)
    result.should match(/陣地構築（筋力）/)
    result.should_not match(/陣地構築（知識）/)
  end
  
  it 'チップボールに乗ったレコンの偵察補正がコストに加算されない' do
    source = <<EOT
＃T14当時、偵察補正に７５％制限はついていなかったため。
ｌ：＊レコンの偵察補正　＝　歩兵，条件発動，（偵察での）感覚、評価＋３、燃料－１万ｔ。

_define_I=D:{
チップボール３_国有：チップボール:体格+3/* 宰相府大規模工廠T14製品 */，
（Ｐ）45-00720-01_ホーリー：東国人＋ウォードレスダンサー＋甲殻型ウォードレスダンサー＋レコン＋補給士官：敏捷+1*知識+3*幸運+2；
－高機能ハンドヘルド：個人所有：情報戦可・情報戦＋３（着用型／武器（片手装備））
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00720-01%A1%A7%A5%DB%A1%BC%A5%EA%A1%BC
};
EOT

    troop = parse_troop(source)
    troop.action_evals_str
  end

  it '搭乗者の行為補正を乗機でフルブースト継承' do
    source = <<EOT
_define_I=D: {ＲＴＲ２_騎士団所有：ＲＴＲ,
（Ｐ）29-00542-01_になし：はてない国人＋騎士＋黒騎士＋赤髪の乗り手＋ぽちの騎士：知識+1*幸運＋2
（ＣＰ）12-00258-02_露木　ツカサ：北国人+名パイロット+ホープ+ハイガード：知識+2；
（ＣＰ）45-00426-01_タルク：初心の民＋犬妖精２＋帝國軍歩兵＋バトルメード＋法の司：敏捷＋1*知識＋3*幸運＋1;
};
EOT
    troop = parse_troop(source)

    rtr = troop.children[0]

    sp = rtr.find_spec('黒騎士の白兵距離戦闘補正')
    sp.should_not be_nil
    Evaluator['白兵戦'].evaluate(troop).should == 37
  end

  it '水中中距離戦闘行為と中距離戦闘補正' do
    source = <<EOT
_define_I=D: {
ソットヴォーチェ・ゼーロ_詩歌藩国所有：水竜ソットヴォーチェ２＋ＨＱ感覚,
（Ｐ）18-00341-01_九音・詩歌：詩歌の民＋龍の乗り手＋吟遊詩人＋ドラゴンシンパシー＋ＨＱ知識＋ＨＱ知識+星見司２：敏捷+1*器用+1*知識+1*幸運+1;
（ＣＰ）18-00347-01_駒地真子：詩歌の民＋犬妖精２＋吟遊詩人＋ドラゴンシンパシー+ＨＱ知識+ＨＱ知識+参謀：敏捷+1*器用+1*知識+1*幸運+2;
（ＣＰ）18-00356-01_岩崎経：詩歌の民＋犬妖精２＋吟遊詩人＋ドラゴンシンパシー+ＨＱ知識+ＨＱ知識+法の司：敏捷+1*幸運+1;
（ＣＰ）18-xx000-xx_シィ：詩歌の民＋犬妖精２＋銃士隊＋竜士隊;
（ＣＰ）18-xx037-xx_ももきち：詩歌の民＋犬妖精２＋銃士隊＋竜士隊;
};
EOT
    troop = parse_troop(source)

    ev = Evaluator['水中中距離戦']

    sot = troop.children[0]
    sot.tqr_appendance([:'水中中距離戦闘行為']).should == [:'中距離戦闘行為']
    sot.tqr_appendance([:'中距離戦闘行為']).should == []
    sot.tqrs.include?(:'中距離戦闘行為').should eq true
    sot.team_tqrs.include?(:'中距離戦闘行為').should eq false
    sot.self_tqrs.include?(:'中距離戦闘行為').should eq true

    troop.tqrs.include?(:'中距離戦闘行為').should eq true
    sp = sot.children[4].find_spec('銃士隊の中距離戦闘補正')
    sot.is_fullboosted_sp?(sp ).should be_truthy
    calc = ev.calculate(troop)
    calc.total.should == 53
  end

  it 'チップボール搭乗のレコンの偵察補正もコストに計上する' do
    source = <<EOT
_define_I=D:{
チップボール１_国有：チップボール，
scout：レコン；
｝；
EOT
    troop = parse_troop(source)

    troop.action_evals_str

    sps = []
    costs = troop.total_cost{|u,sp| sps << sp}

    sp = sps.find{|sp|sp.name == 'レコンの偵察補正'}
    sp.should_not be_nil

    costs.should == {'燃料' => -6}
  end

  it 'ACE+チップ+互尊Ｃの組み合わせを正しく処理できる' do
    source = <<EOT
_define_I=D:{
サンプル：チップボール２,
（P）パイロット1：ポレポレ;
－互尊Ｃ
};
EOT
    troop = parse_troop(source)
    proc {troop.action_evals_str}.should_not raise_error
  end

  it '無人機体の記述' do
    source = <<EOT
_define_I=D:{とらりすさん01：ラグドール＋トラリスの融合｝；
_define_I=D:{とらりすさん02：ラグドール
－トラリスの融合
｝；

Ｌ：トラリスの融合　＝　｛
　ｔ：名称　＝　トラリスの融合（定義）
　ｔ：特殊　＝　｛
　　＊トラリスの融合の全能力補正　＝　乗り物，，全能力，評価＋６。
　｝
｝
EOT
    troop = parse_troop(source)
    proc {troop.action_evals_str}.should_not raise_error
    puts troop.action_evals_str
  end


  it 'オプション装備の乗算記法' do
    source = <<EOT
_define_I=D:{
一号機_になし藩国所有：ＲＴＲ,
－増加燃料装備：全能力、評価－３。ＡＲ＋３。（装甲による）防御、評価＋６。＋されているＡＲを使うと評価、装甲を元に戻す選択を行うことができる。
－ＲＴＲの爆弾＊２：対空を遠距離評価＋２で撃てる（2発装備）
（Ｐ）TEST：はてない国人+ハイ・パイロット;
（ＣＰ）TEST2：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）TEST3：はてない国人＋犬妖精２＋黒騎士＋騎士;
};
EOT
    troop = parse_troop(source)
    puts troop.action_evals_str
    puts troop.evaluators.collect{|ev|ev.name}.join(" , ")
    puts troop.evaluators.collect{|ev|ev.display_name}.join(" , ")
  
    troop.should_not be_nil

    rtr = troop.children[0]
    rtr.all_idresses.should == %w[ＲＴＲ 増加燃料装備 ＲＴＲの爆弾 ＲＴＲの爆弾]
    puts troop.action_evals_str
    troop.context.particular_evaluators.size.should == 2

    ev = troop.find_evaluator('遠距離戦')
    ev.disabled_sps.size.should == 2
    ev.team_alternatives[1].prototype.object_id.should == ev.object_id
    ev.team_alternatives[1].disabled_sps.size.should == 2

    ev.evaluate(troop).should == 39
    source = <<EOT
ｌ：オプション装備数での評価派生処理　←　無効

_define_I=D:{
一号機_になし藩国所有：ＲＴＲ,
－増加燃料装備：全能力、評価－３。ＡＲ＋３。（装甲による）防御、評価＋６。＋されているＡＲを使うと評価、装甲を元に戻す選択を行うことができる。
－ＲＴＲの爆弾＊２：対空を遠距離評価＋２で撃てる（2発装備）
（Ｐ）TEST：はてない国人+ハイ・パイロット;
（ＣＰ）TEST2：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）TEST3：はてない国人＋犬妖精２＋黒騎士＋騎士;
};
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
    troop.action_evals_str

    troop.context.particular_evaluators.size.should == 0

    source = <<EOT
_define_I=D:{
一号機_になし藩国所有：ＲＴＲ,
－増加燃料装備：全能力、評価－３。ＡＲ＋３。（装甲による）防御、評価＋６。＋されているＡＲを使うと評価、装甲を元に戻す選択を行うことができる。
－ＲＴＲの対空ミサイル＊８：対空を遠距離評価＋２で撃てる（2発装備）
（Ｐ）TEST：はてない国人+ハイ・パイロット;
（ＣＰ）TEST2：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）TEST3：はてない国人＋犬妖精２＋黒騎士＋騎士;
};

_define_I=D:{
２号機_になし藩国所有：ＲＴＲ,
－増加燃料装備：全能力、評価－３。ＡＲ＋３。（装甲による）防御、評価＋６。＋されているＡＲを使うと評価、装甲を元に戻す選択を行うことができる。
－ＲＴＲの対空ミサイル＊４：対空を遠距離評価＋２で撃てる（2発装備）
－ＲＴＲの爆弾＊４：対空を遠距離評価＋２で撃てる（2発装備）
（Ｐ）TESTb：はてない国人+ハイ・パイロット;
（ＣＰ）TESTb2：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）TESTb3：はてない国人＋犬妖精２＋黒騎士＋騎士;
};
EOT
    troop = parse_troop(source)
    puts troop.action_evals_str
    ev = troop.find_evaluator('遠距離戦')
    ev.disabled_sps.size.should == 3
    ev.evaluate(troop).should == 42
    
    ev = troop.find_evaluator('（ＲＴＲの対空ミサイル使用数1）遠距離戦闘評価による対空戦闘')
    ev.should_not be_nil
    
  end

  it '補正特殊のないオプション装備の乗算記法' do
    source = <<EOT
_define_I=D:{
一号機：ラビプラス＋うささん,
－ラビプラスのブースター＊２
（Ｐ）TEST：はてない国人+ハイ・パイロット;
（ＣＰ）TEST2：はてない国人+犬妖精２+剣士+騎士;
（ＣＰ）TEST3：はてない国人＋犬妖精２＋黒騎士＋騎士;
};
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    puts troop.action_evals_str
  end

  it 'オプション装備の可能行為に依存する評価の抑制' do
    source = <<EOT
_define_I=D:{rabi：Ｈｉ－うささん，
－Ｈｉ－うささんのミサイル＊１
（Ｐ）TEST：はてない国人+ハイ・パイロット;
（ＣＰ）TEST2：はてない国人+犬妖精２+剣士+騎士;
};
EOT
    troop = parse_troop(source)
    troop.find_evaluator('中距離戦').should be_nil

  end

  it '未来予知補正は操縦・同調で使えない。' do
    source = <<EOT
＃！有効条件：＜未来予知能力者２＞と同じ部隊にいる場合
＃！個別付与：未来予知能力者２の予知，本隊

＃！特殊無効：未来予知能力者２の予知の全判定補正，鍋ＲＦサイベ２_国有

_define_I=D:{
鍋ＲＦサイベ１_国有：リファインサイベリアン＋ＳＨＱ体格,
（P）05-00122-01_矢上ミサ：南国人＋新型舞踏子＋強い舞踏子＋未来予知能力者２＋ミサは恋人：敏捷+1*知識+1*幸運+2;
（CP）05-xx014-xx_鍋谷　にあ：南国人＋舞踏子＋元気な舞踏子＋強い舞踏子;
（CP）05-xx015-xx_鍋野　みわ子：南国人＋舞踏子＋元気な舞踏子＋強い舞踏子;
};

_define_I=D:{
鍋ＲＦサイベ２_国有：リファインサイベリアン＋ＳＨＱ体格,
（P）05-00145-01_鍋嶋　つづみ：南国人＋舞踏子＋元気な舞踏子＋強い舞踏子＋法の司：敏捷+1*感覚+1*知識+1*幸運+1;
（CP）05-xx016-xx_鍋橋　ぴい：南国人＋舞踏子＋元気な舞踏子＋強い舞踏子;
（CP）05-xx017-xx_鍋山　音祢：南国人＋舞踏子＋元気な舞踏子＋強い舞踏子;
};
EOT

    troop = parse_troop(source)

    troop.action_evals_str

    manipulate = troop.find_evaluator('操縦')
    calc = manipulate.calculate(troop)
    calc.children[0].total.should == 57
    calc.children[1].total.should == 40
  end
  
  it '機体内の７５％制限判定時は、特殊のコスト消費判定を行わない。' do
    troop = parse_troop <<EOT
_define_I=D: {
瀬戸口まつり機_東方有翼騎士団所有：チップボール：体格+4,
（Ｐ）00-00526-01_瀬戸口まつり：高位北国人＋戦闘工兵＋宰相の娘＋オペレーター＋ＳＨＱ感覚＋ＳＨＱ感覚＋秘書官２＋久遠：敏捷+1*器用+1*知識+1*幸運+6
＃－久遠：個人所有：白兵距離攻撃（白兵（全般））・中距離攻撃（射撃（全般））、評価＋２。燃料－１万ｔ。パイロットとして搭乗時全判定、評価＋１（着用型／全身）
－ＷＤへの衣嚢付与（イベント）：個人所有：手のひらサイズまでの着用型アイテムを１つ、ポケットに格納することで装備可能になる。この際、評価値補正以外の特殊は通常通り使用できる。＃２個取得していますので２つ装備可能です。
＃－家族の指輪：個人所有：（家内安全のために行動する場合）全判定、評価＋２（着用型／左手先）
＃－詳細：ＷＤへの衣嚢付与にて携帯。特殊は使用できないためコメントアウトしています。
－レーザーピストル：個人所有：近距離戦可能，（射撃（銃）、近距離での）攻撃、評価＋３。属性（レーザー）（着用型／片手持ち武器）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00526-01%A1%A7%C0%A5%B8%CD%B8%FD%A4%DE%A4%C4%A4%EA
－剣技の手ほどきを受ける
－魔法の手ほどきを受ける
};

_define_I=D: {
蒼のあおひと機_東方有翼騎士団所有：チップボール：体格+4,
（Ｐ）44-00097-01_蒼のあおひと：高位北国人＋高機動＋サイコキノ＋マッスルスリンガー＋秘書官２：筋力+7*耐久力+5*外見-5*敏捷+1*器用+1*知識+1*幸運+4
－クリスマスのペンダント：個人所有：幸運，防御、評価＋１（着用型／首）
＃－幸運のお守り：個人所有：幸運＋３（着用型／その他）
＃－ルビーの指輪：個人所有：筋力＋２（着用型／右手先）
－剛力：個人所有：使用制限（食後２時間以内使用可能、この効果は腹が減るまで続く）。筋力＋４
－変身すると筋力ＵＰ：個人所有：使用制限（状況次第で勝手にぬげる）。外見－５，筋力＋３，耐久力＋３
－詳細：http://cwtg.jp/qabbs/bbs2.cgi?action=article&id=9563／以下コメントアウトで注釈
＃剛力と変身すると筋力ＵＰは使用制限時間・状況があるため、常時適用されないものとして個人補正欄には含めていません。
＃また、機体に搭乗しているため、場合分けしても評価値に変更がないので部隊・分隊評価での場合分けもしておりません。
－変態服：個人所有：使用資格（変態）。外見－５、筋力＋５、耐久力＋５（着用型／体）
－決めポーズうえっぷ：個人所有：絶技／周囲は耐久判定で対抗判定を行う。失敗するとゲロってしまい、失敗者一人につき難易は＋１される
－詳細：http://cwtg.jp/qabbs/bbs2.cgi?action=article&id=9191／変身すると筋力ＵＰ、変態服、決めポーズうえっぷの使用資格根拠
－瑠璃の光：個人所有：パートナーをどこにあろうと０時間で召喚できる（着用型／左手先）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00097-01%A1%A7%C1%F3%A4%CE%A4%A2%A4%AA%A4%D2%A4%C8
－剣技の手ほどきを受ける
－魔法の手ほどきを受ける
};

EOT
    troop.action_evals_str
    costs =  troop.total_cost
    costs['燃料'].should == -9
  end

  describe '乗り物のＡＲ' do
    it '通常では10' do
      source = <<EOT
_define_I=D:{
サンプル：チップボール,
（P）パイロット1：ポレポレ;
－互尊Ｃ
};
EOT
      troop = parse_troop(source)
      expect(troop.children.first.ar).to eq 10
    end

    it '指定があれば採用' do
      source = <<EOT
_define_I=D:{
サンプル：チップボール２,
（P）パイロット1：ポレポレ;
－互尊Ｃ
};
EOT
      troop = parse_troop(source)
      expect(troop.children.first.ar).to eq 12
    end

    it 'オプションによるＡＲ増加' do
      source = <<EOT
_define_I=D:{
サンプル：チップボール２,
－おまもりソード
（P）パイロット1：ポレポレ;
－互尊Ｃ
};
EOT
      troop = parse_troop(source)
      expect(troop.children.first.ar).to eq 14
    end

    it '有効条件による変化' do
      source = <<EOT
＃！有効条件：盾を捨てた場合の
_define_I=D:{
サンプル：チップボール２,
（P）パイロット1：ポレポレ;
};
EOT
      troop = parse_troop(source)
      expect(troop.children.first.ar).to eq 15
    end

    it '乗員に摂政' do
      source = <<EOT
_define_I=D:{
サンプル：チップボール２,
（P）パイロット1：摂政;
};
EOT
      troop = parse_troop(source)
      expect(troop.children.first.ar).to eq 11
    end

    it '乗員がＡＲ修正を持つ' do
      source = <<EOT
_define_I=D:{
サンプル：チップボール２,
（P）パイロット1：ポレポレ;
－エアバイクピケ
};
EOT
      troop = parse_troop(source)
      expect(troop.children.first.ar).to eq 12
    end
  end
end
