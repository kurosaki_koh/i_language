# -*- encoding: utf-8 -*-
require 'rubygems'
require 'treetop'

require 'active_record'
require 'jiken/database/i_definition.rb'

DatabaseCconfig = YAML.load_file(File.join(File.dirname(__FILE__),'database.yml') )

class IDefinition < ActiveRecord::Base
  establish_connection( DatabaseCconfig["development"])
end

if !Object.const_defined?(:ILanguage) || !ILanguage.const_defined?(:Calculator)
  require File.join(File.dirname(__FILE__) , '../calculator/common_modules.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/evcc_defs.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/definitions.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/convertible.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/context.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/library.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/specialty.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/unit.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/calculation.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/evaluator.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/expression_adapter.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/team.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/soldier.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/container_unit.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/troop.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/formation_option.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/division.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/crew_division.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/vehicle.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/career.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/vessel.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/fleet.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/rule_check.rb')
  require File.join(File.dirname(__FILE__) , '../calculator/resource_modification_detail.rb')
end
#include ILanguage::FCalc
include ILanguage::Calculator
#require 'jcode'
#$KCODE='u'

Definitions.set_activerecord(IDefinition)
#Definitions.set_hq

def inspect_evals(troop)
  troop.evals_str + "\n" + troop.common_abilities_evals_str + troop.action_evals_str #+ troop.passive_abilities_evals_str
end

  Abilities = ['体格','筋力','耐久力','外見','敏捷','器用','感覚','知識','幸運']

def parse_troop(source)
#  root = @parser.parse(source)
#  raise 'Parse error! '+@parser.failure_reason if root.nil?
#  create_troop(root)
  troop = @context.parse_troop(source)
  @context.variables['検証中機能'] = '有効' unless @context.variable('検証中機能')
  troop
end

def floor2(val)
  ((val * 100 ).floor)*0.01
end

  def inspect_vehicle(vehicle)
    result = ''
    cols = ['器用','感覚','知識','幸運']
    mhash = {}
    cols.each{|c|mhash[c]=0}
    hash = Hash.new{|h,k| h[k]=0}
    optional_keys = vehicle.evals.keys.select{|k| !Abilities.include?(k)}
    vehicle_cols= Abilities + optional_keys
    for data in vehicle.evals_items+ vehicle.evaluate_items
      result << "#{data.name}:#{vehicle_cols.collect{|c|data.value(c).to_s}.join(':')}\n"
      vehicle_cols.each{|c|hash[c] += data.value(c).to_i}
    end
    for data in vehicle.pilot_items
      result << "#{data.name}:#{Abilities.collect{|c|data.value(c).to_s}.join(':')}\n"
      cols.each{|c| mhash[c] = data.value(c).to_i if mhash[c] < data.value(c).to_i}
    end
    result << "機体評価:#{StandardAbilities.collect{|c| hash[c] + mhash[c].to_i }.join(':')}\n"
    for child in vehicle.children
      soldier_str = inspect_soldier(child)
      soldier_str.each_line{|line|
        result << ("    " + line)
      }

    end
    result
  end

  def inspect_soldier(soldier)
    total = Hash.new{|h,k|h[k]=0}
    result = ''
    rows = soldier.evals_items + soldier.evaluate_items
    optional_keys = soldier.evals.keys.select{|k| !Abilities.include?(k)}
    cols= Abilities + optional_keys
    for row in rows
      result << "#{row.name}:#{cols.collect{|c|row.value(c).to_s}.join(':')}\n"
      cols.collect{|c| total[c] += row.value(c).to_i}
    end
    result << "合計：#{Abilities.collect{|c|total[c]}.join(':')}\n"
    result
  end
  def inspect_members(troop)
    result = ""
    for child in troop.children
      result << "■#{child.name}\n"
      case child
      when Vehicle
        result << inspect_vehicle(child)
      when Soldier
        result << inspect_soldier(child)
      end
      result << "\n"
    end
    result
  end

#RSpec::Matchers.define :be_evaluated do |evaluator_name , value|
##  chain :as do |eval_name|
##    @evaluator_name = eval_name
##  end
#  
#  matche do |unit|
#    ev = unit.troop.find_evaluator(evaluator_name)
#    ev.evaluate(unit) == value
#  end
#
#  failure_message_for_should do |unit|
#    ev = unit.troop.find_evaluator(evaluator_name)
#    unit.name + "の評価:#{ev.evaluate(unit)} 期待した値：#{value}"
#    result
#  end
#  failure_message_for_should_not do |unit|
#    ev = unit.troop.find_evaluator(evaluator_name)
#    unit.name + "の評価:#{ev.evaluate(unit)} 期待した値：#{value} 一致しちゃった。"
#  end
#  description do
#    # generate and return the appropriate string.
#  end
#end