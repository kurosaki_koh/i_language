# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')


describe Career do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end

  it "評価の有無" do
    source = <<EOT
_define_career:{輸送車：兵員輸送車,
(銃手）兵員１：北国人＋帝國軍歩兵；
-兵員輸送車の機関銃
兵員２：北国人＋帝國軍歩兵；
};
_define_i=d:{ちっぷさん：チップボール２，
兵員Ｃ：東国人＋狙撃兵；
｝；
EOT
    troop = parse_troop(source)
    career = troop.children[0]

    e_hash = career.evals.dup

    career_keys = ['器用','感覚' ,'知識' ,'幸運' ,'装甲']
    e_hash.keys.size.should == career_keys.size
    career_keys.all?{|key| e_hash.keys.include?(key)}.should be_truthy


    applicable_evaluator_names = [
      '装甲（通常）','装甲（中距離）','装甲（遠距離）',
      '中距離戦','同調（個別）',
#      '追跡（感覚）','追跡（幸運）',
#      '偵察' ,
      ]

    for ev in troop.evaluators
      result = ev.are_applicable_between(career)
      name_included = (applicable_evaluator_names.include?(ev.name) )
      result.should == name_included
    end

    armor = troop.find_evaluator('装甲（通常）')
    armor.evaluate(troop).should == 34

    source = <<EOT
_define_career:{輸送車Ａ：兵員輸送車,
(銃手）兵員１：北国人＋帝國軍歩兵；
-兵員輸送車の機関銃
兵員２：北国人＋帝國軍歩兵＋偵察兵＋追跡者；
};
兵員３：北国人＋帝國軍歩兵；
EOT
    troop = parse_troop(source)
    troop.action_evals_str
    troop.action_names.should include('中距離戦闘行為')

    m_range = troop.find_evaluator('中距離戦')
    m_range.evaluate(troop).should == 14

    career = troop.children[0]

    selection_hash = career.evals_by_selection
    selection_hash.keys.size.should == 5
    selection_values = Vehicle::PilotPotentials.collect{|key|selection_hash[key]}
    selection_values.should == [9,9,8,3]
    selection_hash['装甲'].should == 30

    union_hash = career.evals_by_union
    union_hash.keys.size.should == 5
    union_values =  Vehicle::PilotPotentials.collect{|key|union_hash[key]}
    union_values.should == [11,10,10,6]
    union_hash['装甲'].should == 30

    career.evaluation_mode.should == :union

    troop_evals = Vehicle::PilotPotentials.collect{|key| 
      troop.evaluate(key)

      }
    troop_evals.should == [12,11,12,8]
    troop.evaluate('装甲').should == 30
  end

  it '同調評価が負けてる' do
    source = <<EOT
_define_career:{輸送車Ａ：兵員輸送車,
 (銃手）兵員１：東国人＋ハイギーク；
 -兵員輸送車の機関銃
 兵員２：東国人＋ハイギーク；
};

兵員３：北国人＋帝國軍歩兵；
EOT
    troop = parse_troop(source)
    troop.action_evals_str

    career = troop.children[0]
    career.evaluation_mode.should == :selection
    troop_evals = Vehicle::PilotPotentials.collect{|key|
      troop.evaluate(key)

      }
    troop_evals.should == [9,10,15,5]

  end
end
