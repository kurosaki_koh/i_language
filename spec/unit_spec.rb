# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')


describe Unit do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end
  it "judge basic_bonus? tenyo" do
    source = <<EOT
Ｌ：普通の人　＝　｛
　ｔ：名称　＝　普通の人（人）
　ｔ：評価　＝　全能力０
　ｔ：特殊　＝　｛
　　＊普通の人の職業カテゴリ　＝　，，人アイドレス。
　｝
｝
soldier:普通の人＋天陽;
EOT
    troop = parse_troop(source)
    soldier = troop.children[0]
#    soldier = Soldier.new(@context)
#    soldier.items = ['天陽']
    bonus_sp = soldier.all_bonuses.find{|b|b['定義'] =~ /＊天陽の全能力補正/}

#    soldier.basic_bonus?(bonus_sp,'耐久力').should == true
    bonus_sp.is_ability_bonus?.should == true
    evals = soldier.evals
    for col in Abilities
      evals[col].should == 0
      soldier.evaluate(col).should == 2
    end

    armor = Evaluator['装甲（通常）']
#    armor.evaluate(soldier)
    result = armor.evaluate(soldier)
    result.should == 2
  end

  it "judge basic_bonus? cyborg" do
    soldier = Soldier.new(@context)
    soldier.idresses = ['サイボーグ']
    soldier.name = 'cyborg'
    bonus_sp = soldier.all_bonuses.find{|b|b['定義'] =~ /＊サイボーグの筋力・耐久力補正/}
    bonus_sp.should_not be_nil
    bonus_sp.is_ability_bonus?.should == true
#    soldier.basic_bonus?(bonus_sp,'耐久力').should == true
    puts inspect_soldier(soldier)
    evals = soldier.evals
    evals['筋力'].should == 1
    evals['耐久力'].should == 1
    soldier.evaluate('筋力').should == 3
    soldier.evaluate('耐久力').should == 3
    armor = Evaluator['装甲（通常）']
    armor.evaluate(soldier).should == 2
  end

  it "02：ａｋｉｈａｒｕ藩国(T14)編成検証 " do
    source = <<EOT
//部分有効条件：ヒーローに変身
//有効条件：パイロットとして搭乗している場合での
02-00027-01_涼原秋春：南国人＋医師＋風紀委員会＋生徒会役員＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*幸運+1；
02-00031-01_鴨瀬高次：南国人＋医師＋風紀委員会＋生徒会役員＋法官：敏捷+1*知識+1*幸運+2；
02-00032-01_忌闇装介：南国人＋パイロット＋超薬戦獣＋ヒロイック・パイロット＋ＨＱ感覚＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*知識+1*幸運+2；
02-00034-01_阪明日見：南国人＋医師＋風紀委員会＋生徒会役員＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*知識+1*幸運+6；
02-00036-01_鈴木：南国人＋医師＋風紀委員会＋生徒会役員：幸運+1；
02-00037-01_田中申：南国人＋医師＋風紀委員会＋生徒会役員＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*知識+1*幸運+1；
02-00038-01_和志：南国人＋パイロット＋超薬戦獣＋ヒロイック・パイロット＋ＨＱ感覚＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：筋力+6*敏捷+1*知識+1*幸運+1；
02-00039-01_リバーウィンド：南国人＋医師＋風紀委員会＋生徒会役員＋護民官：敏捷+1*器用+1*知識+1*幸運+1；
02-00841-01_ゆり花：南国人＋医師＋風紀委員会＋生徒会役員；
02-00842-01_勇作：南国人＋医師＋風紀委員会＋生徒会役員；

_define_I=D: {
デカショー１_国有：デカショー,
（Ｐ）02-00028-01_４４４：南国人＋パイロット＋超薬戦獣＋ヒロイック・パイロット＋ＨＱ感覚＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*感覚+5*知識+1*外見+2*幸運+2
};

_define_I=D: {
デカショー２_国有：デカショー,
（Ｐ）02-00030-01_橘：南国人＋パイロット＋超薬戦獣＋ヒロイック・パイロット+ＨＱ感覚＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*知識+1*幸運+2
};
EOT
#    root = @parser.parse(source)
    troop = parse_troop(source)
    result = inspect_evals(troop)

    #puts inspect_members(troop)
    result.should == <<EOT
体格：筋力：耐久力：外見：敏捷：器用：感覚：知識：幸運
25：28：28：24：25：29：40：31：34
装甲（通常）：26
白兵戦：29
追跡（感覚）：40 / 追跡（幸運）：34
偵察：40

○操縦
　・デカショー１_国有：31
　・デカショー２_国有：29

EOT
  end

  it 'ヒロイック・パイロットの搭乗戦闘補正' do
    source = <<EOT
//部分有効条件：ヒーローに変身
//有効条件：パイロットとして搭乗している場合での
_define_I=D: {
デカショー１_国有：デカショー,
（Ｐ）02-00028-01_４４４：南国人＋パイロット＋超薬戦獣＋ヒロイック・パイロット＋ＨＱ感覚＋変身ヒーロー＋ＨＱ外見＋ＨＱ体格＋ＨＱ体格：敏捷+1*感覚+5*知識+1*外見+2*幸運+2
};
EOT
#    root = @parser.parse(source)
    troop = parse_troop(source)
    result = inspect_evals(troop)

    deca = troop.children[0]
    sps = deca.all_bonuses
    puts sps.collect{|sp|sp.name}
    hb = sps.find{|sp| sp.name == 'ヒロイック・パイロットの搭乗戦闘補正'}

    hb.should_not be_nil
    hb.target.object_id.should == deca.object_id
    hb.owner.object_id.should == deca.children[0].object_id
    hb.is_available?.should == true

    melee = Evaluator['白兵戦']
    pb = deca.find_spec('デカショーの白兵距離戦闘補正')
    pb.should_not be_nil
    pb.is_ability_bonus?.should == false
    pb.is_judge_bonus?.should == true
    pb.is_disabled?.should == false
    pb['補正使用条件'].should == "（白兵（兵器）、白兵距離での）"
    sps , resps = deca.sub_evals_items(melee)
    melee.evaluate(troop).should == 23
#    troop.accept_evaluator(melee).should == 23

    armor = Evaluator['装甲（白兵距離）']
    armor.evaluate(troop).should == 20
    troop.evals_str.should include "体格：筋力：耐久力：外見：敏捷：器用：感覚：知識：幸運\n18：19：22：16：19：25：38：27：30"

    (troop.action_evals_str+"\n").should == <<EOT
白兵戦：23
追跡（感覚）：38 / 追跡（幸運）：30
偵察：38

○操縦
　・デカショー１_国有：31


EOT
  end

  it 'num_of_action_owner' do
    source = <<EOT
soldier1:東国人＋理力使い;
soldier2:東国人＋ギーク＋理力使い;
soldier3:東国人＋ハッカー＋理力使い;
EOT
    troop = parse_troop(source)
    result = inspect_evals(troop)

    troop.num_of_action_owner('詠唱戦闘行為').should == 3
    troop.num_of_action_owner('情報戦闘行為').should == 2
    troop.num_of_action_owner('白兵距離戦闘行為').should == 1

    anames = troop.action_names
    anames.include?('詠唱戦闘行為').should == true
    anames.include?('情報戦闘行為').should == false
    anames.include?('白兵距離戦闘行為').should == false
  end

  it '兵科属性＝歩兵' do
    troop = parse_troop('soldier:歩兵＋名パイロット;')
    soldier = troop.children[0]
    sp1 = soldier.find_spec('歩兵の近距離戦闘補正')
    sp1.is_unit_type_match?.should == true

    sp2 = soldier.find_spec('歩兵の位置づけ')
    sp2.is_unit_type_match?.should == true

    sp3 = soldier.find_spec('名パイロットの搭乗戦闘補正')
    sp3.is_unit_type_match?.should == false
  end

  it '兵科属性＝搭乗' do
    source = <<EOT
_define_I=D:{
vehicle:トモエリバー,
soldier:歩兵+名パイロット
};
EOT
    troop = parse_troop(source)
    vehicle = troop.children[0]
    soldier = vehicle.children[0]

    sp1 = soldier.find_spec('歩兵の近距離戦闘補正')
    sp1.is_unit_type_match?.should == false

    sp2 = soldier.find_spec('歩兵の位置づけ')
    sp2.is_unit_type_match?.should == true

    sp3 = soldier.find_spec('名パイロットの搭乗戦闘補正')
    sp3.is_unit_type_match?.should == true

  end

  it '兵科属性＝歩兵かつ搭乗' do
    source =  <<EOT
_define_I=D:{
vehicle:チップボール,
soldier:歩兵+名パイロット
};
EOT

    troop = parse_troop(source)
    vehicle = troop.children[0]
    soldier = vehicle.children[0]

    sp1 = soldier.find_spec('歩兵の近距離戦闘補正')
    sp1.is_unit_type_match?.should == true

    sp2 = soldier.find_spec('歩兵の位置づけ')
    sp2.is_unit_type_match?.should == true

    sp3 = soldier.find_spec('名パイロットの搭乗戦闘補正')
    sp3.is_unit_type_match?.should == true
  end

  it 'Evaluatorの拡張' do
    #Evaluator[](key) は、今後、行為名称からでなく、Evaluator識別名でEvaluatorオブジェクト一つを返す。
    #Evaluator.select(action_name) は、行為名称からそれに関連する全ての評価子を配列で返す。

    melee = Evaluator['白兵戦']
    melee.should_not be_nil
    melee.name.should == '白兵戦'
    melee.action_name.should == '白兵距離戦闘行為'

    evaluators = @context.find_evaluator_by_action_name('白兵距離戦闘行為')
    evaluators.class.should == Array
    evaluators.find{|e|e.name == '白兵戦'}.should_not be_nil
    def_melee = evaluators.find{|e|e.name == '装甲（白兵距離）'}
    def_melee.should_not be_nil
    move_melee = evaluators.find{|e|e.name == '移動（白兵距離）'}
    move_melee.should_not be_nil

  end

  it 'Unit からそのユニットが使用可能なEvaluatorの配列を返す' do
#    root = @parser.parse('soldier:歩兵＋名パイロット;')
    troop = parse_troop('soldier:歩兵＋名パイロット;')
    soldier = troop.children[0]

    evaluators = soldier.evaluators
    evaluators.class.should == Array
    evaluators = troop.evaluators
    evaluators.class.should == Array
  end

  it '条件付き全能力補正' do
    troop = parse_troop <<EOT
//有効条件：ヒーローに変身することで
soldier：変身ヒーロー;
EOT
    soldier = troop.children[0]
    spec = soldier.find_spec('変身ヒーローの変身能力補正')
    soldier.evals['幸運'].should == -2
    soldier.troop_member.should == soldier
    expect(spec.is_unit_type_match?).to eq true
    soldier.is_evaluate_item?(spec).should  eq true
    soldier.evaluate('幸運').should == 6

    troop = parse_troop <<EOT
//有効条件：ヒーローに変身することで
_define_I=D: {
デカショー２_国有：デカショー,
soldier：変身ヒーロー
};
EOT
    deca = troop.children[0]
    deca.evals['幸運'].should == 24
    deca.evaluate('幸運').should == 24
  end

  it '判定修正で情報戦闘＋ｎを指定' do
    source = "32-00635-01_黒埼紘：東国人+ギーク+スペーススペルキャスター+宇宙の賢者+法官：敏捷+1*知識+4*幸運+1：情報戦+1*全判定+1;"
    troop = parse_troop(source)
    hacking = Evaluator['情報戦']
    puts inspect_members(troop)
    hacking.evaluate(troop).should == 23
  end

  it '存在しないアイドレス名を指定' do
    source = "32-00635-01_黒埼紘：東国人+ギーク+ドラッカー;"
    root = @parser.parse(source)
    root.should_not be_nil
    troop = @context.parse_troop(source)
    puts inspect_members(troop)
    @context.definitions.not_founds.size.should == 1
    @context.definitions.not_founds[0].should == 'ドラッカー'
  end

  it 'Unit#troop' do
    source = <<EOT
soldier1:歩兵
_define_I=D: {
デカショー２_国有：デカショー,
pilot：歩兵
};
EOT
    troop = parse_troop(source)
    soldier1 = troop.children[0]
    soldier1.troop.object_id.should == troop.object_id
    decasho = troop.children[1]
    decasho.troop.object_id.should == troop.object_id
    pilot = decasho.children[0]
    pilot.troop.object_id.should == troop.object_id
  end

  it '行為修正' do
    source = <<EOT
soldier1:剣士：：白兵戦+1*白兵距離戦闘行為+1;
EOT

    troop = parse_troop(source)
    soldier = troop.children[0]
    melee = Evaluator['白兵戦']
    melee_defence = Evaluator['装甲（白兵距離）']
    melee.choice_alternative(soldier).total.should == 5
    melee_defence.evaluate(soldier).should == 4
  end

  def put_rd_eval(n,rd)
    lg = Math.log(rd)/Math.log(1.2)
    eval = to_eval(rd)

    if n != eval
      eval = to_eval(rd)
      warn = "!!"
    else
      warn = ''
    end
    ht = to_rd(lg.floor + 1)
    puts [rd,lg,eval,ht , warn].join(',')
    n.should == eval unless n == -7
  end
  it 'RD境界値バグ' do

    for n in -8..150
      rd = to_rd(n)
      puts "# #{n}"
      put_rd_eval(n-1,rd-0.1) if n>= 0
      put_rd_eval(n,rd)
      put_rd_eval(n,rd+0.1)  if n>= 0
      puts '-----------------------'
    end

    source = <<EOT
13-00270-01_竿崎　裕樹：北国人＋偵察兵＋追跡者＋警官＋ＨＱ感覚＋護民官＋天陽＋ＳＨＱ：敏捷+1*器用+1*知識+4*幸運+2；
13-00271-01_かくた：北国人＋摂政＋バトルメード＋侍女＋吏族：知識+6*幸運+1；
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00271-01%A1%A7%A4%AB%A4%AF%A4%BF
13-00272-01_玖礼　蒼麒：北国人＋偵察兵＋追跡者＋警官＋ＨＱ感覚＋吏族＋天陽＋ＳＨＱ：外見+1*知識+3*幸運+2；
13-00276-01_坂下真砂：北国人＋偵察兵＋追跡者＋警官＋ＨＱ感覚＋参謀：外見+2*敏捷+1*知識+4*幸運+2；
＃　//－恩寵の時計：個人所有：外見＋２（着用型／首）
＃　//－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／腕）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00276-01%A1%A7%BA%E4%B2%BC%BF%BF%BA%BD
13-00277-01_辻木　志朗：北国人＋偵察兵＋追跡者＋警官＋ＨＱ感覚＋天陽＋ＳＨＱ：敏捷+1*知識+4*幸運+2；
13-xx014-xx_犬士：北国人＋偵察兵＋追跡者＋警官＋ＨＱ感覚：知識+3；
13-xx015-xx_犬士：北国人＋偵察兵＋追跡者＋警官＋ＨＱ感覚：知識+3；
13-xx016-xx_犬士：北国人＋偵察兵＋追跡者＋警官＋ＨＱ感覚：知識+3；
13-xx017-xx_犬士：北国人＋偵察兵＋追跡者＋警官＋ＨＱ感覚：知識+3；
13-xx018-xx_犬士：北国人＋偵察兵＋追跡者＋警官＋ＨＱ感覚：知識+3；
13-xx019-xx_犬士：北国人＋偵察兵＋追跡者＋警官＋ＨＱ感覚：知識+3；
EOT

    troop = parse_troop(source)
    troop.evaluate('器用').should == 22
  end

  it 'ＨＱ擬似アイドレス' do
    source = <<EOT
soldier:天陽＋ＳＨＱ:体格+1;
EOT

    troop = parse_troop(source)
    Abilities.each do |key|
      if key == '体格'
        troop.evaluate(key).should == 5
      else
        troop.evaluate(key).should == 4
      end
    end
  end

  it '派生評価がその祖先と同じ値なら、表示を省略する。' do
    source = "soldier:歩兵+ギーク;\n"
    troop = parse_troop(source)

    result = troop.action_evals_str

    result.should =~ /装甲（白兵距離）/
    result.should =~ /移動（白兵距離）/
    result.should_not =~ /装甲（近距離）/
    result.should_not =~ /移動（近距離）/
    result.should_not =~ /装甲（中距離）/
    result.should_not =~ /移動（中距離）/
  end

  it '搭乗していない奴の搭乗補正はコストに含めない' do
    source = <<EOT
＃！部分有効条件：戦闘する場合での
veteran1：ベテランパイロット；

_define_I=D:{
deca：デカショー,
veteran2：ベテランパイロット
};
EOT
    troop = parse_troop(source)
    dummy_str = troop.evals_str
    dummy_actions_output = troop.action_evals_str

    veteran1 = @context.units['veteran1']
    spec = veteran1.find_spec('ベテランパイロットの搭乗戦闘補正')
    spec.is_used?.should == false

    veteran2 = @context.units['veteran2']
    spec = veteran2.find_spec('ベテランパイロットの搭乗戦闘補正')
    spec.is_used?.should == true

    troop.total_cost['燃料'].should == -3
  end
  it 'ぽちの騎士の同調行為の７５％制限' do
    source = <<EOT
Ｌ：ぽちの騎士　＝　｛
　ｔ：名称　＝　ぽちの騎士（職業４）
　ｔ：要点　＝　ぽち，守り手
　ｔ：周辺環境　＝　城
　ｔ：評価　＝　体格０，筋力０，耐久力０，外見０，敏捷０，器用０，感覚０，知識０，幸運４
　ｔ：特殊　＝　｛
　　＊ぽちの騎士の職業４カテゴリ　＝　，，派生職業４アイドレス。
　　＊ぽちの騎士のぽちの守護者補正　＝　，条件発動，（ぽちを守る限りにおいて）全判定、評価＋２。
　　＊ぽちの騎士の騎士同調補正　＝　，条件発動，（ぽちを守る限りにおいて）同調、自動成功。７５％制限能力。
　　＊ぽちの騎士の援軍能力　＝　，条件発動，ぽちを守る場合、＜援軍活動＞を使用できる。
　｝
　ｔ：→次のアイドレス　＝　ぽち親衛隊（職業），黒騎士（職業），オタポン（ＡＣＥ）
｝

＃！有効条件：ぽちを守る限りにおいて
knight1:ぽちの騎士；
knight1:soldier；
EOT
    troop = parse_troop(source)
    synchro = Evaluator['同調']
    troop.team_tqrs.should_not include('騎士同調補正'.intern)
    sp = troop.children[0].find_spec('ぽちの騎士の騎士同調補正')
    troop.is_tqr_qualified?(sp).should eq false
    synchro.evaluate(troop).class.should == Fixnum

    source = <<EOT
Ｌ：ぽちの騎士　＝　｛
　ｔ：名称　＝　ぽちの騎士（職業４）
　ｔ：要点　＝　ぽち，守り手
　ｔ：周辺環境　＝　城
　ｔ：評価　＝　体格０，筋力０，耐久力０，外見０，敏捷０，器用０，感覚０，知識０，幸運４
　ｔ：特殊　＝　｛
　　＊ぽちの騎士の職業４カテゴリ　＝　，，派生職業４アイドレス。
　　＊ぽちの騎士のぽちの守護者補正　＝　，条件発動，（ぽちを守る限りにおいて）全判定、評価＋２。
　　＊ぽちの騎士の騎士同調補正　＝　，条件発動，（ぽちを守る限りにおいて）同調、自動成功。７５％制限能力。
　　＊ぽちの騎士の援軍能力　＝　，条件発動，ぽちを守る場合、＜援軍活動＞を使用できる。
　｝
　ｔ：→次のアイドレス　＝　ぽち親衛隊（職業），黒騎士（職業），オタポン（ＡＣＥ）
｝

＃！有効条件：ぽちを守る限りにおいて
knight1:ぽちの騎士；
knight1:soldier＋ぽちの騎士；
EOT
    troop = parse_troop(source)
    synchro = Evaluator['同調']
    synchro.evaluate(troop).class.should == String

  end

  it 'unit#tqrs' do
    source = <<EOT
Ｌ：偵察兵　＝　｛
　ｔ：名称　＝　偵察兵（職業）
　ｔ：要点　＝　偵察機器，野戦服
　ｔ：周辺環境　＝　なし
　ｔ：評価　＝　体格０，筋力０，耐久力１，外見０，敏捷１，器用０，感覚１，知識１，幸運－１
　ｔ：特殊　＝　｛
　　＊偵察兵の職業カテゴリ　＝　，，派生職業アイドレス。
　　＊偵察兵の位置づけ　＝　，，歩兵系。
　　＊偵察兵の白兵距離戦闘行為　＝　歩兵，条件発動，白兵距離戦闘行為が可能。＃白兵距離戦闘評価：可能：（体格＋筋力）÷２
　　＊偵察兵の近距離戦闘行為　＝　，，近距離戦闘行為が可能。＃近距離戦闘評価：可能：（敏捷＋筋力）÷２
　　＊偵察兵の近距離戦闘補正　＝　歩兵，任意発動，（近距離での）攻撃、評価＋２、燃料－１万ｔ。＃近距離戦闘評価：可能：（敏捷＋筋力）／２
　　＊偵察兵の中距離戦闘行為　＝　歩兵，条件発動，中距離戦闘行為が可能。＃中距離戦闘評価：可能：（感覚＋知識）／２
　　＊偵察兵の偵察補正　＝　歩兵，条件発動，（偵察での）感覚、評価＋３、燃料－１万ｔ。７５％制限能力。＃偵察評価：一般：感覚
　｝
　ｔ：→次のアイドレス　＝　吉田遥（ＡＣＥ），特殊部隊員（職業），追跡者（職業）
｝

scout：偵察兵＋犬妖精；
EOT

    troop = parse_troop(source)
    scout = troop.children[0]
    scout.methods include('tqrs')
    scout.tqrs.size.should == 6
    scout.tqrs.should include('白兵距離戦闘行為'.to_sym)
    scout.tqrs.should include('近距離戦闘行為'.to_sym)
    scout.tqrs.should include('中距離戦闘行為'.to_sym)
    scout.tqrs.should include('オペレート行為'.to_sym)
    scout.tqrs.should include('追跡補正'.to_sym)
    scout.tqrs.should include('偵察補正'.to_sym)

  end

  it 'ACEの特殊は燃料消費の対象としない。' do
    source = <<EOT
34-00678-01_室賀兼一：室賀兼一＋名医＋ＳＨＱ＋ＨＱ＋謙者;
34-xx000-xx_王猫タロ・ターマ・ハーナ８世：南国人＋医師＋医師＋名医+敬人;
EOT
    troop = parse_troop(source)
    troop.action_evals_str
    ninja = troop.children[0]
    ninja.methods.include?(:is_ace?).should eq true
    ninja.is_ace?.should eq true
    sp = ninja.find_spec('医師の治療補正')
    sp.is_for_ace?.should eq true
    sp.is_cost_required?.should eq false
    
    cat = troop.children[1]
    cat.is_ace?.should eq false

    troop.total_cost.should == {'燃料' => -18}
  end
 it '行為とコスト計算' do
   source = <<EOT
34-xx000-xx_王猫タロ・ターマ・ハーナ８世：南国人＋医師＋医師＋名医+敬人;
34-xx000-xx_王猫タロ・ターマ・ハーナ８世：南国人＋医師＋医師＋名医+敬人;
34-xx000-xx_王猫タロ・ターマ・ハーナ８世：南国人＋医師＋医師＋名医+敬人;
34-00675-01_東　恭一郎：高位南国人＋炎の料理人＋歩兵＋緊急展開軍+行政士官+ＨＱ知識+ＨＱ知識+謙者：敏捷+1*知識+1*幸運+1;
－アンデットバスター：個人所有：歩兵武装，条件発動，（アンデッドに対する）攻撃、評価＋５。両手持ち武器。
EOT
    troop = parse_troop(source)
    result = troop.action_evals_str
    cost = troop.total_cost()
    cost.should == {'燃料' => -45}

    source = <<EOT
drager:ドラッガー
EOT
    troop = parse_troop(source)
    result = troop.action_evals_str
    cost = troop.total_cost()
    cost.should == {'燃料' => -3}

    source = <<EOT
drager:ドラッガー
soldier:歩兵
soldier:歩兵
soldier:歩兵
EOT
    troop = parse_troop(source)
    result = troop.action_evals_str
    cost = troop.total_cost()
    cost.should == {'燃料' => -27}
  end

  it '銘入り猫士はACEでも食料・燃料コスト消費の対象とする。＆食料消費は半分' do
    source = <<EOT
00-00054-x1_アリア・ロッシ：アリア・プラチナキャット＋高位西国人＋バーニングパイロット＋パイロットエンジニア＋レストア職人＋秘書官猫;
EOT

    troop = parse_troop(source)
    troop.should_not be_nil

    aria = troop.children[0]
    aria.placements.should include('銘入り猫士')
    aria.is_named_npc?.should eq true
    aria.is_npc?.should eq true
    aria.is_ace?.should eq true

    aria.event_cost.should == {'食料'=> -0.5}
    troop.total_cost.should == {'燃料'=> -2}
    troop.organization_cost('食料').should == 1.0
  end

  it '犬士食料消費の境界値チェック' do
    source = <<EOT
＃！個別付与：栄光の野戦炊飯具１号，本隊
00-xx001-xx_dog1：北国人＋帝國軍歩兵＋追跡者＋狙撃兵；
EOT
    troop = parse_troop(source)
    troop.action_evals_str

    troop.organization_cost('食料').should == 1.0
    
    result = troop.event_cost_str
    result.should =~ /食料：0.5万ｔ/

    source = <<EOT
00-xx001-xx_dog1：北国人＋帝國軍歩兵＋追跡者＋狙撃兵；
00-xx002-xx_dog2：北国人＋帝國軍歩兵＋追跡者＋狙撃兵；
EOT
    troop = parse_troop(source)
    troop.action_evals_str

    troop.organization_cost('食料').should == 1.0
    result = troop.event_cost_str
    result.should =~ /食料：1万ｔ/


    source = <<EOT
00-xx001-xx_dog1：北国人＋帝國軍歩兵＋追跡者＋狙撃兵；
00-xx002-xx_dog2：北国人＋帝國軍歩兵＋追跡者＋狙撃兵；
00-xx003-xx_dog3：北国人＋帝國軍歩兵＋追跡者＋狙撃兵；
EOT
    troop = parse_troop(source)
    troop.action_evals_str

    troop.organization_cost('食料').should == 1.0
    result = troop.event_cost_str
    result.should =~ /食料：1.5万ｔ/

  end

  it 'ミラーコートバグ' do
    source = <<EOT
＃！有効条件：｛Ｉ＝Ｄ，ＲＢ，航空機｝に搭乗して、戦闘する場合での
＃！有効条件：宰相府にある機体に搭乗している場合での
＃！有効条件：｛Ｉ＝Ｄ，水上艦船｝に搭乗している場合での

_define_I=D:{ミラコ１：ミラーコート（秘書官仕様）：感覚+3*体格+2,
44-00220-01_奥羽りんく：西国人＋整備士２＋宰相の娘＋撃墜王＋秘書官２：敏捷+1*器用+1*外見+2*知識+1*幸運+1*器用+22*感覚+24*知識+21*幸運+14,
};
EOT
    troop = parse_troop(source)

    troop.action_evals_str.should_not be_nil

    troop.action_names.should include('対要塞戦闘行為')

    anti_fortress = troop.evaluators.find{|ev|ev.name == '対要塞戦闘'}
    anti_fortress.should_not be_nil
    anti_fortress.evaluate(troop).should == 49
  end
  it '0000033: 非一般評価への全能力・全判定補正がうまく処理されない。' do
    source = <<EOT
ＷＮ１_組織所有：連結ミアキス（空母型）：装甲+23*対空戦闘+23：全判定＋２３；
EOT
    troop = parse_troop(source)
    miakis = troop.children[0]
    miakis.evals['装甲'].should == 38
    troop.evaluate('装甲').should == 38

    armory = Evaluator['装甲（通常）']
    miakis.optional_bonus(armory).should == 23
    calc1 = armory.calculate(miakis)
    calc1.total.should == 61
    calc = Evaluator['装甲（通常）'].calculate(troop)

    calc.total.should == 61
  end

  it '0000034: 判定補正を加えることで余計な計算経緯が追加される。' do
    source = <<EOT
（騎）45-00419-01_里樹澪：北国人+帝國軍歩兵+バトルメード+ハイ・バトルメード+HQ感覚+補給士官：敏捷+1*知識+1*幸運+1:白兵戦+2/*守り刀*/;
－久遠
EOT
    troop = parse_troop(source)
    armory_m = Evaluator['装甲（近距離）']
    soldier = troop.children[0]
    soldier.optional_bonus(armory_m).should be_nil
    armory_m.is_omitted?(troop).should eq true
  end
  
  it 'ACE搭乗編成' do
    source = <<EOT
＃！有効条件：＜銀内ユウ＞を帯同しない場合での
_define_I=D:{
ユーフォー２：多国籍要撃機”ユーフォー”,
－多国籍要撃機”ユーフォー”の増槽：ＡＲ＋５、全評価－３（追加された５ＡＲを使い切るまで）
（Ｐ）05-XX009-xx_資源猫士：南国人＋学生＋パイロット
（ＣＰ）銀内優斗３：銀内優斗３;
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00141-01%A1%A7%B6%E4%C6%E2%A1%A1%A5%E6%A5%A6
};
EOT
    troop = parse_troop(source)

    ufo = troop.children[0]
    ufo.aces.size.should == 1
    values = ufo.aces.collect{|ace|ace.evaluate('体格')}
    values.should == [25]
    ufo.evals['体格'].should == 25
    ufo.evaluate('体格').should == 28
    troop.evals['体格'].should == 28

    armor = Evaluator['装甲（通常）']
    items = ufo.sub_evals_items(armor)
    items.size.should == 1
    ufo.is_overridden?('体格').should eq true
    armor.evaluate(troop).should == 28

  end

  it 'ACE搭乗編成2' do
    source = <<EOT
_define_I=D:{
Ｆ３ー１Ｓ：フェイクトモエリバー３＋ＨＱ体格＋ＨＱ感覚＋ＨＱ感覚,
－爆弾：敵が射程に入ると自動で一回の遠距離攻撃が出来、その間、遠距離戦闘評価が＋２になる。ＡＲ－１
－爆弾：敵が射程に入ると自動で一回の遠距離攻撃が出来、その間、遠距離戦闘評価が＋２になる。ＡＲ－１
－爆弾：敵が射程に入ると自動で一回の遠距離攻撃が出来、その間、遠距離戦闘評価が＋２になる。ＡＲ－１
－爆弾：敵が射程に入ると自動で一回の遠距離攻撃が出来、その間、遠距離戦闘評価が＋２になる。ＡＲ－１
（Ｐ）11-11111-11：久藤百佳：久藤百佳；
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00235-01%A1%A7%B5%D7%C6%A3%CB%D3%B7%EE
（ＣＰ）11-xx013-xx：猫士：森国人＋猫妖精＋忍者：知識+6
－保育園の授業：国有：保育園を保持する国家の全アイドレスは知識評価に評価＋６される。（ＨＱ）
（ＣＰ）11-xx014-xx：猫士：森国人＋猫妖精＋忍者：知識+6
－保育園の授業：国有：保育園を保持する国家の全アイドレスは知識評価に評価＋６される。（ＨＱ）
};
EOT
    troop = parse_troop(source)
    evals_str = troop.evals_str
    puts evals_str
    synch = Evaluator['同調']
    momoka = troop.children[0].children[0]
    m_calc = momoka.synchronizer(synch)
    m_calc.is_na?.should eq false
    calc = synch.calculate(troop)
    calc.total.should == 21
  end

  it 'ACE搭乗編成３' do
    source = <<EOT
_define_I=D:{
バンバンジーバイク_玄霧弦耶所有：バンバンジー・バイク,
(P)11-00230-01_玄霧弦耶：玄霧+ＨＱ：,
－アンデットバスター：個人所有：対アンデット攻撃＋５（着用型／両手武器／両手）
－蛇の指輪２：個人所有：大神官の聖別により、耐久＋１（着用型／指輪／右手先）
－玄霧の指輪：個人所有：死期を一日延ばす（着用型／結婚指輪／左手先）
－英雄達のオルゴール：愛を思い出したとき、熱血反応と超辛反応を起こす。勝利の暗号が隠されている。
－残念賞メダル：ダイスを振った際、出目を±１０の範囲で変更できる。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki /index.php?00230-01%A1%A7%B8%BC%CC%B8%B8%B9%CC%ED
}；

＃！評価無効：白兵戦
＃！評価無効：近距離戦
＃！評価無効：遠距離戦

＃！状況：山岳・森林
＃！有効条件：｛山岳，森林｝での
EOT
    troop = parse_troop(source)
    evals_str = troop.evals_str
    evals_str.should_not be_nil
    puts evals_str
    actions_str = troop.action_evals_str
    actions_str.should_not be_nil
    puts actions_str
    leaders = troop.leaders_str
    leaders.should_not be_nil
    puts leaders
  end

  it 'ユニットタグ' do
    source = <<EOT
＃歩兵へのタグ付け
(P)soldier:歩兵；
（ＣＰ）soldier2:帝國軍歩兵；
（騎／歩兵）driver:歩兵
（同・歩兵）fellow:歩兵；
（随行）leo:レオドール；
（搭載）soldier3：歩兵；

_define_I=D{
(随行)チップ：チップボール,
(P)pilot:歩兵＋教導パイロット
};

＃！個別付与：カトラス，歩兵
＃！個別付与：レーザーピストル，搭載
＃！個別付与：電子妖精，なんだか良く分からない何か
EOT
    
    troop = parse_troop(source)
    troop.should_not be_nil

    troop.children[0].tags.should == ['P']
    troop.children[1].tags.should == ['CP']
    troop.children[2].tags.should == ['騎','歩兵']
    troop.children[3].tags.should == ['同','歩兵']
    troop.children[4].tags.should == ['随行']
    troop.children[5].tags.should == ['搭載'] #暫定仕様
#    troop.children[5].tags.should == ['随行']

    troop.children[0].passenger_type.should == :pilot
    troop.children[1].passenger_type.should == :copilot
    troop.children[2].passenger_type.should == :rider
    troop.children[3].passenger_type.should == :fellow
    troop.children[4].passenger_type.should == :attendant
    @context.units['soldier3'].passenger_type.should == nil #暫定仕様
#    @context.units['soldier3'].passenger_type.should == :loaded

    soldiers = @context.tags['歩兵']
    soldiers.size.should == 2
    soldiers[0].name.should == 'driver'
    soldiers[1].name.should == 'fellow'

    loaded = @context.tags['搭載']
    loaded[0].name.should == 'soldier3'
    loaded.size.should == 1

    soldiers.all?{|s|s.all_idresses.should include('カトラス')}
    loaded[0].all_idresses.should include('レーザーピストル')
    @context.warnings.size.should == 1
  end

  it '搭載扱い：登録はするが本隊に加えない' do
    pending #搭載ルールは想像以上に魔窟だった。
    source = <<EOT
_define_I=D:{
レンタカー_矢神サク所有：兵員輸送車,
（機関銃付与者）06-00149-01_楠瀬　藍：愛の民＋名パイロット＋ホープ＋強化新型ホープ＋ＨＱ感覚＋護民官：感覚+4*幸運+2；
－恩寵の時計：個人所有：歩兵時の外見、評価＋２。（着用型／首）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00149-01%A1%A7%C6%EF%C0%A5%A1%A1%CD%F5
（搭載）06-00162-01_冴木悠：愛の民＋名パイロット＋ホープ＋強化新型ホープ＋ＨＱ感覚：敏捷＋1*知識＋1*幸運＋1；
（搭載）06-00769-01_三園晶：愛の民＋サイボーグ＋歩兵＋護民官；
－護民の紋章：個人所有：身分の証として使用され、護民官の待遇を受ける（着用型／手先）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00769-01%A1%A7%BB%B0%B1%E0%BE%BD
｝；
（搭載）soldier:歩兵；
EOT
    troop = parse_troop(source)
    troop.children.size.should == 1
    troop.children[0].children.size.should == 1
  end

  it '随行扱い：本隊・分隊において可能行為算出時に数えない' do
    source = <<EOT
soldier:歩兵；
leo:レオドール；
（随行）soldier2:ハッカー；
EOT
    troop = parse_troop(source)

    troop.members_without_attendants.size.should == 1
    troop.members_without_attendants[0].name == 'soldier'
    troop.num_of_members.should == 1
    troop.action_names.should include('中距離戦闘行為')
    Evaluator['中距離戦'].evaluate(troop).should == 2
    troop.action_names.should_not include('情報戦闘行為')
    Evaluator['偵察'].evaluate(troop).should == 0
    Evaluator['装甲（通常）'].evaluate(troop).should == 40
    Evaluator['装甲（中距離）'].evaluate(troop).should == 40

    synch = Evaluator['同調']
    calc = synch.calculate(troop)
    calc.total.should == 0

  end

  it '個別評価' do
    source = <<EOT
regent1:摂政；
regent2:摂政＋犬妖精２；
soldier:歩兵；
EOT
    troop = parse_troop(source)

    ev = troop.evaluators.find{|ev|ev.name == '外交戦（個別）'}
    ev.should_not be_nil
    ev.is_individualy?.should eq true

    result = troop.action_evals_str
    result.should match(/^外交戦：14/)
    result.should match(/^○外交戦（個別）/)
    result.should match(/regent1：8/)
    result.should match(/regent2：13/)
    result.should_not match(/soldier/)
    puts result

    source = <<EOT
soldier:歩兵；
EOT
    troop = parse_troop(source)
    result = troop.action_evals_str
    result.should_not match(/^外交戦：14/)
    result.should_not match(/^○外交戦（個別）/)

    source = <<EOT
_define_I=D: {
ソットヴォーチェ・ゼーロ_詩歌藩国所有：水竜ソットヴォーチェ２＋ＨＱ感覚,
（Ｐ）18-00341-01_九音・詩歌：詩歌の民＋龍の乗り手＋吟遊詩人＋ドラゴンシンパシー＋ＨＱ知識＋ＨＱ知識+星見司２：敏捷+1*器用+1*知識+1*幸運+1;
－恩寵の短剣：個人所有：歩兵武装，，白兵距離戦闘行為が可能。 , 歩兵武装，条件発動，（白兵（短剣）、白兵距離での）｛攻撃，防御，移動｝、評価＋１。（着用型／片手持ち武器）
－恩寵の時計：個人所有：歩兵，，外見、評価＋２。（着用型／首）
－猫と犬の前足が重なった腕輪：個人所有：，条件発動，同調、評価＋３。同能力重複適用不可。（着用型／左腕）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00341-01%A1%A7%B6%E5%B2%BB%A1%A6%BB%ED%B2%CE
（ＣＰ）18-00347-01_駒地真子：詩歌の民＋犬妖精２＋吟遊詩人＋ドラゴンシンパシー+ＨＱ知識+ＨＱ知識+参謀：敏捷+1*器用+1*知識+1*幸運+2;
－恩寵の時計：個人所有：歩兵，，外見、評価＋２。（着用型／首）
＃－猫と犬の前足が重なった腕輪：個人所有：，条件発動，同調、評価＋３。同能力重複適用不可。（着用型／左腕）
＃上記、太元の仕様により、同調評価への補正が重複してしまうためコメントアウト
－ヘビイチゴのタルト：個人所有：なし
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00347-01%A1%A7%B6%F0%C3%CF%BF%BF%BB%D2
（ＣＰ）18-00356-01_岩崎経：詩歌の民＋犬妖精２＋吟遊詩人＋ドラゴンシンパシー+ＨＱ知識+ＨＱ知識+法の司：敏捷+1*幸運+1;
－大健康の腕輪：個人所有：歩兵，，耐久力、評価＋３。（着用型／右腕）
－フルブライト・ドレス：個人所有：歩兵武装，，外見、評価＋３。 , ，条件発動，同調、評価＋３。（着用型／左腕）
－法の執行者の紋章：個人所有：，条件発動，（法執行時での）全判定、評価＋２。（着用型／右手先）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00356-01%A1%A7%B4%E4%BA%EA%B7%D0
（ＣＰ）18-xx000-xx_シィ：詩歌の民＋犬妖精２＋銃士隊＋竜士隊;
（ＣＰ）18-xx037-xx_ももきち：詩歌の民＋犬妖精２＋銃士隊＋竜士隊;
};
EOT
    troop = parse_troop(source)

    result = troop.action_evals_str
    result.should match('○ドラゴンシンパシー')
    result.should match('九音・詩歌：')
    result.should_not match('シィ：')

    puts result

  end

  it '編成コスト計算' do
    source = <<EOT
＃！編成種別：重編成

soldier:東国人＋サイボーグ＋宇宙軍＋剣士；
soldier:東国人＋サイボーグ＋宇宙軍＋剣士；
EOT

    troop = parse_troop(source)
    troop.should_not be_nil
    result = troop.action_evals_str

    e_cost = troop.total_cost
    for c in troop.children
      for sp in c.specialties
        if sp.spec_type == '補正'
          puts "#{c.name}:#{sp.name} #{sp.cost.inspect} #{sp.is_cost_required?.to_s} \n"
        end
      end
    end

    troop.variable('編成種別').should == '重編成'
    troop.organization_coefficient.should == 3

    troop.organization_cost('食料').should == 2
    troop.organization_cost('燃料').should == 54

    source = <<EOT
＃デフォルトで重編成扱い

soldier:東国人＋サイボーグ＋宇宙軍＋剣士；
soldier:東国人＋サイボーグ＋宇宙軍＋剣士；
EOT

    troop = parse_troop(source)

    result = troop.action_evals_str
    troop.organization_coefficient.should == 3

    troop.organization_cost('食料').should == 2
    troop.organization_cost('燃料').should == 54

    source = <<EOT
＃！編成種別：軽編成

soldier:東国人＋サイボーグ＋宇宙軍＋剣士；
soldier:東国人＋サイボーグ＋宇宙軍＋剣士；
EOT

    troop = parse_troop(source)

    result = troop.action_evals_str
    troop.organization_coefficient.should == 1

    troop.organization_cost('食料').should == 2
    troop.organization_cost('燃料').should == 18
  end

  it '編成コスト文字列出力' do
    source = <<EOT
soldier:東国人＋サイボーグ＋宇宙軍＋剣士；
soldier:東国人＋サイボーグ＋宇宙軍＋剣士；
EOT

    troop = parse_troop(source)
    troop.action_evals_str
    troop.event_cost_str.should == <<EOT
出撃費用
資源：0万ｔ
食料：2万ｔ
燃料：0万ｔ
EOT
    troop.sp_cost_str.should == <<EOT
特殊消費
資源：0万ｔ
食料：0万ｔ
燃料：54万ｔ
EOT

    troop.organization_cost_str.should == <<EOT
合計消費
資源：0万ｔ
食料：2万ｔ
燃料：54万ｔ
EOT
  end

  it 'ドラグンバスター' do
    source = <<EOT
38-00444-02：ヴィザ：暁の民＋ドラゴンスレイヤー＋王＋ライオンハート＋歌い手＋ＨＱ体格＋ＨＱ外見：体格+2*筋力+8*耐久力+8*外見+2*敏捷+3*器用+2*感覚+2*知識+3*幸運+3；
－強靭刀：国有：白兵攻撃＋６（着用型／その他）
38-00435-02：枚方　弐式：暁の民＋大剣士＋ドラゴンスレイヤー＋剣：体格+2*筋力+8*耐久力+8*外見+2*敏捷+2*器用+2*感覚+2*知識+3*幸運+3；
－ドラグンバスター：国有：白兵攻撃＋６、白兵攻撃の燃料を消費しない（着用型／その他）
EOT
    troop = parse_troop(source)
    troop.action_evals_str
    maikata = troop.children[1]
    maikata.total_cost.should == {'燃料'=> -9}

    melee_sp = maikata.find_spec('ドラゴンスレイヤーの白兵距離戦闘補正')

    rms = maikata.resource_modifications
    rms.size.should == 1

    rms[0].is_applicable?(melee_sp).should eq true
    maikata.resource_modification_coefficient(melee_sp,'燃料').should == 0
    maikata.total_cost_with_modification.should == {'燃料' => -3}
    troop.total_cost_with_modification.should == {'燃料' => -6}
    troop.organization_cost('燃料').should == 6.0

    source = <<EOT
＃！個別付与：海軍兵站システム，本隊
ｌ：＊海軍兵站システムの燃料消費削減　＝　，，戦闘動員による燃料消費を１８％に削減する。

38-00444-02：ヴィザ：暁の民＋ドラゴンスレイヤー＋王＋ライオンハート＋歌い手＋ＨＱ体格＋ＨＱ外見：体格+2*筋力+8*耐久力+8*外見+2*敏捷+3*器用+2*感覚+2*知識+3*幸運+3；
－強靭刀：国有：白兵攻撃＋６（着用型／その他）
38-00435-02：枚方　弐式：暁の民＋大剣士＋ドラゴンスレイヤー＋剣：体格+2*筋力+8*耐久力+8*外見+2*敏捷+2*器用+2*感覚+2*知識+3*幸運+3；
－ドラグンバスター：国有：白兵攻撃＋６、白兵攻撃の燃料を消費しない（着用型／その他）
EOT
    troop = parse_troop(source)
    troop.action_evals_str

    troop.resource_modifications.size.should == 1

    maikata = troop.children[1]
    maikata.resource_modifications.size.should == 2

    troop.total_cost_with_modification.should == {'燃料' => -1.08}
    troop.organization_cost('燃料').should == 1

    source = <<EOT
＃！個別付与：海軍兵站システム，本隊
ｌ：＊海軍兵站システムの燃料消費削減　＝　，，戦闘動員による燃料消費を１８％に削減する。

＃38:暁の円卓藩国 T14編成
＃編成表:http://www30.atwiki.jp/thecircleofdaybreak/pages/263.html

＃！有効条件：契約した王と距離１０ｍ以内で一緒に行動する間の
＃！個別付与：藩王の指揮，全隊員
＃！除外：藩王の指揮，白石裕
＃！有効条件：＜藩王の指揮＞に関する

＃！個別付与：王の契約による強化，｛風杜神奈，枚方　弐式，さるき，まさきち｝

Ｌ：王の特産品による強化　＝　｛
　ｔ：名称　＝　王の特産品による強化（定義）
　ｔ：特殊　＝　｛
　　＊王の特産品による強化の特殊補正　＝　，条件発動，（白兵距離での）全判定、評価＋２。
　｝
｝

＃！個別付与：王の特産品による強化，｛風杜神奈，枚方　弐式，さるき，まさきち｝

＃！補正ＨＱ：ドラグンバスターの白兵距離戦闘補正，３
＃！補正ＨＱ：強靭刀の白兵距離戦闘補正，１
＃！補正ＨＱ：ますらおの鎧の防御補正，２

Ｌ：評価定義（三角飛びによる対空）　＝　｛
　ｌ：引数　＝　｛体格，筋力｝
　ｌ：補正対象　＝　攻撃
　ｌ：補正使用条件　＝　白兵距離での
　ｌ：行為分類　＝　白兵距離戦闘行為
｝

38-00697-01：白石裕：暁の民＋藩王＋王＋ライオンハート＋歌い手＋ＨＱ体格＋ＨＱ外見：体格+2*筋力+8*耐久力+8*外見+2*敏捷+3*器用+2*感覚+2*知識+3*幸運+3；
－永劫の炎：個人所有：生命に関する全判定＋３、アンデットに対する攻撃に評価＋３する（着用型／手先）
＃－永劫の炎のつるぎ：個人所有：白兵攻撃を自動成功にする。攻撃が火属性になる（着用型／その他）
－炎の王冠：個人所有：設定国民１万人までを動員して使うことができる（戦闘以外）（着用型／頭）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00697-01%A1%A7%C7%F2%C0%D0%CD%B5
38-00262-02：風杜神奈：暁の民＋レトロライフの狩人＋子供の夢を守る者＋剣＋ＨＱ筋力＋大吏族：体格+2*筋力+8*耐久力+8*外見+2*敏捷+3*器用+2*感覚+2*知識+3*幸運+3；
－強靭刀：国有：白兵攻撃＋６（着用型／その他）
－奇跡石の髪飾り：個人所有：資金５０億と交換可能。身分の証となる（着用型／頭）
－勇気の印：個人所有：士気崩壊を防ぐ（消費型、３個所持）
－ロボからのプレゼント：個人所有：ソードオブエンブリオコールが可能（消費型、３個所持）
＃－山の盾：個人所有：装甲＋８（消費型、１個所持）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00262-02%A1%A7%C9%F7%C5%CE%BF%C0%C6%E0
－山の盾所持根拠ＵＲＬ：http://maki.wanwan-empire.net/characters/265
38-00444-02：ヴィザ：暁の民＋ドラゴンスレイヤー＋王＋ライオンハート＋歌い手＋ＨＱ体格＋ＨＱ外見：体格+2*筋力+8*耐久力+8*外見+2*敏捷+3*器用+2*感覚+2*知識+3*幸運+3；
－強靭刀：国有：白兵攻撃＋６（着用型／その他）
－炎の髪留め：個人所有：老化しなくなる（着用型／頭）
－炎の髪留め所持根拠ＵＲＬ：http://maki.wanwan-empire.net/characters/684
38-00435-02：枚方　弐式：暁の民＋大剣士＋ドラゴンスレイヤー＋剣：体格+2*筋力+8*耐久力+8*外見+2*敏捷+2*器用+2*感覚+2*知識+3*幸運+3；
－ドラグンバスター：国有：白兵攻撃＋６、白兵攻撃の燃料を消費しない（着用型／その他）
38-00050-02：さるき：暁の民＋ドラゴンスレイヤー＋剣＋ソードマスター＋ＨＱ体格＋歌い手＋ＨＱ体格＋ＨＱ外見：体格+2*筋力+8*耐久力+8*外見+2*敏捷+3*器用+2*感覚+2*知識+3*幸運+2；
－強靭刀：国有：白兵攻撃＋６（着用型／その他）
－ますらおの鎧：国有：装甲＋７（着用型／全身）
38-00449-02：エンジェル明美：暁の民＋大剣士＋ドラゴンスレイヤー＋王：体格+2*筋力+8*耐久力+8*外見+2*敏捷+2*器用+2*感覚+2*知識+2*幸運+2；
－強靭刀：国有：白兵攻撃＋６（着用型／その他）
38-00334-02：時雨野　椿：暁の民＋大剣士＋ドラゴンスレイヤー＋王：体格+2*筋力+8*耐久力+8*外見+2*敏捷+3*器用+2*感覚+2*知識+3*幸運+3；
－強靭刀：国有：白兵攻撃＋６（着用型／その他）
38-00125-02：まさきち：暁の民＋大剣士＋ドラゴンスレイヤー＋剣：体格+2*筋力+8*耐久力+8*外見+2*敏捷+2*器用+2*感覚+2*知識+2*幸運+3；
－強靭刀：国有：白兵攻撃＋６（着用型／その他）
38-00123-02：岩澄龍彦：暁の民＋大剣士＋ドラゴンスレイヤー＋剣：体格+2*筋力+8*耐久力+8*外見+2*敏捷+2*器用+2*感覚+2*知識+3*幸運+3；
－強靭刀：国有：白兵攻撃＋６（着用型／その他）
38-00848-01：八重垣慶：暁の民＋大剣士＋ドラゴンスレイヤー＋剣＋法官：体格+2*筋力+8*耐久力+8*外見+2*敏捷+2*器用+2*感覚+2*知識+2*幸運+2；
－強靭刀：国有：白兵攻撃＋６（着用型／その他）

＃！状況：子供の夢を守る場合
＃！有効条件：聞いた子供の夢や子供を守る建前を守る限りにおいて

＃！評価派生：マジックアイテム不可，｛装甲，白兵戦，三角飛びによる対空｝
＃！除外：ドラグンバスター＋強靭刀＋ますらおの鎧，全隊員
EOT

    troop = parse_troop(source)
    troop.action_evals_str

    troop.resource_modifications.size.should == 1

    value = 0
#    troop.total_cost(:all , true){|unit,sp|
#      if sp.cost.has_key?('燃料')
#        coe = sp.owner.resource_modification_coefficient(sp , '燃料')
#        value += sp.cost['燃料'] * 3 * coe
#        puts "#{sp.owner.name}:#{sp.name} #{sp.cost.inspect}, #{coe} : #{value}"
#      end
#    }
    troop.organization_cost('燃料').should == 10
  end
  it '海軍兵站システム' do
    source = <<EOT
＃！個別付与：海軍兵站システム＋炎の料理人の料理強化，【本隊】

ｌ：＊海軍兵站システムの燃料消費削減　＝　，，戦闘動員による燃料消費を１８％に削減する。

soldier:東国人＋サイボーグ＋宇宙軍＋剣士；
soldier2:東国人＋サイボーグ＋宇宙軍＋軌道降下兵；
EOT
    troop = parse_troop(source)
    rm_sp = troop.find_spec('海軍兵站システムの燃料消費削減')
    puts rm_sp.source
    rm_sp.class.should == ResourceModificationSpec

    soldier = troop.children[0]
    sp_melee = soldier.find_spec('剣士の白兵距離戦闘補正')
    sp_event_cost = soldier.find_spec('東国人のイベント時食料消費')
    sp_enh1 = soldier.find_spec('サイボーグの筋力・耐久力補正')
    sp_enh2 = soldier.find_spec('宇宙軍の筋力・耐久力補正')

    rm_sp.is_applicable?(sp_melee).should eq true
    rm_sp.is_applicable?(sp_event_cost).should eq false
    rm_sp.is_applicable?(sp_enh1).should eq true
    rm_sp.is_applicable?(sp_enh2).should eq true

    rm_sp2 = troop.find_spec('炎の料理人の料理強化の食料追加消費')
    rm_sp2.is_applicable?(sp_melee).should eq false
    rm_sp2.is_applicable?(sp_event_cost).should eq true
    rm_sp2.is_applicable?(sp_enh1).should eq false
    rm_sp2.is_applicable?(sp_enh2).should eq false

    soldier2 = troop.children[1]

    rm_sp3 = soldier2.find_spec('軌道降下兵の燃料消費削減')

    rm_sp3.is_applicable?(sp_melee).should eq false
    rm_sp3.is_applicable?(sp_event_cost).should eq false
    rm_sp3.is_applicable?(sp_enh1).should eq true
    rm_sp3.is_applicable?(sp_enh2).should eq true

    troop.resource_modifications.size.should == 2
    soldier.resource_modifications.size.should == 2
    soldier2.resource_modifications.size.should == 3

    soldier.resource_modification_coefficient(sp_melee , '燃料').should == 0.18
    soldier2.resource_modification_coefficient(sp_enh2 , '燃料').should == 0.15

    food_cost_sp = soldier.find_spec('東国人のイベント時食料消費')
    soldier.resource_modification_coefficient(food_cost_sp,'食料').should == 2.0
#    soldier.resource_modification_coefficient(food_cost_sp,'食料').should == 1.0
    soldier.event_cost.should == {'食料' => -1}
    soldier.event_cost_with_modification.should == {'食料' => -2}
    soldier2.event_cost.should == {'食料' => -1}
    soldier2.event_cost_with_modification.should == {'食料' => -2}

    soldier.resource_modification_coefficient(sp_melee,'燃料').should == 0.18

    soldier.total_cost.should == {'燃料' => -24}
    soldier.total_cost_with_modification.should == {'燃料' => -4.32}
    soldier2.total_cost.should == {'燃料' => -18}
    soldier2.total_cost_with_modification.should == {'燃料' => -6*3*0.15}


    troop.organization_cost('燃料').should == 7.0
    troop.organization_cost('食料').should == 4.0

  end
  
  it '消費削減系特殊導入テスト' do
    source = <<EOT
＃！個別付与：初心級宇宙空母の搭載，本隊
＃！部分有効条件：戦闘する場合での

_define_I=D: {Ｐ１_騎士団所有：多国籍戦術戦闘機”ペヤーン”,
－多国籍戦術戦闘機”ペヤーン”の増槽：ＡＲ＋５、全評価－３（追加された５ＡＲを使い切るまで）　この装備は重複装備できない。
（Ｐ）18-00345-01_竜宮・司・ヒメリアス・ドラグゥーン：詩歌の民+歩兵+パイロット+法官：敏捷+1*知識+6*幸運+1*外見+2,
（ＣＰ）29-xx001-xx_犬士０１：西国人+スターファイター+スターライナー+スターリフター+ＳＨＱ感覚
}；

_define_I=D:{
ＲＴＲ１_星鋼京所有：ＲＴＲ＋ＳＨＱ敏捷,
－増加燃料装備：藩国所有：装甲+6*ＡＲ+3*全評価-3　増加分ＡＲ３使用後、全評価+3*装甲-6
（P）42-00612-01_御鷹：北国人＋撃墜王＋トップガン＋教導パイロット＋法の司＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*感覚+5*知識+1*幸運+1;
－法の執行者の紋章：個人所有：
（CP）42-00530-02_木曽池春海：北国人＋撃墜王＋トップガン＋教導パイロット＋護民官＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：器用+1*感覚+2*知識+1*幸運+1;
－互尊Ｃ：個人所有：
（CP）42-00610-01_センハ：北国人＋撃墜王＋トップガン＋教導パイロット＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*幸運+1;};

EOT
    troop = parse_troop(source)
    troop.action_evals_str

    troop.organization_cost('燃料').should == 21

    source = <<EOT
＃！個別付与：栄光の野戦炊飯具１号の効果＋補給士官の業務，本隊
soldier1:暁の民＋大剣士＋剣＋ドラゴンスレイヤー；
soldier2:暁の民＋大剣士＋剣＋ドラゴンスレイヤー+補給士官；
EOT
    troop = parse_troop(source)
    troop.action_evals_str

    troop.organization_cost('食料').should == (4*2*((0.75*0.75*100).floor*0.01)).floor
    troop.organization_cost('燃料').should == (6*3*0.75).floor

    source = <<EOT
＃！個別付与：戦争の天才の物資掌握，｛soldier1,soldier2｝
＃！個別付与：陸軍兵站システム，本隊

soldier1:はてない国人＋戦争の天才＋歩兵＋生徒会役員；
soldier2:はてない国人＋歩兵＋学生＋生徒会役員；
soldier3:北国人＋帝國軍歩兵＋偵察兵＋名パイロット；
EOT
    troop = parse_troop(source)
    troop.action_evals_str

    troop.organization_cost('燃料').should == (6*3*0.5 + 3*3).floor
    troop.organization_cost('食料').should == (1*3*0.75).floor

   source = <<EOT
＃！個別付与：整備士官の業務＋資源低減技術の開発＋資源削減計画，本隊
＃！部分有効条件：戦闘する場合での

_define_I=D: {Ｐ１_騎士団所有：多国籍戦術戦闘機”ペヤーン”,
－多国籍戦術戦闘機”ペヤーン”の増槽：ＡＲ＋５、全評価－３（追加された５ＡＲを使い切るまで）　この装備は重複装備できない。
（Ｐ）18-00345-01_竜宮・司・ヒメリアス・ドラグゥーン：詩歌の民+歩兵+パイロット+整備士官：敏捷+1*知識+6*幸運+1*外見+2,
（ＣＰ）29-xx001-xx_犬士０１：西国人+スターファイター+スターライナー+スターリフター+ＳＨＱ感覚
}；

_define_I=D:{
ＲＴＲ１_星鋼京所有：ＲＴＲ＋ＳＨＱ敏捷,
－増加燃料装備：藩国所有：装甲+6*ＡＲ+3*全評価-3　増加分ＡＲ３使用後、全評価+3*装甲-6
（P）42-00612-01_御鷹：北国人＋撃墜王＋トップガン＋教導パイロット＋法の司＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：敏捷+1*感覚+5*知識+1*幸運+1;
－法の執行者の紋章：個人所有：
（CP）42-00530-02_木曽池春海：北国人＋撃墜王＋トップガン＋教導パイロット＋護民官＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚：器用+1*感覚+2*知識+1*幸運+1;
－互尊Ｃ：個人所有：
（CP）42-00610-01_センハ：北国人＋撃墜王＋トップガン＋教導パイロット＋ＨＱ感覚＋ＨＱ感覚＋ＨＱ感覚＋補給士官：敏捷+1*幸運+1;};

EOT
    troop = parse_troop(source)
    troop.action_evals_str

    troop.organization_cost('資源').should == (5*((0.9*0.75*0.9*100.floor)*0.01)).floor
  end
  
  it '消費削減と追加消費係数の分離' do
    source = <<EOT
＃！個別付与：料理人の料理強化＋炎の料理人の料理強化＋陸軍兵站システム，本隊
soldier1:暁の民＋大剣士＋剣＋料理人；
soldier2:暁の民＋蟲使い＋剣＋炎の料理人；
EOT

    troop = parse_troop(source)
    troop.action_evals_str

    soldier1 = troop.children[0]
    sp1 = soldier1.find_spec('暁の民のイベント時食料消費')
    spms = soldier1.select_resource_modifications(sp1,'食料')
    spms.size.should == 3
    soldier1.resource_multiplier(sp1,'食料').should == 4.0
    soldier1.resource_reduction_coefft(sp1,'食料').should == 0.75

    soldier2 = troop.children[1]
    sp2 = soldier2.find_spec('暁の民のイベント時食料消費')
    soldier2.resource_multiplier(sp2,'食料').should == 8.0
    soldier2.resource_reduction_coefft(sp1,'食料').should == 0.75

    troop.organization_cost('食料').should == 36

  end

  it '料理人と栄光の野戦炊飯具１号は人アイドレスの食料消費にのみ影響する' do
    source = <<EOT

＃！個別付与：炎の料理人の料理強化＋陸軍兵站システム，本隊

ｌ：＊炎の料理人の料理強化の食料追加消費　＝　，，（食事として）の食料消費を２倍にする。
ｌ：＊東国人のイベント時食料消費　＝　，条件発動，（一般行為判定を伴うイベントに参加するごとに、食事として）食料－１万ｔ。

soldier1:東国人＋軌道降下兵＋大剣士＋剣＋天陽；　＃１＊２＋１＝３
soldier2:東国人＋蟲使い＋ギーク＋炎の料理人；　＃１＊２＊２＝４
EOT

    troop = parse_troop(source)
    troop.action_evals_str

    soldier1 = troop.children[0]
    rm1 = soldier1.resource_modifications.find{|sp|sp.name == '炎の料理人の料理強化の食料追加消費'}
    rm2 = soldier1.resource_modifications.find{|sp|sp.name == '陸軍兵站システムの食料消費削減'}

    ev_cost1 = soldier1.find_spec('東国人のイベント時食料消費')
    ev_cost2 = soldier1.find_spec('天陽のイベント時食料消費')

    ev_cost1.separated_conditions(ev_cost1['消費単位']).should == ['一般行為判定を伴うイベントに参加するごとに','食事として']
    rm1.is_applicable?(ev_cost1).should eq true
    rm1.is_applicable?(ev_cost2).should eq false

    rm2.is_applicable?(ev_cost1).should eq true
    rm2.is_applicable?(ev_cost2).should eq true

    troop.organization_cost('食料').should == 5

  end

  it 'ユニットの航路数を得られる' do
    source = <<EOT
soldier1:暁の民＋大剣士＋剣＋料理人；
EOT
    troop = parse_troop(source)
    soldier1 = troop.children[0]
    soldier1.hops.should be_nil

    source = <<EOT
_define_vessel:{
Ｄ１_国有：「きゃりっじ」,
（Ｐ）44-00220-01_奥羽りんく：西国人＋猫の決戦存在＋スペーススターシップオフィサー＋宰相の娘＋秘書官：外見+2*敏捷+1*器用+1*知識+1*幸運+1；
＃－恩寵の時計：個人所有：外見＋２（着用型／首）
－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／腕）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00220-01%A1%A7%B1%FC%B1%A9%A4%EA%A4%F3%A4%AF
（Ｐ）00-xx054-xx_猫士1：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
（ＣＰ）00-xx055-xx_猫士2：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
（ＣＰ）00-xx056-xx_猫士3：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
};
EOT
    troop = parse_troop(source)
    carridge = troop.children[0]
    hops = carridge.hops
    hops.should == 2
  end

  it '蟲使い（個人単位）の食料消費２倍' do
    source = <<EOT
soldier:東国人＋蟲使い＋ギーク＋理力使い；
EOT
    troop = parse_troop(source)
    troop.action_evals_str

    soldier = troop.children[0]
    food_cost = soldier.find_spec('東国人のイベント時食料消費')
    rms = soldier.select_resource_modifications(food_cost,'食料')
    rms.size.should == 1
    rms[0].name.should == '蟲使いの食料追加消費'
    troop.organization_cost('食料').should == 2
  end
  it 'T16迄の猫・犬士食料消費切り上げルール再現' do
    source = <<EOT
00-xx001-xx_dog1：はてない国人＋帝國軍歩兵；
00-xx002-xx_dog2：はてない国人＋帝國軍歩兵；
00-xx003-xx_dog3：はてない国人＋帝國軍歩兵；
EOT
    troop = parse_troop(source)
    troop.action_evals_str
    troop.organization_cost('食料').should == 1

    source = <<EOT
ｌ：猫・犬士食料消費の切り上げ　←　'有効'

00-xx001-xx_dog1：はてない国人＋帝國軍歩兵；
00-xx002-xx_dog2：はてない国人＋帝國軍歩兵；
00-xx003-xx_dog3：はてない国人＋帝國軍歩兵；
EOT
    troop = parse_troop(source)
    troop.action_evals_str
    troop.organization_cost('食料').should == 2

    source = <<EOT
ｌ：猫・犬士食料消費の切り上げ　←　有効

＃！個別付与：海軍兵站システム，本隊
＃！消費削減ＨＱ：海軍兵站システムの燃料消費削減，１８％　＃宰相府の海軍兵站システムＨＱを適用

_define_I=D:{東雲壱_たけきの藩国所有：東雲ノ鎧,
（Ｐ）26-00497-01_志水高末：東国人＋拳法家＋龍の使い＋武繰＋ＨＱ体格＋参謀：敏捷+1*知識+1*幸運+1；
}；
EOT

    troop = parse_troop(source)
    troop.action_evals_str
    troop.organization_cost('燃料').should == 2

  end

  describe 'タプルによる評価出力' do
    subject  { parse_troop(source)}
    context 'タプルの配列を返す' do
      let(:source) {"test：アウィナイト；\n"}
      it '最初のメンバーのevals_tuples が配列を返す' do
        subject.children[0].evals_tuples.should == [['体格' , 14] , ['筋力' , 14],['耐久力' , 15],['外見' ,14 ],['敏捷' ,18 ],['器用' ,6 ],['感覚' ,0 ],['知識' ,7 ],['幸運' ,7 ]]
      end
      it '部隊のevals_tuplesが配列を返す' do
        subject.evals_tuples.should == [['体格' , 14] , ['筋力' , 14],['耐久力' , 15],['外見' ,14 ],['敏捷' ,18 ],['器用' ,6 ],['感覚' ,0 ],['知識' ,7 ],['幸運' ,7 ]]
      end

      it 'common_abilities_evals_tuples' do
        subject.common_abilities_evals_tuples.should == [['装甲（通常）','自動成功，なし'],['魅力',10]]
      end

      #action_evals_str
      it 'action_evals_tuples' do
        puts 'test:' +  subject.action_evals_str
        subject.action_evals_tuples.should == [
            ['白兵戦','自動成功'],['遠距離戦',12], ['侵入（敏捷・器用）' ,12 ] ,['侵入（幸運）' ,7 ] ,
            ['追跡（感覚）' ,0 ] ,  ['追跡（幸運）' ,7 ] , ['偵察' ,  0] , ['隠蔽' ,  7] ,
            ['陣地構築（筋力）' , 14 ] ,['陣地構築（知識）' , 7 ] ,['歌唱' , 10 ]
        ]
      end

      #leaders_taplues
      it 'leaders_tuples' do
        puts subject.leaders_str
        subject.leaders_tuples.should == [['同調：test' , 14]]

#        p parse_troop("test:アウィナイト；\ntest2:アウィナイト；").leaders_tuples
      end

      it 'all_evals_tuples' do
        evals =  subject.ar_tuple + subject.evals_tuples + subject.common_abilities_evals_tuples +
            subject.action_evals_tuples + subject.leaders_tuples


        subject.all_evals_tuples.should == evals

      end

    end
  end

  describe 'AR' do
    it '初期ARが指定されている' do
      source = <<EOT
cat:猫神
EOT
      troop = parse_troop(source)
      expect(troop.children.first.initial_ar).to eq 12
      expect(troop.children.first.ar).to eq 12
    end

    it 'デフォルトで10' do
      source = <<EOT
Ｌ：普通の人　＝　｛
　ｔ：名称　＝　普通の人（人）
　ｔ：評価　＝　全能力０
　ｔ：特殊　＝　｛
　　＊普通の人の職業カテゴリ　＝　，，人アイドレス。
　｝
｝
soldier:普通の人
EOT
      troop = parse_troop(source)
      soldier = troop.children.first
      expect(soldier.ar).to eq 10
      expect(soldier.initial_ar).to eq 10
      expect(soldier.ar_modifiers.size).to eq 0
    end

    it 'ARボーナスがある。' do
      source = <<EOT
soldier：空中戦型ホープツー＋摂政
－エアバイクピケ
EOT
      troop = parse_troop(source)
      soldier = troop.children.first
      expect(soldier.ar_modifiers.size).to eq 2
      expect(soldier.ar_modifiers[0]['ＡＲ']).to eq -1
#      expect(soldier.ar_modifiers[1]['ＡＲ']).to eq 5
      expect(soldier.ar_modifiers[1]['ＡＲ']).to eq 3
      expect(soldier.ar).to eq 12
    end

    it '有効条件付きのARボーナスがある。' do
      source = <<EOT
＃！有効条件：飛行状態での
soldier：空中戦型ホープツー＋摂政
－エアバイクピケ
EOT
      troop = parse_troop(source)
      soldier = troop.children.first
      expect(soldier.ar_modifiers.size).to eq 3
      expect(soldier.ar_modifiers[0]['ＡＲ']).to eq 5
      expect(soldier.ar_modifiers[1]['ＡＲ']).to eq -1
      expect(soldier.ar_modifiers[2]['ＡＲ']).to eq 3
      expect(soldier.ar).to eq 17
    end


  end

end

