# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'../fcalc/fcalc_defs.rb')


describe ILanguage::FCalc::FCalcFormatParser do
  before(:each) do
    @fparser = ILanguage::FCalc::FCalcFormatParser.new
#    @context = Context.new
  end


  it '即値' do
    root = @fparser.parse('１２３')

    root.should_not be_nil
    number = root.interpret
    number.should_not be_nil
    number.evaluate.should == 123

    root = @fparser.parse('１２３．４')

    root.should_not be_nil
    root.interpret.evaluate.should == 123.4
  end

  it '二項式' do
    root = @fparser.parse('１+２')
    root.should_not be_nil
#    root.extension_modules.should include(BinaryExprNode)
    bi_node = root.interpret
    bi_node.class.should ==  ILanguage::FCalc::BinaryExprFunctor
    bi_node.function_name.should == :add
    bi_node.evaluate.should == 3

    root = @fparser.parse('１-２')
    root.should_not be_nil
#    root.extension_modules.should include(BinaryExprNode)
    bi_node = root.interpret
    bi_node.class.should ==  ILanguage::FCalc::BinaryExprFunctor
    bi_node.function_name.should == :subtract
    bi_node.evaluate.should == -1

    root = @fparser.parse('１*２')
    root.should_not be_nil
    bi_node = root.interpret
    bi_node.class.should ==  ILanguage::FCalc::BinaryExprFunctor
    bi_node.function_name.should == :multiply
    bi_node.evaluate.should == 2

    root = @fparser.parse('１/２')
    root.should_not be_nil
    bi_node = root.interpret
    bi_node.class.should ==  ILanguage::FCalc::BinaryExprFunctor
    bi_node.function_name.should == :divide
    bi_node.evaluate.should == 0.5

    root = @fparser.parse('１ + ２ +3')
    root.should_not be_nil
    bi_node = root.interpret
    bi_node.class.should ==  ILanguage::FCalc::BinaryExprFunctor
    bi_node.function_name.should == :add
    bi_node.evaluate.should == 6

    root = @fparser.parse('2 * ２ + 3')
    root.should_not be_nil
    bi_node = root.interpret
    bi_node.class.should ==  ILanguage::FCalc::BinaryExprFunctor
    array = root.last_expr.to_list
    ILanguage::FCalc::BinaryExprNode.find_first_operator(array).should == 1
    rpn = ILanguage::FCalc::BinaryExprNode.sort_to_rpn(array)
    rpn.collect{|item|item.inspect_expr}.should == [2 ,2 ,:multiply , 3 ,:add ]
#    puts root.inspect_expr
    array.size.should == 5
    bi_node.evaluate.should == 7

    root = @fparser.parse('2 ＋ ３ ＊４')
    root.should_not be_nil
    bi_node = root.interpret
    bi_node.evaluate.should == 14

    root = @fparser.parse('2 * 3 + 4* 5')
    root.should_not be_nil
    bi_node = root.interpret
    bi_node.evaluate.should == 26
  end

  it '括弧式' do
    root = @fparser.parse('（１+２）')
    root.should_not be_nil
    bi_node = root.interpret
    bi_node.evaluate.should == 3

    root = @fparser.parse('（2+3）*4')
    root.should_not be_nil
    bi_node = root.interpret
    bi_node.evaluate.should == 20

    root = @fparser.parse('（2+3*4）*5')
    root.should_not be_nil
    bi_node = root.interpret
    bi_node.evaluate.should == 70

  end

  it '変数代入' do
    source = <<EOT
ｌ：変数Ａ　←　３
EOT
    root = @fparser.parse(source)
#    root = @fparser.parse('ｌ：変数Ａ　← 3')
    root.should_not be_nil
    root.interpret.evaluate.should == 3
  end

  it '配列' do
    root = @fparser.parse('｛３｝')
    root.should_not be_nil
    root.interpret.evaluate.should == [3]

    root = @fparser.parse('｛３，４，２｝')
    root.should_not be_nil
    root.interpret.evaluate.should == [3,4,2]

#    root = @fparser.parse('｛３，４，２｝［１］')
#    root.should_not be_nil
#    root.interpret.evaluate.should == 4
  end

  it '変数参照' do
    source = <<EOT
ｌ：変数Ａ　←　4
＜変数Ａ＞＊２
EOT
    root = @fparser.parse(source)
    root.should_not be_nil
    root.interpret.evaluate.should == 8

    source = <<EOT
ｌ：亜細亜の曙のトークン　←　4
＜亜細亜の曙のトークン＞
EOT
    root = @fparser.parse(source)
    root.should_not be_nil
    root.interpret.evaluate.should == 4

    source = <<EOT
ｌ：亜細亜の曙のトークン　←　4
ｌ：ＲＴＲの対空ミサイル発射数　←　３
＜亜細亜の曙のトークン＞　＋　２　＊　＜ＲＴＲの対空ミサイル発射数＞
EOT
    root = @fparser.parse(source)
    root.should_not be_nil
    root.interpret.evaluate.should == 10

    source = <<EOT
ｌ：亜細亜の曙のトークン　←　4
ｌ：皇帝の気合度　←　２
ｌ：ＲＴＲの対空ミサイル発射数　←　＜皇帝の気合度＞＊２
＜亜細亜の曙のトークン＞　＋　２　＊　＜ＲＴＲの対空ミサイル発射数＞
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
    root.interpret.evaluate.should == 12

    source = <<EOT
ｌ：検証中機能　←　true
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
    root.interpret.evaluate.should == true
  end

  it '関数定義' do
    source = <<EOT
ｌ：直積　←　関数（引数１：数，引数２：数値）｛
　＜引数１＞＊＜引数２＞
｝

＜直積＞（２，3）
EOT

    proc{
      root =  @fparser.parse(source).interpret
    }.should raise_error(RuntimeError , "仮引数'引数１'の型名が不正です：数。")

    source = <<EOT
ｌ：直積　←　関数（引数１：数値，引数２：数値）｛
　＜引数１＞＊＜引数２＞
｝

＜直積＞（２，3）
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
    root.interpret.evaluate.should == 6

    source2 = <<EOT
ｌ：intersect　←　関数（param1：数値，param2：数値）｛
　＜param1＞＊＜param2＞
｝

ｌ：var1　←　＜intersect＞（２，３）


＜intersect＞（＜var1＞，4）
EOT

    root = @fparser.parse(source2)
    root.should_not be_nil
    root.interpret.evaluate.should == 24
  end

  it 'FunctorCalculation' do
    root = @fparser.parse('１+２')

    functor = root.interpret
    calc = ILanguage::FCalc::FunctorCalculation.new
    result = functor.evaluate(functor.context , :calculation => calc)
    result.should == 3
    calc.children.size.should ==1
    last = calc.last_child
    last.function_name.should == :add
    last.args.should == [1 , 2]
    last.result.should == 3

    root = @fparser.parse('１+２*3')

    functor = root.interpret
    calc = ILanguage::FCalc::FunctorCalculation.new
    result = functor.evaluate(functor.context , :calculation => calc)
    result.should == 7

    calc.children.size.should ==2
    calc1 = calc.children[0]
    calc1.function_name.should == :multiply
    calc1.args.should == [2 , 3]
    calc1.result.should == 6

    calc2 = calc.children[1]
    calc2.function_name.should == :add
    calc2.args.should == [1 , 6]
    calc2.result.should == 7

  end

  it 'コメント' do
#    root = @fparser.parse('34  ')
    root = @fparser.parse('34  #comment')
    root.should_not be_nil
    root.interpret.evaluate.should == 34

    source = <<EOT
ｌ：ある変数　←　３　＃コメント

＜ある変数＞＋４
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
    root.interpret.evaluate.should == 7

  end

  it '組み込み関数' do
    root = @fparser.parse('＜ＲＤ＞（１+２）')

    functor = root.interpret
    rd_functor = functor.context.variable('ＲＤ')
    rd_functor.should_not be_nil
    rd_functor.function_name.should == 'ＲＤ'
    calc = ILanguage::FCalc::FunctorCalculation.new
    result = functor.evaluate(functor.context , :calculation => calc)
    result.should == 1.7

    calc.children.size.should == 2
    calc.to_s.should == "1 + 2 = 3\nＲＤ(3) = 1.7"


    root = @fparser.parse('＜合計＞（3,4,5）')
    functor = root.interpret
    result = functor.evaluate(functor.context , :calculation => calc)
    result.should == 12


    root = @fparser.parse('＜評価変換＞（＜ＲＤ＞（3）＋＜ＲＤ＞（５））')
    functor = root.interpret
    result = functor.evaluate(functor.context , :calculation => calc)
    result.should == 7

    source = <<EOT
ｌ：黒埼の知力　←　３０

＜ＲＤ合算＞（
　32　，　＃ＥＨＧ２の陣地構築（筋力）
　＜黒埼の知力＞
）
EOT


    root = @fparser.parse(source)
    root.should_not be_nil
    functor = root.interpret
    calc = ILanguage::FCalc::FunctorCalculation.new
    result = functor.evaluate(functor.context , :calculation => calc)
    result.should == 34
  end

  it 'ＲＤ合算演算子”＆”' do
    source = <<EOT
３ ＆ ４
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
    functor = root.interpret
    calc = ILanguage::FCalc::FunctorCalculation.new
    result = functor.evaluate(functor.context , :calculation => calc)
    result.should == 7

    calc.to_s.should match(/ＲＤ合算\(3 , 4\) = 7$/)
  end

  it '循環参照' do
    source = <<EOT
ｌ：変数Ａ　←　＜変数Ｂ＞

ｌ：変数Ｂ　←　＜変数Ｃ＞

ｌ：変数Ｃ　←　＜変数Ａ＞

ｌ：変数Ｄ　← ３

ｌ：変数Ｅ　←　＜変数Ｄ＞

＜変数Ａ＞＋＜変数Ｄ＞
EOT

    root = @fparser.parse(source)

    root.should_not be_nil
    expr = root.interpret

    var_a = expr.context.variable('変数Ａ')
    var_a.is_circular?(expr.context).should be_truthy

    var_d = expr.context.variable('変数Ｅ')
    var_d.is_circular?(expr.context).should be_falsey

    proc {expr.evaluate}.should raise_error(RuntimeError , /循環参照/)

    calc = ILanguage::FCalc::FunctorCalculation.new
    proc{expr.evaluate(expr.context , :calculation => calc)}.should raise_error(RuntimeError , /循環参照/)

    calc.result.should be_nil
    calc.error.to_s =~ /循環参照/
#    proc {expr.evaluate}.should raise_error(RuntimeError , /test/)
  end
  
  it '関数参照（適用なし）' do
    source = <<EOT
ｌ：変数　←　関数（Ａ：数値）｛
　＜Ａ＞＊０.７５
｝

＜変数＞
EOT

    root = @fparser.parse(source)
    expr = root.interpret
    var =  expr.evaluate
    var.class.should == ILanguage::FCalc::UserDefinedFunctor
    #    proc {expr.evaluate}.should raise_error(RuntimeError , /test/)
  end

  it '関数を返す関数' do
    source = <<EOT
ｌ：変数　←　関数（Ａ：数値）｛
　関数（Ｂ：数値）｛
　　＜Ａ＞＋＜Ｂ＞
　｝
｝

ｌ：カリー化関数　←　＜変数＞（２）

＜カリー化関数＞（３）
EOT

    root = @fparser.parse(source)
    expr = root.interpret
    ctx = expr.context
    curry_var = ctx.variable('カリー化関数')
    curry_var.class.should == ILanguage::FCalc::ReferenceFunctor
    curry_func = curry_var.evaluate(ctx)
    curry_func.class.should == ILanguage::FCalc::UserDefinedFunctor

    var =  expr.evaluate(ctx)
    var.class.should <= Numeric
    var.should == 5
    #    proc {expr.evaluate}.should raise_error(RuntimeError , /test/)
  end

  it '関数のみの式' do
    source = <<EOT
関数（Ａ：関数，Ｂ：関数）｛
　関数（引数１：任意）｛
　　＜B＞（＜Ａ＞（＜引数１＞））
　｝
｝
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
    expr = root.interpret

    expr.class.should <= ILanguage::FCalc::UserDefinedFunctor
  end

  it '関数合成' do
    source = <<EOT
ｌ：関数合成テスト　←　関数（Ａ：関数，Ｂ：関数）｛
　関数（引数１：任意）｛
　　＜Ｂ＞（＜Ａ＞（＜引数１＞））
　｝
｝

ｌ：インクリメント　←　関数（引数：数値）｛
　＜引数＞＋１
｝

ｌ：三倍　←　関数（引数：数値）｛
　＜引数＞＊３
｝

ｌ：足して三倍　←　＜関数合成テスト＞（＜インクリメント＞，＜三倍＞）

ｌ：ビルトイン合成テスト　←　＜関数合成＞（＜インクリメント＞，＜三倍＞）

＜足して三倍＞（２）
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
    expr = root.interpret
    expr.class.should == ILanguage::FCalc::ReferenceFunctor
    expr.evaluate.should == 9

    ctx = expr.context
    merge = ctx.variable('関数合成テスト')
    inc = ctx.variable('インクリメント')
    triple = ctx.variable('三倍')

    merged = merge.evaluate(ctx,:args => [triple , inc])
    merged.evaluate(ctx , :args => [2]).should == 7

    b_ref = ctx.variable('ビルトイン合成テスト')
    b_test = b_ref.evaluate(ctx)
    result = b_test.evaluate(ctx,:args => [3])
    result.class.should <= Numeric
    result.should == 12

    calc = ILanguage::FCalc::FunctorCalculation.new
    b_test.evaluate(ctx,:args => [3] , :calculation => calc)
    calc.to_s.should == "3 + 1 = 4\n4 * 3 = 12"
  end
  it '数値の％表記' do
    source = <<EOT
ｌ：海軍兵站システム効果　←　７５％＊０．７５

＜海軍兵站システム効果＞＊７５％
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
    expr = root.interpret

    expr.evaluate.should == 0.421875

  end

  it 'Expr#calculate' do
    source = <<EOT
ｌ：海軍兵站システム効果　←　７５％＊０．７５

＜海軍兵站システム効果＞＊７５％
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
    expr = root.interpret

    calc = expr.calculate(expr.context)
    calc.should_not be_nil
    puts calc.to_s
  end

  it '負数' do
    source = <<EOT
２＊－３
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
    expr = root.interpret

    expr.evaluate.should == -6
    calc = expr.calculate(expr.context)

    calc.result.should == -6
  end

  it '負数2' do
    source = <<EOT
-２＊－３
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
    expr = root.interpret

    calc = expr.calculate(expr.context)

    calc.result.should == 6
  end

  it '文字列リテラル' do
    source = <<EOT
ｌ：文字１　←　'abc"'
ｌ：文字２　←　"ａｂｃ'"
ｌ：文字３　←　”ａｂｃ’”
ｌ：文字４　←　’ａｂｃ”’

＜文字１＞
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
    expr = root.interpret

    ctx = expr.context
    calc = expr.calculate(ctx)
    calc.result.should == 'abc"'

    ctx.variable('文字２').should == "ａｂｃ'"
    ctx.variable('文字３').should == "ａｂｃ’"
    ctx.variable('文字４').should == "ａｂｃ”"

  end

  it '配列リテラル' do
    source = <<EOT
ｌ：配列１　←　｛1 ,3 ，15｝
ｌ：配列２　←　{'装甲' ， "情報戦防御"}

＜配列１＞
EOT

    root = @fparser.parse(source)
    puts @fparser.failure_reason if root.nil?
    root.should_not be_nil
    expr = root.interpret

    ctx = expr.context
    calc = expr.calculate(ctx)
    calc.result.should == [1,3,15]

    ctx.variable('配列２').evaluate.should == ['装甲' , "情報戦防御"]

  end


  it '関数を引数とする関数の型チェック' do
    source = <<EOT
ｌ：ｎ倍する関数　←　関数（倍数：数値）｛
　関数（引数１：数値）｛
　　＜引数１＞　＊　＜倍数＞
　｝
｝

ｌ：三倍する　←　＜ｎ倍する関数＞（３）

ｌ：ｎ倍してから１足す　←　関数（引数２：数値，ｎ倍関数：関数）｛
　＜ｎ倍関数＞（＜引数２＞）＋１
｝

＜ｎ倍してから１足す＞（４，＜三倍する＞）
EOT

    root = @fparser.parse(source)
    root.should_not be_nil
#    root.extension_modules.should include(BinaryExprNode)
    expr = root.interpret

    calc = expr.calculate
    calc.result.should == 13
  end
end