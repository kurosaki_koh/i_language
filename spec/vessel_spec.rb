# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')


describe Vessel do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end

  it "基本" do
    source = <<EOT
＃ ！有効条件：位置づけ（艦船系）の乗り物に搭乗している場合での
＃ ！有効条件：位置づけ（艦船系）の乗り物の艦上で活動する場合での
_define_vessel:{vessel：ムーン級パトロールフリゲート,
salor1：船乗り；
salor2：船乗り；
};
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    vessel = @context.units['vessel']
    vessel.unit_type.include?('搭乗').should == true
    salor1 = vessel.children[0]
    salor1.evals['体格'].should == 1
    salor1.unit_type.include?('搭乗').should == true

    salor1_p_conds = salor1.all_partial_conditions
    salor1_p_conds.any?{|re| '位置づけ（艦船系）の乗り物に搭乗している場合での' =~ re}.should be_truthy
    salor1_p_conds.any?{|re| '位置づけ（艦船系）の乗り物の艦上で活動する場合での' =~ re}.should be_truthy
    sp_bonus = salor1.find_spec('船乗りの艦上補正')
    sp_bonus.target.should == salor1
    sp_bonus.is_applicable?(salor1,'体格').should

    salor1.evaluate('体格').should == 3
    salor1.is_evaluate_item?(sp_bonus).should be_truthy

    troop.evals['体格'].should == 6
    troop.evaluate('体格').should == 6
    cost = troop.total_cost
    cost.should_not be_nil
    puts cost.inspect

    estr = troop.evals_str
    estr.should_not be_nil
    puts estr

    puts troop.evals.keys
    armor = Evaluator['装甲（通常）']
    troop.evals_keys.include?(armor.class_name).should be_truthy
    sp_bonus.is_applicable?(vessel,'装甲').should be_truthy
    
    armor.evaluate(troop).should == 19
  end

  it '冒険艦' do
    source = <<EOT
＃ ！有効条件：｛水上艦船，宇宙艦船｝にコパイロットとして搭乗している場合での
＃ ！有効条件：宇宙艦船に搭乗している場合での
＃ ！有効条件：位置づけ（艦船系）の乗り物に搭乗している場合での

＃！個別付与：藩王の指揮，全隊員
＃！除外：藩王の指揮，さくらつかさ

_define_vessel:{vessel1：冒険艦大逆転号,
（Ｐ）17-00321-01：さくらつかさ：東国人＋船乗り＋藩王＋海賊＋玉の守護者：敏捷+1*器用+1*知識+1*幸運+1；
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00321-01%A1%A7%A4%B5%A4%AF%A4%E9%A4%C4%A4%AB%A4%B5
（Ｐ）17-00326-01：曲直瀬りま：東国人＋摂政＋スペーススターシップオフィサー＋海賊＋法官：外見+2*敏捷+1*知識+1*幸運+1
－恩寵の時計：個人所有：外見＋２（着用型／首に着用するもの）
－猫と犬の前足が重なった腕輪：個人所有：同調判定＋３（着用型／腕輪）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00326-01%A1%A7%B6%CA%C4%BE%C0%A5%A4%EA%A4%DE
（Ｐ）17-00328-01：夜狗　樹：東国人＋船乗り＋理力使い＋海賊＋宇宙艦長＋ＨＱ感覚：敏捷+1*幸運+1
－猫と犬の前足が重なった腕輪：個人所有：同調判定＋３（着用型／腕輪）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00328-01%A1%A7%CC%EB%B6%E9%A1%A1%BC%F9
（Ｐ）17-00339-01：きみこ：東国人+船乗り＋スペーススターシップオフィサー＋海賊＋吏族：敏捷+1*知識+1*幸運+1
－猫と犬の前足が重なった腕輪：個人所有：同調判定＋３（着用型／腕輪）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00339-01%A1%A7%A4%AD%A4%DF%A4%B3
（Ｐ）17-00781-01：菩鋳螺：東国人＋船乗り＋理力使い＋海賊：
－卒業記念記章：個人所有：なし（着用型／その他）
－卒業証書：個人所有：なし
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00781-01%A1%A7%CA%EE%C3%F2%CD%E6
（Ｐ）17-00784-01：光儀：東国人＋船乗り＋理力使い＋海賊：
－卒業証書：個人所有：なし
－アンデットバスター：個人所有：アンデッドに対する攻撃＋５（着用型／その他）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00784-01%A1%A7%B8%F7%B5%B7
（ＣＰ）17-00132-01：不変空沙子：東国人＋船乗り＋機関士＋海賊＋ＨＱ知識＋ヤガミの恋人＋ＨＱ外見＋ＨＱ幸運＋ＨＱ幸運：敏捷+1*知識+1*幸運+1;
＃―ヤガミの恋人取得根拠：http://blog.tendice.jp/200705/article_63.html
（ＣＰ）17-00323-01：オカミチ：東国人＋船乗り＋機関士＋海賊＋ＨＱ知識＋護民官：外見+2*敏捷+1*幸運+1
－恩寵の時計：個人所有：外見＋２（着用型／首に着用するもの）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00323-01%A1%A7%A5%AA%A5%AB%A5%DF%A5%C1
（ＣＰ）17-00325-01：寂水：東国人＋船乗り＋理力使い＋海賊＋吏族：器用+5
（ＣＰ）17-00327-01：支倉玲：東国人＋船乗り＋機関士＋海賊＋ＨＱ知識＋吏族：外見+2*敏捷+1*知識+4*幸運+1
－恩寵の時計：個人所有：外見＋２（着用型／首に着用するもの）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00327-01%A1%A7%BB%D9%C1%D2%CE%E8
（ＣＰ）17-00334-01：アキラ・フィーリ・シグレ艦氏族：東国人＋船乗り＋理力使い＋海賊＋参謀：外見+2*敏捷+1*器用+5*幸運+1*耐久力+1
－恩寵の時計：個人所有：外見＋２（着用型／首に着用するもの）
－蛇の指輪２：個人所有：耐久力＋１（着用型／手先に着用するもの）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00334-01%A1 %A7%A5%A2%A5%AD%A5%E9%A1%A6%A5%D5%A5%A3%A1%BC%A5%EA%A1%A6%A5%B7%A5%B0%A5%EC%B4%CF%BB%E1%C2%B2
（ＣＰ）17-xx001-xx：犬士：東国人＋船乗り＋理力使い＋海賊：
（ＣＰ）17-xx002-xx：犬士：東国人＋船乗り＋理力使い＋海賊：
（ＣＰ）17-xx003-xx：犬士：東国人＋船乗り＋理力使い＋海賊：
（ＣＰ）17-xx004-xx：犬士：東国人＋船乗り＋理力使い＋海賊：
（ＣＰ）17-xx005-xx：犬士：東国人＋船乗り＋理力使い＋海賊：
（ＣＰ）17-xx006-xx：犬士：東国人＋船乗り＋理力使い＋海賊：
（ＣＰ）17-xx007-xx：犬士：東国人＋船乗り＋理力使い＋海賊：
（ＣＰ）17-xx008-xx：犬士：東国人＋船乗り＋理力使い＋海賊：
（ＣＰ）17-xx009-xx：犬士：東国人＋船乗り＋理力使い＋海賊：
};
EOT
    troop = parse_troop(source)
    puts troop.evals_str

  end

  it 'T15公用輸送騎士団' do
    source = <<EOT
＃ ！有効条件：｛艦船，宇宙艦船，航空機，Ｉ＝Ｄ｝にコパイロットとして搭乗している場合での
_define_vessel:{
１番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）42-00438-02_深織志岐：北国人＋テストパイロット＋帝國軍歩兵＋整備士２：知識+1*幸運+1 ；
（ＣＰ）18-xx026-xx_リトルリトルボイコット：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）36-xx011-xx_戸隠　一葉(とがくし　かずは)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};
_define_vessel:{
２番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）18-xx013-xx_うーた：西国人＋整備士２＋整備士２＋テストパイロット；
（ＣＰ）36-xx019-xx_郁宮(いく)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）36-xx020-xx_羽月(はつき)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};

_define_vessel:{
３番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）18-xx014-xx_リン：西国人＋整備士２＋整備士２＋テストパイロット；
（ＣＰ）36-xx027-xx_双樹　七都(ふたき　なつ)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）36-xx028-xx_仁村　八雲(にむら　やくも)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};
_define_vessel:{
４番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）18-xx015-xx_虎徹：西国人＋整備士２＋整備士２＋テストパイロット；
（ＣＰ）36-xx043-xx_詠水(よみ)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）36-xx044-xx_詩誉(しよ)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};
_define_vessel:{
５番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）18-xx017-xx_パン：西国人＋整備士２＋整備士２＋テストパイロット；
（ＣＰ）36-xx046-xx_士狼(しろう)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）36-xx047-xx_夜綱(よつな)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};

_define_vessel:{
６番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）18-xx018-xx_コフレ：西国人＋整備士２＋整備士２＋テストパイロット；
（ＣＰ）36-xx048-xx_詩荻　夜月(しおぎ　よるつき)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）42-xx001-xx_ロート：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};

_define_vessel:{
７番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）18-xx019-xx_ロミィ：西国人＋整備士２＋整備士２＋テストパイロット；
（ＣＰ）42-xx002-xx_グラウ：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）42-xx007-xx_オーランジェ：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};

_define_vessel:{
８番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）18-xx020-xx_ツクシ：西国人＋整備士２＋整備士２＋テストパイロット；
（ＣＰ）42-xx008-xx_ブラオン：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）42-xx009-xx_ヴァイス：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};

_define_vessel:{
９番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）18-xx022-xx_ぎんなん：西国人＋整備士２＋整備士２＋テストパイロット；
（ＣＰ）42-xx010-xx_プルプァ：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）42-xx015-xx_アカネ＝ワーレンティア：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};

_define_vessel:{
１０番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）18-xx025-xx_ケト：西国人＋整備士２＋整備士２＋テストパイロット；
（ＣＰ）42-xx016-xx_アカリ＝フラァヴス：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）42-xx017-xx_ハガネ＝アダマス：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};
EOT
    troop = parse_troop(source)
    puts "T15公用輸送騎士団"
    puts result = troop.evals_str
    result.should == "体格：筋力：耐久力：外見：敏捷：器用：感覚：知識：幸運\n28：25：28：26：27：34：32：35：28"

    armor = Evaluator['装甲（通常）']
    armor.evaluate(troop).should == 20

    first_ship = troop.children[0]
    synchro = Evaluator['同調']
#    members , value = first_ship.primary_synchronizer(synchro).to_a
    scalc = first_ship.primary_synchronizer(synchro)
    scalc.children.size.should == 1
    scalc.operands.size.should == 0
    scalc.total.should == 8
    
    scalc = synchro.details(troop)
    leaders = scalc.all_leaves
    leaders.size.should == 1
    leaders[0].unit.name.should == '深織志岐'
    leaders[0].base_value.should == 8
    parent_bonuses = scalc.parent_bonuses
#    operands = scalc.all_operands(leaders[0])
#    operands.size.should == 2
#    operands.collect{|o|o.bonus}.sum.should == 6
#    scalc.base_value.should == 8
    scalc.total.should == 14
#    scalc.operands.size.should == 2
#    scalc.operands.collect{|b|b.bonus}.sum.should == 6
#    scalc.total == 14
#    members , value ,bonuses = synchro.details(troop).to_a
#    members.size.should == 1
#    members[0].name.should == '深織志岐'
#    bonuses.should_not be_nil
#    bonuses.size.should == 2
#    bonuses.collect{|b|b.bonus}.sum.should == 6
#    value.should == 14

    second_ship = troop.children[1]
#    members , value  ,bonuses = synchro.details(second_ship).to_a
#    value.should == 9
#    bonuses.should == []
    scalc = synchro.details(second_ship)
    scalc.total.should == 9
    scalc.operands.size == 0
  end

  it 'T15ＦＶＢ編成' do
    source = <<EOT
＃！特殊無効：海賊の搭乗補正
＃！特殊無効：海賊の艦上補正
＃！特殊無効：船乗りの艦上補正

_define_vessel:{
フリゲート１番艦「太郎月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00328-01_夜狗　樹：東国人+船乗り+理力使い+海賊+ＨＱ知識：敏捷+1*幸運+1；
-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00328-01%A1%A7%CC%EB%B6%E9%A1%A1%BC%F9
};
_define_vessel:{
フリゲート２番艦「麗月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00339-01_きみこ：東国人+船乗り+理力使い+海賊+ＨＱ知識+吏族：敏捷+1*知識+1*幸運+1；
-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00339-01%A1%A7%A4%AD%A4%DF%A4%B3
};

_define_vessel:{
フリゲート３番艦「祭月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00784-01_光儀：東国人+船乗り+理力使い+海賊+ＨＱ知識；
-卒業記念記章：個人所有：なし（着用型／その他）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00784-01%A1%A7%B8%F7%B5%B7
};

_define_vessel:{
フリゲート４番艦「卯月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx001-xx_にら：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート５番艦「皐月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx002-xx_あーちゃん：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート６番艦「松風月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx003-xx_もふ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート７番艦「蘭月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx004-xx_ごん太：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート８番艦「葉月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx005-xx_ｌａｉｎ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート９番艦「紅葉月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx006-xx_ムニ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート１０番艦「陽月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx007-xx_ぬいさん：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート１１番艦「神楽月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00326-01_曲直瀬りま：東国人+摂政+海賊+船乗り：敏捷+1*知識+1*幸運+1*外見+2；
＃-恩寵の時計：個人所有：外見、評価＋2（着用型／首に着用するもの）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00326-01%A1%A7%B6%CA%C4%BE%C0%A5%A4%EA%A4%DE
};

_define_vessel:{
フリゲート１２番艦「暮古月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00331-01_栗田雷一：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識+吏族：敏捷+1*知識+1*幸運+1；
};

_define_vessel:{
フリゲート１３番艦「青月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00327-01_支倉玲：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識+吏族：敏捷+1*知識+4*幸運+1*外見+2；
＃-恩寵の時計：個人所有：外見、評価＋2（着用型／首に着用するもの）
-高機能ハンドヘルド：個人所有：情報戦、評価＋3（着用型／片手持ち武器）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00327-01%A1%A7%BB%D9%C1%D2%CE%E8
};

_define_vessel:{
フリゲート１４番艦「弦月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00334-01_アキラ・フィーリ・シグレ艦氏族：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識+参謀：体格+3*筋力+3*耐久力+3*外見+5*敏捷+4*器用+8*感覚+3*知識+3*幸運+4；
＃-恩寵の時計：個人所有：外見、評価＋2（着用型／首に着用するもの）
＃-エステル・フィーリ・シグレ艦氏族２の加護：個人ＡＣＥ：＜時雨＞の全能力、評価＋３
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00334-01%A1%A7%A5%A2%A5%AD%A5%E9%A1%A6%A5%D5%A5%A3%A1%BC%A5%EA%A1%A6%A5%B7%A5%B0%A5%EC%B4%CF%BB%E1%C2%B2
};

_define_vessel:{
フリゲート１５番艦「眉月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00781-01_菩鋳螺：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識；
-卒業記念記章：個人所有：なし（着用型／その他）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00781-01%A1%A7%CA%EE%C3%F2%CD%E6
};

_define_vessel:{
フリゲート１６番艦「新月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx008-xx_すたすきー：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート１７番艦「望月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx009-xx_はっち：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート１８番艦「立待月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx010-xx_らっしー：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート１９番艦「居待月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx011-xx_ぱとらっしゅ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート２０番艦「寝待月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx012-xx_ねろ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート２１番艦「更待月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx013-xx_きゃぐにー：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート２２番艦「残月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx014-xx_れいしー：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};
EOT
    troop = parse_troop(source)

    aa = Evaluator['対空戦闘']
    keys = troop.evals.keys
    puts keys
    keys.include?('対空戦闘')
    aa.evaluate(troop).should == 41

    spell = Evaluator['詠唱']
    spell.evaluate(troop).should == 26

    source = <<EOT
＃ ！有効条件：位置づけ（艦船系）の乗り物の艦上で活動する場合での
＃ ！有効条件：位置づけ（艦船系）の乗り物に搭乗している場合での
_define_vessel:{
フリゲート１番艦「太郎月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00328-01_夜狗　樹：東国人+船乗り+理力使い+海賊+ＨＱ知識：敏捷+1*幸運+1；
-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00328-01%A1%A7%CC%EB%B6%E9%A1%A1%BC%F9
};
_define_vessel:{
フリゲート２番艦「麗月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00339-01_きみこ：東国人+船乗り+理力使い+海賊+ＨＱ知識+吏族：敏捷+1*知識+1*幸運+1；
-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00339-01%A1%A7%A4%AD%A4%DF%A4%B3
};

_define_vessel:{
フリゲート３番艦「祭月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00784-01_光儀：東国人+船乗り+理力使い+海賊+ＨＱ知識；
-卒業記念記章：個人所有：なし（着用型／その他）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00784-01%A1%A7%B8%F7%B5%B7
};

_define_vessel:{
フリゲート４番艦「卯月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx001-xx_にら：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート５番艦「皐月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx002-xx_あーちゃん：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート６番艦「松風月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx003-xx_もふ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート７番艦「蘭月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx004-xx_ごん太：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート８番艦「葉月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx005-xx_ｌａｉｎ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート９番艦「紅葉月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx006-xx_ムニ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート１０番艦「陽月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx007-xx_ぬいさん：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート１１番艦「神楽月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00326-01_曲直瀬りま：東国人+摂政+海賊+船乗り：敏捷+1*知識+1*幸運+1*外見+2；
＃-恩寵の時計：個人所有：外見、評価＋2（着用型／首に着用するもの）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00326-01%A1%A7%B6%CA%C4%BE%C0%A5%A4%EA%A4%DE
};

_define_vessel:{
フリゲート１２番艦「暮古月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00331-01_栗田雷一：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識+吏族：敏捷+1*知識+1*幸運+1；
};

_define_vessel:{
フリゲート１３番艦「青月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00327-01_支倉玲：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識+吏族：敏捷+1*知識+4*幸運+1*外見+2；
＃-恩寵の時計：個人所有：外見、評価＋2（着用型／首に着用するもの）
-高機能ハンドヘルド：個人所有：情報戦、評価＋3（着用型／片手持ち武器）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00327-01%A1%A7%BB%D9%C1%D2%CE%E8
};

_define_vessel:{
フリゲート１４番艦「弦月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00334-01_アキラ・フィーリ・シグレ艦氏族：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識+参謀：体格+3*筋力+3*耐久力+3*外見+5*敏捷+4*器用+8*感覚+3*知識+3*幸運+4；
＃-恩寵の時計：個人所有：外見、評価＋2（着用型／首に着用するもの）
＃-エステル・フィーリ・シグレ艦氏族２の加護：個人ＡＣＥ：＜時雨＞の全能力、評価＋３
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00334-01%A1%A7%A5%A2%A5%AD%A5%E9%A1%A6%A5%D5%A5%A3%A1%BC%A5%EA%A1%A6%A5%B7%A5%B0%A5%EC%B4%CF%BB%E1%C2%B2
};

_define_vessel:{
フリゲート１５番艦「眉月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00781-01_菩鋳螺：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識；
-卒業記念記章：個人所有：なし（着用型／その他）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00781-01%A1%A7%CA%EE%C3%F2%CD%E6
};

_define_vessel:{
フリゲート１６番艦「新月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx008-xx_すたすきー：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート１７番艦「望月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx009-xx_はっち：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート１８番艦「立待月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx010-xx_らっしー：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート１９番艦「居待月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx011-xx_ぱとらっしゅ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート２０番艦「寝待月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx012-xx_ねろ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート２１番艦「更待月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx013-xx_きゃぐにー：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート２２番艦「残月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx014-xx_れいしー：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};
EOT
    troop = parse_troop(source)

    original = @context.units['夜狗　樹']
    source = troop.crew_division.children[0]
    copy = troop.crew_division_getted_off.children[0]
    source.object_id.should_not == copy.object_id
    copy.unit_type.include?('搭乗').should_not == true
    original.is_troop_member?.should_not == true
#    source.is_troop_member?.should == true
#    copy.is_troop_member?.should == true
    spell.evaluate(troop).should == 29
  end

  it '艦船とそれ以外の混在' do
    source = <<EOT
＃ ！有効条件：｛艦船，宇宙艦船，航空機，Ｉ＝Ｄ｝にコパイロットとして搭乗している場合での

soldier：歩兵；

_define_vessel:{
１番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）42-00438-02_深織志岐：北国人＋テストパイロット＋帝國軍歩兵＋整備士２：知識+1*幸運+1 ；
（ＣＰ）18-xx026-xx_リトルリトルボイコット：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）36-xx011-xx_戸隠　一葉(とがくし　かずは)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};
EOT

    proc{troop = parse_troop(source)}.should raise_error(RuntimeError , /艦船編成に艦船以外のユニットは混在できません。/)

    source = <<EOT
＃ ！有効条件：｛艦船，宇宙艦船，航空機，Ｉ＝Ｄ｝にコパイロットとして搭乗している場合での

_define_vessel:{
１番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）42-00438-02_深織志岐：北国人＋テストパイロット＋帝國軍歩兵＋整備士２：知識+1*幸運+1 ；
（ＣＰ）18-xx026-xx_リトルリトルボイコット：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）36-xx011-xx_戸隠　一葉(とがくし　かずは)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};

soldier：歩兵；
EOT

    proc{troop = parse_troop(source)}.should raise_error(RuntimeError , /艦船編成に艦船以外のユニットは混在できません。/)

  end

  
  it '艦隊での同調' do
    source = <<EOT
＃ ！有効条件：位置づけ（艦船系）の乗り物の艦上で活動する場合での
＃ ！有効条件：位置づけ（艦船系）の乗り物に搭乗している場合での
_define_vessel:{
フリゲート：ムーン級パトロールフリゲート,
（Ｐ）17-00339-01_きみこ：東国人+船乗り+理力使い+海賊+ＨＱ知識
-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
};
EOT
    troop = parse_troop(source)
    synchro = Evaluator['同調']

    vessel = troop.children[0]
    crew = vessel.children[0]
    sp = vessel.find_spec('海賊の艦上補正')
    sp.should_not be_nil

    crew.evaluate('体格').should == 6
    vessel.evaluate('体格').should == 6
    troop.evaluate('体格').should == 6


    sp = vessel.ability_bonuses.find{|sp|sp.name == '海賊の艦上補正'}
    sp.should_not be_nil
    sp.is_target?(vessel,'装甲').should be_truthy
    sp.is_applicable?(vessel,'装甲').should be_truthy
    sp.is_target?(vessel,'体格').should be_falsey
    sp.is_target?(crew,'体格').should be_truthy

    sp.is_unit_type_match?.should be_truthy

    sp.is_available?.should be_truthy
    sp.is_ability_match?('体格').should be_truthy
    sp.target('装甲').object_id.should == vessel.object_id
    sp.is_ability_match?('装甲').should be_truthy

    str = troop.evals_str
    str.should_not be_nil

    e_items = vessel.evaluate_items()
    sp = e_items.find{|sp|sp.name == '海賊の艦上補正'}
    sp.should_not be_nil
    vessel.is_evaluate_item?(sp).should be_truthy
    crew.is_evaluate_item?(sp).should be_truthy

    crew_e_items = crew.evaluate_items
    crew_e_items.find{|sp|sp.name == '海賊の艦上補正'}.should be_truthy
    crew_e_items.find{|sp|sp.name == '海賊の搭乗補正'}.should be_nil
    crew_e_items.find{|sp|sp.name == '船乗りの艦上補正'}.should be_truthy

    sp_toujou = vessel.judge_bonuses.find{|sp|sp.name == '海賊の搭乗補正'}
    sp_toujou.should_not be_nil

    sp_toujou.is_unit_type_match?.should be_truthy
    sp_toujou.is_target?(vessel).should be_truthy
    sp_toujou.is_available?.should be_truthy
    sp_toujou.is_ability_match?('体格').should be_truthy
    sp_toujou.target('装甲').object_id.should == vessel.object_id
    sp_toujou.is_ability_match?('装甲').should be_truthy

    vessel.is_evaluate_item?(sp_toujou).should be_truthy

#    scalc1 = synchro.details(crew)
#    scalc1.total.should == 8

    scalc2 = synchro.details(vessel)
    scalc2.total.should == 8

    scalc = synchro.details(troop)
    scalc.total.should == 10

    armor = Evaluator['装甲（通常）']
    armor.evaluate(troop).should == 20

  end

  it '艦隊編成の同調２' do
    source = <<EOT
＃ ！有効条件：位置づけ（艦船系）の乗り物の艦上で活動する場合での
＃ ！有効条件：位置づけ（艦船系）の乗り物に搭乗している場合での
_define_vessel:{
フリゲート１番艦「太郎月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00328-01_夜狗　樹：東国人+船乗り+理力使い+海賊+ＨＱ知識：敏捷+1*幸運+1；
-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00328-01%A1%A7%CC%EB%B6%E9%A1%A1%BC%F9
};
_define_vessel:{
フリゲート２番艦「麗月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00339-01_きみこ：東国人+船乗り+理力使い+海賊+ＨＱ知識+吏族：敏捷+1*知識+1*幸運+1；
-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00339-01%A1%A7%A4%AD%A4%DF%A4%B3
};

_define_vessel:{
フリゲート３番艦「祭月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00784-01_光儀：東国人+船乗り+理力使い+海賊+ＨＱ知識；
-卒業記念記章：個人所有：なし（着用型／その他）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00784-01%A1%A7%B8%F7%B5%B7
};

_define_vessel:{
フリゲート４番艦「卯月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx001-xx_にら：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート５番艦「皐月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx002-xx_あーちゃん：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};
_define_division:{フリゲート第一分隊,
フリゲート２番艦「麗月」_組織所有
フリゲート４番艦「卯月」_組織所有
フリゲート５番艦「皐月」_組織所有
};

EOT

    troop = parse_troop(source)
    div1 = @context.units['フリゲート第一分隊']
    div1.all_conditions.size.should == troop.all_conditions.size
    synchro = Evaluator['同調']

    scalc = synchro.details(troop)
    scalc.total.should == 10

    scalc1 = synchro.details(div1)
    scalc1.total.should == 10
  end

  it '艦隊での同調３' do
    source = <<EOT
＃！有効条件：位置づけ（艦船系）の乗り物の艦上で活動する場合での
＃！有効条件：位置づけ（艦船系）の乗り物に搭乗している場合での

＃！個別付与：猫と犬の前足が重なった腕輪，本隊

_define_vessel:{
フリゲート：ムーン級パトロールフリゲート,
（Ｐ）17-00339-01_きみこ：東国人+船乗り+理力使い+海賊+ＨＱ知識
-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
};
EOT
    troop = parse_troop(source)
    puts troop.evals_str

    result = troop.action_evals_str
    puts result
    puts troop.leaders_str
    synchro = @context.evaluators['同調']
    calc = synchro.details(troop)
    calc.total.should == 10
  end

  it '艦船の操縦評価' do
    source = <<EOT

＃ ！有効条件：｛艦船，宇宙艦船，航空機，Ｉ＝Ｄ｝にコパイロットとして搭乗している場合での
_define_vessel:{
１番艦_公用輸送騎士団所有：巨鐵級＋ＨＱ輸送量＋ＨＱ輸送量＋ＨＱ輸送量,
（Ｐ）42-00438-02_深織志岐：北国人＋テストパイロット＋帝國軍歩兵＋整備士２：知識+1*幸運+1 ；
（ＣＰ）18-xx026-xx_リトルリトルボイコット：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
（ＣＰ）36-xx011-xx_戸隠　一葉(とがくし　かずは)：西国人＋整備士２＋整備士２＋バックシートの女房２＋ＳＨＱ知識；
};
EOT
    troop = parse_troop(source)
    vessel = troop.children[0]
    calc = Evaluator['操縦（個別）'].calculate(vessel)
    calc.class.should == TeamCalculation
#    Evaluator['操縦'].evaluate(vessel).should == 23
    calc.total.should == 23
  end

  it '艦船編成の搭乗者による可能行為評価' do
    source =<<EOT
＃ ！有効条件：位置づけ（艦船系）の乗り物の艦上で活動する場合での
＃ ！有効条件：位置づけ（艦船系）の乗り物に搭乗している場合での
_define_vessel:{
フリゲート：ムーン級パトロールフリゲート,
（Ｐ）17-00339-01_きみこ：東国人+船乗り+理力使い+海賊+ＨＱ知識
-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
};
EOT
    troop = parse_troop(source)
    str = troop.evals_str
    puts str
    vessel = troop.children[0]
    crew = vessel.children[0]
    spec = crew.find_spec('海賊の搭乗補正')

    crew.is_evaluate_item?(spec).should be_falsey
    spec.is_target?(crew,'体格').should == false
    spec.is_applicable?(crew,'体格').should be_falsey
##    spec.is_target?(crew).should be_falsey
#    spec.is_applicable?(crew,'詠唱').should be_falsey

    get_off_div = vessel.crew_division_getted_off
    crew_getted_off = get_off_div.children[0]

    spell_spec = crew_getted_off.find_spec('理力使いの詠唱戦闘補正')
    spell_spec.is_target?(crew_getted_off).should be_truthy

    Evaluator['詠唱'].evaluate(troop).should == 11

#    control = Evaluator['操縦']
    control = Evaluator['操縦（個別）']


    parent = vessel.crew_division.parent
#    control.match?(spec).should be_truthy
    vessel.crew_division.parent = nil

    spec.is_target?(crew).should be_truthy
    spec.owner.unit_type.should == Set.new(['','歩兵','搭乗'])
    control.match?(spec).should be_truthy
    vessel.crew_division.parent = parent
    control.evaluate(vessel).should == 13
  end

    it '別状況化での増槽' do
    source = <<EOT
_define_I=D:{
Ｕ４_国有：多国籍要撃機”ユーフォー”，
－多国籍要撃機”ユーフォー”の増槽
（Ｐ）06-00147-01_霰矢蝶子：愛の民＋整備士２＋魔術的舞踏子＋やわらか舞踏子＋ＳＨＱ知識＋ＨＱ感覚＋霰矢惣一郎の恋人：敏捷+1*器用+1*知識+1*幸運+2*外見+2*幸運+1;
　－恩寵の時計：個人所有：外見＋２（着用型／首）
　－クローバーのしおり：個人所有：幸運＋１
　－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／手首）
　－アンデットバスター：個人所有：対アンデット、攻撃修正＋５（着用型／その他）
　－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00147-01%A1%A7%F0%C7%CC%F0%C4%B3%BB%D2
（ＣＰ）06-00161-01_矢神サク：愛の民＋整備士２＋魔術的舞踏子＋やわらか舞踏子＋ＳＨＱ知識＋ＨＱ感覚＋ヤガミの恋人＋ＨＱ幸運＋ＨＱ幸運＋ＨＱ外見：敏捷＋1*知識＋1*幸運＋2;
};

＃！状況：増槽装備時
＃！有効条件：追加された５ＡＲを使い切るまで
EOT
    troop = parse_troop(source)
    ufo = troop.children[0]

    str = troop.evals_str
    @context.change_situation_to('増槽装備時')
    tank = ufo.find_spec('多国籍要撃機”ユーフォー”の増槽の全能力補正')
    tank.is_available?.should be_truthy
    ufo.is_evaluate_item?(tank).should be_truthy
    e_items =
    puts troop.evals_str
    puts @context.situation_names
  end

  it '艦船編成で歩兵限定補正が使える件' do
    source = <<EOT
_define_vessel:{
コランダム級経済的輸送艦４_国有（初恋運輸）:コランダム級経済的輸送艦＋輸送量15万t，
＃（コランダム級経済的輸送艦ＨＱ＋アイドレス工場ＨＱ継承第二世代＋西国人＋猫妖精＋歩兵ＨＱ継承第三世代＝累積：輸送量45万t）
（P)25-00857-01_琴月雷那：商業の民＋パイロット＋整備士２＋名パイロット；
－海図：個人所有：これを持つ者が乗る船は航路が＋１される。この効果は同様のアイテムによって増えることはない。
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00857-01%A1%A7%B6%D7%B7%EE%CD%EB%C6%E1
（P)25-xx015-xx：資源猫士１５：商業の民＋パイロット＋整備士２＋名パイロット；
（P)25-xx016-xx：資源猫士１６：商業の民＋パイロット＋整備士２＋名パイロット；
};
EOT

    troop = parse_troop(source)
    corandam = troop.children[0]
    pilot = corandam.children[0]
#    pilot.is_troop_member?.should be_falsey
    spec = pilot.find_spec('商業の民の外見・幸運補正')
    spec.is_unit_type_match?('外見').should be_truthy

    pilot_dup = troop.crew_division.children[0]
    spec_dup = pilot_dup.find_spec('商業の民の外見・幸運補正')

    pilot_dup.should_not == pilot
#    pilot_dup.is_troop_member?.should be_truthy
    tm = pilot_dup.troop_member
    spec_dup.is_unit_type_match?('外見').should be_truthy
    pilot_dup.is_evaluate_item?(spec_dup).should be_truthy
    pilot_dup.evaluate('外見').should == 9
    troop.crew_division.evaluate('外見').should == 15

    corandam.crew_division.evaluate('外見').should == 15

  end
  
  it '本隊に自動成功補正が付いても大丈夫' do
    source = <<EOT
Ｌ：第２バッジシステムのバックアップによる付与　＝　｛
 ｔ：名称　＝　第２バッジシステムのバックアップによる付与（定義）
　ｔ：特殊　＝　｛
　　＊第２バッジシステムのバックアップによる付与の隠蔽看破補正　＝　，条件発動，（隠蔽を看破する場合の）全判定、自動成功。その情報を各国へと送ることができる。＃芥辺境空港航空基地の隠蔽看破補正に準じる
　｝
｝


＃！個別付与：第２バッジシステムのバックアップによる付与，本隊

/*　本隊に関する記述　*/

_define_vessel:{
テンダーヘッド_リワマヒ国有：きゃりっじＡＷＡＣＳ＋ＨＱ装甲，
（Ｐ）06-00838-01_七周シナモン：西国人+パイロット+整備士２+名パイロット；
（Ｐ）25-00857-01_琴月雷那：商業の民＋パイロット＋整備士２＋名パイロット＋整備士官；
（ＣＰ）34-00430-01_薊：高位南国人＋猫妖精２＋ドラッガー＋補給士官＋HQ知識＋HQ知識：敏捷+1*知識+1；
};
EOT

    troop = parse_troop(source)
    reveal = Evaluator['隠蔽看破']
    troop.evaluators.collect{|ev|ev.name}.should include('隠蔽看破')
    reveal.evaluate(troop).should == '自動成功'

    source = <<EOT
Ｌ：第２バッジシステムのバックアップによる付与　＝　｛
 ｔ：名称　＝　第２バッジシステムのバックアップによる付与（定義）
　ｔ：特殊　＝　｛
　　＊第２バッジシステムのバックアップによる付与の隠蔽看破補正　＝　，条件発動，（隠蔽を看破する場合の）全判定、自動成功。その情報を各国へと送ることができる。＃芥辺境空港航空基地の隠蔽看破補正に準じる
　｝
｝


＃！個別付与：第２バッジシステムのバックアップによる付与，テンダーヘッド_リワマヒ国有

/*　本隊に関する記述　*/

_define_vessel:{
テンダーヘッド_リワマヒ国有：きゃりっじＡＷＡＣＳ＋ＨＱ装甲，
（Ｐ）06-00838-01_七周シナモン：西国人+パイロット+整備士２+名パイロット；
（Ｐ）25-00857-01_琴月雷那：商業の民＋パイロット＋整備士２＋名パイロット＋整備士官；
（ＣＰ）34-00430-01_薊：高位南国人＋猫妖精２＋ドラッガー＋補給士官＋HQ知識＋HQ知識：敏捷+1*知識+1；
};
EOT

    troop = parse_troop(source)
    awacs = troop.children[0]
    awacs.all_idresses.should include('第２バッジシステムのバックアップによる付与')
    awacs.tqrs.should include('隠蔽看破補正'.to_sym)
    troop.tqrs.should include('隠蔽看破補正'.to_sym)
    troop.evaluators.collect{|ev|ev.name}.should include('隠蔽看破')
    reveal.evaluate(awacs).should == '自動成功'
  end

  it 'T16 FVT' do
    source = <<EOT
＃！個別付与：猫と犬の前足が重なった腕輪，【本隊】

_define_vessel:{
こんごう級宇宙巡洋艦_組織所有：こんごう級宇宙巡洋艦＋ＨＱ艦隊戦闘,
（Ｐ）17-00339-01_きみこ：東国人+船乗り+海賊+スペーススターシップオフィサー+吏族：敏捷+1*知識+1*幸運+1；
＃-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
＃太元では同能力を重複加算してしまうため、コメントアウトで無効化

-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00339-01%A1%A7%A4%AD%A4%DF%A4%B3
（Ｐ）17-xx001-xx_にら：東国人+船乗り+理力使い+海賊+ＨＱ知識；
（Ｐ）17-xx002-xx_あーちゃん：東国人+船乗り+理力使い+海賊+ＨＱ知識；
（Ｐ）17-xx003-xx_もふ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
（Ｐ）17-xx004-xx_ごん太：東国人+船乗り+理力使い+海賊+ＨＱ知識；
（ＣＰ）17-00132-01_不変空沙子：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識+ヤガミの恋人+ＨＱ外見+ＨＱ幸運+ＨＱ幸運：敏捷+1*知識+1*幸運+1；
（ＣＰ）17-00323-01_オカミチ：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識+護民官：敏捷+1*幸運+1；
-恩寵の時計：個人所有：外見、評価＋2（着用型／首に着用するもの）
-高機能ハンドヘルド：個人所有：情報戦、評価＋3（着用型／片手持ち武器）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00323-01%A1%A7%A5%AA%A5%AB%A5%DF%A5%C1
（ＣＰ）17-00326-01_曲直瀬りま：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識：敏捷+1*知識+1*幸運+1；
-恩寵の時計：個人所有：外見、評価＋2（着用型／首に着用するもの）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00326-01%A1%A7%B6%CA%C4%BE%C0%A5%A4%EA%A4%DE
（ＣＰ）17-00327-01_支倉玲：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識+吏族：敏捷+1*知識+4*幸運+1；
-恩寵の時計：個人所有：外見、評価＋2（着用型／首に着用するもの）
-高機能ハンドヘルド：個人所有：情報戦、評価＋3（着用型／片手持ち武器）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00327-01%A1%A7%BB%D9%C1%D2%CE%E8
（ＣＰ）17-00328-01_夜狗　樹：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識+宇宙艦長+ＨＱ感覚：敏捷+1*幸運+1；
＃-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
＃太元では同能力を重複加算してしまうため、コメントアウトで無効化

-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00328-01%A1%A7%CC%EB%B6%E9%A1%A1%BC%F9
（ＣＰ）17-00781-01_菩鋳螺：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識；
-卒業記念記章：個人所有：なし（着用型／その他）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00781-01%A1%A7%CA%EE%C3%F2%CD%E6
（ＣＰ）17-00784-01_光儀：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識+星見司２；
-卒業記念記章：個人所有：なし（着用型／その他）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00784-01%A1%A7%B8%F7%B5%B7
（ＣＰ）17-xx005-xx_ｌａｉｎ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
（ＣＰ）17-xx006-xx_ムニ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
（ＣＰ）17-xx007-xx_ぬいさん：東国人+船乗り+理力使い+海賊+ＨＱ知識；
（ＣＰ）17-xx008-xx_すたすきー：東国人+船乗り+理力使い+海賊+ＨＱ知識；
（ＣＰ）17-xx009-xx_はっち：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};
EOT
    troop = parse_troop(source)
    result = troop.action_evals_str
    result.should_not be_nil
  end
  
  it '操縦評価で割烹着（器用＋１）あり' do
    source = <<EOT
_define_vessel:{
ＦＥＧボウルドカーン_国有：ＦＥＧボウルドカーン＋ＳＨＱ輸送量＋ＳＨＱ輸送量,
（Ｐ）03-00875-01_Ｃａｓｓａｄｙ：西国人＋パイロット＋整備士２＋名整備士＋ＨＱ知識;
（Ｐ）03-xx010-xx_大吉：西国人＋パイロット＋整備士２＋名整備士＋ＨＱ知識;
（Ｐ）03-xx011-xx_メイ：西国人＋パイロット＋整備士２＋名整備士＋ＨＱ知識;
（ＣＰ）03-00069-01_小鳥遊敦：高位西国人+ＷＳＯ+航海士+名パイロット+吏族：外見+5*敏捷+1*器用+1*知識+1;
－割烹着：個人所有：歩兵，条件発動，（調理での）全判定、評価＋１。（弁当が和風になる）。 , 歩兵，，外見、評価＋１。母性を感じる笑顔のため魅力的に感じる。 , 歩兵，，器用、評価＋１。袖を押さえるため器用に振舞える。重ね着。（着用型／重ね着）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00069-01%A1%A7%BE%AE%C4%BB%CD%B7%C6%D8
（ＣＰ）03-xx012-xx_真夜：西国人+猫妖精２+パイロット+名パイロット;
（ＣＰ）03-xx013-xx_リー：西国人+猫妖精２+パイロット+名パイロット;
};
EOT
    troop = parse_troop(source)

    ctrl = Evaluator['操縦（艦隊）']
    calc = ctrl.calculate(troop)
    calc.total.should == 18
  end

  it '艦船と搭乗補正と一般評価' do
    source = <<EOT
_define_vessel:{
フリゲート１４番艦「弦月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00334-01_アキラ・フィーリ・シグレ艦氏族：東国人+船乗り+機関士+海賊+ＨＱ知識+ＨＱ知識+参謀：体格+3*筋力+3*耐久力+3*外見+5*敏捷+4*器用+8*感覚+3*知識+3*幸運+4；
＃-恩寵の時計：個人所有：外見、評価＋2（着用型／首に着用するもの）
＃-エステル・フィーリ・シグレ艦氏族２の加護：個人ＡＣＥ：＜時雨＞の全能力、評価＋３
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00334-01%A1%A7%A5%A2%A5%AD%A5%E9%A1%A6%A5%D5%A5%A3%A1%BC%A5%EA%A1%A6%A5%B7%A5%B0%A5%EC%B4%CF%BB%E1%C2%B2
};

_define_vessel:{
フリゲート２０番艦「寝待月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx012-xx_ねろ：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート２１番艦「更待月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx013-xx_きゃぐにー：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};

_define_vessel:{
フリゲート２２番艦「残月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-xx014-xx_れいしー：東国人+船乗り+理力使い+海賊+ＨＱ知識；
};
EOT

    troop = parse_troop(source)
    cha = Evaluator['魅力']
    calc = cha.calculate(troop)
    calc.total.should == 16

  end
  
  it '艦船編成での一般行為禁則' do
    #質疑 http://cwtg.jp/qabbs/bbs2.cgi?action=article&id=10320 の回答待ち。
    source = <<EOT
＃ ！有効条件：位置づけ（艦船系）の乗り物に搭乗している場合での
＃ ！有効条件：位置づけ（艦船系）の乗り物の艦上で活動する場合での
_define_vessel:{vessel：ムーン級パトロールフリゲート,
salor1：船乗り；
salor2：船乗り；
};
EOT
    troop = parse_troop(source)

    actions = troop.action_names
    actions.should_not include('侵入行為')
    actions.should_not include('隠蔽行為')
    actions.should_not include('陣地構築行為')
    actions.should include('歌唱行為')

    result = troop.action_evals_str
    result.should_not match(/侵入/)
    result.should_not match(/陣地構築/)
    result.should_not match(/隠蔽/)
    result.should match(/歌唱/)
  end
  
  it 'vesselでのその他特殊のコスト' do
    source = <<EOT
＃！有効条件：宰相府にある機体に搭乗している場合での
ｌ：＊ドッグバッグ（通常型）のイベント時燃料消費　＝　（削除）
ｌ：＊ドッグバッグ（通常型）の特殊　＝　，条件発動，燃料－２万ｔ。１航路につつき－１万ｔとする。燃料の代わりにマイル５を使ってもよい。


_define_vessel:{
Ｄ１_国有：ドッグバッグ（通常型）,
（Ｐ）44-00220-01_奥羽りんく：西国人＋猫の決戦存在＋スペーススターシップオフィサー＋宰相の娘＋秘書官：外見+2*敏捷+1*器用+1*知識+1*幸運+1；
＃－恩寵の時計：個人所有：外見＋２（着用型／首）
－猫と犬の前足が重なった腕輪：個人所有：同調＋３（着用型／腕）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00220-01%A1%A7%B1%FC%B1%A9%A4%EA%A4%F3%A4%AF
（ＣＰ）00-xx054-xx_猫士：西国人＋スターファイター＋スターライナー＋スターリフター＋ＳＨＱ感覚；
};
EOT
    troop = parse_troop(source)
    d1 = troop.children[0]
    sp_ary = []
    troop.action_evals_str
    troop.total_cost{|unit,sp|
      sp_ary << sp
    }

    sp_etc = troop.children[0].find_spec('ドッグバッグ（通常型）の特殊')
    sp_etc.cost.should == {'燃料'=>-2}
    sp_names = sp_ary.collect{|sp| sp.name }
    sp_names.include?('ドッグバッグ（通常型）の特殊').should be_truthy
  end
  it '艦船編成でも外交戦評価を出力する。' do
    source = <<EOT
_define_vessel:{
フリゲート１１番艦「神楽月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00326-01_曲直瀬りま：東国人+摂政+海賊+船乗り：敏捷+1*知識+1*幸運+1*外見+2；
＃-恩寵の時計：個人所有：外見、評価＋2（着用型／首に着用するもの）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00326-01%A1%A7%B6%CA%C4%BE%C0%A5%A4%EA%A4%DE
};
EOT
    troop = parse_troop(source)

    cha = troop.evaluators.find{|ev|ev.name == '魅力'}
    cha.is_getted_off_only?.should be_truthy

    diplomacy  = troop.evaluators.find{|ev|ev.name == '外交戦'}
    diplomacy.should_not be_nil
    diplomacy.is_getted_off_only?.should be_truthy

    diplomacy2  = troop.evaluators.find{|ev|ev.name == '外交戦（個別）'}
    diplomacy2.should be_nil

    calc = diplomacy.calculate(troop)
    calc.total.should == 15

    source = <<EOT
_define_vessel:{
フリゲート１１番艦「神楽月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00326-01_曲直瀬りま：東国人+摂政+海賊+船乗り：敏捷+1*知識+1*幸運+1*外見+2；
（CP）二人目の摂政：東国人+摂政+海賊+船乗り；
};
EOT

    troop = parse_troop(source)

    diplomacy2  = troop.evaluators.find{|ev|ev.name == '外交戦（個別）'}
    diplomacy2.should_not be_nil
    diplomacy2.is_getted_off_only?.should be_truthy

    calc = diplomacy2.calculate(troop)
    calc.total.should be_nil
    calc.children.size.should == 2

    source = <<EOT
_define_I=D:{
とある：ＲＴＲ,
（Ｐ）17-00326-01_曲直瀬りま：東国人+摂政+海賊+船乗り：敏捷+1*知識+1*幸運+1*外見+2；
（CP）二人目の摂政：東国人+摂政+海賊+船乗り；
};
EOT

    troop = parse_troop(source)

    diplomacy  = troop.evaluators.find{|ev|ev.name == '外交戦'}
    diplomacy.should_not be_nil
    diplomacy.evaluate(troop).should == 14

    diplomacy2  = troop.evaluators.find{|ev|ev.name == '外交戦（個別）'}
    diplomacy2.should_not be_nil

    rtr = troop.children[0]
    puts rtr.default_partial_conditions
  end

  it '艦船の装甲評価に、歩兵のみの防御補正が足されてはならない。' do
    source = <<EOT
_define_vessel:{
コランダム_国有：コランダム級経済的輸送艦＋ＨＱ輸送量５万＋ＨＱ輸送量５万＋ＳＨＱ輸送量１０万ｔ，
（Ｐ）44-00220-xx_奥羽恭兵(奥羽りんく個人ＡＣＥ)：奥羽恭兵２
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00220-01%A1%A7%B1%FC%B1%A9%A4%EA%A4%F3%A4%AF
（Ｐ）00-xx068-xx_ロベルティーネ：西国人＋舞踏子＋魔術的舞踏子＋魔法少女
（Ｐ）00-xx069-xx_ローザ：西国人＋舞踏子＋魔術的舞踏子＋魔法少女
}; 
EOT
    troop = parse_troop(source)

    ev = troop.find_evaluator('装甲（通常）') #.should_not be_nil
#    ev = troop.find_evaluator('装甲（通常・艦船）')
    ev.should_not be_nil
    calc = ev.calculate(troop)
    calc.total.should == 19

    result = troop.common_abilities_evals_str
    result.should match(/装甲（通常）：19/)
  end

  it '無人艦船' do
    source = <<EOT
＃！評価派生：反撃での，対空戦闘
＃！有効条件：反撃する場合

_define_vessel:{
０１ツバキ_組織所有：フラワー級宇宙駆逐艦+HQ装甲，
};
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    proc{troop.action_evals_str}.should_not raise_error
    manipulation = Evaluator['操縦（個別）']

    calc = manipulation.calculate(troop.children[0])
    calc.is_na?.should be_truthy
  end
  
  it '個別艦船評価の計算においては、７５％制限による消費カウントを行なってはならない' do
    troop = parse_troop <<EOT
_define_vessel:{
ガーネット３_紅葉国所有：ガーネット級護衛艦,
（Ｐ）03-00700-01_観：高位西国人＋バーニングパイロット＋名整備士＋チューニングマスター＋ＳＨＱ知識＋ＨＱ知識＋ＨＱ知識＋補給士官＋ＨＱ知識＋ＨＱ知識＋ＨＱ知識＋ＨＱ知識：敏捷＋1*幸運＋1;
－カトラス：個人所有：白兵戦可能。白兵｛攻撃，防御，移動｝＋２。（着用型アイテム／【片手持ち武器】）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00700-01%A1%A7%B4%D1
};
_define_vessel:{
ガーネット４_紅葉国所有：ガーネット級護衛艦,
（Ｐ）05-00126-01_鍋　黒兎：南国人＋舞踏子＋元気な舞踏子＋強い舞踏子：敏捷+1*知識+1*幸運+2;
};
EOT
    costs =  troop.total_cost
    costs['燃料'].should == -2
    costs['資源'].should == 0
  end
  
  it '別状況下の一般行為判定に船乗りの艦上補正が加算されない' do
    troop = parse_troop <<EOT
_define_vessel:{
ガーネット１_紅葉国所有：ガーネット級護衛艦,
（Ｐ）24-00163-01_舞花・Ｔ・ドラッヘン：東国人＋船乗り＋海賊＋Ｓ＋宇宙艦長＋ＨＱ感覚：敏捷+1*知識+1*幸運+2;
－大健康の腕輪：個人所有：耐久力+3（着用型アイテム／腕）
－誓いの指輪：個人所有 ：誓いを守ろうとする限り＋１修正　「”我らの名は盾。ゆるぎなき愛に誓い、子らの笑みと希望の守護者たらん”」（着用型アイテム／手先）
－個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00163-01%A1%A7%C9%F1%B2%D6%A1%A6%A3%D4%A1%A6%A5%C9%A5%E9%A5%C3%A5%D8%A5%F3
};

＃！状況：ある状況
EOT
    troop.action_evals_str
    
    troop.context.change_situation_to('ある状況')
    puts troop.context.current_situation_name
    ev = troop.find_evaluator('偵察')
#    pilot = troop.context.units['舞花・Ｔ・ドラッヘン']
#    sp = pilot.find_spec('船乗りの艦上補正')
#    ev.match?(sp).should be_truthy
#    troop.no_sp?(ev).should be_falsey
    ev.evaluate(troop).should == 15
  end
  
end
