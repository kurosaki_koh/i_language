# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')

describe ILanguage::FCalc::FCalcFormatParser do
  before(:each) do
    @parser = EVCCParser.new
    @context = Context.new
  end

  it '編成と数式を混ぜて記述' do
    source = <<EOT
soldier:帝國軍歩兵＋追跡者；

（３＋４）＊２－２*３
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
    troop.expr.should_not be_nil
    troop.expr.evaluate.should == 8
  end

  it '評価式定義' do
    source = <<EOT
Ｌ：評価定義（中距離戦合計）　＝　｛
　ｌ：式　＝　3+4
　ｌ：行為分類　＝　中距離戦闘行為
｝

soldier１:歩兵；
soldier２：帝國軍歩兵；
EOT

    troop = parse_troop(source)
    troop.should_not be_nil

    ev = troop.find_evaluator('中距離戦合計')
    ev.should_not be_nil

    ev.evaluate(troop).should == 7

    result = troop.action_evals_str
    result.should match(/中距離戦合計：7/)
  end
  
  it 'ｌ：変数　←　代入値' do
    source = <<EOT
ｌ：検証中機能　←　true

_define_I=D:{ＲＴ１０：ＲＴＲ,
(P)43-xx027-xx_菊花：西国人＋帝國軍歩兵＋スターライナー＋スターリフター＋ＨＱ感覚；
(CP)43-xx032-xx：聡慎：北国人＋帝國軍歩兵＋はぐれメード＋ナースメード２；
(CP)43-xx033-xx：呂旋：北国人＋帝國軍歩兵＋はぐれメード＋ナースメード２
}；
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
#    @context.variables['検証中機能'].should == 'true'
    @context.variable('検証中機能').evaluate.should be_truthy
  end

  it 'ｌ：変数　←　代入値（任意文字列）' do
    source = <<EOT
ｌ：検証中機能　←　有効

_define_I=D:{ＲＴ１０：ＲＴＲ,
(P)43-xx027-xx_菊花：西国人＋帝國軍歩兵＋スターライナー＋スターリフター＋ＨＱ感覚；
(CP)43-xx032-xx：聡慎：北国人＋帝國軍歩兵＋はぐれメード＋ナースメード２；
(CP)43-xx033-xx：呂旋：北国人＋帝國軍歩兵＋はぐれメード＋ナースメード２
}；
EOT
    troop = parse_troop(source)
    troop.should_not be_nil
#    @context.variables['検証中機能'].should == 'true'
    @context.variable('検証中機能').evaluate.should be_truthy
  end

  it '評価子を式で利用' do
    source = <<EOT
Ｌ：評価定義（魅力に加算）　＝　｛
　ｌ：式　＝　＜魅力＞＋３
｝

soldier１:歩兵；
soldier２：帝國軍歩兵；
EOT

    troop = parse_troop(source)
    troop.should_not be_nil

    ev = troop.find_evaluator('魅力に加算')
    ev.should_not be_nil

    ev.evaluate(troop).should == 9

    result = troop.action_evals_str
    result.should match(/魅力に加算：9/)
  end

  it '評価子を特定ユニットに対して適用' do
    source = <<EOT
Ｌ：評価定義（魅力に加算）　＝　｛
　ｌ：式　＝　＜魅力＞（＜soldier2＞)＋３
｝

soldier1:歩兵；
soldier2：帝國軍歩兵；
EOT

    troop = parse_troop(source)
    troop.should_not be_nil

    ev = troop.find_evaluator('魅力に加算')
    ev.should_not be_nil

    ev.evaluate(troop).should == 7

    result = troop.action_evals_str
    result.should match(/魅力に加算：7/)
  end

  it '評価子で組み込み関数を利用' do
    source = <<EOT
Ｌ：評価定義（ＲＤ合算テスト）　＝　｛
　ｌ：式　＝　３　＆　４
｝

soldier１:歩兵；
soldier２：帝國軍歩兵；
EOT

    troop = parse_troop(source)
    troop.should_not be_nil

    rdmerge = troop.variable('ＲＤ合算')
    rdmerge.should_not be_nil
    ev = troop.find_evaluator('ＲＤ合算テスト')
    ev.should_not be_nil

    ev.evaluate(troop).should == 7

    result = troop.action_evals_str
    result.should match(/ＲＤ合算テスト：7/)
  end

  it '状況別変数定義' do
    source = <<EOT
ｌ：変数Ａ　←　１

Ｌ：評価定義（変数と状況）　＝　｛
　ｌ：式　＝　＜変数Ａ＞
｝

soldier1:歩兵；
soldier2：帝國軍歩兵；

EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    var = troop.variable('変数Ａ')
    var.should_not be_nil
    var.evaluate.should == 1
    result = troop.action_evals_str
    result.should_not be_nil

    ev = troop.find_evaluator('変数と状況')
    calc = ev.calculate(troop)
    puts "`#{calc.to_s}`"

    calc.total.should == 1

    source = <<EOT
ｌ：変数Ａ　←　１

Ｌ：評価定義（変数と状況）　＝　｛
　ｌ：式　＝　＜変数Ａ＞
｝

soldier1:歩兵；
soldier2：帝國軍歩兵；

＃！状況：状況２
ｌ：変数Ａ　←　２
EOT
    troop = parse_troop(source)
    troop.should_not be_nil

    var = troop.variable('変数Ａ')
    var.should_not be_nil
    var.evaluate.should == 1
    result = troop.action_evals_str
    result.should_not be_nil


    ev = troop.find_evaluator('変数と状況')
    calc = ev.calculate(troop)

    calc.total.should == 1

    @context.change_situation_to('状況２')
    calc = ev.calculate(troop)

    calc.total.should == 2
  end


  it '型チェック（２項演算子）' do
    source = <<EOT
Ｌ：評価定義（型違反）　＝　｛
　ｌ：式　＝　＜soldier2＞＋３
｝

soldier1:歩兵；
soldier2：帝國軍歩兵；
EOT

    troop = parse_troop(source)
    troop.should_not be_nil

    ev = troop.find_evaluator('型違反')
    calc = ev.calculate(troop)
    calc.result.should be_nil
    ILanguage::FCalc::Parameter.find_type(Soldier).should == Unit
    calc.error.to_s.should == "エラー：関数 add の引数 引数１ の型(ユニット)が定義（数値）と一致しません。"

  end

  it '型チェック（ユーザ定義関数）' do
    source = <<EOT
Ｌ：評価定義（型違反）　＝　｛
　ｌ：式　＝　＜直積＞（2，<soldier2>）
｝

ｌ：直積　←　関数（関数引数１：数値，関数引数２：数値）｛
　＜関数引数１＞＊＜関数引数２＞
｝


soldier1:歩兵；
soldier2：帝國軍歩兵；
EOT

    troop = parse_troop(source)
    troop.should_not be_nil

    ev = troop.find_evaluator('型違反')
    calc = ev.calculate(troop)
    calc.result.should be_nil
    calc.error.to_s.should == "エラー：関数  の引数 関数引数２ の型(ユニット)が定義（数値）と一致しません。"

  end

  it '型チェック（ビルトイン関数）' do
    source = <<EOT
Ｌ：評価定義（型違反）　＝　｛
　ｌ：式　＝　＜ＲＤ合算＞（2，<soldier2>）
｝


soldier1:歩兵；
soldier2：帝國軍歩兵；
EOT

    troop = parse_troop(source)
    troop.should_not be_nil

    ev = troop.find_evaluator('型違反')
    calc = ev.calculate(troop)
    calc.result.should be_nil
    calc.error.to_s.should == "関数 ＲＤ合算 の可変長引数の型(ユニット)が宣言(配列（数値）)と一致しません。"
  end

  it '数式のみの編成データ' do
    source = <<EOT
1+2
EOT

    troop = parse_troop(source)
    troop.should_not be_nil
    troop.evaluators.size.should == 0

    result = troop.expression_evals_str
    result.should == '1+2 = 3'

  end

  it '関数定義のみの編成データ' do
    source = <<EOT
ｌ：テスト　←　関数（テスト：数値）｛
　３
｝
EOT

    proc{ troop = parse_troop(source)}.should raise_error(RuntimeError , '部隊にユニットが存在しません。')

  end

  it '未定義変数を含む数式のみの編成データ' do
    source = <<EOT
＜みていぎ＞＋３
EOT

    troop = parse_troop(source)

    troop.should_not be_nil
    result = troop.expression_evals_str
    result.should .should == "＜みていぎ＞＋３ = N/A"
  #raise_error(RuntimeError , '識別子 みていぎ が定義されていません。')
  end

  it '太元組み込み時の循環参照チェック' do
    source = <<EOT
ｌ：変数Ａ　←　＜変数Ｂ＞

ｌ：変数Ｂ　←　＜変数Ｃ＞

ｌ：変数Ｃ　←　＜変数Ａ＞

ｌ：変数Ｄ　← ３

ｌ：変数Ｅ　←　＜変数Ｄ＞

＜変数Ａ＞＋＜変数Ｄ＞
EOT

    troop = parse_troop(source)

    expr = troop.expr

    var_a = troop.variable('変数Ａ')
    var_a.is_circular?(troop.context).should be_truthy

    var_d = expr.context.variable('変数Ｅ')
    var_d.is_circular?(troop.context).should be_falsey

    proc {expr.evaluate(troop)}.should raise_error(RuntimeError , /循環参照/)

    troop.expression_evals_str.should == '＜変数Ａ＞＋＜変数Ｄ＞ = N/A'
#    proc {expr.evaluate}.should raise_error(RuntimeError , /test/)
  end

  it '計算結果がユニットになる式' do
    source = <<EOT
ほへいさん:東国人＋帝國軍歩兵；

ｌ：変数Ａ　←　＜ほへいさん＞
＜変数Ａ＞
EOT
    troop = parse_troop(source)

    expr = troop.expr
    result = troop.expression_evals_str
    puts result
    result.should_not be_nil 
  end

  it '太元書式組み込み時の配列代入' do
    source = <<EOT
ほへいさん:東国人＋帝國軍歩兵；

ｌ：随行ユニットが提出可能な評価分類　←　｛'装甲'，'情報戦防御'｝
＜随行ユニットが提出可能な評価分類＞
EOT
    troop = parse_troop(source)

    expr = troop.expr
    result = troop.expression_evals_str
    puts result
    result.should_not be_nil
    puts troop.action_evals_str
  end
end
