# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'./calculator_test_helper.rb')

describe Definitions do
  before(:each) do
    @context = Context.new
    @definitions = Definitions.new(:activerecord => IDefinition)
  end

  it 'Definition のインスタンス化' do
    hacker_def = @definitions['ハッカー']
    hacker_def['Ｌ：名'].should == 'ハッカー'
  end

  it '暫定登録定義への参照' do
    @definitions['テスト'].should be_nil
    @definitions['テスト'] = {'名称'=> 'test'}

    @definitions['テスト'].should_not be_nil
    @definitions['テスト']['名称'].should == 'test'

    definitions2 = Definitions.new(:activerecord => IDefinition)
    definitions2['テスト'].should be_nil

  end

  it '非一般評価のＨＱ擬似アイドレス' do
    source = <<EOT
_define_vessel:{
フリゲート１番艦「太郎月」_組織所有：ムーン級パトロールフリゲート＋ＨＱ装甲＋ＨＱ装甲,
（Ｐ）17-00328-01_夜狗　樹：東国人；
-猫と犬の前足が重なった腕輪：個人所有：同調、評価＋３（着用型／腕に着用するもの）
-個人取得ＨＱ根拠ＵＲＬ：http://farem.s101.xrea.com/idresswiki/index.php?00328-01%A1%A7%CC%EB%B6%E9%A1%A1%BC%F9
};
EOT
    armor = Evaluator['装甲（通常）']
    troop = parse_troop(source)
    armor.evaluate(troop).should == 17
  end

end

