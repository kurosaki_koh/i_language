# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator
    MeleeWeapons = %w[格闘 剣 槍 短剣 スコップ 鞭 打撃武器 斧 ドリル 絶対物理防壁 パイルバンカー].collect{|name|"白兵（#{name}）"}
    ShootWeapons = %w[射撃（銃） 射撃（弓） 射撃（魔の力） 射撃（砲撃） 射撃（爆弾） 射撃（その他）]

    Evaluators = {}
    EvaluatorsArray =[]
    RegisteredActionNames = []
    ClassInstances = Hash.new{|h,k| h[k] = []}
    ActionNameOrder = {}
    ImplicitEvaluators = []
    class Evaluator
      attr :name , true
      attr :args, true
      attr :bonus_filter , true
      attr :action_name , true
      attr :class_name , true
      attr :optional , true
      attr :tags , true
      attr :is_standard , true
      attr :particular_situation_name , true
      attr :order , true
      attr :ancestor , true
      attr :predefined,true
      attr :implicitly,true
      attr :conditions , true
      attr :bonus_target , true
      attr :whole_condition , true
      attr :represent_order , true
      attr :alternatives , true
      attr :team_alternatives , true
      attr :alt_name , true
      attr :target_filters , true
      attr :display_name , true
      attr :is_individualy , true
      attr :target_set_type , true
      attr :is_include_evaluate_items , true
      attr :chained_evaluators , true
      attr :branch_calculation_class , true
      attr :leaf_calculation_class , true
      attr :is_parent_eval_enabled , true
      attr :is_parent_bonus_enabled , true
      attr_accessor :get_off_only
      attr_accessor :disabled_sps 
      attr_accessor :is_for_optional_equips
      attr_accessor :prototype , :target_exception_filters
      attr_accessor :optional_rule
      
      alias is_getted_off_only? get_off_only

      alias is_parent_eval_enabled? is_parent_eval_enabled
      alias is_parent_bonus_enabled? is_parent_eval_enabled
      alias is_for_optional_equips? is_for_optional_equips
      
      @@order = 0

      def initialize
        @alternatives = [self]
        @weapon_class = nil
        @target_filters = nil
        @target_exception_filters = nil
        @target_set_type = :default
        @is_include_evaluate_items = true
        @is_parent_eval_enabled = true
        @is_parent_bonus_enabled = true
        @chained_evaluators = [self]
        @leaf_calculation_class = Calculation
        @additional_tqr_ids = []
        @get_off_only = false
        @disabled_sps = []
        @is_for_optional_equips = false
        @prototype = nil
        @optional_rule = nil
        @represent_order = @order = 0
      end

      def cache_id
        (@target_filters || @target_exception_filters) ? self.object_id : nil
      end

      def is_include_evaluate_items?
        @is_include_evaluate_items
      end

      def is_individualy?
        !!@is_individualy
      end

      def target_set(troop)
        case @target_set_type
        when :default
          troop.children
        when :characters
          if self.is_getted_off_only?
            troop.getted_off_characters
          else
            troop.characters
          end
        when :band
          troop.band
        else
          troop.children
        end
      end

      class TargetFilter
        attr_reader :condition
        attr_accessor :parent
        def initialize(cond , context = nil)
          @condition = case cond
          when Regexp
            cond
          when String
            if context && context.units.has_key?(cond)
              cond
            else
              /#{cond}/
            end
          else
            cond
          end
        end
        
        def self.create_filters(conditions , context = nil)
          conditions.collect{|cond|self.new(cond , context)}
        end
        
        def is_target?(unit)
          case condition
          when String
            unit.name == condition
          when Regexp
            flag = unit.sp_include?(condition)
            flag && if self.parent.disabled_sps.size == 0
              true
            else
              !!unit.specialties.find{|sp|
                source = sp.source
                source =~ condition && self.parent.disabled_sps.all?{|re| source !~ re}
              }
            end
          when Proc
            condition[unit]
          end
        end
      end
      
      def target_filters=(conditions)
        @target_filters = accept_filters(conditions)
      end
      
      def target_exception_filters=(conditions)
        @target_exception_filters = accept_filters(conditions)
      end
      
      def accept_filters(conditions)
        conditions.collect{|cond|
          result = case cond
          when TargetFilter
            cond
          else
            TargetFilter.new(cond)
          end
          result.parent = self
          result
        }
      end
      private :accept_filters
      
      def do_target?(unit)
        result = true
        result &= @target_filters.any?{|cond|cond.is_target?(unit)} if @target_filters
        result &= @target_exception_filters.all?{|cond|!cond.is_target?(unit)} if @target_exception_filters
        result &= !(unit.is_attendant? &&
            !(unit.context.variable('随行ユニットが提出可能な評価分類').include?(self.class_name)))

        result &= self.is_individualy? ||  unit.evals_keys.include?(self.args(unit)[0]) || self.is_action_bonus?
      end

      def are_applicable_between(unit)
        self.do_target?(unit) && unit.is_targetted?(self)
      end
      
      def is_battle_evaluator?
        @battle ||= ['攻撃','防御','移動'].include?(bonus_target)
      end

      def select_alternatives(unit)
        if self.team_alternatives.size > 1 && (unit.class < ContainerUnit || unit.is_ace?)
          klasses = unit.weapon_classes
          if is_battle_evaluator?
            @team_alternatives.select{|alt|klasses.include?(alt.alt_name)}
          else
            @team_alternatives
          end
        else
          if is_battle_evaluator?
            names = unit.weapon_names
            selected = @alternatives.select{|alt|names.include?(alt.alt_name)}
            selected.unshift(@alternatives[0]).uniq
          else
            @alternatives
          end
        end
      end

      def choice_alternative(unit)
        alternatives = self.select_alternatives(unit)
        if alternatives.size == 1
          unit.calculate(alternatives[0])
        else
          calcs =  alternatives.collect{|ev|
            unit.calculate(ev)
          }
          result = calcs.max{|a,b|
            a <=> b
          }
          result ? result : unit.calculate(self)
        end
      end
      
      def self.create(name,action_name , *args , &filter)
        evaluator = self.new
        initialize_values(evaluator , name , action_name , *args , &filter)
        self.register(evaluator)
        evaluator
      end

      def define_alternative(name , additional_conds , for_container = false, &filter)
        evaluator = self.clone
        evaluator.implicitly = false
        evaluator.name = name
        additional_conds = [/#{additional_conds}/] if additional_conds.class == String
        evaluator.conditions |= additional_conds
        evaluator.instance_eval filter if filter
        evaluator.represent_order = @@order
        @@order += 100
        if for_container
          self.team_alternatives << evaluator
        else
          self.alternatives << evaluator
        end
        evaluator.team_alternatives = [evaluator]
        evaluator.alternatives = [evaluator]
        evaluator.prototype = self
        evaluator
      end

      def clone
        result = super
        idx = @alternatives.index(self)
        result.alternatives = @alternatives.dup
        result.team_alternatives = @team_alternatives.dup
        result.chained_evaluators = @chained_evaluators.dup
        result.chained_evaluators[0] = result if @chained_evaluators[0] == self

        result.disabled_sps = @disabled_sps.dup
        result
      end

      def disabled_sps
        @prototype ? @prototype.disabled_sps : @disabled_sps
      end
      
      def define_weapon_alternative(alt_name,ev)
        original_name = ev.name
        reg = /([^（]+)（(.+)）/o
        alt_name =~ reg
        @weapon_class = $1.dup
        weapon = $2

        self.name =~ /([^（]+)(（(.+)）)?/o
        conditions = case @weapon_class
        when '白兵'
          [/白兵（格闘）/o,/白兵（全般）/,/白兵（兵器）/o]
        when '射撃'
          [/射撃（全般）/o,/射撃（兵器）/o]
        else
          []
        end
        ev.conditions |= conditions
        conditions |= [/#{alt_name}/]
        alt_ev_name = sprintf('%s（%s%s）',original_name,@weapon_class ? (@weapon_class + '・') : nil , weapon)
        alt_ev = self.define_alternative(alt_ev_name , conditions)
        alt_ev.alt_name = alt_name
        alt_ev
      end

      def self.create_optional(name,action_name , *args , &filter)
        evaluator = self.new
        evaluator.optional = true
        initialize_values(evaluator , name , action_name , *args , &filter)
        self.register(evaluator)
        evaluator
      end

      def self.create_standard(name,action_name , *args , &init)
        evaluator = self.new
        evaluator.is_standard = true
        initialize_values(evaluator , name , action_name , *args , &init)
        self.register(evaluator)
        evaluator
      end

      def Evaluator.order
        @@order
      end

      def self.register(ev)
        name = ev.name
        action_name = ev.action_name
        Evaluators[name] = ev
        ev.represent_order = ev.order = @@order
        @@order += 100
        EvaluatorsArray << ev
        RegisteredActionNames << action_name.to_sym if action_name && !RegisteredActionNames.include?(action_name.to_sym)
        ClassInstances[ev.class_name] << ev
        ImplicitEvaluators << ev if ev.implicitly

      end

      def self.initialize_values(ev ,  name , action_name_or_tags , *args , &init)
        tags = []
        ev.name = ev.class_name = name
        ev.display_name = name if ev.display_name.nil?

        case action_name_or_tags
        when String
          action_name = action_name_or_tags
        when Array
          action_name = action_name_or_tags.shift
          tags = action_name_or_tags
        when Hash
          action_name = action_name_or_tags[:action_name]
          tags = action_name_or_tags[:tags] || []
          class_name = action_name_or_tags[:class_name]
        else
          action_name = nil
        end
        ev.action_name = action_name
        ev.args = args
        ev.tags = tags
        ev.class_name = class_name if class_name
        ev.optional = false if ev.optional.nil?
        ev.predefined = true
        ev.implicitly = false
        ev.whole_condition = false
        ev.conditions = []
        ev.bonus_target = ''
        ev.instance_eval &init
        ev.team_alternatives = [ev]
        
        self.init_alternatives(ev)

      end

      def self.init_alternatives(ev)
        if ev.bonus_target == '攻撃' && ev.name != '詠唱'

          ev.alternatives =  [ev]
          ev.team_alternatives = [ev]
          for sub_class in ['白兵','射撃']
            alt_name = case ev.name
            when /(.+)（(.+)）$/
              "#{$1}（#{$2}・#{sub_class}）"
            else
              ev.name + "（#{sub_class}）"
            end
            alt_ev = ev.define_alternative(alt_name,[/#{sub_class}（.+）/],true)
            alt_ev.alt_name = sub_class
          end
          for weapon_name in MeleeWeapons + ShootWeapons
            ev.define_weapon_alternative(weapon_name,ev)
          end
        elsif %w[防御 移動].include?(ev.bonus_target)
          ev.conditions << /(白兵|射撃)（.+）/o
        end
        
      end

      def is_predefined?
        @predefined
      end
      
      def is_implicitly?
        @implicitly
      end

      alias is_predefined? predefined
      alias is_implicitly? implicitly
      alias is_standard? is_standard
      

      def Evaluator.original_evaluator(class_name)
        if ClassInstances.has_key?(class_name)
          ClassInstances[class_name][0]
        else
          raise "存在しない評価子クラス名'#{class_name}'が指定されています。"
        end
      end

      def args(unit = nil)
        if unit && unit.evals_keys.include?(self.class_name)
          [self.class_name]
        else
          @args
        end
      end

      def condition_match?(sp)
        condition_matched = if whole_condition
          !(sp['補正使用条件'] =~ conditions[0]).nil?
        else
          is_available_sp?(sp,conditions)
        end

        condition_matched && if sp.is_ability_bonus?
          self.args.any?{|arg|sp.is_ability_match?(arg)}
        else
          sp.is_ability_match?(bonus_target)
        end
      end

      def is_available_sp?(sp , conditions = [])
        sp.is_available?(conditions) && (!self.target_filters || sp.owner.troop.is_tqr_qualified?(sp,self))
      end
      
      def match?(sp)
        case
        when self.disabled_sps.any?{|re|sp.source =~ re}
          false
        when !sp.is_unit_type_match?
          false
        when !@is_include_evaluate_items && sp.troop_member.is_evaluate_item?(sp)
          false
        when sp.class == FlexibleBonusSpec && sp.is_judge_bonus?
          names = [self.name , self.action_name , self.class_name , "全判定"] +  self.tags
          is_available_sp?(sp) && names.include?(sp.flexible_target)
        when bonus_filter.nil? && condition_match?(sp)
          true
        when bonus_filter && bonus_filter.call(sp)
          true
        when @is_include_evaluate_items && is_available_sp?(sp)
          self.args.any?{|arg| sp.is_ability_match?(arg)}
        else
          false
        end
      end

      def particular_situation(unit)
        prepare_particular_situation(unit)
        result = yield
        restore_situation(unit)
        result
      end
      
      def prepare_particular_situation(unit)
        if @particular_situation_name
          current = unit.context.current_situation_name
          unit.context.push_particular_situation(@particular_situation_name)
          unit.swapped_situation_name = current
        end
      end

      def restore_situation(unit)
        if @particular_situation_name
          unit.context.pop_particular_situation
          unit.swapped_situation_name = nil
        end
      end


      def calculate(unit)
        unit.update_situation
        unit.current_situation.calculation_cache[self] ||= begin
          particular_situation(unit){
            unit.calculate(self)
          }
#          restore_situation(unit)
        end
      end
      
      def evaluate(unit)
        calc = self.calculate(unit)
#        calc && calc.total
        result = calc && calc.total  # calc.total には燃料消費フラグの副作用があるため、飛ばす事はできない。
        self.is_action_bonus? ? nil : result
      end

      def Evaluator.[](key)
        Evaluators[key]
      end

      def is_undefined?
        @args.size == 0 && @bonus_filter.nil?
      end

      def is_omitted?(unit)
        clan = (unit.troop.evaluators + ImplicitEvaluators).select{|ev|ev.class_name == self.class_name}
        self_calc = self.calculate(unit)
        return true if self_calc.nil? || self_calc.is_na? || (self_calc.total.class != String && self.is_implicitly?)
        return false if unit.all_containments.include?(self.name)
        same_value = clan.find{|ev|
          current_calc = ev.calculate(unit)
          next if current_calc.is_na?
          !unit.all_containments.include?(ev.name) &&
            current_calc.total == self_calc.total &&
            (current_calc.total.class == String ? self.particular_situation_name == ev.particular_situation_name : current_calc.to_digest == self_calc.to_digest) &&
            ev.represent_order < self.represent_order 
        }

        !(same_value.nil?)
      end
      
      def optional_availability(team)
        if !self.is_predefined?
          self.ancestor.optional_availability(team)
        else
          !self.optional_rule || 
            self.optional_rule.is_available?(team.children,team)
        end
      end
      
      def is_available?(team)
        particular_situation(team){
          optional_availability(team) && (self.is_standard? || self.action_name.nil? || team.is_action_available?(self.action_name))
        }
#        prepare_particular_situation(team)
#        restore_situation(team)
#        result
      end

      def is_action_bonus?
        args.size == 1 && args[0] == '' && !action_name.nil?
      end

      def chained_evaluator(unit)
        if @alternatives.size > 1
          self.choice_alternative(unit).evaluator
        else
          @chained_evaluators[0]
        end
      end

      def sp_target(sp)
        case self.target_set_type
        when :default , :band
          sp.owner.troop_member
        when :characters
          sp.owner
        else
          sp.owner.troop_member
        end

        #if target.troop.formation == :fleet
      end

      def is_branch?(unit)
        unit.class <= Troop
      end

      def is_leaf?(unit)
        !is_branch?(unit)
      end

      def branch_calculation_class
        @branch_calculation_class || (self.is_individualy? ? CalculationSet : TeamCalculation)
      end

      def create_calculation(unit)
        if is_branch?(unit)
          self.branch_calculation_class().new(unit,self)
        else
          @leaf_calculation_class.new(unit,self)
        end        
      end

      def format(unit )
        calc = calculate(unit)
        return '' if self.is_action_bonus?

        line = display_name + '：'
        if calc
          line << calc.total.to_s
        else
          line << '-'
        end
        line = "\n・" + line if is_for_optional_equips? #オプション装備についての評価子なら、前行から強制的に改行を加える。
        line
      end
      
      def hash_for_diff(unit)
        calc = calculate(unit)
        return {} if self.is_action_bonus?
        
        key = display_name
        if calc
          return {key => calc.total}
        else
          return {}
        end
      end
    end

    class IndividualyEvaluator < Evaluator
      def initialize
        super
        @is_individualy = true
      end
        
      def format(unit)
        calc = calculate(unit)
        return '' if calc.children.size == 0
        result = "○#{name}\n"
        for child in calc.children
          next if child.is_na?
          result << "　・#{child.unit.name}：#{child.inspect_result}\n"
        end
        result
      end
      
      def hash_for_diff(unit)
        result = {}
        calc = calculate(unit)
        return result if calc.children.size == 0
        header1 = "#{display_name}＠"
        for child in calc.children
          next if child.is_na?
          child.hash_for_diff.each {|key , val| 
            result.update({(header1+key) => val } )
          }
        end
        result
      end
    end
    
    class AbilityEvaluator < Evaluator
      
      def calculate(unit)
        unit.update_situation
        unit.current_situation.calculation_cache[self] ||= begin
#          prepare_particular_situation(unit)
          value =  unit.evaluate(args[0])
          calc = Calculation.new(unit , self)
          calc.unit = unit
          calc.base_value = value
          calc.arg_name = args[0]
          calc.arg_names = args.dup
#          restore_situation(unit)
          calc
        end
      end
      
    end
    
    class AbilitiesEvaluator < IndividualyEvaluator
      def initialize
        super
        @sub_evaluators = {}
      end
        
      def calculate(unit)
        unit.update_situation
        unit.current_situation.calculation_cache[self] ||= begin
#          prepare_particular_situation(unit)
          root = nil
          particular_situation(unit) {
            root = CalculationSet.new(unit , self)
            keys = StandardAbilities.select{|col| unit.evals_keys.include?(col)}
            for key in keys
              @sub_evaluators[key] ||= begin 
                ev = AbilityEvaluator.new
                AbilityEvaluator.initialize_values(ev , key , nil , key){|ev|
                  ev.conditions = []
                  ev.bonus_target = key.dup
                  ev.class_name = key.dup
                }
                ev
              end
              root.add(@sub_evaluators[key].calculate(unit))
            end
          }

#          restore_situation(unit)
          root
        end
      end
      
      def format(unit ,opts ={})
        unit_keys = unit.evals_keys
        keys = StandardAbilities.select{|item| unit_keys.include?(item) }
        
        calc = calculate(unit)
        header = keys.join('：')+"\n"
        values = calc.children.collect{|item| item.base_value }
        values_line = values.join("：")+"\n"
       
        diff_line = if self.ancestor && unit.context.variable('一般評価派生での差分値表示')
          diff = calc.difference(  self.ancestor.calculate(unit)  )
          '(' + diff.collect{|d| sprintf("%+d" , d)}.join("：")+")\n"
        else
          ''
        end
        
        result =  header + values_line + diff_line
        if self.particular_situation_name
          "\n（#{self.particular_situation_name}）\n" + result
        else
          result.strip
        end
      end
    end
    
    class SynchronizeEvaluator < Evaluator
      Details = Struct.new("SynchronizerDetail" , :member , :value, :team_bonuses)
      Details.class_eval do
        def inspect
          [self.member[0].name , self.value , self.member.size].inspect
        end
      end
      def evaluate(unit)
        details(unit).total
      end

      def calculate(unit)
        if unit.class <= Troop
          details(unit)
        else
          super
        end
      end

      def details(unit)
#        prepare_particular_situation(unit){
        particular_situation(unit){
          case unit
          when Soldier 
            unit.synchronizer(self)
          else
            unit.primary_synchronizer(self)
          end
        }
#        restore_situation(unit)
#        calc
      end
      
      def hash_for_diff(unit)
        result = {}
        calc = calculate(unit)
        return result if calc.children.size == 0
        header1 = "#{display_name}＠"
        calc.hash_for_diff.each do |key , val|
          result.update(header1 + key => val)
        end
        result
      end
    end

    class InnerEvaluator < Evaluator
      Details = Struct.new(:member , :value , :team_bonuses)
    end

    class IndividualAbilityEvaluator < Evaluator
      attr :actor_filter , true
      Details = Struct.new("IndividualAbilityDetail" ,:member, :value )
      def evaluate(unit)
        case unit
        when Soldier , Vehicle , Vessel
          unit.evaluate(self)
        else
          unit.accept_evaluator(self, self.actors(unit))
        end
      end

      def actors(unit)
        if unit.class <= Troop
          unit.children.select{|c| actor_filter.call(c)}
        else
          actor_filter.call(unit) ? [unit] : []
        end
      end

      def details(unit)
        result = Details.new({})
        result.member = actors(unit)
        result.value = evaluate(unit)
        result
      end
    end

    class AlternativeEvaluator < Evaluator
      Details = Struct.new('AlternativeDetail',:member,:value , :each_values,:sub_evaluators)
      EachDetails = Struct.new('AlternativeEachData',:evaluator , :value , :sub_values) 

      def self.initialize_values(ev ,  name , action_name_or_tags , *args , &filter)
        args = args.collect{|arg|Evaluator[arg]}
        super(ev,name,action_name_or_tags,*args , &filter)
      end

      def evaluate(unit)
        if !(unit.class <= Troop)
          unit.accept_evaluator(choice(unit))
        else
          details(unit).value
        end
      end

      def details(unit)
        result = Details.new([])
        result.member = unit.children
        result.each_values = []
        result.sub_evaluators = args
        for member in result.member
          ev = choice(member)
          result.each_values << EachDetails.new(ev,evaluate(unit),unit.sub_evals(ev))
        end
        result.value = total(result)
        result
      end

      def total(detail)
        sub_evals = Hash.new{|h,k|h[k]=[]}
        for each_detail in detail.each_values
          sub_evals[each_detail.evaluator] << each_detail.sub_values
        end
        ev_values = []
        #別種の評価子ごとに評価
        for ev in sub_evals.keys
          values = []
          for arg in ev.args
            sub_evals = sub_evals[ev].sub_values.collect{|sub_eval|sub_eval[arg]}
            values << sum_evals(sub_evals)
          end
          ev_values << (values.sum / values.size).to_i
        end
        #評価子ごとの評価を合算
        sum_evals(ev_values)
      end

      def choice(unit)
        values = {}
        for ev in args
          values[ev] = ev.evaluate(unit)
        end
        values.keys.max_by{|key|values[key]}
      end
    end

    class ControlEvaluator < Evaluator
      def calculate(unit)
        calc = if defined?(unit.crew_division)
          crew_division = unit.crew_division
          parent = crew_division.parent
          crew_division.parent = nil
          calc = super(crew_division)
          crew_division.parent = parent
          calc
        elsif unit.class <= Vehicle
          super(unit.pilots)
        else
          super
        end

        calc
      end
    end

    ILanguage::FCalc::FunctorCalculation.class_eval do

      def total
        self.last_child.result
      end

      def is_na?
        !self.total
      end

      def name
        self.function_name
      end

      def to_digest
        Digest::MD5.hexdigest(self.to_s)
      end
    end
   
    class ExpressionEvaluator < Evaluator
      attr_accessor :expr

      def is_undefined?
        false
      end

      def evaluate(unit)
        begin
          @expr.evaluate(unit)
        rescue => e
          nil
        end
      end

      def calculate(unit)
        calc = ILanguage::FCalc::FunctorCalculation.new
        begin
          calc.result = @expr.evaluate(unit , :calculation => calc)
        rescue NoMethodError => e
          calc.result = nil
          calc.error = "NoMethodError:引数の型が間違っていませんか？\n#{$@[0]}\n"
        rescue RuntimeError => e
          calc.result = nil
          calc.error = e
        end
        calc
      end
    end

    #   class EvaluatorAdaptor < DelegateClass(Evaluator)
    #     attr_accessor :evaluator
    #
    #     def evaluate(context)
    #       @evaluator
    #     end
    #   end

    Evaluator::create('白兵戦',{
        :action_name => '白兵距離戦闘行為',
        :class_name => '白兵戦' ,
      },'体格','筋力') {|ev|
      ev.conditions = [/白兵距離.*?での$/o]
      ev.bonus_target = '攻撃'
    }
    
    IndividualyEvaluator::create_optional('絶技による白兵戦',{
        :action_name => '白兵距離戦闘行為',
        :class_name => '白兵戦' ,
      },'体格','筋力') {|ev|
      ev.conditions = [/^絶技.+使用時$/o,/白兵距離.*?での$/o]
      ev.bonus_target = '攻撃'
      ev.target_filters = [/（絶技戦において）白兵距離攻撃を行うことができる。/o]
    }
    
    Evaluator::create('近距離戦',{
        :action_name => '近距離戦闘行為',
        :class_name => '近距離戦闘'
      },'筋力','敏捷'){|ev|
      ev.conditions = [/射撃戦として/o,/近距離.*?での$/o]
      ev.bonus_target = '攻撃'
    }

    module MiddleRangeTargetSet
      def self.extended(ev)
        ev.conditions = [/射撃戦として/o,/中距離.*?での$/o]
        ev.bonus_target = '攻撃'
      end
      
      def target_set(troop)
        #評価対象集合に兵員輸送車系があった場合の追加処理
        result = super.reverse
        careers = result.select{|target|
          target.class.to_s.to_sym == :'ILanguage::Calculator::Career' #どういうわけかandLinux上ではクラスCareerとの比較でSegmentation Faultするため、文字列比較でバグを回避。
        }
        for career in careers
          idx = result.index(career)
          if idx
            if gunner = career.children.find{|child| child.passenger_type == :gunner}
              #銃手がいたら兵員輸送車と置き換える
              result[idx]=gunner
            else
              #銃手がいなければ取り除く
              result.delete_at(idx)
            end
          end
        end
        result.reverse
      end
    end
   
    Evaluator::create('中距離戦','中距離戦闘行為','感覚','知識'){|ev|
      ev.extend MiddleRangeTargetSet

    }

    Evaluator::create_optional('中距離戦・衛生兵のみ','中距離戦闘行為','器用','知識'){|ev|
      ev.extend MiddleRangeTargetSet
      ev.display_name = '中距離戦（衛生兵のみ）'
      ev.target_filters = [/衛生兵/o]

    }

    Evaluator::create_optional('中距離戦・衛生兵以外','中距離戦闘行為','感覚','知識'){|ev|
      ev.extend MiddleRangeTargetSet
      ev.display_name = '中距離戦（衛生兵以外）'
      ev.target_exception_filters = [/衛生兵/o]
    }

    ExpressionEvaluator::create_optional('中距離戦（合算）','中距離戦闘行為','感覚','知識'){|ev|
      ev.extend MiddleRangeTargetSet
      ev.expr = ILanguage::FCalc::Parser.parse('＜中距離戦・衛生兵のみ＞ ＆　＜中距離戦・衛生兵以外＞').interpret

    }
    
    IndividualyEvaluator::create_optional('琴弓の一撃による対空戦闘',nil,'感覚','知識'){|ev|
      ev.conditions = [/射撃戦として/o,/中距離.*?での$/o]
      ev.bonus_target = '攻撃'

      ev.target_filters = [proc{|u|u.idresses.include?('琴弓の一撃')}]
    }

    Evaluator::create('ランス投げ白兵戦',{
        :action_name => 'ランス投げ戦闘行為',
        :class_name => '白兵戦'
      },'体格','筋力') {|ev|
      ev.conditions = [/ランス投げによる/o,/白兵距離.*?での$/o]
      ev.bonus_target = '攻撃'
    }

    Evaluator::create('ランス投げ近距離戦',{
        :action_name => 'ランス投げ戦闘行為',
        :class_name => '近距離戦闘'
      },'筋力','敏捷'){|ev|
      ev.conditions = [/ランス投げによる/o,/射撃戦として/o,/近距離.*?での$/o]
      ev.bonus_target = '攻撃'
    }
    Evaluator::create('ランス投げ中距離戦','ランス投げ戦闘行為','感覚','知識'){|ev|
      ev.conditions = [/ランス投げによる/o,/射撃戦として/o,/中距離.*?での$/o]
      ev.bonus_target = '攻撃'
    }
    Evaluator::create('遠距離戦',{
        :action_name => '遠距離戦闘行為',
        :class_name => '遠距離戦闘'
      },'敏捷','感覚'){|ev|
      ev.conditions = [/遠距離.*?での$/o]
      ev.disabled_sps = [/超遠距離での/]
      ev.bonus_target = '攻撃'
    }
    Evaluator::create('超遠距離戦闘','超遠距離戦闘行為','知識','幸運'){|ev|
      ev.conditions = [/超遠距離.*?での$/o]
      ev.bonus_target = '攻撃'
    }

    Evaluator::create('装甲（通常）',{
        :tags => ['装甲','防御'],
        :class_name => '装甲'
      },'体格','耐久力') {|ev|
      ev.conditions = [/装甲での/o]
      ev.bonus_target = '防御'
    }

    #    Evaluator::create_optional('装甲（通常・艦船）',{
    #        :tags => ['装甲','防御'],
    #        :class_name => '装甲'
    #      },'体格','耐久力') {|ev|
    #      ev.conditions = [/装甲での/o]
    #      ev.bonus_target = '防御'
    #      ev.display_name = '装甲（通常）'
    #      ev.disabled_sps = [/　＝　歩兵(武装)?，/o]
    #    }
    #
    Evaluator::create('装甲（白兵距離）',{
        :action_name => '白兵距離戦闘行為' ,
        :tags => ['装甲','防御'],
        :class_name => '装甲'
      },'体格','耐久力') {|ev|
      ev.conditions = [/白兵距離.*?での/o,/装甲での/o]
      ev.bonus_target = '防御'
    }
    Evaluator::create('装甲（近距離）',{
        :action_name => '近距離戦闘行為' ,
        :tags => ['装甲','防御'],
        :class_name => '装甲'
      },'体格','耐久力') {|ev|
      ev.conditions = [/射撃戦として/o,/近距離.*?での/o,/装甲での/o]
      ev.bonus_target = '防御'
    }
    Evaluator::create('装甲（中距離）',{
        :action_name => '中距離戦闘行為' ,
        :tags => ['装甲','防御'],
        :class_name => '装甲'
      },'体格','耐久力') {|ev|
      ev.conditions = [/射撃戦として/o,/中距離.*?での/,/装甲での/o]
      ev.bonus_target = '防御'
    }
    Evaluator::create('装甲（遠距離）',{
        :action_name => '遠距離戦闘行為' ,
        :tags => ['装甲','防御'],
        :class_name => '装甲'
      },'体格','耐久力') {|ev|
      ev.conditions = [/遠距離での/o,/装甲での/o]
      ev.bonus_target = '防御'
    }

    Evaluator::create_optional('装甲（スカールドのみ）',{
        :tags => ['装甲','防御'],
        :class_name => '装甲'
      },'体格','耐久力') {|ev|
      ev.conditions = [/装甲での/o]
      ev.bonus_target = '防御'
      ev.target_set_type = :band
    }

    Evaluator::create_optional('装甲（小型メカのみ）',{
        :tags => ['装甲','防御'],
        :class_name => '装甲'
      },'小型メカ装甲') {|ev|
      ev.conditions = [/装甲での/o]
      ev.bonus_target = '防御'
      ev.target_filters = ['遠隔動作補助システム']
    }

    Evaluator::create_optional('敏捷による防御（通常）',{:tags => ['防御'],:class_name => '防御'},'敏捷') {|ev|
      ev.conditions = [/回避での/o]
      ev.bonus_target = '防御'
    }

    Evaluator::create_optional('敏捷による防御（白兵距離）',{
        :action_name => '白兵距離戦闘行為' ,
        :tags => ['防御'],
        :class_name => '防御'
      },'敏捷') {|ev|
      ev.conditions = [/回避での/o,/白兵距離.*?での/o]
      ev.bonus_target = '防御'
    }

    Evaluator::create_optional('敏捷による防御（近距離）',{
        :action_name => '近距離戦闘行為' ,
        :tags => ['防御'],
        :class_name => '防御'
      },'敏捷') {|ev|
      ev.conditions = [/回避での/o,/射撃戦として/o,/近距離.*?での/o]
      ev.bonus_target = '防御'
    }

    Evaluator::create_optional('敏捷による防御（中距離）',{
        :action_name => '中距離戦闘行為' ,
        :tags => ['防御'],
        :class_name => '防御'
      },'敏捷') {|ev|
      ev.conditions = [/回避での/o,/射撃戦として/o,/中距離.*?での/o]
      ev.bonus_target = '防御'
    }

    Evaluator::create_optional('器用による防御（通常）',{:tags => ['防御'] ,:class_name => '防御'},'器用') {|ev|
      ev.conditions = []
      ev.bonus_target = '防御'
    }

    Evaluator::create_optional('器用による防御（白兵距離）',{
        :action_name => '白兵距離戦闘行為' ,
        :tags => ['防御'],:class_name => '防御'},'器用') {|ev|
      ev.conditions = [/白兵距離.*での/o]
      ev.bonus_target = '防御'
    }

    Evaluator::create_optional('器用による防御（近距離）',{
        :action_name => '近距離戦闘行為' ,
        :tags => ['防御'],:class_name => '防御'},'器用') {|ev|
      ev.conditions = [/射撃戦として/o,/近距離.*?での$/o]
      ev.bonus_target = '防御'
    }

    Evaluator::create_optional('器用による防御（中距離）',{
        :action_name => '中距離戦闘行為' ,
        :tags => ['防御'],:class_name => '防御'},'器用') {|ev|
      ev.conditions = [/射撃戦として/o,/中距離.*?での/o]
      ev.bonus_target = '防御'
    }


    Evaluator::create('移動（通常）' , {:tags => ['移動'] ,:class_name => '移動'} , '敏捷'){|ev|
      ev.conditions = []
      ev.bonus_target = '移動'
      ev.display_name = '移動'
      ev.implicitly = true
    }

    #    Evaluator::create_optional('移動自動成功' ,  {:tags => ['移動'] ,:class_name => '移動'}  , '敏捷'){|ev|
    #      ev.conditions = []
    #      ev.display_name = '移動'
    #      ev.bonus_target = '移動'
    #    }

    Evaluator::create('移動（白兵距離）',{
        :tags => ['移動'] ,
        :action_name => '白兵距離戦闘行為',
        :class_name => '移動'
      } , '敏捷') {|ev|
      ev.conditions = [/白兵距離.*?での/o]
      ev.bonus_target = '移動'
    }

    Evaluator::create('移動（近距離）',{
        :tags => ['移動'] ,
        :action_name => '近距離戦闘行為',
        :class_name => '移動'
      },'敏捷') {|ev|
      ev.conditions = [/射撃戦として/o,/近距離.*?での/o]
      ev.bonus_target = '移動'
    }
    Evaluator::create('移動（中距離）',{
        :tags => ['移動'] ,
        :action_name => '中距離戦闘行為',
        :class_name => '移動'
      },'敏捷') {|ev|
      ev.conditions = [/射撃戦として/o,/中距離.*?での/o]
      ev.bonus_target = '移動'
    }

    Evaluator::create('水中白兵戦','水中白兵距離戦闘行為','体格','筋力') {|ev|
      ev.conditions = [/水中/o,/白兵距離.*?での$/o]
      ev.bonus_target = '攻撃'
    }

    Evaluator::create('防御（水中）',{
        :tags => ['防御'] ,
        :action_name => '水中白兵距離戦闘行為',
        :class_name => '防御'
      },'体格','耐久力') {|ev|
      ev.conditions = [/水中での/o]
      ev.bonus_target = '防御'
    }

    Evaluator::create('水中中距離戦',{
        :action_name => '水中中距離戦闘行為' ,
        :class_name => '水中中距離戦闘'},'感覚','知識'){|ev|
      ev.conditions = [/水中/o,/中距離.*?での$/o]
      ev.bonus_target = '攻撃'
    }

    Evaluator::create('水中遠距離戦',{
        :action_name => '水中遠距離戦闘行為',
        :class_name => '水中遠距離戦闘'
      },'敏捷','感覚'){|ev|
      ev.conditions = [/水中/o,/遠距離.*?での$/o]
      ev.bonus_target = '攻撃'
    }
    Evaluator::create('対ＲＢ戦闘',{
        :action_name => '対ＲＢ戦闘行為' ,
        :class_name => '対ＲＢ戦闘'},'対ＲＢ戦闘'){|ev| false } #特になし

    Evaluator::create('対空戦闘',{
        :action_name => '対空戦闘行為',
        :class_name => '対空戦闘'
      },'対空戦闘')    {|ev|
      ev.conditions = [/対空戦闘での/o,/射撃（.+）/o]
      ev.bonus_target = '攻撃'
    }

    Evaluator::create_optional('遠距離戦闘評価による対空戦闘',{
        :action_name => '対空戦闘行為',
        :class_name => '対空戦闘'
      },'敏捷','感覚')    {|ev|
      ev.conditions = [/対空戦闘での/o,/遠距離での/o]
      ev.bonus_target = '攻撃'
      ev.target_filters = [/対空戦闘判定に遠距離戦闘評価を使うことができる。/o]
      def ev.condition_match?(sp)
        super && sp.source !~ /属性（爆弾）/o
      end
      #      ev.bonus_filter = proc{|sp|
      #        self.condition_match?(sp) #&& !sp.separated_conditions.include?()'射撃（爆弾）')
      #      }
    }

    Evaluator::create('対潜水艦戦闘',{
        :action_name => '対潜水艦戦闘行為',
        :class_name => '対潜水艦戦闘'
      },'対潜水艦戦闘')    {|ev|
      ev.conditions = [/対潜水艦での/o]
      ev.bonus_target = '攻撃'
    } #現状、該当する補正は特にない。

    Evaluator::create('艦隊戦闘',{
        :action_name => '艦隊戦闘行為',
        :class_name => '艦隊戦闘'
      },'艦隊戦闘')    {|ev| ev.bonus_target = '攻撃' } #現状、該当する補正は特にない。

    Evaluator::create('対要塞戦闘',{
        :action_name => '対要塞戦闘行為',
        :class_name => '対要塞戦闘'
      },'対要塞戦闘')    {|ev|
      ev.conditions = [/対要塞戦闘での/o]
      ev.bonus_target = '攻撃'
    }

    Evaluator::create('要塞戦闘',{
        :action_name => '要塞戦闘行為',
        :class_name => '対要塞戦闘'
      },'対要塞戦闘')    {|ev|
      ev.conditions = [/対要塞戦闘での/o]
      ev.bonus_target = '攻撃'
    }

    Evaluator::create('詠唱','詠唱戦闘行為','器用','知識') {|ev|
      ev.conditions = [/詠唱での/o]
      ev.bonus_target = '攻撃'
    }

    Evaluator::create_optional('詠唱による防御（通常）',{
        :tags => ['防御'],
        :class_name => '防御'
      },'器用','知識') {|ev|

      ev.conditions = [/詠唱での/o]
      ev.bonus_target = '防御'
      ev.target_filters = [/(人|種族)カテゴリ/o]
    }

    Evaluator::create('糸による白兵戦','糸戦闘行為','器用') {|ev|
      ev.conditions = [/糸操作での/o]
      ev.bonus_target = '攻撃'
    }

    Evaluator::create('糸による近距離戦','糸戦闘行為','器用') {|ev|
      ev.conditions = [/糸操作での/o]
      ev.bonus_target = '攻撃'
    }

    Evaluator::create('糸操作','糸操作行為','器用') {|ev|
      ev.conditions = [/糸操作での/o]
      ev.bonus_target = '全判定'
    }

    Evaluator::create('流れを戻す','流れを戻す行為','器用','知識') {|ev|
      ev.conditions = [/詠唱での/o]
      ev.bonus_target = '流れを戻す判定'
      ev.target_filters = [/(人|種族)カテゴリ/o]
    }

    Evaluator::create('詠唱での探査','探査行為','器用','知識') {|ev|
      ev.conditions = [/詠唱での/o]
      ev.bonus_target = '探査'
      ev.target_filters = [/(人|種族)カテゴリ/o]
    }

    Evaluator::create('情報戦','情報戦闘行為','器用','知識'){|ev|
      ev.conditions = []
      ev.bonus_target = '情報戦闘'
      #        sp.is_ability_match?('情報戦闘') && sp.is_available?
    }

    Evaluator::create_optional('情報戦防御',{:class_name => '情報戦防御'},'情報戦防御'){|ev|
      ev.conditions = []
      ev.bonus_target = '情報戦闘'
    }

    Evaluator::create('ネットワーク維持',['ネットワーク維持行為','情報戦闘'],'器用','知識'){|ev|
      ev.conditions = [/ネットワーク維持での/o]
      ev.bonus_target = '情報戦闘'
      #      ev.bonus_filter = proc{|sp|
      #        (sp.is_ability_match?('情報戦闘') && sp.is_available?) ||
      #          (sp.is_available?([/ネットワーク維持での/o])&& sp.is_ability_match?('全判定'))
      #      }
    }

    Evaluator::create('ネットワーク追跡',['ネットワーク追跡行為','情報戦闘'],'器用','知識'){|ev|
      ev.conditions = [/ネットワーク追跡での/o]
      ev.bonus_target = '情報戦闘'
    }
    #    Evaluator::create('宇宙戦闘行為') {|ev| sp['行為名'] == '宇宙戦闘行為'}
    #    Evaluator::create('夜間戦闘行為') {|ev| sp['行為名'] == '夜間戦闘行為'}

    Evaluator::create('治療',nil,'器用','知識') {|ev|
      ev.class_name = '治療'
      ev.action_name = '治療行為'
      ev.bonus_target = '治療'
      #      ev.implicitly = true
      false
    }

    Evaluator['治療'].define_alternative('治療（医学）',[/医学での/])
    Evaluator['治療'].define_alternative('治療（奇跡）',[/医学での/,/奇跡による/])
    Evaluator['治療'].define_alternative('治療（詠唱）',[/医学での/,/詠唱での/])
    

    Evaluator::create('幸運' , {:tags => ['幸運'] ,:class_name => '幸運'} , '幸運'){|ev|
      ev.conditions = []
      ev.bonus_target = '幸運'
      ev.implicitly = true
    }

    Evaluator::create('治療での幸運',{
        :action_name => '治療行為' ,
        :class_name => '幸運' ,
        :ancestor => '幸運'
      },'幸運') {|ev|
      ev.conditions = [/治療時に幸運評価が求められた場合での/o]
      ev.bonus_target = '幸運'
    }

    Evaluator::create('ヘンな治療',{
        :action_name => 'ヘンな治療行為' ,
        :tags => ['ヘンな治療'] ,
        :class_name => '治療'},'外見','耐久力') {|ev|
      ev.conditions = [/医学での/o,/ヘンな治療での/o]
      ev.bonus_target = '治療'
    }

    Evaluator::create_optional('ヘンな治療・蛇神の僧侶のみ',{
        #        :action_name => 'ヘンな治療行為',
        :tags => ['ヘンな治療'] ,
        :class_name => '治療（合算）'},'外見','耐久力') {|ev|
      ev.conditions = [/医学での/o,/ヘンな治療での/o]
      ev.bonus_target = '治療'
      ev.display_name = 'ヘンな治療'
      ev.target_filters = [/蛇神の僧侶/o]
    }

    Evaluator::create_optional('ヘンじゃない治療',{
        #        :action_name => 'ヘンな治療行為',
        :tags => ['ヘンな治療'],
        :class_name => '治療（合算）'
      },'器用','知識') {|ev|
      ev.conditions = [/医学での/,/奇跡による/]
      ev.bonus_target = '治療'
      ev.target_exception_filters = [/蛇神の僧侶/o]
    }

    ExpressionEvaluator::create_optional('治療（合算）',{
        #       :action_name => 'ヘンな治療行為',
        :tags => ['ヘンな治療'],
        :class_name => '治療（合算）' }) {|ev|
      ev.expr = ILanguage::FCalc::Parser.parse('＜ヘンじゃない治療＞ ＆　＜ヘンな治療・蛇神の僧侶のみ＞').interpret
    }

    Evaluator::create('ヘンな治療による蘇生',{
        :action_name => 'ヘンな治療行為',
        :tags => ['ヘンな治療'] },'外見','耐久力') {|ev|
      ev.conditions = [/医学での/o,/ヘンな治療での/o,/蘇生に関する/o]
      ev.bonus_target = '治療'
    }

    Evaluator::create('調伏（外見）',{
        :action_name => '調伏行為',
        :tags => ['調伏'] },'外見') {|ev|
      ev.bonus_target = '調伏'
      ev.bonus_filter = proc{|sp|
        (sp.condition_match?(['奇跡による','蘇生判定に関する']) && sp.is_ability_match?('治療') ) ||
          condition_match?(sp)
      }
    }

    Evaluator::create('調伏（幸運）',{
        :action_name => '調伏行為',
        :tags => ['調伏'] },'幸運') {|ev|
      ev.bonus_target = '調伏'
      ev.bonus_filter = proc{|sp|
        (sp.condition_match?(['奇跡による','蘇生判定に関する']) && sp.is_ability_match?('治療') ) ||
          condition_match?(sp)
      }
    }

    Evaluator::create('整備','整備行為','器用','知識') {|ev|
      ev.conditions = []
      ev.bonus_target = '整備'
    }
    
    Evaluator::create_optional('遠隔整備（ＡＲ１距離）','整備行為','器用','知識') {|ev|
      ev.conditions = [/^ＡＲ１距離での$/o]
      ev.bonus_target = '整備'
      ev.target_filters = [/遠隔動作補助システム/o]
    }
    
    Evaluator::create_optional('遠隔整備（ＡＲ２距離）','整備行為','器用','知識') {|ev|
      ev.conditions = [/^ＡＲ２距離での$/o]
      ev.bonus_target = '整備'
      ev.target_filters = [/遠隔動作補助システム/o]
    }
    
    Evaluator::create_optional('遠隔整備（ＡＲ３距離）','整備行為','器用','知識') {|ev|
      ev.conditions = [/^ＡＲ３距離での$/o]
      ev.bonus_target = '整備'
      ev.target_filters = [/遠隔動作補助システム/o]
    }
    
    Evaluator::create('オペレート','オペレート行為','外見','感覚'){|ev|
      ev.conditions = []
      ev.bonus_target = 'オペレート'
    }
    Evaluator::create_standard('侵入（敏捷・器用）',{
        :action_name => '侵入行為',
        :tags => ['侵入行為'] ,
        :class_name => '侵入'
      },'敏捷','器用') {|ev|
      ev.conditions = []
      ev.bonus_target = '侵入'
    }
    Evaluator::create_standard('侵入（幸運）',{
        :action_name => '侵入行為',
        :tags => ['侵入行為'] ,
        :class_name => '侵入'
      },'幸運') {|ev|
      ev.conditions = [/幸運(による|での)/o]
      ev.bonus_target = '侵入'
    }

    Evaluator::create_optional('侵入（技術＆特殊使用）',{
        :action_name => '侵入行為',
        :tags => ['侵入行為'] ,
        :class_name => '侵入'
      },'外見') {|ev|
      ev.conditions = [/幸運(による|での)/o , /変装時/o]
      ev.bonus_target = '侵入'
    }

    Evaluator::create_optional('追跡自動成功',{
        :action_name => '追跡行為',
        :tags => ['追跡行為'] ,
        :class_name => '追跡' ,
      },'幸運') {|ev|
      ev.display_name = '追跡'
      ev.bonus_target = '追跡'
    }

    Evaluator::create_standard('追跡（感覚）',{
        :action_name => '追跡行為',
        :tags => ['追跡行為'] ,
        :class_name => '追跡'
      },'感覚'){|ev|
      ev.conditions = [/嗅覚での/o]
      ev.bonus_target = '追跡'
    }
    Evaluator::create_standard('追跡（幸運）',{
        :action_name => '追跡行為',
        :tags => ['追跡行為'] ,
        :class_name => '追跡'
      },'幸運') {|ev|
      ev.conditions = [/嗅覚での/o]
      ev.bonus_target = '追跡'
    }

    Evaluator::create_standard('偵察','偵察行為','感覚'){|ev|
      ev.conditions = [/偵察(時)?での/o]
      ev.bonus_target = '感覚'
    }
    Evaluator::create_standard('隠蔽','隠蔽行為','幸運'){|ev|
      ev.conditions = []
      ev.bonus_target = '隠蔽'
    }
    Evaluator::create('隠蔽看破',nil,'幸運') {|ev|
      ev.conditions = [/隠蔽を看破/o]
      ev.bonus_target = '全判定'
      #      sp['補正使用条件'] =~ /隠蔽を看破/ && sp.is_ability_match?('隠蔽看破') #実際に補正対象に”隠蔽看破”と記述された特殊は存在しない。（”全判定”のみ合致）
    }
    Evaluator::create_standard('陣地構築（筋力）',['陣地構築行為','陣地構築'],'筋力') {|ev|
      ev.conditions = [/陣地構築作業での/o]
      ev.bonus_target = '筋力'
    }

    Evaluator::create_standard('陣地構築（知識）',['陣地構築行為','陣地構築'],'知識') {|ev|
      ev.conditions = [/陣地構築作業での/o]
      ev.bonus_target = '知識'
    }

    Evaluator::create('予知夢','予知夢行為','幸運'){|ev|
      ev.conditions = [/（予知夢での）/o]
      ev.bonus_target = '全判定'
    }

    Evaluator::create('魅力',{:class_name => '魅力'},'外見','知識') {|ev|
      ev.conditions = []
      ev.bonus_target = '魅力'
      ev.get_off_only = true
    }
    #    cha.actor_filter = Proc.new{|unit| !(unit.class <= Vehicle)}

    Evaluator::create_standard('歌唱','歌唱行為','外見','知識') {|ev|
      ev.conditions = []
      ev.bonus_target = '歌唱'
    }
    Evaluator::create('外交戦',nil,'外見') {|ev|
      ev.conditions = []
      ev.bonus_target = '外交戦'
      ev.target_filters = [/(外交戦|交渉)能力/o]
      ev.target_set_type = :characters
      ev.get_off_only = true
    }

    IndividualyEvaluator::create('外交戦（個別）',nil,'外見') {|ev|
      ev.conditions = []
      ev.bonus_target = '外交戦'
      ev.target_filters = [/外交戦能力/o]
      ev.target_set_type = :characters
      ev.get_off_only = true
    }

    Evaluator::create('交渉',nil,'外見') {|ev|
      ev.conditions = []
      ev.bonus_target = '交渉'
      ev.target_set_type = :characters
      ev.get_off_only = true
    }

    IndividualyEvaluator::create('ドラゴンシンパシー',nil,'感覚') {|ev|
      ev.conditions = []
      ev.bonus_target = '感覚'
      ev.target_set_type = :characters
      ev.target_filters = [/遠隔地にある龍の声を送り、あるいは龍の声を聞くことができる。/o]
      ev.is_include_evaluate_items = true
      ev.is_parent_eval_enabled = true
      #      ev.disabled_sps = [/教導パイロットの指導の特殊補正/o]
    }

    Evaluator::create('降下',nil,'耐久','敏捷') {|ev|
      ev.conditions = []
      ev.bonus_target = '降下'
    }
    Evaluator::create('治安維持',nil,'治安維持') {|ev|
      ev.conditions = [/（治安維持での）/o]
      ev.bonus_target = '全判定'
      ev.whole_condition = true
    }

    Evaluator::create('災害対応活動',nil,'災害対応活動') {|ev|
      ev.conditions = [/（災害対応活動での）/o]
      ev.bonus_target = '全判定'
      ev.whole_condition = true
    }
    #    Evaluator::create('対潜水艦戦闘','対潜水艦戦闘行為','対潜水艦戦闘') {|ev| sp['補正使用条件'] == '（対潜水艦戦闘での）'}
    #    Evaluator::create('艦隊戦闘','艦隊戦闘行為','艦隊戦闘') {|ev| sp['補正使用条件'] == '（艦隊戦闘での）'}

    Evaluator::create('夢の中での攻撃','夢の中での攻撃行為','敏捷','外見') {|ev|
      ev.conditions = [/（夢の中での）/o]
      ev.bonus_target = '攻撃'
      ev.whole_condition = true
    }

    Evaluator::create('目利き','目利き行為','感覚','知識') {|ev|
      ev.conditions = [/目利きでの/o]
      ev.bonus_target = '全判定'
      ev.target_set_type = :characters
      ev.get_off_only = true
    }

    Evaluator::create('商談','商談行為','外見') {|ev|
      ev.conditions = [/商談での/o]
      ev.bonus_target = '全判定'
      ev.target_set_type = :characters
      ev.get_off_only = true
    }

    Evaluator::create('犯罪捜査','犯罪捜査行為','感覚','知識') {|ev|
      ev.conditions = []
      ev.bonus_target = '犯罪捜査'
      ev.target_set_type = :characters
      ev.get_off_only = true
    }

    Evaluator::create_optional('魔法知識',nil,'知識') {|ev|
      ev.conditions = []
      ev.bonus_target = '知識'
      ev.target_set_type = :characters
      ev.target_filters = [/受けることによって魔法の知識を得る（知識判定を行うことができる）。/o]
      ev.get_off_only = true
    }

    Evaluator::create_optional('対空銃手の対空戦闘','対空戦闘行為','耐久力','感覚') {|ev|
      ev.conditions = [/対空戦闘での/o]
      ev.bonus_target = '攻撃'
      ev.target_filters = [/＊対空銃手の対空戦闘行為/o]
    }

    Evaluator::create_optional('三角とびによる対空戦闘','白兵距離戦闘行為','体格','筋力') {|ev|
      ev.conditions = [/白兵距離での/o]
      ev.bonus_target = '攻撃'
      ev.target_filters = [/対空戦闘判定で白兵戦評価を使うことができる。/o]
    }

    Evaluator::create_optional('鎮静',nil,'器用') {|ev|
      ev.conditions = [/＜調和者の鎮静能力＞での/o]
      ev.bonus_target = '器用'
      ev.target_filters = [/＊調和者の鎮静補正/o]
    }

    IndividualyEvaluator::create_optional('召喚',nil,'知識','幸運') {|ev|
      ev.conditions = []
      ev.bonus_target = '召喚'
      ev.target_filters = [/(精霊|式神|悪魔)召喚能力/o]
      ev.target_set_type = :characters
      ev.get_off_only = true
    }
    
    IndividualyEvaluator::create_optional('メンチ',nil,'外見') {|ev|
      ev.conditions = []
      ev.bonus_target = 'メンチ'
      ev.target_filters = [/メンチを切ることができる。/o]
      ev.target_set_type = :characters
      ev.get_off_only = true
    }

    IndividualyEvaluator::create_optional('植物操作判定',nil,'知識') {|ev|
      ev.conditions = []
      ev.bonus_target = '植物操作判定'
      ev.target_filters = [/植物操作能力/o]
      ev.target_set_type = :characters
      ev.get_off_only = true
    }

    IndividualyEvaluator::create_optional('サイコキネシス',nil,'筋力') {|ev|
      ev.conditions = []
      ev.bonus_target = 'サイコキネシス'
      ev.target_filters = [/離れたものを動かすことができる。/o]
      ev.target_set_type = :characters
      ev.get_off_only = true
    }

    IndividualyEvaluator::create_optional('Ｉ＝Ｄデザイナーの解析',nil,'筋力') {|ev|
      ev.conditions = []
      ev.bonus_target = '解析'
      ev.target_filters = [/解析として写真などから敵性能を類推できる。/o]
      ev.target_set_type = :characters
      ev.get_off_only = true
    }

    IndividualyEvaluator::create_optional('動物操作判定',nil,'外見','知識') {|ev|
      ev.conditions = []
      ev.bonus_target = '動物操作判定'
      ev.target_filters = [/動物操作能力/o]
      ev.target_set_type = :characters
      ev.get_off_only = true
    }

    Evaluator::create_optional('歌唱（歌魔法）','歌唱行為','外見','知識') {|ev|
      ev.conditions = []
      ev.bonus_target = '歌唱'
      ev.target_filters = [/＊音楽家/o]
    }

    Evaluator::create('夜間戦闘',"夜間戦闘行為" , '') {|ev|
      ev.conditions = [/（夜間戦闘での）/o]
      ev.bonus_target = '全判定'
      ev.whole_condition = true
    }

    SynchronizeEvaluator::create('同調',{:class_name => '同調'},'外見') {|ev|
      ev.conditions = []
      ev.bonus_target = '同調'
      ev.is_include_evaluate_items = false
    }

    module EvaluatorForContainerUnitOnly
      def target_set(troop)
        troop.children.select{|c|c.class <= ContainerUnit}
      end
    end

    module EvaluatorForPilotsOnly
      def is_branch?(unit)
        unit.class != Soldier
      end
    end

    sync_2 = IndividualyEvaluator::create('同調（機内）',{:class_name => '同調'},'外見') {|ev|
      ev.conditions = []
      ev.display_name = '同調'
      ev.bonus_target = '同調'
      ev.conditions = [/機体内での/o]
      ev.target_set_type = :characters
      ev.is_include_evaluate_items = true
      ev.extend EvaluatorForPilotsOnly
      ev.branch_calculation_class = SynchronizeCalculation
      ev.leaf_calculation_class = LeaderCalculation
      ev.is_parent_eval_enabled = true

      def ev.do_target?(unit)
        result = super
        result && if unit.parent.are_all_members_npc?
          true
        else
          unit.class == Soldier && unit.is_qualified_as_leader?
        end
      end

    }

    #    ctrl = ControlEvaluator::create('操縦',{
    #        :action_name => nil ,
    #        :class_name => '操縦'},'感覚','器用') {|ev|
    #      ev.conditions = []
    #      ev.bonus_target = '操縦'
    #      ev.is_include_evaluate_items = true
    #    }


    #    ctrl_i = ControlEvaluator::create_optional('操縦（個別）',{
    ctrl_i = Evaluator::create_optional('操縦（個別）',{
        :action_name => nil ,
        :class_name => '操縦'
      },'感覚','器用') {|ev|
      ev.conditions = []
      ev.bonus_target = '操縦'
      ev.display_name = '操縦'
      ev.target_set_type = :characters
      #      ev.target_filters = [/乗り物カテゴリ/o]
      ev.is_include_evaluate_items = true
      ev.extend EvaluatorForPilotsOnly
      #      ev.is_parent_eval_enabled = false
      ev.is_parent_eval_enabled = true
    }

    
    def ctrl_i.choice_alternative(unit)
      if unit.class <= Vehicle
        #        unit.crew_division.calculate(@alternatives[0])
        @alternatives[0].calculate(unit)
      else
        super(unit)
      end
    end

    IndividualyEvaluator::create_optional('操縦',{
        :action_name => nil ,
        :class_name => '操縦'},'感覚','器用') {|ev|
      ev.conditions = []
      ev.bonus_target = '操縦'
      ev.target_filters = [/乗り物カテゴリ/o]
    }

    Evaluator::create_optional('操縦（艦隊）',{
        :action_name => nil ,
        :class_name => '操縦'},'感覚','器用') {|ev|
      ev.conditions = []
      ev.bonus_target = '操縦'
      ev.target_set_type = :characters
      ev.is_include_evaluate_items = true
      ev.is_parent_bonus_enabled = false
    }

    sync_i = IndividualyEvaluator::create_optional('同調（個別）',{
        :action_name => nil ,
        :class_name => '同調'
      },'外見') {|ev|
      ev.conditions = [/機体内での/]
      ev.bonus_target = '同調'
      ev.target_filters = [/乗り物カテゴリ/o]
      def ev.target_set(unit)
        super.select{|child| 
          child.children.size > 1
        }
      end
      ev.is_include_evaluate_items = true
      ev.chained_evaluators = [Evaluator['同調（機内）']]
      ev.is_parent_eval_enabled = false
    }

    sync_i.alternatives = [sync_2]

    def sync_i.choice_alternative(unit)
      if unit.class <= Vehicle
        @alternatives[0].calculate(unit)
      else
        super(unit)
      end
    end

    ac_order = 0
    for sym in RegisteredActionNames
      ActionNameOrder[sym] = ac_order
      ac_order += 1
    end
  end
end

