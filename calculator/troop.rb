# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator

    class Troop < Unit
      attr :soldiers , true
      attr :divisions , false
      attr :formation , false
      attr :particular_evaluators , false
#      attr :abilities_evaluators , false

      include Team

      def initialize(context)
        super
        @divisions = []

        class <<@divisions
          attr :context,true
        end
        @divisions.context = context
        def @divisions.<<(unit)
          super
          context.register(unit)
        end

        @formation = nil
        @particular_evaluators = []
        @abilities_evaluators = []
        
        @unit_type = Set.new(['歩兵','搭乗'])
        @cost_required = false
      end

      def all_divisions
        [self] + @divisions
      end

      def all_division_names
        @all_division_names ||= self.all_divisions.collect{|div|div.name}
      end

      def all_flexible_bonus(situation_name = self.context.current_situation_name)
        unless current_situation.all_flexible_bonus_cache
          result = {}
          sit_ary = self.context.situation_stack.collect{|s_name| self.situations[s_name]}
          for situation in sit_ary
            result.update(situation.flexible_bonus)
          end
          result.update(self.context.all_flexible_bonus(situation_name))
          current_situation.all_flexible_bonus_cache = result
        end
        current_situation.all_flexible_bonus_cache
      end

      def all_situation_names
        @all_situation_names ||= self.context.situation_names | self.situation_names
      end
      
      #      def is_unit_type_match?(sp)
      #        true
      #      end

      def reset_evals
        super
        children.each{|c|c.reset_evals}
      end


      def act_restrictions
        update_situation
        current_situation.general_cache[:act_restrictions] ||= begin
          result = super.dup
        #歌唱行為・陣地構築（知識）の例外：乗り物が含まれる編成では評価を出せない。
          result |= [:'歌唱行為'] if children.any?{|c| c.class <= Vehicle}
        #質疑（http://cwtg.jp/qabbs/bbs2.cgi?action=article&id=10320 ）に基づく行為禁則
          result |= [:'侵入行為',:'隠蔽行為'] if children.any?{|c|
            c.class <= ContainerUnit && !c.is_regarded_as_soldier?
          }
          result |= [:'陣地構築行為'] if children.any? {|c|
            c.class <= ContainerUnit && !c.sp_include?(/陣地構築補正/)
          }
          result
        end
      end

      def action_names
        result = super

        for action in CommonActions
          ev = @context.find_evaluator_by_action_name(action)[0]
          if children.any?{|c|
              keys = c.evals_keys
              ev.args.all?{|arg| keys.include?(arg)}
            }
            result << action unless result.include?(action)
          end
        end
        
        restriction = self.act_restrictions
        result.delete_if{|item| 
          restriction.include?(item.intern)
        }
        result
      end

      def all_action_names
        result = self.action_names.dup
        for s_name in @context.particular_situation_names
          self.context.push_particular_situation(s_name)
          acs = self.action_names
          for action in acs
            result << "（#{s_name}）" + action unless result.include?(action)
          end
          self.context.pop_particular_situation
        end
        result
      end

      def no_sp?(evaluator)
        update_situation
        current_situation.no_sp_cache[evaluator] ||= begin
          any_bonus = all_bonuses_cache.find{|sp|
            evaluator.match?(sp) && is_tqr_qualified?(sp,evaluator)
          }
          any_bonus.nil?
        end
      end

      def includes_attendant?
        update_situation
        current_situation.general_cache[:includes_attendant?] ||= self.children.any?{|c|c.is_attendant?}
      end

      def calculate(evaluator)
        update_situation
        current_situation.calculation_cache[evaluator] ||= begin
          #一般評価は関連する補正がない場合、部隊の一般評価から算出するだけで済ませる。
          result = if evaluator.is_standard && (no_sp?(evaluator) && !self.includes_attendant?)
            calc = Calculation.new(self,evaluator)
            for arg_name in calc.arg_names
              sub_calc = Calculation.new(self,evaluator)
              sub_calc.base_value = evaluate(arg_name)
              sub_calc.arg_name = arg_name
              sub_calc.operands = self.ability_bonuses.select{|sp|evaluator.match?(sp) && sp.is_ability_match?(arg_name)}
              calc.arguments[arg_name] = sub_calc
            end
            calc
          else
            super
          end
          result.use if result
          result
        end
      end

      def event_cost(with_modification = false , &block)
        result = Hash.new{|h , k | h[k] = 0}
        for child in children + band
          cost = child.event_cost(with_modification , &block)
          cost.each_pair{|k,v| result[k] += v}
        end
        result
      end

      def hops
        @hops ||= begin
          hops = children.collect{|c|c.hops}
          hops.include?(nil) ? nil : hops.min
        end
      end
      
      def idresses=(val)
        @idresses = val
      end

      def idresses
        @idresses ||= []
      end

      def actions
        result = {}
        self.action_names.each{|n|result[n]=[]}
        return result
      end

      def reflesh
        super
      end

      def self_evaluators
        result = super.dup
        for opt_evs in @@formation_options
          result = opt_evs.modify(self,result)
        end
        result
      end

      def disable_evaluator_for_no_optional(evs)
        for ev in self.context.no_optional_evaluators
          ev.particular_situation(self){
            unless self.action_names.include?(ev.action_name)
              evs.delete(ev)
            end
          }
        end
      end

      def evaluators
        update_situation
        current_situation.evaluators ||= begin
          if self.children.size == 0
            current_situation.general_cache[:troop_evaluator_ids] = []
            current_situation.evaluators = []
          else
            result = self_evaluators
            result |= @context.particular_evaluators.select{|ev| ev.is_available?(self)}
            disable_evaluator_for_no_optional(result)
            
            i = 1
            troop_evaluators =  result.sort_by{|ev|[(ev.order || 999999) , i+=1]}
            current_situation.general_cache[:troop_evaluator_ids] = troop_evaluators.collect{|ev|ev.object_id}
            #            @troop_evaluators = result.sort_by{|ev|[(ev.order || 999999) , i+=1]}
            #            @troop_evaluator_ids = @troop_evaluators.collect{|ev|ev.object_id}
            current_situation.evaluators = troop_evaluators
          end
        end
        #        current_situation.evaluators
      end

      def expr
        self.context.statements.reverse.find{|st|not st.class <= ILanguage::FCalc::LamdaFunctor}
      end

      def expression_evals_str
        result = begin
          expr.evaluate(self)
        rescue
          'N/A'
        end
        if expr
          "#{expr.source} = #{result}"
        else
          nil
        end
      end

      def ar_str
        "ＡＲ："+self.ar.to_s + "\n"
      end

      def result_str
        result = self.ar_str + "\n"
        result << self.evals_str + "\n" + self.common_abilities_evals_str + self.action_evals_str + "\n" + self.leaders_str + "\n"
        result <<  self.fleet_evals_str if self.formation == :fleet
        result
      end

      def all_result_str
        result = ''
        for div in [self] + self.divisions
          for s_name in div.all_situation_names
            div.context.change_situation_to(s_name,false)
            div.change_situation_to(s_name)
            result << div.name + ' ' + s_name + "\n" + div.result_str
          end
        end
        self.context.change_to_default_situation(self)
        self.change_to_default_situation(self)
        result + troop.cost_result_str
      end

      def cost_result_str
        self.event_cost_str + self.sp_cost_str + self.organization_cost_str
      end
      
      def evaluator_include?(ev)
        evaluators if current_situation.general_cache[:troop_evaluator_ids].nil?
        current_situation.general_cache[:troop_evaluator_ids].include?(ev.object_id)
      end
      
      def primary_synchronizer(ev)
        update_situation
        current_situation.calculation_cache[ev] ||= super(ev)
      end
      
      def is_cost_required?
        @is_cost_required ||= self.variable('コスト計上分隊').include?(self.name)
      end

      def is_sp_used?(sp)
        swapped_situation.used_sp_cache.include?(sp.name.intern)
      end

      def find_evaluator(name)
        evaluators.find{|ev|ev.name == name}
      end

      def variable(name)
        var = super
        return var if var

        var = self.context.variable(name)
        return var if var
        
        if ev = find_evaluator(name)
          eva = EvaluatorAdapter.new(self,name)
          self.variables[name]=eva
          return eva
        end

        if child = children.find{|c|c.name == name}
          return child
        end
        return nil
      end

      def characters
        self.children.collect{|c|
          case c
          when Soldier
            c
          when Vehicle , Vessel
            c.children
          end
        }.flatten
      end

      def getted_off_characters
        self.children.collect{|c|
          case c
          when Soldier
            c
          when Vehicle , Vessel
            c.crew_division_getted_off.children
          end
        }.flatten
      end

      def Troop.add_optional_rule(option_command)
        @@formation_options << option_command
      end

      def Troop.add_common_evaluator(name)
        EvaluatorOption.add(:any? , name){|c|
          ev = Evaluator[name]
          bonuses = c.all_bonuses
          bonuses += c.children.collect{|c|c.all_bonuses}.flatten if c.class <= Vehicle
          !bonuses.find{|sp| ev.bonus_filter.call(sp) }.nil?
        }
      end

      def evals_items
        result = self.evaluations.select{|ev| ev.keys.size > 0}
        result += EvalData.create('部隊補正',@personal_bonuses) if @personal_bonuses
        result
      end


      def organization_type
        @organization_type ||= begin
          var = self.context.variable('編成種別')

          if var.nil?
            if self.troop.formation == :fleet ||  self.troop.children.all?{|unit|unit.is_ace?}
              var = self.context.variables['編成種別'] = '軽編成'
            else
              var = self.context.variables['編成種別'] = '重編成'
            end
          end
          var
        end
      end

      def organization_coefficient
        @oe ||= begin
          var = self.organization_type
          (var.nil? || var == '重編成') ? 3 : 1
        end
      end

      def change_formation_to(form)
        case form
        when :fleet , :cavalry
          if @formation.nil? || @formation == form
            extend Troop.formations[form]
            @formation = form
          else
            raise '艦船編成に艦船以外のユニットは混在できません。'
          end
        when :troop
          @formation = form
        end
      end

      def event_cost_str(&proc)
        e_cost = self.event_cost(&proc).dup

        "出撃費用\n" + format_costs(e_cost)
      end

      def sp_cost_str(&proc)
        "特殊消費\n" + format_costs(self.total_cost(&proc))
      end

      def organization_cost_hash
        cost = {}
        for key in %w[資源 食料 燃料]
          cost[key] = self.organization_cost(key)
        end
        for key in %w[生物資源 資金]
          val = self.organization_cost(key)
          cost[key] = val if val.to_f > 0
        end
        cost
      end
      
      def organization_cost_str
        result = "合計消費\n"
        result +   format_costs( organization_cost_hash() )
      end

      def format_costs(costs)
        result = ''
        keys = %w[資源 食料 燃料]
        keys << '生物資源' if costs.has_key?('生物資源')
        for key in keys
          result += sprintf("%s：%g万ｔ\n" , key , (costs[key] || 0).abs)
        end
        if costs.has_key?('資金')
          result += sprintf("%s：%d億\n" , '資金' , (costs['資金'] || 0).abs)
        end
        result
      end

      def collect_all_sp_for_view
        update_situation
        current_situation.general_cache[:collect_all_sp_for_view] ||= begin
          super.inject([]){|distinct , sp| 
            distinct << sp unless distinct.find{|inserted| inserted.name == sp.name}
            distinct
          }
        end
      end
      
      def all_resource_modifications
        update_situation
        current_situation.general_cache[:all_resource_modifications] ||= begin
          self.collect_all_sp.select{|sp|sp.spec_type == '消費修正'}
        end
      end
      
      def resource_modification_details
        update_situation
        current_situation.general_cache[:all_resource_modifications] ||= begin
          self.collect_all_sp.select{|sp|sp.spec_type == '消費修正'}
        end
      end

      @@formations = {}
      def Troop.register_formation(symbol , form_module)
        @@formations[symbol] = form_module
      end

      def Troop.formations
        @@formations
      end

      def make_result(targets = [:ar , :evals , :action_evals , :leaders , :fleet , :costs] , hash = nil )
        hash = if hash.nil? || targets.size >= 6
          Hash.new{|h,k|
                h[k] = Hash.new{|h2,k2|
                  h2[k2] = {}
                }
          }
        end

        for div in [self] + self.divisions
          for s_name in div.all_situation_names
            div.context.change_situation_to(s_name,false)
            div.change_situation_to(s_name)

            hash[div.name][s_name][:ar] = div.ar_str if targets.include?(:ar)
            e_str = div.evals_str
            hash[div.name][s_name][:evals] = e_str if targets.include?(:evals)
            actions_str = div.common_abilities_evals_str + div.action_evals_str
            hash[div.name][s_name][:action_evals] = actions_str if targets.include?(:action_evals)
            leaders_str =  div.leaders_str
            hash[div.name][s_name][:leaders] = leaders_str if targets.include?(:leaders)
            fleet_str = ''
            if div.formation == :fleet
              fleet_str = div.fleet_evals_str
              hash[div.name][s_name][:fleet] = fleet_str if targets.include?(:fleet)
            end
          end
        end


        self.context.change_to_default_situation(self)
        self.change_to_default_situation(self)
        hash['本隊']['（基本）'][:costs] = self.cost_result_str  if targets.include?(:costs)

        hash
      end

      def evals_hash
        StandardAbilities.inject({}) do |hash ,key|
          hash[key] = self.evaluate(key)
          hash
        end
      end

      def action_evals_hash
        self.evaluators.select{|ev|
          !self.all_containments.include?(ev.name) && (ev.is_individualy? || !ev.is_omitted?(self))
        }.inject({}) do |hash , ev|
          hash.update(ev.hash_for_diff(self)) 
          hash
        end
      end
      
      def leader_evals_hash
        self.context.evaluators['同調'].hash_for_diff(self)
      end
      
      CostKeysOrder = ['出撃費用' , '特殊消費' , '合計消費'].collect{|a| (['資源' , '燃料' , '食料']).collect{|b|"#{a}/#{b}" } }.flatten
      def costs_hash
        result = CostKeysOrder.inject({}){|hash , key| hash[key] = 0 ; hash}

        self.event_cost.each_pair{|key , val | result["出撃費用/#{key}"]=val.abs}
        self.total_cost.each_pair{|key , val | result["特殊消費/#{key}"]=val.abs}
        self.organization_cost_hash.each_pair{|key , val | result["合計消費/#{key}"]=val.abs}
        result
      end
      
      def hash_for_diff
        result = evals_hash.merge(action_evals_hash)
#        temp =  leader_evals_hash
        result.merge!(leader_evals_hash)
        result.merge!(costs_hash) if self.class == Troop && self.current_situation_name == '（基本）'
        result
      end

      def hash_for_api
        {'divisions' =>  self.all_divisions.collect{|div|
             {'name' => div.name ,
              'evaluations' => div.all_evals_tuples ,
              'members' => div.children.collect{|c| {'name' => c.name}} ,
              'tqrs' => (div.all_action_names + div.non_action_bonus_tqrs).collect{|tqr|tqr.to_s} ,
              'idresses' => self.context.definitions.keys.select{|name| name !~ /^(S?U?HQ|Ｓ?Ｕ?ＨＱ)/} ,
              'command_resources' => self.context.converted_command_resources
             }
          }
        }
      end
      
      def tqrs
        update_situation
        current_situation.tqrs ||= begin
          result = super
          action_sps = self.collect_all_sp.reject{|sp|sp.spec_type == '補正'}
          action_sps.each{|sp|
            pr = sp.owner.parent
            sp.use if result.include?(sp.tqr_id)  && self.is_cost_required?
          }
          result
        end
      end

      def is_convertible?
        @is_convertible ||= self.children.size == 1
      end

      private
      def evals=(hash)
        @evals = hash
      end
    end
  end
end
