# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator
#    module Definitions
    class Definitions
      def self.set_activerecord(table)
        @@table = table
#        @@yaml_db = {} if @@table.class <= ActiveRecord
      end

      def self.activerecord_enabled?
        !@@table.nil?
      end
      
      def self.[](name,rev=nil)
        @@table.find_by_name_and_revision(name , rev)
      end


      def set_hq
#        self['ＨＱ']={'評価' => StandardAbilities.inject({}){|hash,c|hash[c]=1 ; hash} ,'特殊定義' => {},'Ｌ：名' => 'ＨＱ'}
#        self['ＳＨＱ']={'評価' => StandardAbilities.inject({}){|hash,c|hash[c]=2 ; hash} ,'特殊定義' => {},'Ｌ：名' => 'ＳＨＱ'}
        self['HQ']={'評価' =>{'全能力' => 1} ,'特殊定義' => {},'Ｌ：名' => 'ＨＱ'}
        self['ＨＱ']={'評価' =>{'全能力' => 1} ,'特殊定義' => {},'Ｌ：名' => 'ＨＱ'}
        self['SHQ']={'評価' => {'全能力' => 2} ,'特殊定義' => {},'Ｌ：名' => 'ＳＨＱ'}
        self['ＳＨＱ']={'評価' => {'全能力' => 2} ,'特殊定義' => {},'Ｌ：名' => 'ＳＨＱ'}
        self['UHQ']={'評価' => {'全能力' => 3} ,'特殊定義' => {},'Ｌ：名' => 'ＵＨＱ'}
        self['ＵＨＱ']={'評価' => {'全能力' => 3} ,'特殊定義' => {},'Ｌ：名' => 'ＵＨＱ'}
        for col in StandardAbilities + %w[装甲 対空戦闘 対潜水艦戦闘 遠距離戦闘 対要塞戦闘]
          hqname = 'ＨＱ'+col
          self[hqname] = {'評価' => {col => 1},'特殊定義' => {} , 'Ｌ：名' => hqname}
          hqname2 = 'HQ'+col
          self[hqname2] = {'評価' => {col => 1},'特殊定義' => {} , 'Ｌ：名' => hqname}
          shqname = 'ＳＨＱ'+col
          self[shqname] = {'評価' => {col => 2},'特殊定義' => {}, 'Ｌ：名' => shqname}
          shqname2 = 'SHQ'+col
          self[shqname2] = {'評価' => {col => 2},'特殊定義' => {}, 'Ｌ：名' => shqname}
          uhqname = 'ＵＨＱ'+col
          self[uhqname] = {'評価' => {col => 3},'特殊定義' => {}, 'Ｌ：名' => uhqname}
          uhqname2 = 'UHQ'+col
          self[uhqname2] = {'評価' => {col => 3},'特殊定義' => {}, 'Ｌ：名' => uhqname}
        end
      end

      attr :warnings , false
      attr_accessor :revision
      
      def initialize(options)
        db_path = File.join(File.dirname(__FILE__),'../parser/idefs.yml')
        @yaml_db = {}
        if options[:activerecord]
          Definitions.set_activerecord(options[:activerecord])
        elsif File.exist?(db_path)
          @yaml_db = YAML.load(open(db_path,'r'))
        end
        set_hq()
        self.reflesh

        @revision = options[:revision]
      end

      def reflesh
        @not_founds_hash = {}
        @warnings = {}
      end

      def not_founds
        @not_founds_hash.keys
      end

      def [](name)
        if Definitions.activerecord_enabled?
          rec = if @yaml_db.has_key?(name)
            @yaml_db[name]
          else
            ((record = Definitions[name,@revision]) && record.compiled_hash)
          end
          @yaml_db[name] = rec
        else
          rec = @yaml_db[name]
        end
        if record && record.warnings
          @warnings[name] = YAML.load(record[:warnings])
        end
        if rec.nil?
          @not_founds_hash[name]=name
        end
        return rec
      end
      def []=(name,value)
        @yaml_db[name]=value
      end

      def keys
        @yaml_db.keys
      end

      def self.library(name)
        hash = self[name]
        Library.new(hash)
      end
    end
  end
end