# -*- encoding: utf-8 -*-

module ILanguage
  module Calculator
    module Convertible

      def troop_hash
        {} #must be overridden
      end

      def troop
        nil #must be overridden
      end

      def owner_name

        unit = self.troop.children.first
        if unit.character_no && unit.character_no[-2..-1] =~ /\d+/  && unit.is_ace?
          unit.idresses.first + '（ＡＣＥ）'
        else
          unit.name
        end
      end

      def parse_command_resources
        result = []

        hash = Hash[*troop_hash['divisions'].first['evaluations'].flatten(1)]
        name = self.owner_name

        %w"追跡 装甲 陣地構築 侵入 同調".each { |key| select_max_value(hash, key) }

        select_max_value(hash, '治療', /治療$/)


        for key in hash.keys
          result << {
              name: "#{name}の#{key}",
              power: hash[key],
              tag: key
          }
        end

        command_resources = troop_hash['divisions'].first['command_resources']
        result += command_resources if command_resources

        return result
      end

      private
      def select_max_value(hash, keyword, re = nil)
        re = /^#{keyword}.*/ unless re
        keys = hash.keys.select { |key| re =~ key }
        max_value = keys.collect { |key| hash[key] }.max_by { |val| val.class == String ? 99999 : val }
        keys.each { |k| hash.delete(k) }
        hash[keyword] = max_value if max_value
        max_value
      end
    end
  end
end
