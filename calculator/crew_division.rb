# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator
    class CrewDivision < Division

      def initialize(context)
        super
        @unit_type = Set.new(['搭乗','歩兵'])
      end
      
      def children
        unless @div_children
          @div_children = super
          for c in @div_children
            c.unit_type << '搭乗' unless c.unit_type.include?('搭乗')
#            c.unit_type.delete('歩兵')
            c.unit_type.delete('歩兵武装')

            cache = c.all_partial_conditions | @context.units[c.name].troop_member.all_partial_conditions
            c.current_situation.all_partial_conditions_cache = cache
          end
        end
        @div_children
      end

    end

    module CrewDivisionChild
      def all_partial_conditions
        update_situation
        current_situation.all_partial_conditions_cache ||= begin
          #乗員分隊の乗員についてpartial_conditionsを取得する際は、本隊における乗員ユニットのそれと和集合を取る。
          #これは、CrewDivision においては乗員分隊のchildren直下に乗員ユニットが来ることで、ContainerUnit#default_partial_conditions を経由できず、
          #搭乗条件が正しく解釈されない問題を解決するため。
          super | @context.units[self.name].troop_member.all_partial_conditions
        end
      end
    end
    
    module CrewDivisionFactory
      def self.included(mod)
        mod.class_eval do
          alias :children_origin :children
          attr :crew_division_cache , true
          attr :crew_division_getted_off_cache , true
        end
      end

      def self.extended(mod)
        mod.instance_eval do
          alias :children_origin :children
          def crew_division_cache
            @crew_division_cache
          end

          def crew_division_cache=(value)
            @crew_division_cache = value
          end
          def crew_division_getted_off_cache
            @crew_division_getted_off_cache
          end

          def crew_division_getted_off_cache=(value)
            @crew_division_getted_off_cache = value
          end
        end
      end


      
      def crew_division
        update_situation
        self.current_situation.general_cache[:crew_division_cache] ||= create_division
      end

      def create_division(klass = CrewDivision)
          div = klass.new(self.context)
          div.name = self.name
          member_names = []
          for c in [self , self.children_origin].flatten
            if c.class <= ContainerUnit
              member_names += c.children_origin.collect{|crew| crew.name}
            end
          end
          div.member_names = member_names
          div.parent = klass == CrewDivision ? self : nil
          div.children.each{|c| c.extend CrewDivisionChild }
          
          div
      end

      def crew_division_getted_off
        self.crew_division_getted_off_cache ||= create_division(Division)
      end
    end
  end
end