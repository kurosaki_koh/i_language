# -*- encoding: utf-8 -*-
#class Object
#  def inspect
#    if self.class <= Hash
#      super
#    else
#      "ERROR"
#    end
#  end
#end

module ILanguage
  module Calculator
    class LibOwnersRelation
      attr_accessor :owners , :rms
      
      def initialize
        @owners = []
        @rms = nil
      end
    end
    class ResourceModificationDetail
      #attr_accessor :target_spec , :modification_spec , :related_units , :resource_type
      attr_reader :troop
      
      def initialize(troop , r_type)
        @troop = troop
        @resource_type = r_type
        @classified = nil
        @cost_by_libraries = Hash.new{|cl1 , key1|
          cl1[key1] = Hash.new{|cl2,key2| cl2[key2] = {}}
        }
        @appended_sp = Hash.new{|as , key1|
          as[key1] = {}
        }
        @relation = Hash.new{|r , key1|
          r[key1] = {}
        }
        
        classify.each do |key , sps|
          spa = sps.to_a
          spa.each do |sp|
            append_lib_owner_relation(key , sp)
            append_cost_for_library(key , sp)
          end
        end
      end
      
      def select_sp
        #        troop.select_cost_required_sp.select{|sp| sp.cost.has_key?(r_type)}
        event_costs = troop.collect_all_sp.select{|sp|
          sp.is_event_cost?
          
        }
        reqs = troop.select_cost_required_sp.to_a
        results = event_costs + reqs
        results.select{|sp| sp.cost.has_key?(@resource_type)}
      end
      
      def classify
        @classified ||= begin
          sps = Set.new(select_sp)

          sps.classify{|sp|
            rms = sp.owner.select_resource_modifications(sp,@resource_type)
            rms.collect{|rmsp| rmsp.name}.sort.join(",")
            #          Set.new([key , sp.owner.name])
          }
        end
      end
      
      def print_details
        result = []
        return result if is_simple?
        hash =  classify
        for key in hash.keys
          result << print_detail(key , hash[key] )
        end
        result
      end
      
      def detail_header(rms , lib_names)
        header_libs = lib_names.join("＋")
        header_costs = lib_names.collect{|lib_name| "#{cost_by_library(rms , lib_name ).to_i.abs}万ｔ"}.join","
        "#{header_libs}：【所有組織／所有者】：#{@resource_type}：#{header_costs}\n"
      end
      
      def detail_rms(rms , lib_name)
        rms_by_key_and_lib(rms,lib_name).collect{|rm|
          "－#{rm.lib.name}：【所有組織／所有者】：#{rm.r_value.gsub(/(＃.+$)/,'')}\n"
        }.join
      end
      
      def detail_owners(rms , lib_name)
        owners_array = owners(rms ,  lib_name ).sort_by{|item|item.order}
        names = owners_array.collect{|o|o.name}
        "－対象者(#{owners_array.size})："+names.join('，') + "\n"
      end
      
      def detail_owners_for_event_cost(rms,lib_name)
        owners_array = owners(rms ,  lib_name ).sort_by{|item|item.order}
        soldiers , vehicles = owners_array.partition{|unit| unit.class <= Soldier}
        npc , pcs = soldiers.partition{|o| o.is_npc? }        
        
        nums = []
        nums << "PC#{pcs.size}" if pcs.size > 0
        nums << "NPC#{npc.size}" if npc.size > 0
        nums << "#{'乗り物' if (pcs.size + npc.size) > 0}#{vehicles.size}" if vehicles.size > 0
        
        names = (pcs + npc + vehicles).collect{|o|o.name}.join('，')

        "－対象者(#{nums.join(',')})：#{names}\n"
      end
      
#      def detail_total(rms , lib_name)
#          specs_by_rms_and_library(rms , lib_name).inject(0.0) do |total2 , sp| 
#            total2 += sp.cost_value(@resource_type , true)
#            total2
#          end
#      end
      
#      def detail_totals(rms , lib_name)
#          specs_by_rms_and_library(rms , lib_name).inject(0.0) do |total2 , sp| 
#            total2 += sp.cost_value(@resource_type , true)
#            total2
#          end
#      end

      def detail_totals(rms , lib_names)
        cost_set = Hash.new{|hash,key|hash[key]=0}
        num = 0
        value = lib_names.inject(0.0) do |total , lib_name|
          total += specs_by_rms_and_library(rms , lib_name).inject(0.0) do |total2 , sp| 
            val = sp.cost_value(@resource_type , true)
            cost_set[val]+=1
            total2 += val
            total2
          end
          total
        end
        col2 = cost_set.keys.collect{|val| "#{val.abs}×#{cost_set[val]}"}.join(" + ")
         "－#{@resource_type}：#{col2}：#{value.abs}万ｔ\n"
      end
      
#      def detail_totals_for_event_cost(rms , lib_names)
#        value = lib_names.inject(0.0) do |total , lib_name|
#          total += specs_by_rms_and_library(rms , lib_name).inject(0.0) do |total2 , sp| 
#            total2 += sp.cost_value(@resource_type , true)
#            total2
#          end
#          total
#        end
#         "－#{@resource_type}：#{value.abs}万ｔ\n"
#      end
      
      def print_detail(key , sps )
        sp = sps.to_a[0]
        #        header = "#{sp.lib.name}：【所有国・人】：#{@resource_type}：#{sp.cost[@resource_type].to_i.abs}万ｔ\n"
        #        
        #        libs_hash = sps.classify{|sp|sp.lib.name}
        #        libs_hash.each{|key,sps|
        #          
        #        }
        libs = @relation[key].keys
        details = Set.new(libs).classify{|lib_name| owners(key , lib_name) }
        
        result = []
        
        for owners in details.keys
          lib_names = details[owners].to_a

          
          header = detail_header(key , lib_names)
          rms =  detail_rms(key , lib_names[0] )
          sp = specs_by_rms_and_library(key,lib_names[0])[0]
          targets = if sp.is_event_cost?
            detail_owners_for_event_cost(key , lib_names[0])
          else
            detail_owners(key , lib_names[0])
          end
          total = detail_totals(key , lib_names)

          result << header + rms + targets + total
        end
        
        result.join("\n")
      end
      
      #ユニットごとの消費
      def consumption_by_unit
        
      end
      
      def modification_rate
        
      end
      
      #実際の消費
      def total_consumption
        
      end
      
      def is_simple?
        classify.keys.size == 1
      end
      
      def cost_by_library(key , lib_name)
        @cost_by_libraries[key][lib_name][:cost]
      end
      
      def specs_by_rms_and_library(key , lib_name)
        @cost_by_libraries[key][lib_name][:sp_set]
      end
      
      def rms_names_by_rms_and_library(key , lib_name)
        @cost_by_libraries[key][lib_name][:rm_set]
      end
      
      def owners(key , lib_name)
        @relation[key][lib_name].owners.dup
      end
      
      def rms_by_key_and_lib(key , lib_name)
        @relation[key][lib_name].rms.dup
      end
      def append_lib_owner_relation(key , sp)
        @relation[key][sp.lib.name] ||= LibOwnersRelation.new
        @relation[key][sp.lib.name].rms= sp.owner.select_resource_modifications(sp,@resource_type)
        @relation[key][sp.lib.name].owners |= [sp.owner]
      end
      
      def append_cost_for_library(key , sp)
        unless @appended_sp[key][sp.name]
          cost = sp.cost
          @appended_sp[key][sp.name] = cost
          
          @cost_by_libraries[key][sp.lib.name][:cost] ||= 0
          @cost_by_libraries[key][sp.lib.name][:cost] += cost[@resource_type]
        end
        @cost_by_libraries[key][sp.lib.name][:sp_set] ||= []
        #          @cost_by_libraries[key][sp.lib.name][:sp_set].find{|item| item == sp.name}
        @cost_by_libraries[key][sp.lib.name][:sp_set] << sp 
        #          end
      end
    end
  end
end
