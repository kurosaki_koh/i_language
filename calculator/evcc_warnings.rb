# coding: utf-8

module ILanguage
  module EVCC
    class Warning
      attr :command , true
      attr :message , true
      @@warnings = []
      def Warning.add(warning)
        @@warnings << warning
      end

      def Warning.check(root)
        @@warnings.collect{|warning| warning.do_check(root)}.compact
      end

      def initialize(msg,&cmd)
        @message = msg
        @command = cmd
        Warning.add(self)
      end

      def do_check(root)
        @message if @command.call(root)
      end
    end

    Warning.new('特別なユニット名”部隊全員”は廃止予定です。”全隊員”もしくは”全ユニット”のどちらかを使い分けて下さい。http://bb2.atbb.jp/echizenrnd/viewtopic.php?p=46#46'){|root|
      ds = root.directives
      ds.any?{|d| defined?(d.targetable) && d.targetable.text_value != '' && d.targetable.target.text_value.include?('部隊全員')}
    }
  end
end