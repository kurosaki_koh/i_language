# coding: utf-8

# #gem "activesupport", "=2.3.8"
require 'active_support/core_ext/float'

require 'yaml'
#require 'yaml_waml'
require 'set'

module ILanguage
  module Calculator
    StandardAbilities = ['体格','筋力','耐久力','外見','敏捷','器用','感覚','知識','幸運']
    CommonAbilities = %w[装甲 防御 魅力 移動]
    CommonActions = ['偵察行為','追跡行為','侵入行為','隠蔽行為','陣地構築行為','歌唱行為']
    ID_SYMBOL = :'Ｉ＝Ｄ'
    StandardAbilitySet = Set.new(StandardAbilities)

    AdditionalTqrs = {
      :'水中白兵距離戦闘行為' => :'白兵距離戦闘行為' ,
      :'水中中距離戦闘行為' => :'中距離戦闘行為' ,
      :'水中遠距離戦闘行為' => :'遠距離戦闘行為'
    }

    def tqr_appendance(tqr_ids)
      tqr_ids.collect{|tqr|AdditionalTqrs[tqr]}.compact
    end

    def is_standard_ability?(name)
      StandardAbilitySet.include?(name)
    end
    
    def to_rd(val)
      result = (1.2 ** val) #.round(1)
      result >= 0.05 ? result.round(1) : result
    end

    def to_eval(val)
      if val.class == Float
        rounded = val.round(1)
        val = rounded if rounded > 0
      end
      low = (Math.log(val) / Math.log(1.2)).floor
      high_threshold = to_rd(low+1)
      (val >= high_threshold) ?  low + 1 : low
    end

    def sum_evals(values)
      rd = 0
      values.each do|v|
        rd += to_rd(v)
      end
      to_eval(rd)
    end

    module ParserUtil
      def parse_array(str)
        content = if str =~ /｛(.+?)｝/
          $1.split('，')
        else
          [str.sub(/。$/,'')]
        end
      end
    end
    module UnitTypeOwner
      def is_unit_type_match?(sp)
        utype = if sp.class <= Specialty
          sp.unit_type
        else
          sp
        end
        utype == '' || self.unit_type.include?(utype)
      end
    end
  end
end
