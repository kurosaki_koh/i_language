# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator
    class Specialty

      def self.name(sp_source)
        sp_source =~ /＊(.+?)　＝/
        $1
      end

      def to_hash
        @hash
      end
      def initialize(owner,hash,name)
        @owner = owner
        @hash = hash
        @idress_name = name

        @lib = owner.context.create_library(name) if owner
        @name = hash['名称'] || Specialty.name(hash['定義'])
        @name =~ /#{@idress_name}の(.+?)$/
        @spec_name = $1

        @applicable_hash = {}
        @is_available = {}
        action_name = @lib.action_names.find{|a_name|
          a_name.sub(/行為$/,'補正') == @spec_name
        }
        @is_action = !!action_name

        @is_tqr = case spec_type
        when '行為' , '行為補正'
          action_name = hash['行為名']
          true
        else
          !!(/７５％制限(能力)?。/o).match(source) || @is_action #関連する行為がある。
        end
        @tqr_id = case
        when action_name
          action_name.to_sym
        when %w[補正 その他定義].include?(spec_type)
          @spec_name ? @spec_name.to_sym : nil
        else
          nil
        end
        @note = hash['付記']
        @for_ace = false
      end

      attr :for_ace , true
      attr_reader :spec_name , :note

      def tqr_requirement
        @tqr_id
      end

      def is_action?
        @is_action
      end
      
      def is_tqr?
        @is_tqr
      end

      def is_for_ace?
        @for_ace
      end

      def tqr_id
        @tqr_id
      end

      def [](key)
        @hash[key]
      end

      def []=(key,value)
        @hash[key]=value
      end

      #特殊種別を返す。
      def spec_type
        @spec_type ||= @hash['特殊種別']
      end

      def unit_type
        @unit_type ||= @hash['兵科種別'] || ''
      end

      #この特殊のownerがTroop直下のユニットならowner,そうでなければowner.parent
      def troop_member
        @owner.is_troop_member? ? @owner : @owner.parent
      end

      #定義本文を返す。
      def source
        @source ||= @hash['定義']
      end
      
      def r_value
        @r_valute ||= begin
          source =~ /　＝　(.+)/
          rval = $1
          array = rval.split('，' , 3)
          array.size == 3 ? array[2] : rval
        end
      end

      def action_name
        @action_name ||= self['行為名']
      end

      #特殊が属するアイドレスのＬ：名を返す。
      def idress_name
        @idress_name
      end

      #特殊の名称（／＊（.+）　＝　／の＄１」を返す。
      def name
        @name
      end

      #特殊の所有者を返す。
      def owner
        @owner
      end

      #Ｌ：定義ハッシュ
      def lib
        @lib
      end

      def is_disabled?(unit = owner)
        unit.all_disabled_sps.include?(self.name)
      end

      def troop_member
        @owner.troop_member
      end

      def is_unit_type_match?
        @is_unit_type_match ||= self.target.is_unit_type_match?(self)
      end

      #特殊の対象を返す。
      #歩兵なら所有者本人を、乗り物へ継承される行為なら乗り物を返す。
      def target(ability = nil)
        if self.troop_member.is_unit_type_match?(self)
          spec_type == '行為' ? troop_member : @owner
        else
          return troop_member
        end
      end

      #前提条件を満たしているか？
      def is_available?(additional_partial_conditions = [])
        @is_available[additional_partial_conditions] ||= begin
          condition_match?(additional_partial_conditions) 
        end
      end

      #航路毎消費コストがある。
      def is_consumed_by_hops?
        @is_consumed_by_hops ||= self.spec_type == '航路移動時消費' && self['消費単位']== '１航路につき'
      end

      def condition_match?(additional_partial_conditions = [])
        case
        when condition == ''
          true
        when owner.all_conditions.any?{|cond| case cond
            when String
              condition == cond
            when Regexp
              condition =~ cond
            else
              false
            end
          }
          true
        else
          p_conds = owner.all_partial_conditions + additional_partial_conditions
          separated_conditions.all?{|cond|
            p_conds.any?{|p_cond| p_cond.match(cond)}
          }
        end
      end

      def condition
        @condition ||= self['行為可能条件'].nil? ? '' : self['行為可能条件']
      end

      def separated_conditions(condition_str = self.condition)
        @s_conds ||= begin
          if condition_str  == '' || condition_str.nil?
            []
          else
            cond_str = condition_str.dup
            cond_str.gsub!(/^（/o,'')
            cond_str.gsub!(/）$/o,'')
            cond_str.split('、')
          end
        end
      end

      WeaponPropertyRE =  /((白兵|射撃)（.+）)/o
      def weapon_names
        unless defined? @weapon_names
          if separated_conditions
            wnames_str = separated_conditions.find{|cond|cond =~ WeaponPropertyRE }
            if wnames_str
              wnames = wnames_str.split('，')
              @weapon_names = wnames.collect{|wname|
                wname =~ WeaponPropertyRE
                $1
              }
            else
              @weapon_names = nil
            end
          else
            @weapon_names = nil
          end
        end
        @weapon_names
      end

      def weapon_classes
        if !(defined? @weapon_classes ) && weapon_names
          @weapon_classes = weapon_names.collect{|name|
            name =~ WeaponPropertyRE
            $2
          }
        else
          @weapon_classes = nil
        end
        @weapon_classes
      end

      def has_cost?
        @has_cost ||= begin
          cost = @hash['コスト']
          cost && cost.keys.size > 0
        end
      end

      def is_cost_required?
        return false if is_disabled? || !is_unit_type_match? || is_for_ace?
        tqr_qualified = !is_tqr? || owner.troop.is_tqr_qualified?(self) || owner.cost_cache.has_key?(self.name.intern)

        flag = self.has_sp_cost? && self.has_cost? && tqr_qualified && (self.is_event_cost? || is_available?)
        self.use if flag
        flag
      end

      def is_event_cost?
        spec_type == 'イベント時消費'
      end

      def has_sp_cost?
        ['行為','補正','その他定義','航路移動時消費'].include?(spec_type)
      end

      def cost
        @hash['コスト'] && @hash['コスト'].dup
      end
      
      def cost_value(r_type , with_modification=false  )
        cost_hash = self.cost
        return 0 if cost_hash.nil? || cost_hash[r_type].nil?
        self.owner.npc_event_cost_hook(cost_hash , self)
        
        value = cost_hash[r_type]
        value *= (self.spec_type == 'イベント時消費') ? 1 : self.owner.troop.organization_coefficient
        value *= self.owner.troop.hops.to_i if self.is_consumed_by_hops?

        value *= self.owner.rm_rate(self,r_type) if with_modification
        value
      end
      
      def use
        unless @used
          intern = self.name.intern
          owner.troop.swapped_situation.used_sp_cache |= [intern]
          owner.cost_cache[intern]=true if self.has_cost? && (owner.parent.nil? || owner.troop.is_cost_required?)
          @used = true
        end
      end
    end

    class Evaluation
      attr :name
      attr :evals_hash , true
      attr :all_eval , false
      attr :all_abi , false
      attr :owner , false
      
      def initialize(i_definition , owner)
        @owner = owner
        if i_definition
          @evals_hash = i_definition['評価'] ?  i_definition['評価'].dup :  {}
          @name = i_definition['Ｌ：名'] if @evals_hash
          @all_eval = @evals_hash['全評価'] if @evals_hash.has_key?('全評価')
          @all_abi = @evals_hash['全能力'] if @evals_hash.has_key?('全能力')
        else
          @evals_hash = nil
        end
      end

      def add(other)
        #        keys = (StandardAbilities | other.optional_keys).compact
        keys = other.evals_hash.keys
        if keys.include?('全能力')
          StandardAbilities.each{|k| @evals_hash[k] ||= 0}
          @evals_hash['対空戦闘'] ||= 0 if (owner.action_names.include?('対空戦闘行為') && owner.is_ace?) #決戦号特例
          keys.delete('全能力')
        end
        for key in keys
          @evals_hash[key] ||= 0
          @evals_hash[key] += other.evals_hash[key].to_i
        end
        @all_eval = @all_eval.to_i + other.all_eval.to_i
        @all_abi = @all_abi.to_i + other.all_abi.to_i
      end

      def is_not_found?
        @evals_hash.nil?
      end

      def value(col)
        value = @evals_hash[col]
        alls = [@all_eval.to_i , @all_abi.to_i].max
        if (is_standard_ability?(col) || owner.class <= Vehicle || owner.is_ace?) && alls > 0
          value ||= 0
          value +=  [@all_eval.to_i , @all_abi.to_i].max
        end
        value
      end

      def values
        @evals_hash && @evals_hash.values
      end

      def [](col)
        value(col) || 0
      end

      def []=(key,value)
        @evals_hash[key] = value
      end

      #能力値種別を返す。
      def keys
        @evals_hash.keys
      end

      def has_key?(k)
        @evals_hash.has_key?(k)
      end
      #追加能力値種別（対空・治安維持など）があれば配列で返す。なければnil。
      def optional_keys
        source_keys = keys.dup
        source_keys.delete_if{|i| is_standard_ability?(i)}
        source_keys
      end

      def to_hash
        self
      end
    end

    class ARModifierSpec < Specialty
      def condition
        @condition ||= self['修正使用条件']
      end
    end

    class BonusSpec < Specialty
      AbilityBonusNames = %w[
        体格 筋力 耐久力 外見 敏捷 器用 感覚 知識 幸運 全能力
        治安維持
        災害対応活動
        災害救助
        対要塞戦闘
        対空戦闘
        住みやすさ
        装甲
        対潜水艦戦闘
        超遠距離戦闘
        水中中距離戦闘
        対ＲＢ戦闘
        艦隊戦闘
      ].collect{|name|name.to_sym}
      
      def initialize(owner,hash,name)
        super
        @used = false
        @is_unit_type_match = {}
      end

      def tqr_requirement
        @tqr_id
      end

      def tqr_id
        @is_action ? nil : @tqr_id
      end

      def is_used?
        @used
      end

      def bonus_targets
        @bonus_targets ||= @hash['補正対象']
      end
      
      #能力値補正（所有者本人の能力に加算）か？
      def is_ability_bonus?
        @is_ability_bonus ||= AbilityBonusNames.include?(bonus_targets[0].to_sym)
      end

      #判定補正（判定ユニット（歩兵なら本人、乗り物にのっていれば乗り物）に加算）か？
      def is_judge_bonus?
        !is_ability_bonus?
      end

      #包括補正か？
      WholeKeywords = Set.new(['全能力' , '全判定' , '全評価'])
      def is_whole_bonus?
        @is_whole_bonus ||= WholeKeywords.include?(bonus_targets[0])
      end

      def is_target?(unit,bonus_target = nil)
        if self.is_ability_bonus?
          target_unit = if bonus_target.nil?
            @owner
          elsif is_standard_ability?(bonus_target)
            @owner
          else
            troop_member
          end
          return target_unit.object_id == unit.object_id
        end

        case self.troop_member
        when Soldier
          (@owner.object_id == unit.object_id )& @owner.is_troop_member?
        when Vehicle , Vessel
          target = @owner.class <= Soldier ? troop_member : @owner
          target.object_id == unit.object_id
        when CrewDivision
          false
        when Troop , Division
          true
        else
          false
        end
      end

      #      #前提条件を満たしているか？
      #      def is_available?(additional_partial_conditions = [])
      #        @is_available[additional_partial_conditions] ||= begin
      #          self.owner.troop.is_tqr_qualified?(self) && case
      #          when condition == ''
      #            true
      #          when owner.all_conditions.any?{|cond| case cond
      #              when String
      #                condition == cond
      #              when Regexp
      #                condition =~ cond
      #              else
      #                false
      #              end
      #            }
      #            true
      #          else
      #            p_conds = owner.all_partial_conditions + additional_partial_conditions
      #            separated_conditions.all?{|cond|
      #              p_conds.any?{|p_cond| p_cond.match(cond)}
      #            }
      #          end
      #        end
      #      end
      #
      def condition
        @condition ||= self['補正使用条件']
      end
      #
      #      def separated_conditions
      #        unless @s_conds
      #          @s_conds = $1.split('、') if condition =~ /^（(.+)）$/
      #        end
      #        @s_conds
      #      end

      def is_ability_match?(ability)
        if self.is_whole_bonus?
          case bonus_targets[0]
          when '全能力'
            #            if self.owner.is_unit_type_match?('乗り物')
            if self.target(ability).is_unit_type_match?('搭乗') || self.target.is_ace?
              return true
            else
              return is_standard_ability?(ability)
            end
          when '全判定'
            return true
          end
        else
          return bonus_targets.include?(ability) 
        end
        
      end

      def is_unit_type_match?(ability = nil)
        @is_unit_type_match[ability] ||= begin
          target = self.target
          return false if target.nil?
          (t = target.troop_member) ? t.is_unit_type_match?(self) : false
        end
      end

      #適用可能か？
      #補正対象ユニット、兵科種別、前提条件、判定対象を考慮した判定を行う。
      def is_applicable?(target , ability) #,optional_conditions = [])
        key = [target,ability]
        unless @applicable_hash.has_key?(key)
          flag = (is_unit_type_match?(ability) && is_target?(target,ability) && is_available? && is_ability_match?(ability))
          @applicable_hash[key] = !!flag
        end
        @applicable_hash[key]
      end

      #補正値を返す。
      def bonus
        @bonus ||= self['補正']
      end

      def is_cost_required?
        return false if is_for_ace? || !has_cost?

        flag = owner.cost_cache.has_key?(self.name.intern)
        return false if !flag && (self.is_disabled? || !is_unit_type_match? )
        
        flag ||= self.is_action? && self.owner.troop.is_tqr_qualified?(self) && self.is_unit_type_match?

        flag || super
      end 
      
      def is_string?
        bonus.class == String
      end

      #補正対象を返す。
      #歩兵なら所有者本人を、搭乗者のもつ判定への補正なら乗り物を返す。
      def target(bonus_target = nil)
        case
        when self.is_judge_bonus?
          troop_member
        when bonus_target.nil?
          @owner
        else
          is_standard_ability?(bonus_target) ? @owner : troop_member
        end
        #eturn self.is_ability_bonus? ? @owner : troop_member
      end

      #判定対象がcolと一致したら補正値を、それ以外はnilを返す。
      def value(col)
        if col.nil?
          nil
        elsif (self.is_ability_bonus? && self.is_ability_match?(col)) || self.is_judge_bonus?
          bonus
        else
          nil
        end
      end

      def to_hash
        hash = {}
        StandardAbilities.each{|c|
          if is_whole_bonus? || is_ability_match?(c)
            hash[c] = self.value(c)
          end
        }
        hash
      end
      def keys
        to_hash.keys
      end
    end

    class FlexibleBonusSpec < BonusSpec
      def flexible_target
        self.owner.all_flexible_bonus[self.name]
      end

      def is_flexible_target_disabled?
        flexible_target.nil?
      end

      #能力値補正（所有者本人の能力に加算）か？
      def is_ability_bonus?
        return false if is_flexible_target_disabled?

        (StandardAbilities + ['全能力'] ).include?(flexible_target)
      end

      #判定補正（判定ユニット（歩兵なら本人、乗り物にのっていれば乗り物）に加算）か？
      def is_judge_bonus?
        f_target = flexible_target
        return false if is_flexible_target_disabled?
        return true if Evaluators.has_key?(f_target)
        RegisteredActionNames.include?(f_target.to_sym) ||
          ClassInstances.keys.include?(f_target) || f_target == '全判定'
      end

      def is_ability_match?(c)
        return true if is_standard_ability?(c) && flexible_target == '全能力'
        return flexible_target == c
      end

      def is_whole_bonus?
        flexible_target == '全判定'
      end
      #補正対象を返す。
      #歩兵なら所有者本人を、搭乗者のもつ判定への補正なら乗り物を返す。
      def target(ability = nil)
        return nil if is_flexible_target_disabled?
        return @owner if self.is_ability_bonus?
        return self.is_judge_bonus? ? troop_member : nil
      end

    end

    class ResourceModificationSpec < Specialty
      def resource_type
        @resource_type ||= self['修正対象']
      end

      def coefficient
        @coefficient ||= self['修正倍率']
      end

      def conditions
        @conditions ||= begin
          condition_str.split('、').collect{|cond|/#{cond}/}
        end
      end

      def condition_str
        @condition_str ||= begin
          str = self['対象条件']['補正使用条件'].dup
          str.gsub!(/^（/o,'')
          str.gsub!(/）$/o,'')
          str
        end
      end

      def modification_type
        @modification_type ||= self['対象条件']['条件'].intern
      end

      def bonus_targets
        @bonus_targets ||= self['対象条件']['補正対象']
      end

      def is_conditions_match?(sp)
        self.conditions.all?{|cond_re|
          sp.separated_conditions.find{|cond| cond =~ cond_re}
        }
      end

      def is_applicable?(sp)
        case
        when self.modification_type == :'基本オプション装備時の'
          !!(sp.lib.categories['カテゴリ']=~/基本オプション/o && sp.cost && sp.cost.has_key?(self.resource_type))
        when self.bonus_targets == [] && self.condition_str != ''
          #"基本オプション装備時の"でない かつ 補正対象がない＝　リソース前提条件とリソース種別さえ合致すればtrue
          condition_flag = if sp.spec_type == 'イベント時消費'
            sp.separated_conditions(sp['消費単位']).any?{|cond| cond == self.condition_str}
          else
            false
          end
          self.is_resource_type_match?(sp) && condition_flag
        when self.bonus_targets == [] && self.condition_str == ''
          self.is_resource_type_match?(sp)
        when !(sp.class <= BonusSpec)
          #補正対象あり かつ 対象特殊が補正特殊でない＝false
          false
        else
          flag1 = (sp.bonus_targets & self.bonus_targets).size > 0
          flag2 = self['対象条件']['補正使用条件'] == '' || self.is_conditions_match?(sp)
          flag1 & flag2
        end
      end

      def is_resource_type_match?(sp)
        sp['コスト'].has_key?(self.resource_type)
      end
      
      def is_whole_modification?
        self['対象条件']['補正使用条件'] == '' &&  self['対象条件']['補正対象'] == []
      end

    end

    class ActRestrictionSpec < Specialty
      def act_restrictions
        self['行為制約'].collect{|act|act.to_sym}
      end
    end
  end
end

module Jiken
  module Parser #:nodoc:
    module SpecialtyDef #:nodoc: all
      def parse_cost(text)
        text = sub_nums(text)
        result = {}
        re = /((燃料|資源|食料|生物資源|資金|マイル|｛犬士または猫士｝)((\+|\-)\d+(\.\d+)?))/o
        while text =~ re
          result[$2]=$3.to_f
          text.sub!(re,'')
        end
        return result
      end
    end
  end
end