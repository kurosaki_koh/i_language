# -*- encoding: utf-8 -*-
require 'digest/md5'

module ILanguage
  module Calculator
    module Recursive
      def recursive_hash
        self.all_leaves.inject({}) do |hash , c|
          hash.update( {"#{self.evaluator.display_name}＠#{c.sub_header}" => c.total})
          hash
        end
      end
      def all_leaves
        children.collect{|c|c.to_a }.flatten
      end

      def distinct_operands
        all_operands = children.collect{|c|
          c.arg_names.collect{|arg|
            c.arguments[arg] && c.arguments[arg].operands
          }
        }.flatten.compact + self.operands_no_dup
        all_operands = all_operands.select{|op|op.class <= BonusSpec}
        result = all_operands.inject({}){|hash , op| hash[op.name]=op ; hash}
        result.values.sort_by{|sp|sp.name}
      end 

      def add(child)
        child.parent = self
        children << child
        self
      end
      
    end

    class Calculation
      include Comparable
      include UnitTypeOwner
      
      attr :unit , true
      attr :evaluator , true
      attr :base_value , true
      attr :operands , true
      attr :total , true
      attr :rd , true
      attr :arguments , false
      attr :parent , true
      attr :arg_name , true
      attr :arg_names , true
      attr :unit_type , true

      def initialize(unit , evaluator)
        @unit = unit
        @evaluator = evaluator
        @arguments = {}
        @is_string = false
        @arg_names = @evaluator.args(@unit)
      end

      def reset
        @arguments_avg = nil
        @base_value = nil
        @operands = nil
        @total = nil
        @rd = nil
        @operands_no_dup = nil

      end

      def is_string?
        if @is_string.nil?
          return arguments_avg.class == String
        else
          @is_string
        end
      end

      def is_string=(value)
        @is_string
      end

      def is_valuable?
        @parent.nil? || @parent.class == CalculationSet
      end

      def arguments_avg
        @arguments_avg ||= begin
          sum_args = 0.0
          for arg_name in arg_names
            argument = arguments[arg_name]
            return nil if argument.nil?
            if argument.total.class == String
              @is_string = true
              @arguments_avg = argument.total
              @total = @base_value = @arguments_avg
              return @arguments_avg
            end
            total = argument.total
            sum_args += total
          end
          sum_args / arg_names.size
        end
      end

      def base_value
        @base_value ||= begin
          avg = arguments_avg
          return nil if avg.nil?
          if avg.class == String
            @base_value = avg
            return avg
          end
          avg.truncate
        end
      end

      def use
        operands.each{|op|op.use if op.class <= BonusSpec}
        arguments.values.each{|op|op.use}
      end

      def operands
        @operands ||= @unit.judge_bonuses.select{|sp| @evaluator.match?(sp) && evaluator.sp_target(sp).object_id == self.object_id }
      end


      def Calculation.select_singleton_bonuses(bonuses)
        bonuses.select{|op|op.class <= Specialty && op.note =~ /同能力重複適用不可/o}.collect{|sp|sp.name}.uniq
      end
      
      def operands_no_dup
        @operands_no_dup ||= begin
          @operands_no_dup = operands.dup

          leaf = self.unit
          if leaf.troop.object_id != leaf.object_id
            super_bonuses = leaf.troop.all_bonuses.select{|sp|self.evaluator.match?(sp) && self.is_unit_type_match?(sp)}
            no_dup_names_from_parent = Calculation.select_singleton_bonuses(super_bonuses)

            if no_dup_names_from_parent.size > 0
              @operands_no_dup.delete_if{|sp|sp.class <= BonusSpec && no_dup_names_from_parent.include?(sp.name) }
            end
          end
          @operands_no_dup += parent_bonuses.dup if @arg_name.nil?

          no_dup_names = Calculation.select_singleton_bonuses(@operands_no_dup)
          for no_dup_name in no_dup_names
            indice = []
            @operands_no_dup.each_with_index do |item,idx|
              indice << idx if item.name == no_dup_name
            end
            indice.shift
            indice.reverse.each{|idx|@operands_no_dup.delete_at(idx)}
          end
          @operands_no_dup
        end
      end

     def total
        @total ||= begin
          value = base_value
          if value.class == String
            @total =  value
            return @total
          elsif value.nil?
            return nil
          end

          for op in operands_no_dup
            if op.is_string?
              @is_string = true
              value = op.bonus
              break
            end
            value += op.bonus
          end
          value
        end
      end

      def rd
        @rd ||= to_rd(total)
      end

      def is_na?
        if @rd.nil? && !@arg_name && (arguments.size == 0 || arguments.values[0].is_na?)
          @rd = 0
        end
        @rd == 0
      end

      def first_member
        @unit
      end

      def to_a
        [self]
      end

      def to_digest
        Digest::MD5.hexdigest(self.to_code)
      end

      def to_code
        if @arguments.size > 0
          arg_names.collect{|arg|@arguments[arg].to_code}.join
        else
          ([@arg_name , @base_value]+operands_no_dup.to_a.collect{|b|b.name}).to_s
        end
      end

      def inspect_result
        self.total.to_s
      end

      def <=>(b)
        a_total = self.is_na? ? -99999 : (self.arguments_avg.class == String ? 99999 : self.arguments_avg)
        b_total = b.is_na? ? -99999 : (b.arguments_avg.class == String ? 99999 : b.arguments_avg)
        [a_total,self.evaluator.represent_order] <=> [b_total,b.evaluator.represent_order]
      end

      #      def parent_bonuses
      #        []
      #      end

      def parent_bonuses
        if self.is_valuable? && self.evaluator !~ /同調/o
          leaf = self.parent
          bonuses = []
          while leaf
            bonuses += leaf.operands
            leaf = leaf.parent
          end
          bonuses.select{|sp|
            sp.class <= BonusSpec &&
              #              sp.owner.object_id == sp.target.object_id &&
            self.is_unit_type_match?(sp) &&
              (!self.evaluator.is_individualy? || !sp.is_disabled?(self.unit))
          }
        else
          []
        end
      end

      def unit_type
        @unit_type ||= begin
          result = unit.unit_type.dup

          if is_valuable? && self.evaluator.name !~ /同調/ && unit.parent.class <= ContainerUnit
            result << '搭乗'
          end
          result
        end
      end
      
      def hash_for_diff
        {evaluator.display_name => total}
      end
      
      def sub_header
        parent_name = (parent && (parent.unit.class == CrewDivision) )? parent.unit.name : nil
        name = "#{self.unit.character_no + '_' if self.unit.character_no}#{self.unit.name}"
        [parent_name , name].compact.join('：')
      end
    end

    class CalculationSet < Calculation
      attr :children , false
      def initialize(unit , evaluator)
        super
        @children = []
        @arguments = Hash.new{|h , k|
          calc = Calculation.new(@unit , @evaluator)
          #          calc = @evaluator.create_calculation(@unit)
          calc.arg_name = k
          h[k]=calc
        }
      end

      def ==(other)
        self.children.zip(other.children).all?{|a , b| a.unit == b.unit && a.total == b.total}
      end
      
      def difference(other)
        self.children.zip(other.children).collect{|a,b|a.total - b.total}
      end
      
      def total
        nil
      end

      def unit_type
        ['歩兵' , '搭乗']
      end

      def use
        super
        @children.each{|c|c.use}
      end

      include Recursive
      def hash_for_diff
        recursive_hash
      end
      
    end

    class TeamCalculation < Calculation
      attr :children , false
      attr :arguments , false
      
      include Recursive
      
      def initialize(unit , evaluator)
        super
        @children = []
        @rd_total = {}
        @argument_eval = {}
        @arguments = Hash.new{|h , k|
          target_bonuses = @unit.evaluate_items(evaluator)

          rd = rd_total(k)
          #          calc = @evaluator.create_calculation(@unit)
          calc = Calculation.new(@unit , @evaluator)
          calc.parent = self
          calc.arg_name = k
          if rd.class == String
            @argument_eval[k] = @total
            calc.is_string = true
            calc.base_value = @total
          elsif rd.nil?
            calc.rd = nil
          else
            calc.base_value = to_eval(rd) if rd > 0
            calc.operands = target_bonuses.select{|sp|
              sp.is_applicable?(self,k) && sp.bonus_targets.include?(k)
            }
          end
          h[k] = calc
        }
        @arg_names = @evaluator.args.dup
        @arg_names |= [evaluator.class_name] if unit.evals.has_key?(evaluator.class_name)
      end

      def use
        super
        @children.each{|c|c.use}
      end

      def is_na?
        if @rd.nil? && @children.all?{|c|c.is_na?}
          @rd = 0
        end
        @rd == 0
      end

      def rd_total(arg_name)
        @rd_total[arg_name] ||= begin
          rd_total = @unit.children.size == 0 ? 1 : 0
          for c in children
            arg = c.arguments[arg_name]
            if arg.nil? || arg.is_na?
              next
            elsif arg.total.class == String
              @is_string = true
              rd_total = @total = arg.total
              break
            elsif arg.total.nil?
              next
            else
              rd_total += to_rd(arg.total)
            end
          end
          rd_total
        end
      end
      
      def arguments_avg
        @arguments_avg ||= begin
          sum_args = 0.0
          size = 0

          for arg_name in arg_names
            next if arg_name == @evaluator.class_name && arg_names.size != 1
            argument = arguments[arg_name]
            next if argument.nil?
            if argument.total.class == String
              @is_string = true
              @arguments_avg = @total
              return @total
            end
            if argument.total
              sum_args += argument.total
              size += 1
            end
          end
          if size > 0
            sum_args / size
          else
            nil
          end
        end
      end

      def arguments_rdsum
        unless defined?(@arguments_rdsum)
          if arg_names.include?(@evaluator.class_name) && !@evaluator.args.include?(@evaluator.class_name)
            argument = arguments[@evaluator.class_name]
            if argument.nil? || argument.total.nil?
              @arguments_rdsum = nil
              return nil
            end
            if argument.total.class == String || arguments_avg.class == String
              @is_string = true
              @arguments_rdsum = argument.total
              return @arguments_rdsum
            end
            rdsum = to_rd(argument.total)
            rdsum += to_rd(arguments_avg.truncate) if arguments_avg
            @arguments_rdsum = to_eval(rdsum)
          else
            @arguments_rdsum = nil
          end
        else
          @arguments_rdsum
        end
      end

      def base_value
        @base_value ||= begin
          value = arguments_rdsum || arguments_avg
          if is_string?
            @base_value = @total
            return @total
          end
          return nil if value.nil?
          value.truncate
        end
      end

      def to_a
        @children
      end


      def to_code
        (arg_names + [base_value]+operands_no_dup.collect{|b|b.name}).to_s
      end

      def to_digest
        code = @children.collect{|c|c.to_code}.join + self.to_code
        #        puts code
        Digest::MD5.hexdigest(code)
      end

      #      def parent_bonuses
      #        leaf = self.parent
      #        bonuses = []
      #        while leaf
      #          bonuses += leaf.operands
      #          leaf = leaf.parent
      #        end
      #        bonuses.select{|sp|sp.class <= BonusSpec && sp.owner.object_id == sp.target.object_id &&
      #            self.is_unit_type_match?(sp)}
      #      end

      def unit_type
        @unit_type ||= begin
          result = []
          for type_name in ['歩兵' , '搭乗' , '乗り物']
            result << type_name if self.children.all?{|c|c.unit_type.include?(type_name)}
          end
          result
        end
      end
      
    end

    class SynchronizeCalculation < TeamCalculation
      def initialize(unit , ev)
        super
      end

      def rd_total(key)
        max_calc = children.max
        return nil if children.max.nil?
        max_calc = max_calc.arguments[key]
        m_total = max_calc.total
        case m_total
        when String
          max_calc.total
        when nil
          nil
        else
          max_calc.rd
        end
      end
      
      def base_value
        @base_value ||= begin
          @base_value = children.max.total
          if @base_value.class == String
            @total = @base_value
            @is_string = true
          end
          @base_value
        end
      end

      def first_member
        children[0].first_member
      end

      def select_leader_candidate
        max_calc = children.max
        value = max_calc.total
        children.select{|c|c.total == value}
      end

      def inspect_result
        select_leader_candidate.collect{|c| "#{c.unit.name}：#{self.total}"}.join(' / ')
      end
      
      include Recursive
      def hash_for_diff
        select_leader_candidate.inject({}){|hash , child|
          hash.update( { child.unit.name => self.total})
          hash
        }
      end
    end

    class LeaderCalculation < Calculation

      def inspect_result
        "#{@unit.name}：#{self.total}"
      end
    end
  end
  
end