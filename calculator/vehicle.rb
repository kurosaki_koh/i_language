# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__) , 'crew_division.rb')
module ILanguage
  module Calculator
    class Vehicle < ContainerUnit
      include ILanguage::Calculator::ParserUtil
      include CrewDivisionFactory

      attr :vehicle_type , true

      def initialize(context)
        super
        @override ={}
      end
      
      def unit_type
        if self.is_regarded_as_soldier?
          @unit_type + [ '歩兵']
        else
          @unit_type
        end
      end

      InhibitActions = %w[歌唱行為 オペレート行為 整備行為].collect{|item|item.intern}

      def act_restrictions
        update_situation
        current_situation.general_cache[:act_restrictions] ||= begin
          super | InhibitActions
        end
      end
      
      def action_names
        update_situation
        current_situation.action_names ||= begin
          result = self_action_names | team_action_names
          result.delete_if{|name| act_restrictions.include?(name.intern)}
          result
        end
      end

      PilotPotentials = ['器用','感覚','知識','幸運']

      def non_ace_pilots
        @non_ace_pilots ||= children.select{|c|!(c.is_ace? && !c.is_named_npc?)}
      end

      def aces
        @aces ||= children.select{|c|c.is_ace? && !c.is_named_npc?}
      end
      
      def pilot_potentials
        result = {}
        for col in StandardAbilities
          result[col]= non_ace_pilots.collect{|s| s.evaluate(col)}.max
        end
        result
      end

      def is_overridden?(col)
        evals if @override[col].nil?
        @override[col]
      end
      
      def evals_by_selection
        update_situation
        current_situation.general_cache[:evals_by_selection] ||= begin
          evals = self.evals_org
          #非ＡＣＥの搭乗者の一般評価４種から最大値を求め、これを機体の評価に足す。
          potent = pilot_potentials()
          for col in PilotPotentials
            evals[col] += potent[col] if potent[col]
          end

          #ＡＣＥ搭乗者の評価と比較し、高い方を採用する。
          for col in StandardAbilities
            if evals.has_key?(col)
              values = [evals[col]] + aces.collect{|ace|ace.evaluate(col)}
              max = values.max
              @override[col] = (evals[col] != max)
              evals[col] = max
            end
          end
          evals
        end
      end

      
      def evals
        update_situation
        current_situation.evals ||= self.evals_by_selection
      end

      def pilot_items
        result = []
        for child in non_ace_pilots
          hash = {}
          for col in PilotPotentials
            hash[col] = child.evaluate(col)
          end
          result << EvalData.new(child.name , hash)
        end
        result
      end

      def ace_items
        result = []
        for child in aces
          hash = {}
          for col in StandardAbilities
            hash[col] = child.evaluate(col)
          end
          result << EvalData.new(child.name , hash)
        end
        result
      end

      def sub_evals(evaluator)
        result = {}
        items = sub_evals_items(evaluator) 
        #自動成功、ないし自動失敗の場合
        if auto = items.find{|sp|sp.class <= BonusSpec && sp.is_string?}
          evaluator.args.each{|arg| result[arg] = auto.bonus}
          return  result
        end
#        items.each{|item| sum_bonus += item.bonus.to_i}
        evals_var = evals_org
        for arg in evaluator.args
#          sum_bonus = 0
#          ev = evaluate(arg)
          if is_overridden?(arg)
            result[arg] = self.evaluate(arg)
          else
            ev = evals_var[arg]
  #          evis = items
            items.each do |evi|
              ev += evi.value(arg).to_i
            end
            result[arg] = ev
          end
        end
        result
      end

      def evaluate(key)
        value = evals_org[key].to_i
        if PilotPotentials.include?(key)
          potents = pilot_potentials
          value += potents[key].to_i
        end

        value = (aces.collect{|ace|ace.evaluate(key)} + [value]).max if is_standard_ability?(key)
        
        target_bonuses = evaluate_items()
        target_bonuses.each{|sp|
          if sp.is_applicable?(self,key)
            value += sp.bonus.to_i
            sp.use if sp.class <= BonusSpec
          end
        }
        value
      end

      def sub_evals_items(evaluator)
        result = evaluate_items(evaluator) + super
        
        max_items = []
        for arg in evaluator.args
          max_ceis = []
          max_value = -9999
          next if is_overridden?(arg)
          next unless PilotPotentials.include?(arg)
          for c in non_ace_pilots
            ceis = c.sub_evals_items(evaluator).delete_if{|sp|
              (sp.class <= BonusSpec && sp.is_judge_bonus?) ||
                !is_tqr_qualified?(sp,evaluator) ||
                c.is_evaluate_item?(sp)
              }
            child_evaluate  = c.evaluate(arg)
            value = child_evaluate + ceis.collect{|cei|cei.bonus}.compact.sum
            if value > max_value
              max_value = value
              max_ceis = [EvalData.new("#{c.name}:#{arg}:#{child_evaluate}" , {arg => child_evaluate})] + ceis
            end
          end
          max_items |= max_ceis
        end
        max_items | result
      end

      def calculate(ev)
        update_situation
        current_situation.calculation_cache[ev] ||= begin
          if ev.is_branch?(self)
            calc =  self.crew_division.calculate(ev)
            calc.unit_type = ['搭乗']
            apply_self_bonus(calc,ev)
            return calc
          end
          calculation = ev.create_calculation(self)
          items = self.sub_evals_items(ev)
          if auto = items.find{|sp|sp.class <= BonusSpec && sp.is_string?}
            calculation.total = auto.bonus
            calculation.operands = [auto]
            calculation.is_string = true
            for arg_name in ev.args(self)
              arg = ev.create_calculation(self)
              arg.base_value = auto.bonus
              arg.arg_name = arg_name
              arg.operands = [auto]
              calculation.arguments[arg_name] = arg
            end
          else
            for arg_name in ev.args(self)
              arg = ev.create_calculation(self)
              arg.base_value = if is_overridden?(arg_name)
                self.evals[arg_name]
              else
                self.evals_org[arg_name]  
              end
              arg.arg_name = arg_name
              ops = items.select{|item|item.value(arg_name).to_i != 0}
              if ev.is_action_bonus?
                ops |= self.all_bonuses.select{|sp|
                 (sp.is_judge_bonus? && ev.match?(sp))
                }
              end
              arg.operands = ops
              calculation.arguments[arg_name] = arg
            end
          end
          calculation.operands = []
          calculation
        end
      end

      def specialties
        update_situation
        current_situation.specialties ||= begin
          
          results = super
          childrens_sps = children.collect{|c|
            c.actions.keys.collect{|key| c.actions[key]}.flatten
          }.flatten.compact
          vehicle_action_names = results.select{|sp|
            sp.spec_type == '行為' || sp.spec_type == '行為補正'
          }.collect{|sp|
            sp.action_name
          }

          for v_name in vehicle_action_names
            results += childrens_sps.select{|sp|
              sp.action_name == v_name && (is_unit_type_match?(sp))
            }
          end

          pilot_actions = children.collect{|c|c.actions_flatten}.flatten
          pilot_actions = pilot_actions.select{|sp| is_unit_type_match?(sp)}
          pilots_available = @pilots.team_action_names
          pilots_available_sp = pilot_actions.select{|sp|pilots_available.any?{|name|name == sp.action_name}}
          pilots_only_actions = pilots_available_sp.select{|sp|vehicle_action_names.all?{|v_name| v_name != sp['行為名'] }}

          #行為名＞tqr_id変換。ついでに水中●距離戦闘行為＞水中●距離戦闘補正＆●距離戦闘補正有効化のためのトリック
          action_name_keys = self.self_tqrs

          p_bonuses = children.collect{|c|
            c.specialties.select{|sp| self.is_fullboosted_sp?(sp) }
          }.flatten.dup
          results | pilots_only_actions | p_bonuses
        end
      end

      def is_fullboosted_sp?(sp)
        keys = self.tqrs
        sp.spec_type == '補正' &&
          sp.target == self &&
            (self.is_tqr_qualified?(sp) ||
               keys.include?(sp.tqr_requirement)
            )
      end

      def synchronizer(ev)
        update_situation
        current_situation.calculation_cache[ev] ||= begin
          if self.children.size == 0
            calc = ev.create_calculation(self)
            calc.rd = nil
            return calc
          end
          p_sync = primary_synchronizer(ev)
          if p_sync.total.class == String
            p_sync
          else
            super
          end
        end
      end
    end
  end
end
