# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator
    module TqrManager
      def num_of_action_owner(action_name)
        soldiers_action_names = children.collect{|soldier| soldier.action_names}.flatten
        return soldiers_action_names.select{|name| name == action_name}.size
      end

      def num_of_members(ev = nil)
        members_without_attendants(ev).size
      end

      def team_action_names
        update_situation
        current_situation.team_action_names ||= begin
          soldiers_action_names = members_without_attendants.collect{|soldier| soldier.action_names}.inject([]){|array,item| array|item}
          soldiers_action_names.select{|action_name| is_action_available?(action_name) }
        end
      end

      def members_without_attendants(ev = nil)
        self.members_without_attendants_cache[ev && ev.cache_id] ||= begin
          target_set = if ev 
            ev.target_set(self).select{|c|ev.are_applicable_between(c)}
          else
            self.children
          end
          target_set.select{|c| !c.is_attendant?}
        end
      end

      def team_tqrs(ev = nil)
        update_situation
        current_situation.team_tqrs[ev && ev.cache_id] ||= begin
          soldiers_tqr = members_without_attendants(ev).collect{|soldier| soldier.tqrs}.flatten
          hash = Hash.new{|h,k|h[k]=0}
          soldiers_tqr.each{|tqr_id| hash[tqr_id] += 1}
          result = hash.keys.select{|key| (hash[key].to_f / num_of_members(ev))>= 0.75}
          result = result | tqr_appendance(result)
        end
      end

      def action_names
        update_situation
        current_situation.action_names ||= begin
          self_action_names | team_action_names
        end
      end

      def is_action_available?(action_name)
        if (num_of_action_owner(action_name).to_f / num_of_members) >= 0.75
          return true
        else
          sps =  children.collect{|c|c.actions_flatten}.inject([]){|array,item|
            array | item
          }.select{|sp|
            sp.action_name == action_name
          }
          return sps.find{|sp| sp['付記'] =~ /７５％制限無視/o}
        end
      end

      def are_all_members_npc?
        @all_members_are_npc ||= !!children.all?{|c|
           c.class == Soldier && !c.is_qualified_as_leader?
        }
      end
      
      def primary_synchronizer(ev,members = children , add_bonus = true)
        all_members = if (self.class <= Troop && self.formation == :fleet) || self.class == Vessel
          self.crew_division.children
        else
          members
        end
        all_members_are_npc = all_members.all?{|c| c.class == Soldier && ((c.is_npc? || c.is_attendant?) && !c.is_named_npc?)}
        max_value = -9999
        sync_members = []
        max_calc = nil
        for c in members #all_members
          calc = c.synchronizer(ev)
          next if calc.is_na?
          max_calc ||= calc
          member0 = calc.first_member
          next if member0.class == Soldier && ((member0.is_npc? || member0.is_attendant?) && !member0.is_named_npc?) && !all_members_are_npc

          value = calc.total
          calc.unit = calc.first_member if value.class == String
          case value
          when '自動成功' , '自動失敗'
            if max_value == value
              sync_members << calc
            elsif max_value.class <= Integer || value == '自動失敗'
              max_value = value
              max_calc = calc
              sync_members = [calc]
            end
          else
            next if max_value.class == String
            if max_value < value
              sync_members = [calc]
              max_value = value
              max_calc = calc
            elsif max_value == value
              sync_members << calc
            end
          end
        end

        scalc = SynchronizeCalculation.new(self,ev) # , modified_value )
        scalc.children.concat sync_members
        scalc.is_string = true if max_value.class == String
        scalc.arguments['外見'] = max_calc
        if  max_value.class != String && add_bonus
          e_items = self.evals_items.select{|item|item.has_key?('外見')}
          e_items += self.evaluate_items.select{|item|item.is_applicable?(self,'外見')}
          eval_datas = e_items.collect{|item|EvalData.new(item.name , item.value('外見').to_i)}
          eval_datas += self.select_bonuses(ev,'外見')
          opt_bonus_value = optional_bonus(ev)
          if !opt_bonus_value.nil?
            unit_type_name = case self
            when Troop
              '（部隊補正）'
            when Division
              '（分隊補正）'
            when Vessel , Vehicle
              '（ユニット補正）'
            else
              '（謎の補正）'
            end
            eval_datas << EvalData.new(unit_type_name , opt_bonus_value)
          end
          children_operands = eval_datas.dup
          if max_calc
            children_operands.delete_if{|item|!max_calc.arguments['外見'].operands.find{|bonus|bonus.object_id == item.object_id}.nil?}
            children_operands = max_calc.operands + children_operands
            max_calc.reset
            max_calc.operands = []
          end
          children_operands.each{|item|
            item.use if item.class <= BonusSpec
          }
          scalc.operands = children_operands
        else
          max_calc.operands = [] if max_calc
          scalc.operands = []
        end
        scalc
      end

      def is_tqr_qualified?(target_sp,ev = nil)
        sp_class = target_sp.class
        tqr_id = case 
        when sp_class <= Specialty
          if target_sp.is_tqr?
            target_sp.tqr_requirement
          else
            return true
          end
        when sp_class <= String
          target_sp.intern
        when sp_class <= Symbol
          target_sp
        end
        cache_key = [tqr_id , ev && ev.cache_id]
        update_situation
        unless current_situation.tqr_qualified_cache.has_key?(cache_key)
          current_situation.tqr_qualified_cache[cache_key]  = begin
            all_tqrs = self.self_tqrs | self.team_tqrs(ev)
#            all_tqrs.include?(tqr_id)
            normal_tqr_required = all_tqrs.include?(tqr_id)
            action_bonus_tqr_required = begin
              judge_name = tqr_id.to_s.sub(/補正$/,'')
              action_name = judge_name + '行為'
              !CommonActions.include?(judge_name) && all_tqrs.include?(action_name.to_sym)
            end
            normal_tqr_required || action_bonus_tqr_required

          end
        end
        current_situation.tqr_qualified_cache[cache_key]
      end

      def tqrs
        update_situation
        current_situation.tqrs ||= (self.self_tqrs | self.team_tqrs)
      end

    end

    module Team
      include TqrManager
      include ILanguage::Calculator
      
      def evals
        update_situation
        current_situation.evals ||= evals_org
      end

      def evals_keys
        @evals_keys ||= begin
          result = super
          for child in self.children
            result |= child.evals_keys
          end
          result.to_a
        end
      end

      def evals_org
        soldier_eval = {}
        soldier_rd = Hash.new{|hash,key|hash[key]={}}
        sum_rd = Hash.new{|hash,key| hash[key]=0}
        initial_rd = children.size == 0 ? 1 : 0
#        StandardAbilities.each{|c|
#          sum_rd[c]= initial_rd
#        }
        for soldier in children
          keys = soldier.evals_keys
          soldier_eval[soldier]={}

          for col in keys
            val = soldier_eval[soldier][col] = soldier.evaluate(col)
            rd = soldier_rd[soldier][col] = to_rd(val)
            sum_rd[col] += rd
          end
        end

        result = {}
#          begin
          for col in sum_rd.keys
            result[col] = to_eval(sum_rd[col])
          end
#          rescue => e
#            raise "evals:#{sum_rd.inspect},col=#{col}:#{e.to_s}"
#          end
        evl = Evaluation.new(nil,self)
        evl.evals_hash = result
        for evaluation in self.evaluations.select{|ev| ev.keys.size > 0}
          evl.add(evaluation)
        end
        apply_personal_bonuses(evl)
        evl
      end

      def calculate(evaluator)
        return nil if evaluator.args.size == 0
        update_situation
        current_situation.calculation_cache[evaluator] ||= begin

          calc = evaluator.create_calculation(self)

          for child in evaluator.target_set(self)
            if evaluator.are_applicable_between(child)
              c_ev = evaluator.chained_evaluator(child)
              child_calc = child.calculate(c_ev)
            else
              child_calc = Calculation.new(child,evaluator)
              child_calc.rd = 0
            end
            child_calc.parent = calc
            calc.add(child_calc)
          end

          if evaluator.is_parent_bonus_enabled? && !evaluator.is_action_bonus?
            target_bonuses = ability_bonuses().dup
            target_bonuses += self.parent.ability_bonuses() if self.parent
            for arg_name in calc.arg_names
              calc.arguments[arg_name].operands = target_bonuses.select{|sp|
                evaluator.match?(sp) && calc.is_unit_type_match?(sp)
#                (sp.bonus_targets.include?(arg_name) || sp.bonus_targets[0] == '全能力') && calc.is_unit_type_match?(sp)
              }
              calc.use
              if @personal_bonuses
                for item in [arg_name ,'全能力']
                  if @personal_bonuses.has_key?(item)
                    next if @personal_bonuses[item] == 0
                    calc.arguments[arg_name].operands <<  EvalData.new("部隊補正:#{item}" , @personal_bonuses[item])
                  end
                end
              end
            end
          end
          apply_self_bonus(calc,evaluator)
        end
      end

      def self_bonus_caption
        "（部隊補正）"
      end


      def ar
        members = if children.any?{|c|c.passenger_type == :rider} && children.any?{|c|c.passenger_type == :fellow}
                    children.select{|c|c.passenger_type == :rider}
                  else
                    children
                  end
        members.collect{|c|c.ar || 10 }.min
      end
    end
  end
end
