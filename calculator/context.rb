# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator

    class UnitHash < Hash
      def [](name)
        result = super
        if result.nil? && name =~ /([0-9A-Za-z_]{2}-[0-9A-Za-z_]{5}-[0-9A-Za-z_]{2})(:|：|_|＿)(.+)/
          name_only = $3
          no_and_name = $1 + '_' + $3
          case
          when self.has_key?(no_and_name)
            super(no_and_name)
          when self.has_key?(name_only)
            super(name_only)
          else
            result
          end
        else
          result
        end
      end
    end
    
    class Situation
      attr :container , true
      attr :name , false

      attr :conditions , false
      attr :disabled_sps , false
      attr :partial_conditions , false
      attr :flexible_bonus , false
      attr :idress_appendants , false
      attr :idress_exceptions , false
      attr :containments , false
      attr_reader :variables

      CacheNames = [
        :all_conditions_cache ,
        :all_disabled_sps_cache ,
        :all_partial_conditions_cache ,
        :all_flexible_bonus_cache ,
        :all_idresses_cache ,
        :all_containments_cache ,

        :specialties ,
        :libraries ,
        :placements ,
        :evaluations ,
        :action_names ,
        :tqrs ,
        :self_tqrs ,
        :self_action_names , 
        :ability_bonuses ,
        :judge_bonuses ,
        :all_bonuses ,
        :weapon_names ,
        :weapon_classes ,
        :etc ,
        :evaluators ,
        :evals ,
        :sp_include_cache ,
        :evaluate_cache ,
        :evaluate_items ,
        :all_bonuses_cache ,
        :no_sp_cache ,
        :is_evaluate_item_cache ,
        :used_sp_cache ,
        :tqr_qualified_cache ,
        :team_action_names ,
        :team_tqrs ,
        :calculation_cache ,
        :general_cache ,
        :resource_modifications_cache
      ]

      for sym in CacheNames
        attr sym , true
      end

      def initialize(name)
        @conditions = ['']
        @disabled_sps = []
        @partial_conditions = []
        @flexible_bonus = {}
        @idress_appendants = []
        @idress_exceptions = []
        @containments = []
        @variables = {}
        @name = name

        init_caches
      end

      def init_caches
        @sp_include_cache = {}
        @evaluate_cache = {}
        @no_sp_cache = {}
        @used_sp_cache = []
        @tqr_qualified_cache = {}
        @calculation_cache = {}
        @is_evaluate_item_cache = {}
        @general_cache = {}
        @team_tqrs = {}
        @evaluate_items = {}
      end

      def reset_caches
        for sym in CacheNames
          instance_variable_set('@'+sym.to_s , nil)
        end
        init_caches
      end

    end

    module SituationContainer
      include ILanguage::FCalc::ContextModule
      attr_accessor :current_situation_name , :default_situation , :current_situation
      attr_accessor :situations , :situation_names , :situation_stack
      attr_accessor :swapped_situation_name
      attr_accessor :ref_count
      DefaultSituationName = '（基本）'

      def initialize_situations
        self.situations = Hash.new{|hash,name|
          hash[name] = Situation.new(name)
        }
        self.default_situation = self.situations[DefaultSituationName]
        self.current_situation = self.default_situation.dup
        self.current_situation_name = DefaultSituationName
        self.situation_names = [DefaultSituationName]
        self.situation_stack = [DefaultSituationName]
        self.swapped_situation_name = nil
        reset_situation_caches()
      end

      for prop_name in [:flexible_bonus , :partial_conditions , :disabled_sps , :conditions  , 
          :idress_appendants , :idress_exceptions , :containments]
        class_eval( %Q{
          def #{prop_name.to_s}(situation_name = self.context.current_situation_name)
            self.situations[situation_name].#{prop_name.to_s}
          end
          } , __FILE__ , 107)
      end
      for prop_name in [:partial_conditions , :disabled_sps , :conditions , :containments]
        class_eval(%Q{
          def all_#{prop_name.to_s}(situation_name = self.context.current_situation_name)
            change_situation_to(situation_name) if self.current_situation_name != situation_name
            unless current_situation.all_#{prop_name.to_s}_cache
              result = []
              stacks = (self.context.situation_stack + self.situation_stack).uniq
              situations = stacks.collect{|s_name| self.situations[s_name]}
              for situation in situations
                result |= situation.#{prop_name.to_s}
              end
              result |= self.parent.all_#{prop_name.to_s}(situation_name) if self.parent
              result |= self.context.all_#{prop_name.to_s}(situation_name) if self.class <= Troop
              current_situation.all_#{prop_name.to_s}_cache = result
            end
            current_situation.all_#{prop_name.to_s}_cache
          end
          },__FILE__ , 114)
      end

      def all_idresses(situation_name = self.context.current_situation_name)
        change_situation_to(situation_name) if self.current_situation_name != situation_name
        unless current_situation.all_idresses_cache
          result = self.idresses.dup
          stacks = (self.context.situation_stack + self.situation_stack).uniq
          sit_ary = stacks.collect{|s_name| self.situations[s_name]}
          exps = []
          for situation in sit_ary
            result += situation.idress_appendants
            exps += situation.idress_exceptions
          end
          for exp_name in exps
            index = result.index(exp_name)
            result.delete_at(index) if index
          end
          current_situation.all_idresses_cache = result
        end
        current_situation.all_idresses_cache
      end

      def all_flexible_bonus(situation_name = self.context.current_situation_name)
        unless current_situation.all_flexible_bonus_cache
          result = {}
          stacks = (self.context.situation_stack + self.situation_stack).uniq
          situations = stacks.collect{|s_name| self.situations[s_name]}
          for situation in situations
            result.update(situation.flexible_bonus)
          end
          if self.parent
            result.update(self.parent.all_flexible_bonus(situation_name))
          end
          current_situation.all_flexible_bonus_cache = result
        end
        current_situation.all_flexible_bonus_cache
      end


      def reset_situation_caches
        current_situation.reset_caches
      end

      def change_to_default_situation(division)
        name = division.name
        self.situation_stack = if self.class == Context
          [DefaultSituationName ]
        else
          [name , DefaultSituationName]
        end
        if self.class <= Troop
          for c in self.children
            c.change_to_default_situation(division)
          end
          #            self.context.change_to_default_situation(division)
        end
      end

      def change_situation_for_register(name , div_name_flag = false)
        self.situation_names << name unless div_name_flag
        self.situation_stack = [DefaultSituationName , name]
      end

      def change_situation_to(name ,name_register_flag = true)
        self.situation_names << name if name_register_flag && !self.situation_names.include?(name)
        change_to_default_situation(self.troop)
        if ! [DefaultSituationName , self.troop.name].include?(name)
          push_situation(name)
        end
      end

      def push_situation(name , flag = true)
        self.situation_stack.push(name)
      end

      def pop_situation
        self.situation_stack.pop if self.situation_stack.size > 1
      end
      
      def update_situation
        if self.context.current_situation_name != self.current_situation_name
          change_situation_to(self.context.current_situation_name)
        end
      end

      def current_situation
        self.situations[self.situation_stack[-1]]
      end

      def swapped_situation
        self.swapped_situation_name ? self.situations[self.swapped_situation_name] : self.current_situation
      end

      def current_situation_name
        #        self.current_situation.name
        self.situation_stack[-1]
      end

      def variable(name)
        update_situation
        stacks = (self.context.situation_stack + self.situation_stack).uniq.reverse
        situations = stacks.collect{|s_name| self.situations[s_name]}
        value = nil
        situations.each do |s|
          if s.variables[name]
            value = s.variables[name]
            break
          end
        end

        return value if value
        self.parent && self.parent.variable(name)
      end

      def variables
        update_situation
        self.current_situation.variables
      end
    end

    class Context
      include SituationContainer

      include ILanguage::Calculator::Convertible

      attr :troop,true
      attr :definitions , false
      attr :warnings , false
      attr :particular_evaluators , false
      attr :particular_situation_names , false
      attr :units , false
      attr :tags , false
      attr :evaluator_order , true
      attr :evaluators , false
      attr :derrived_evaluator_count , false

      attr_reader :statements , :fcalc_parser
      attr_accessor :no_optional_evaluators
      attr_reader :default_abilities_evaluator
      attr_reader :converted_command_resources
      attr_reader :converted_specialties
      attr_reader :converted_specialty_names

      def initialize(revision = nil)
        reflesh()
        @definitions = Definitions.new(:activerecord => IDefinition , :revision => revision)
        @evcc_parser = EVCCParser.new
        @sp_parser =  EVCCParser.new
        @fcalc_parser =  ILanguage::FCalc::FCalcFormatParser.new
        @warnings = []
        @particular_evaluators = []
        @evaluator_order = Evaluator.order
        @evaluators = Evaluators.dup
        @evaluators.each_pair do |key,value|
          @evaluators[key] = value.clone
        end
        @particular_situation_names = []
        @libraries = {}
        @derrived_evaluator_count = {}
        @tags = {}
        @variables = {}
        @statements = []
        @derive_evaluator_cache={}
        @no_optional_evaluators = []
        
        @unit_list = {}
        
        @unit_order = 1
        
        @default_abilities_evaluator = AbilitiesEvaluator.new
        AbilitiesEvaluator.initialize_values(@default_abilities_evaluator ,  '一般評価' , nil , nil ){}
        @abilities_evaluators = []

        @converted_command_resources = []
        @converted_specialties = {}
        @converted_specialty_names = []
      end
      
      def abilities_evaluators
        @abilities_evaluators.dup
      end

      def add_abilities_evaluators(evs)
        @abilities_evaluators.concat(evs)
      end
      def find_evaluator_by_action_name(action_name)
        result = @evaluators.values.select{|e|
          puts @evaluators.values.inspect if e.class == Array or e.class == String
          e.action_name == action_name && e.optional == false
        }
        if result.size == 0
          ev = Evaluator.new
          ev.display_name = ev.name = action_name + '（未定義）'
          ev.action_name = action_name
          ev.bonus_filter = nil
          ev.args = []
          return [ev]
        end
        result
      end
      
      def common_abilities_evaluators
        %w[装甲（通常） 魅力 移動（通常）].collect{|name|@evaluators[name]}
      end

      def parent
        nil
      end

      def reflesh
        @troop = nil
        @warnings = []
        @units = UnitHash.new
        #        @variables = {}
        #        @statements = []
        @particular_evaluators = []
        @abilities_evaluators = []
        initialize_situations()
      end

      def register(unit)
        unless @units.has_key?(unit.name)
          @units[unit.name]=unit 
        end
        unless @unit_list.has_key?(unit)
          @unit_list[unit] = unit.name
          unit.order = @unit_order
          @unit_order += 1
        end
        
        if defined?(unit.character_no) && unit.character_no != nil
          key = unit.character_no + '_' + unit.name
          @units[key]=unit unless @units.has_key?(key)
        end

        for tag in unit.tags
          @tags[tag] ||= []
          @tags[tag] << unit
        end

      end

      def context
        self
      end

      def failure_reason
        @evcc_parser.failure_reason
      end

      def not_founds
        @definitions.not_founds
      end

      def parse_library(node)
        sp_text = node.specialty_text
        if sp_text.nil?
          error_reason = "定義パッチ内のｔ：特殊を解析できません。\n／＊／\n#{node.text_value}\n／＊／"
          raise error_reason
        end
        sp_node = SpecialtyParser.parse(node.specialty_text)
        if sp_node.nil?
          raise SpecialtyParser.failure_reason
        end
        hash = node.to_hash
        sp_hash = sp_node.specialty_defs.collect{|sp| sp.to_hash}
        if sp_hash.nil?
          raise "定義パッチ内のｔ：特殊が正しく記述されていません。\n／＊／\n#{sp_text}\n／＊／"
        end
        hash['特殊定義']= sp_hash
        @definitions[node.name]=hash
      end

      def parse_specialty_patch(node)
        library_name = node.library_name
        definition  = @definitions[library_name]
        raise "ライブラリ定義 '#{library_name}' が存在しません。" if definition.nil?
        source = definition['原文']
        re = /#{node.sp_name.text_value}.+/
        if source =~ re
          source.sub!(re , node.sp_def_source)
        else
          re = /ｔ：特殊　＝　｛.+/
          new_source = ''
          lines = source.each_line.to_a
          until (line = lines.shift) =~ /ｔ：特殊　＝/
            new_source << line
          end
          new_source << line

          re = /(　|\s+)*＊#{library_name}/
          while (line = lines.shift) =~ re
            new_source << line
          end
          new_source << ('　　' + node.sp_def_source + "\n")
          new_source << line
          until lines.size == 0
            new_source << lines.shift
          end
          source = new_source
        end
        root = @sp_parser.parse(source)
        if root.nil?
          raise "特殊定義パッチが正しく記述されていません。\n#{@sp_parser.failure_reason}\n／＊／\n#{source}\n／＊／"
        end
        lib = root.libraries[0]
        parse_library(lib)
      end

      def parse_troop(source)
        root = if source.class == String
          @evcc_parser.parse(source)
        else
          source
        end
        raise @evcc_parser.failure_reason unless root
        self.reflesh
        @definitions.reflesh
        @warnings = root.check_warning()
        @troop = Troop.new(self)
        @troop.name = '本隊'
        register(@troop)
        @units['【本隊】']=@troop
        self.variables['コスト計上分隊'] = ['本隊']

        for node in root.libraries
          parse_library(node)
        end

        for node in root.specialties
          parse_specialty_patch(node)
        end

        nodes = [:variables , :troop_members , :divisions , :evaluator_definitions , :resourcifies ].collect{|name|
          root.send(name)
        }.flatten

        
        sts = root.statements
        asigns , sts = sts.partition{|st|st.extension_modules.include?(ILanguage::FCalc::AsignmentNode)}
        directives =root.directives
        directive_and_asigns = directives  + asigns
        nodes += directive_and_asigns.sort_by{|node|node.interval.first}
        nodes.each{|node| node.interpret(self)}

        sts.each do |st|
          stmt = st.interpret(self)
          stmt.source = st.text_value if defined?(stmt.source)
          self.statements << stmt
        end
        

        change_situation_to(@situation_names[0])
        reset_situation_caches()

        for div in [troop,troop.divisions].flatten
          div.change_to_default_situation(div)
          context.variables['コスト計上分隊'] |= [div.name] if context.variables['多兵科編成']
        end
        
        check_rule_violation(troop)
        init_predefined_variables()
        init_derived_evaluators_for_option_items(root , troop) if self.variable('オプション装備数での評価派生処理')
        troop.node = root
        if self.definitions.warnings.keys.size > 0
          @warnings  += self.definitions.warnings.collect{|k| k[1].collect{|msg|k[0]  + '：' + msg} }.flatten
        end

        troop
      end

      def check_rule_violation(troop)
        if troop.children.size == 0 && troop.idresses.size == 0 && !troop.expr
          raise '部隊にユニットが存在しません。'
        end
        for c in  units.values
          if !(c.class <= Troop) && c.all_idresses.all?{|name|self.definitions[name].nil?}
            raise "ユニット#{c.name}に有効なアイドレス名が一つも指定されていません。"
          end
        end

        #ACE込み編成で降車分隊を定義している
        if troop.characters.any?{|c|c.is_ace?}

          #if
        end
      end

      DefaultValuesOfPredefinedVariables = {
        '随行ユニットが提出可能な評価分類' => ['装甲','情報戦防御'] ,
        'オプション装備数での評価派生処理' => true ,
        '一般評価派生での差分値表示' => false
      }
      def init_predefined_variables
        for key in DefaultValuesOfPredefinedVariables.keys
          self.variables[key] = case
          when  self.variables[key].nil?
            DefaultValuesOfPredefinedVariables[key]
          when  self.variables[key].class <= ILanguage::FCalc::Functor
            self.variables[key].evaluate
          else
            self.variables[key]
          end
        end
        self.variables.update(ILanguage::FCalc::BuiltinFunctions)
      end

      def select_related_evaluators(troop,sp)
        troop.self_evaluators.select{|ev|ev.condition_match?(sp)}
      end

      def disable_action_of_optional(optional , evs)
        return if (definition = @definitions[optional]).nil?
        return if (action = definition['特殊定義'].find{|sp|sp['特殊種別']=='行為'}).nil?

        action_sp = Specialty.new(self , action ,optional )
        related_evs = evs.select{|ev|ev.action_name == action['行為名']}
        return if related_evs.size == 0
        ev = related_evs.last
        related_evs.each do |ev|
          ev.particular_situation_name = "#{optional}無効"
        end
        @no_optional_evaluators |= related_evs
        begin
          current_situation = self.current_situation
          self.change_situation_to(ev.particular_situation_name , false)
          self.current_situation.disabled_sps.replace(self.current_situation.disabled_sps| [Specialty.name(action_sp['定義'])])
        ensure
          self.change_situation_to(current_situation , false)
        end
      end

      def init_derived_evaluators_for_option_items(root , troop)
        options = root.select_nodes(VehicleOptionDef)
        return if options.size == 0

        last_situation_name = self.current_situation_name
        disabled_idresses = {}
        disable_re = []
        disabled_evs = []
        for vehicle in troop.children.select{|c|c.class <= Vehicle}
          for optional in vehicle.optional_equipments.keys

            definition  = @definitions[optional]
            next if definition.nil?
            sps = definition['特殊定義'].select{|sp|sp['特殊種別']=='補正'}.collect{|sp|
              BonusSpec.new(self,sp , optional)
            }

            evs = sps.collect{|sp| select_related_evaluators(troop,sp)}.flatten
            num_of_optionals = vehicle.optional_equipments[optional]
            for num_of_fire in 1..num_of_optionals
              s_name = "#{optional}使用数#{num_of_fire}"
              derive_evaluators(s_name ,evs.collect{|ev|ev.name} , true)
              exceptions = [optional] * (num_of_optionals - num_of_fire)
              vehicle.idress_exceptions.concat(exceptions)
            end
            disabled_idresses[optional] ||= begin
              disabled_evs |= evs
              true
            end
          end
        end
        disable_re = disabled_idresses.keys.collect{|key|/＊#{key}の.+?　＝　/}
        disabled_idresses.keys.each do |optional|
          disable_action_of_optional(optional,disabled_evs)
        end
        disabled_evs.each do |ev|
          Evaluator.init_alternatives(ev)
          ev.disabled_sps.replace(ev.disabled_sps | disable_re)
          #          ev.is_for_optional_equips = true
        end

        self.change_situation_to(last_situation_name , false)
      end

      def create_evaluation(name,owner)
        Evaluation.new(@definitions[name],owner)
      end

      def create_spec(owner,idress_name,for_ace = false)
        idef_hash = @definitions[idress_name]
        return [] if idef_hash.nil?
        spec_hashs = idef_hash['特殊定義']

        spec_hashs.collect{|sp|
          klass = case sp['特殊種別']
          when '補正'
            sp['補正対象'][0] == '任意の能力' ? FlexibleBonusSpec : BonusSpec
          when '初期ＡＲ修正'
            ARModifierSpec
          when '消費修正'
            ResourceModificationSpec
          when '行為制約'
            ActRestrictionSpec
          else
            Specialty
          end
          sp = klass.new(owner,sp,idress_name)
          sp.for_ace = for_ace
          sp
        }
      end

      def create_library(name)
        @libraries[name] ||= begin
          idef_hash = @definitions[name]
          idef_hash && Library.new(idef_hash)
        end
      end

      def for_each_unit(names)
        for name in names
          name = name.text_value if name.class < Treetop::Runtime::SyntaxNode
          unit = @units[name]
          yield unit if unit
        end
      end

      def is_sp_used?(sp)
        current_situation.used_sp_cache.include?(sp.name.intern)
      end

      def push_particular_situation(s_name)
        current = current_situation_name
        push_situation(s_name , false)
        push_situation(s_name + '@' + current , false)
      end

      def pop_particular_situation
        pop_situation
        pop_situation
      end

      def create_clone(ev,s_name)
        @derive_evaluator_cache[[ev.name,s_name]] ||= begin
          ev_clone = ev.clone
          ev_clone.particular_situation_name = s_name
          derrived_source = if ev_clone.name =~ /^（/
            ancestor = ev
            begin
              ancestor = ancestor.ancestor
            end until ancestor.is_predefined? || ancestor.nil?

            ancestor
          else
            ev_clone
          end

          ev_clone.name = "（#{s_name}）#{derrived_source.name}"
          ev_clone.display_name = "（#{s_name}）#{derrived_source.display_name}"

          ev_clone.ancestor = ev
          ev_clone.predefined = false
          ev_clone.implicitly = false

          #          if ev.alternatives[0].object_id == ev.object_id
          #            ev_clone.alternatives[0] = ev_clone
          #          else
          #            ev_clone.alternatives[0] = ev.alternatives[0].clone
          #          end

          self.derrived_evaluator_count[ev.name] ||= ev.represent_order + 20
          self.derrived_evaluator_count[ev.name] += 1
          ev_clone.represent_order = self.derrived_evaluator_count[ev.name]
          Evaluator.init_alternatives(ev_clone)

          ev_clone
        end
      end

      def select_ancestors(e_name)
        return [self.default_abilities_evaluator] if e_name == '一般評価'
        e = self.evaluators[e_name] || (self.particular_evaluators + self.abilities_evaluators).find{|pev|pev.name == e_name}
        evs = if e.nil?
          self.evaluators.values.select{|ev|(ev.tags.include?(e_name) || ev.class_name == e_name)&& !ev.optional }.sort_by{|ev|ev.represent_order}
        else
          [e]
        end
        raise "評価名'#{e_name}'が存在しません。"  if evs.size == 0
        evs
      end

      def derive_evaluators(s_name , ev_names,abbrev = false)
        #        s_name = self.name
        self.change_situation_to(s_name , false)
        result = []
        for e_name in ev_names
          evs = select_ancestors(e_name)
          ev_clones = evs.collect{|ev| self.create_clone(ev , s_name)}
          if abbrev
            ev_clones.each do |ev|
              ev.display_name = "（#{s_name}）"
              ev.is_for_optional_equips = true
            end
          end
          standards , others = ev_clones.partition{|ev| ev.class <= AbilitiesEvaluator}
          self.particular_evaluators.concat(others)
          self.add_abilities_evaluators(standards)
          #          end
          result += evs
        end
        self.particular_situation_names << s_name
        result
      end

      def hash_for_api
        {'not_found' => not_founds.dup , 'warnings' => @warnings.dup}.merge(self.troop.hash_for_api)
      end

      def troop_hash
        @troop_hash ||= hash_for_api
      end
    end
  end
end