# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__) , 'crew_division.rb')

module ILanguage
  module Calculator
    class Vessel < ContainerUnit
      include Team
      include CrewDivisionFactory

      def initialize(context)
        super
        @unit_type |= ['歩兵' , '歩兵武装']
      end
      
      def evaluate(col)
        update_situation
        current_situation.evaluate_cache[col] ||= begin
          is_standard = is_standard_ability?(col)
          if is_standard
            crew_division.evaluate(col)
          else
            value = evals[col].to_i
            target_bonuses = evaluate_items()
            target_bonuses.each do |sp|
              next if is_standard && sp.is_ability_bonus?
              if sp.is_applicable?(self,col)
                value += sp.bonus.to_i
                sp.use if sp.class <= BonusSpec
              end
            end
            value
          end
        end
      end

     def dup
        clone = super
        clone.crew_division_cache = nil
        clone.crew_division_getted_off_cache = nil
        @placements = nil
        clone
      end

      def calculate(ev)
        update_situation
        current_situation.calculation_cache[ev] ||= begin
          #艦船の評価以外はpilots_division へ移譲
          if self.troop.evals.has_key?(ev.class_name)
            self_calculate(ev)
          else
            calc = @pilots.calculate(ev)
            apply_self_bonus(calc,ev)
          end
        end
      end

      def is_evaluate_item?(sp)
        self_keys = self.evals_keys
        self_keys.any?{|key| sp.is_applicable?(self,key)}
      end

      def specialties
        current_situation.specialties ||= begin
          specs = super
          for c in self.crew_division.children #children #_origin
            specs += c.judge_bonuses.select{|sp|is_unit_type_match?(sp) && self.crew_division.is_tqr_qualified?(sp)}
            specs += c.ability_bonuses.select{|sp|is_unit_type_match?(sp) && sp.bonus_targets[0] == '全能力' && self.crew_division.is_tqr_qualified?(sp)}
          end
          specs
        end
      end

      def children
        crew_division.children
      end

      def placements
        unless @placements
          placements_sp =  @context.create_spec(self,self.idresses[0])
          @placements = placements_sp.collect{|sp|sp['位置づけ']}.flatten
        end
        @placements
      end

      def default_partial_conditions #の乗り物の艦上で活動する場合
        super + placements.collect{|ca| /位置づけ（#{ca}）の乗り物(に搭乗|の艦上で活動)/}
      end

      alias :super_primary_synchronizer  :primary_synchronizer
      def synchronizer(ev)
        update_situation
        current_situation.calculation_cache[ev] ||=
          super_primary_synchronizer(ev)
      end

      def primary_synchronizer(ev)
        update_situation
        current_situation.calculation_cache[[ev,false]] ||=
          super_primary_synchronizer(ev,crew_division_getted_off.children  , false)
      end

      def initial_ar
        general_cache(:initial_ar) do
          sps = self.specialties.select{|sp|sp['特殊種別'] == 'アタックランク'  && !sp.is_disabled?}
          if sps.size > 0
            sps.collect{|sp|sp['ＡＲ']}.max || '不定'
          else
            '不定'
          end
        end
      end

      def ar
        unless initial_ar == '不定'
          initial_ar + ar_modifiers.collect{|sp|sp['ＡＲ']}.sum
        else
          initial_ar
        end
      end
    end
  end
end