# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator

    class CheckList
      attr_reader :list
      def initialize
        @list = {}
      end

      def [](key)
        list[key]
      end
      
      def register(array)
        for item in array
          list[item.no]=item
        end
      end

      def sorted_list
        @list.keys.sort.collect{|key|list[key]}
      end
    end

    class RuleCheck
      attr_accessor :no , :title , :targets , :check_proc
      @@registered = {}
      def self.register(check)
        @@registered[check.no] = check
      end

      def no(value = nil)
        @no = value if value
        @no
      end

      def title(value = nil)
        @title = value if value
        @title
      end

      def targets(value = nil)
        @targets = value if value
        @targets
      end

      def check_proc(value = nil)
        @check_proc =  value if value
        @check_proc
      end

      def initialize(&proc)
        self.instance_eval(&proc)
        RuleCheck.register(self)
      end

      def do_check(troop)
        target_array = self.collect_target(troop)
        if target_array.size > 0
          target_array.collect{|target|
            result = self.check_proc.call(target)
            result.unit = target
            result
          }
        else
          nil
        end
      end

      def collect_target(troop)
        case @targets
        when :characters
          troop.characters
        when :vehicles
          troop.children.select{|ch| ch.class <= ContainerUnit}
        when :troop
          [troop]
        when :divisions
          [troop] + troop.divisions
        when Proc
          @targets.call(troop)
        end
      end

      def check_procedure(targets)
        
      end

    end

    class RuleCheckResult
      attr_accessor :unit , :node ,:info ,  :judge

      def initialize
        @info = {}
        @judge = {}
      end
    end

    TaigenValidations = CheckList.new
    TaigenValidations.register [
      RuleCheck.new{
        no  '5'
        title  '一人のメンバーが複数の分隊に属していない。（ある分隊パターンにおいて、複数の分隊に同時に属していない） '
        targets  :troop

        check_proc  proc{|troop|
          res = RuleCheckResult.new
          res.info[:troop] = {}
          res.info[:troop] = ['【本隊】'] + troop.children.collect{|ch|ch.name}
          res.info[:divisions] = []
          for div in troop.divisions
            res.info[:divisions] << ([div.name] + div.children.collect{|ch|ch.name})
          end
          res
        }
      } ,
    ]
    RuleValidations = CheckList.new
    RuleValidations.register [
      RuleCheck.new{
        no  '1.1'
        title  '分隊することで、本体と比較して兵科が三つ以上に増えている。（本体＞I=D・歩兵・医師など） '
        targets  :troop

        check_proc  proc{|troop|
          res = RuleCheckResult.new
          res.info[:troop] = {}
          res.info[:troop][:name] = '【本隊】'
          res.info[:troop][:actions] = troop.action_names.dup #.delete_if{|act|CommonActions.include?(act)}.compact
          res.info[:divisions]=[]
          for div in troop.divisions
            res.info[:divisions] << {:name => div.name , :actions => div.action_names.dup}
          end
          ([res.info[:troop]] + res.info[:divisions]).each do |div|
            div[:actions].delete_if{|act|CommonActions.include?(act)}.compact!
          end

          res
        }
      } , 
      RuleCheck.new{
        no  '2.1'
        title  '装備箇所を持つ場合、明記されている。 '
        targets  proc{|troop|
          troop.characters.select{|ch| ch.node.items.size > 0}
        }

        check_proc  proc{|ch|
          res = RuleCheckResult.new
          items = ch.node.items.collect{|item| item.text_value}
          items.delete_if{|item|item =~ /個人取得ＨＱ根拠ＵＲＬ/o}
          items.compact!
          res.info[:name] = ch.name

          res.info[:items] = ch.items.collect{|item|
            hash = {}
            lib = ch.context.create_library(item)
            point =lib && lib.find_spec_by_type('着用箇所')

            hash[:source] = items.shift
            hash[:equip_point] = point && point['定義']
            hash
          }
          res
        }
      } ,
      RuleCheck.new{
        no '3.1'
        title '＊着用アイドレスが乗員種別（P ないし CP）の要件を満たしている。'
        targets :vehicles

        check_proc  proc{|vc|
          res = RuleCheckResult.new
          res.info[:name] = vc.name
          res.info[:vehicle_type_name] = vc.container_def.name
          res.info[:options] = vc.node.item_names.dup
          
          res.info[:pilot_requirement] = vc.pilot_requirements
          res.info[:copilot_requirement] = vc.copilot_requirements
          res.info[:vehicle_categories] = vc.container_def.categories['カテゴリ'].gsub(/(｛|｝|。)/,'').split('，')

          pilots = vc.children.select{|u|u.passenger_type==:pilot}
          res.info[:pilot_names] = pilots.collect{|u|u.name}
          res.info[:pilot_licences] = pilots.collect{|u|u.specialties.select{|sp|sp.spec_type == 'パイロット資格'}.collect{|sp|sp['パイロット資格']}.flatten.uniq}

          copilots = vc.children.select{|u|u.passenger_type==:copilot}
          res.info[:copilot_names] = copilots.collect{|u|u.name}
          res.info[:copilot_licences] =  copilots.collect{|u|u.specialties.select{|sp|sp.spec_type == 'コパイロット資格'}.collect{|sp|sp['コパイロット資格']}.flatten.uniq}




          set = Set.new
          res.judge[:pilot_requirement] = begin
            if res.info[:pilot_requirement].nil?
              true
            else
              num_flag = pilots.size == res.info[:pilot_requirement]['人数']
              num_flag && res.info[:pilot_licences].all?{|licences|
                (Set.new(licences) & res.info[:vehicle_categories]).size >= 1 ||
                  licences.include?('すべて')
              }
            end
          end

          res.judge[:copilot_requirement] = begin
            if res.info[:copilot_requirement].nil?
              true
            else
              num_flag = copilots.size == res.info[:copilot_requirement]['人数']
              num_flag && res.info[:copilot_licences].all?{|licences|
                (Set.new(licences) & res.info[:vehicle_categories]).size >= 1 ||
                  licences.include?('すべて')
              }
            end
          end

          res
        }
      }

    ]

  end
end