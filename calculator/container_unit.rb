# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator
    class ContainerUnit < Unit
      include ILanguage::Calculator::TqrManager
      include ILanguage::Calculator::ParserUtil
      attr :pilots , true

      def initialize(context)
        super
        @pilots = Troop.new(context)
        @unit_type = Set.new(['搭乗','乗り物'])
        @idresses = []
      end

      def reflesh
        super
        @conainer_specs = nil
        children.each{|c| c.reflesh}
      end

      def name=(string)
        @name = string
        @pilots.name = string
      end

      def reset_evals
        super
        pilots.children.each{|c|c.reset_evals}
      end

      def dup
        clone = super
        clone.pilots = Troop.new(context)

        @pilots.children.each{|c|
          child_clone = c.dup
          child_clone.reflesh
          clone.add child_clone
        }

        clone
      end

#      alias self_specialties specialties
      def container_specs
        @conainer_specs ||= self.all_idresses.collect{|idress| @context.create_spec(self,idress)}.flatten
      end

      def container_def
        @container_def ||= begin
          self.libraries.find{|lib|lib.is_container?}
        end
      end

      def pilot_requirements
        container_def.pilot_requirements
      end

      def copilot_requirements
        container_def.copilot_requirements
      end


      def categories
        unless @categories
          categories_sp = self.container_specs.select{|sp| sp.spec_type == 'カテゴリ' && sp['カテゴリ種別'] == '乗り物'}
          @categories = categories_sp.collect{|sp|
            parse_array(sp['カテゴリ'])
          }.inject([]){|array,item| array | item.collect{|i|i.intern} }
        end
        @categories
      end

      def default_partial_conditions
        categories.collect{|ca| /#{ca}.*?に搭乗/} | [/^乗り物(のアイドレス)?に搭乗して/o]
      end

      def children
        @pilots.children
      end

      alias characters children

      def add(child)
        @pilots.children << child if child.passenger_type != :loaded
        child.parent = self
        context.register(child)
      end

      def idresses
        @idresses
      end

      def idresses=(value)
        if value.class == Array
          @idresses = value
        end
      end

      def container_type
        self.all_idresses[0]
      end

      def event_cost(with_modification = false , &block)
        cost = super.dup
        children_cost = @pilots.event_cost(with_modification , &block)
        children_cost.each_pair{|k,v| cost[k] += v}
        cost
      end

      def etc
        update_situation
        current_situation.etc ||= begin
          self_etcs = specialties.select{|sp|
            sp.spec_type == 'その他定義'
          }
          for c in children
            self_etcs += c.etc.select{|sp|
              sp.is_unit_type_match? && self.is_tqr_qualified?(sp)
            }
          end
          self_etcs
        end
      end

      def is_tqr_qualified?(sp,ev = nil)
        pilots.is_tqr_qualified?(sp,ev) || super(sp,ev)
      end

      def is_regarded_as_soldier?
        @soldier_regard ||= !!container_specs.find{|sp| sp.source =~ /乗り物にのってないものとみなしてこれを扱う（歩兵扱い）。/o}
      end


      def ar_modifiers
        result = super || []
        result += self.children.collect{|c|c.ar_modifiers}.flatten.select{|sp|sp['ＡＲ'] < 0} if self.children.size > 0
        result
      end

    end
  end
end
