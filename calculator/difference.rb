# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator
    class Difference
      attr_accessor :left , :right 

      attr_accessor :keys_order
      
      class Item
        attr_accessor :name , :lvalue , :rvalue
        def initialize(n , l , r)
          @name ,@lvalue , @rvalue = n , l ,r
        end
    
        def delta
          @rvalue && @lvalue && @rvalue - @lvalue 
        end
    
        def to_s
          @name.to_s + "：" + case
          when @lvalue && @rvalue
            sprintf("%d %s %d (%+d)" , @lvalue , than_char , @rvalue ,delta) 
          when @rvalue.nil?
            @lvalue.to_s
          when @lvalue.nil?
            @rvalue.to_s
          end
        end
    
        private
    
        def than_char
          case 
          when @lvalue < @rvalue
            '<'
          when @lvalue > @rvalue
            '>'
          else
            '='
          end
        end
      end
  
      def self.between(t1 , t2)
        l = t1.hash_for_diff
        r = t2.hash_for_diff
        diff = Difference.new(l ,r)
        diff.keys_order = t1.evaluation_names_order
        diff
      end

      def initialize(l , r)
        @left = l
        @right = r
      end
  
      def deleted_items
        @deleted ||= create_items(@left.keys - @right.keys)
      end
  
      def added_items
        @added ||= create_items(@right.keys - @left.keys)
      end
  
      def modified_items
        @modified ||= create_items((@right.keys & @left.keys).select{|key| @right[key] != @left[key]} )
      end
  
      def key_order(key)
        return  @keys_order[key] + 10 if @keys_order.has_key?(key)
        
        case
        when StandardAbilities.include?(key)
          StandardAbilities.index(key)
        when key =~ /＠/o
          200
        when key =~ /\//o
          (Troop::CostKeysOrder.index(key) || 10) + 300
        else
          100
        end
      end
      
      def print
        result = ''
        result += "・削除\n#{format_items(deleted_items)}\n"  if deleted_items.size > 0
        result += "・追加\n#{format_items(added_items)}\n"  if added_items.size > 0
        result += "・変更\n#{format_items(modified_items)}\n"  if modified_items.size > 0
        result
      end
      
      def format_items(items)
        items.collect{|item|item.to_s}.join("\n")
      end
      private
      def create_items(keys)
        items = keys.collect{|key| Item.new(key , @left[key] , @right[key])}
        if @keys_order
          items.sort_by{|item|key_order(item.name)}
        else
          items
        end
      end
    end
  end
end