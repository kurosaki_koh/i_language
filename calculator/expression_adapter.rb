# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator
    ILanguage::FCalc::Parameter.register_type Unit , 'ユニット'
    
    class EvaluatorAdapter < ILanguage::FCalc::Functor
      def initialize(context ,identifier)
        super(context)
        troop = context
        @identifier = identifier
        @evaluator = troop.find_evaluator(identifier)
      end

      def do_process(arguments , context = @context , args ={})
        params =  args[:args]
        unit = if !params.nil? && params.size > 0
          unit = params[0].evaluate(context,args)
          if calc = args[:calculation]
            calc.last_child.args[0] = unit
          end
          unit
        else
          context
        end
        calc = @evaluator.calculate(unit)
        calc.total
      end

      def to_s
        "Evaluator:#{@identifier} , #{@parameters.collect{|param|param.class.to_s}.join(' ')}"
      end

      def format(calc)
        "＜#{@identifier}＞(#{calc.args[0] && calc.args[0].name}) = #{calc.result.to_s}"
      end
    end
  end
end