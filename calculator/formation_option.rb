# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator

    class Troop
      class FormationOption
        attr :evaluators
        attr :condition_proc

        def self.register(option_command)
          Troop.add_optional_rule(option_command)
        end

        def self.add(method_symbol , *evaluators,&cond)
          option_command = self.new(method_symbol , *evaluators , &cond)
          self.register(option_command)
        end

        def initialize(method_symbol , *opts ,&cond)
          @condition_proc = cond
          @method_symbol = method_symbol
          @modify_procs = opts.select{|opt|opt.class <= Proc}
        end

        def is_available?(children,troop)
          case @method_symbol
          when :all?
            children.all?{|c| self.condition_proc.call(c,troop)}
          when :any?
            children.any?{|c| self.condition_proc.call(c,troop)}
          when :by_troop
            self.condition_proc.call(children[0],troop)
          else
            false
          end
        end

        def modify(troop,array)
          if self.is_available?(troop.children,troop)
            @modify_procs.each{|proc| proc.call(troop , array) }
          end
          array
        end
      end

      class EvaluatorOption < FormationOption
        def initialize(method_symbol , *evaluators,&cond)
          super
          disabler_ary , @evaluator_names = evaluators.partition{|c| c =~ /^\-.+/o}
          @disabler_names = disabler_ary.collect{|str|str.gsub('-','')}
        end

        def modify(troop,array)
          if self.is_available?(troop.children,troop)
            evs = @evaluator_names.collect{|name|
              troop.context.evaluators[name]
            }.select{|ev|
              ev.action_name.nil? || troop.action_names.include?(ev.action_name)
            }.flatten
            evs.each{|ev|
              ev.optional_rule = self
              
              }
            array |= evs
            array.reject!{|c| @disabler_names.include?(c.name)}
          end
          array
        end
      end


      @@formation_options = []

      #敏捷による防御。「ユニットが航空機である」または「/防御判定に敏捷(評価)?を使うことができる。/」の特殊を持つ。１００％制限
      EvaluatorOption.add(:all? , '敏捷による防御（通常）'){|c,t|
        (c.is_airplane? || c.is_evadable?) && c.class <= Vehicle
      }

      #敏捷による防御。「/防御判定に敏捷(評価)?を使うことができる。/」の特殊を持つ。１００％制限
      EvaluatorOption.add(:all? , '敏捷による防御（通常）','敏捷による防御（白兵距離）','敏捷による防御（近距離）','敏捷による防御（中距離）'){|c,t|
        #        klass = c.specialties.find{|sp| sp.spec_type =='カテゴリ'}
        #        is_airplane = klass ? (klass['カテゴリ種別'] == '乗り物' && klass['カテゴリ'] =~ /航空機/ ) : false
        #        is_evadable = c.sp_include?(/防御判定に敏捷(評価)?を使うことができる。/o)
        #
        #        !is_airplane && is_evadable
        !c.is_airplane? && c.is_evadable?
      }

      #器用による防御。「受け流し能力」所有者で７５％制限を満たす。
      EvaluatorOption.add(:any? , '器用による防御（通常）','器用による防御（白兵距離）','器用による防御（近距離）','器用による防御（中距離）'){|c,t|
        t.is_tqr_qualified?('受け流し能力')
      }

      #対空戦闘の除外。/対空戦闘判定に遠距離戦闘評価を使うことができる。/を持つものが１００％。
      EvaluatorOption.add(:all? , '-対空戦闘'){|c,t|
        c.sp_include?(/対空戦闘判定に遠距離戦闘評価を使うことができる。/o)
      }

      #      #艦船編成の装甲評価例外。
      #      EvaluatorOption.add(:any? , '-装甲（通常）','装甲（通常・艦船）'){|c,t|
      #        c.class <= Vessel
      #      }

      #遠距離戦闘評価による対空戦闘。/対空戦闘判定に遠距離戦闘評価を使うことができる。/が一人以上いる。
      EvaluatorOption.add(:any? , '遠距離戦闘評価による対空戦闘'){|c,t|
        c.sp_include?(/対空戦闘判定に遠距離戦闘評価を使うことができる。/o)
      }

      #詠唱による防御（通常）。/防御判定に詠唱戦闘評価を使うことができる。/が一人以上いる。
      EvaluatorOption.add(:by_troop , '詠唱による防御（通常）'){|c,t|
        t.tqrs.include?(:'詠唱での防御能力')
      }

      #治安維持。（治安維持での）を持つ者が一人以上いる。
      EvaluatorOption.add(:any? , '治安維持'){|c,t|
        c.sp_include?(/（治安維持での）/o)
      }

      #災害対応活動。（災害対応活動での）を持つ者が一人以上いる。
      EvaluatorOption.add(:any? , '災害対応活動'){|c,t|
        c.sp_include?(/（災害対応活動での）/o)
      }

      #隠蔽看破。/隠蔽を(看破|見破ろうと)する/を持つ者が７５％以上。
      EvaluatorOption.add(:any? , '隠蔽看破'){|c,troop|
        re = /隠蔽を(看破|見破ろうと)する/o
        sp = c.find_spec(re) || troop.find_spec(re)
        sp && troop.is_tqr_qualified?(sp)
      }

      #      #移動。郵便配達人が含まれていれば、移動自動成功とする。
      #      EvaluatorOption.add(:any? , '移動自動成功'){|c,troop|
      #        re = /移動、自動成功。/o
      #        sp = c.find_spec(re) || troop.find_spec(re)
      #        sp && troop.is_tqr_qualified?(sp) && sp.is_available?
      #      }

      #追跡。追跡者の自動成功が含まれていれば、「追跡（感覚）」と「追跡（幸運）」の表示を抑止し、「追跡：自動成功」とだけ出力する。
      EvaluatorOption.add(:any? , '追跡自動成功'){|c,troop|
        re = /追跡、自動成功。/o
        sp = c.find_spec(re) || troop.find_spec(re)
        sp && troop.is_tqr_qualified?(sp)
      }


      #陣地構築（知識）。部隊全員が乗り物でない。
      EvaluatorOption.add(:all? , '陣地構築（知識）' , '魅力'){|c,t| !(c.class <= Vehicle) }

      #魔法知識。秘書官のうち「魔法の手ほどきを受ける」を持っている物がいる場合のみ。
      EvaluatorOption.add(:by_troop , '魔法知識'){|c,troop|
        troop.characters.any?{|ch|
          ch.sp_include?(/受けることによって魔法の知識を得る（知識判定を行うことができる）。/o)
        }
      }

      #      #魔法知識（搭乗時）。秘書官のうち「魔法の手ほどきを受ける」を持っている物がいる場合のみ。
      #      EvaluatorOption.add(:by_troop , '魔法知識（搭乗時）'){|c,troop|
      #        troop.characters.any?{|ch|
      #          ch.sp_include?(/受けることによって魔法の知識を得る（知識判定を行うことができる）。/o) &&
      #            ch.parent.class <= Vehicle
      #        }
      #      }

      #対空銃手の対空戦闘。対空銃手が編成内に存在する場合のみ。
      EvaluatorOption.add(:any? , '対空銃手の対空戦闘'){|c,troop|
        c.all_idresses.include?('対空銃手')
      }

      #三角とびによる対空戦闘。三角とび習得者が編成内にいる場合のみ
      EvaluatorOption.add(:any? , '三角とびによる対空戦闘'){|c,troop|
        c.all_idresses.include?('三角とび')
      }

      #調和者による鎮静
      EvaluatorOption.add(:any? , '鎮静'){|c,troop|
        c.all_idresses.include?('調和者')
      }

      #電子妖精（レンジャー版）の情報戦防御
      EvaluatorOption.add(:any? , '情報戦防御'){|c,troop|
        c.all_idresses.include?('電子妖精（レンジャー版）の情報防壁')
      }
      
      #外交戦
      EvaluatorOption.add(:any? , '外交戦'){|c,troop|
        troop.characters.any?{|ch| ch.sp_include?(/外交戦能力/o) }
      }

      #外交戦（個別）
      EvaluatorOption.add(:any? , '外交戦（個別）'){|c,troop|
        troop.characters.select{|ch| ch.sp_include?(/外交戦能力/o) }.size >= 2
      }

      #交渉
      EvaluatorOption.add(:by_troop , '交渉','外交戦'){|c,troop|
        num_of_negotiator = troop.characters.select {|ch|
          !ch.is_attendant? && ch.sp_include?(/交渉能力/o)
        }.size
        (num_of_negotiator == troop.num_of_members) || troop.sp_include?(/交渉能力/o)
      }


      #ドラゴンシンパシー
      EvaluatorOption.add(:by_troop , 'ドラゴンシンパシー'){|c,troop|
        troop.characters.any?{|ch|ch.all_idresses.include?('ドラゴンシンパシー')}
        #        case c
        #        when Soldier
        #          c.all_idresses.include?('ドラゴンシンパシー')
        #        when Vehicle , Vessel
        #          c.children.any?{|pilot| pilot.all_idresses.include?('ドラゴンシンパシー') }
        #        end
      }

      #召喚
      EvaluatorOption.add(:by_troop , '召喚'){|c,troop|
        troop.characters.any?{|ch| ch.sp_include?(/(精霊|式神|悪魔)召喚能力/o) }
      }

      #メンチ
      EvaluatorOption.add(:by_troop , 'メンチ'){|c,troop|
        troop.characters.any?{|ch| ch.sp_include?(/メンチを切ることができる。/o) }
      }

      #植物操作判定
      EvaluatorOption.add(:by_troop , '植物操作判定'){|c,troop|
        troop.characters.any?{|ch| ch.sp_include?(/植物操作能力/o) }
      }

      #動物操作判定
      EvaluatorOption.add(:by_troop , '動物操作判定'){|c,troop|
        troop.characters.any?{|ch| ch.sp_include?(/動物操作能力/o) }
      }

      #サイコキネシス
      EvaluatorOption.add(:by_troop , 'サイコキネシス'){|c,troop|
        troop.characters.any?{|ch| ch.sp_include?(/離れたものを動かすことができる。/o) }
      }

      #Ｉ＝Ｄデザイナーの解析
      EvaluatorOption.add(:by_troop , 'Ｉ＝Ｄデザイナーの解析'){|c,troop|
        troop.characters.any?{|ch| ch.sp_include?(/解析として写真などから敵性能を類推できる。/o) }
      }

      #音楽家の歌魔法
      EvaluatorOption.add(:by_troop , '歌唱（歌魔法）'){|c,troop|
        troop.characters.any?{|ch| ch.sp_include?(/音楽家の魔法/o) }
      }
      
      #操縦（搭乗者）
      EvaluatorOption.add(:any? , '操縦'){|c,troop|
        c.class <= Vehicle
      }

      #同調（個別）
      EvaluatorOption.add(:any? , '同調（個別）'){|c,troop|
        c.class <= Vehicle && c.children.size > 1 && troop.formation != :fleet
      }

      #装甲（スカールドのみ
      EvaluatorOption.add(:by_troop , '装甲（スカールドのみ）'){|c,troop|
        troop.band.size > 0 &&
          troop.band.any?{|c|
          c.etc.any?{|sp|
            sp.source =~ /防御判定時にはバンド対象/o
          }
        }
      }

      #変化の術による侵入
      EvaluatorOption.add(:all? , '侵入（技術＆特殊使用）'){|c,troop|
        c.all_idresses.include?('変化の術')
      }
      
      #治療とヘンな治療の合算
      EvaluatorOption.add(:by_troop , 'ヘンじゃない治療','ヘンな治療・蛇神の僧侶のみ','治療（合算）'){|c,troop|
        if troop.characters.any?{|ch| ch.all_idresses.include?('蛇神の僧侶') } && troop.characters.any?{|ch|ch.action_names.include?('治療行為')}
          set = Set.new(['治療行為','ヘンな治療行為'])
          ((troop.children.select{|child|(set & child.action_names).size > 0}.size.to_f / troop.num_of_members ) >= 0.75)
        else
          false
        end
      }
      
      #衛生兵の中距離戦と通常の中距離戦の合算
      EvaluatorOption.add(:by_troop , '中距離戦・衛生兵のみ','中距離戦・衛生兵以外','中距離戦（合算）'){|c,troop|
        troop.action_names.include?('中距離戦闘行為') &&
          troop.characters.any?{|ch| ch.all_idresses.include?('衛生兵')} &&
          troop.characters.any?{|ch| !ch.all_idresses.include?('衛生兵')}
      }

      #衛生兵の中距離戦のみ
      EvaluatorOption.add(:by_troop , '中距離戦・衛生兵のみ'){|c,troop|
        troop.action_names.include?('中距離戦闘行為') &&
          troop.characters.all?{|ch| ch.all_idresses.include?('衛生兵')} 
      }

      #琴弓の一撃による対空戦闘
      EvaluatorOption.add(:any? , '琴弓の一撃による対空戦闘'){|c,troop|
        c.all_idresses.include?('琴弓の一撃')
      }
      
      #絶技音速剣
      EvaluatorOption.add(:any? , '絶技による白兵戦'){|c,troop|
        c.sp_include?(/（絶技戦において）白兵距離攻撃を行うことができる。/o)
      }

      #遠隔動作補助システム
      EvaluatorOption.add(:any? , '装甲（小型メカのみ）','遠隔整備（ＡＲ１距離）','遠隔整備（ＡＲ２距離）','遠隔整備（ＡＲ３距離）'){|c,troop|
        c.all_idresses.include?('遠隔動作補助システム')
      }
      
      CareerAvailableEvaluations = ['装甲','同調']
      FormationOption.add(:any? , proc{|troop , dummy| 
          troop.children.each do |unit|
            if unit.class <= Career && unit.action_names.include?('中距離戦闘行為')
              unit.evaluator_filter = proc{|ev|
                !!(/^中距離戦/o).match(ev.name) || CareerAvailableEvaluations.include?(ev.class_name)
              }
            end
          end
        }){|c,troop| c.class == Career }

      #乗り物を含む部隊では陣地構築（知識）、魅力、歌唱、隠蔽判定ができない。
      EvaluatorOption.add(:any? , '-陣地構築（知識）' , '-魅力' ,  '-歌唱' ,'-隠蔽' ){|c,troop| 
        c.class <= Vehicle 
      }

      #質疑（http://cwtg.jp/qabbs/bbs2.cgi?action=article&id=10320 ）の回答があり次第、以下のコードは削除する予定。
      #ここから
      #I=D以外の乗り物を含む部隊では、さらに陣地構築（筋力）もできない。
      EvaluatorOption.add(:any? , '-陣地構築（筋力）' , '-陣地構築（知識）' ){|c,troop|
        c.class <= Vessel || (c.class <= Vehicle && !c.categories.include?(ID_SYMBOL))
      }
      #ここまで

      #艦船を含む部隊では、操縦（艦船）を追加する。
      EvaluatorOption.add(:any? , '操縦（艦隊）' ){|c,troop| c.class <= Vessel  }
      
      FormationOption.add(:any? , proc{|troop , evaluators| 
          evaluators.select{|ev|ev.class_name == '装甲'}.each{|ev|
            ev.disabled_sps = [/　＝　歩兵(武装)?，/o]
          }
        }){|c,troop| 
        c.class <= Vessel 
        
        }
      
      
      #      Troop.add_common_evaluator('偵察')
      #      Troop.add_common_evaluator('隠蔽看破')
      #      Troop.add_common_evaluator('陣地構築（筋力）')

    end
  end
end