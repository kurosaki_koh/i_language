# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__) , 'vehicle.rb')
module ILanguage
  module Calculator
    class Career < Vehicle

      def initialize(context)
        super
        @unit_type = Set.new(['歩兵'])
      end

      def unit_type
        @unit_type
      end

      def evals
        update_situation
        current_situation.evals ||= begin
          if self.evaluation_mode == :union
            self.evals_by_union
          else
            self.evals_by_selection
          end
        end
      end

      def evaluate(key)
        value = evals[key].to_i

        target_bonuses = evaluate_items()
        target_bonuses.each{|sp|
          if sp.is_applicable?(self,key)
            value += sp.bonus.to_i
            sp.use if sp.class <= BonusSpec
          end
        }
        value
      end

      def pilot_potentials
        result = {}
        for col in Vehicle::PilotPotentials
          result[col]= non_ace_pilots.collect{|s| s.evaluate(col)}.max
        end
        result
      end

      def evals_by_union
        update_situation
        current_situation.general_cache[:evals_by_union] ||= begin
          result = self.evals_org.dup #輸送車自身
          union_hash = self.pilots.evals #搭乗者のRD合算

          for key in Vehicle::PilotPotentials
            result[key] += union_hash[key]
          end
          result
        end
      end

      def evals_keys
        @evals_keys ||= (super | Vehicle::PilotPotentials).to_a
      end
      
      def is_overridden?(col)
        false #ACEは載せられません……
      end

      def evaluation_mode
        update_situation
        current_situation.general_cache[:evaluation_mode] ||= begin
          return :selection if self.children.size <= 1

          union_hash = self.pilots.evals_org
          union_max = union_hash.values.max

          ev = self.troop.find_evaluator('同調（個別）')
          sync_val = ev.alternatives[0].calculate(self).total
          sync_val = sync_val * 4 - 6

          union_max <= sync_val ? :union : :selection
        end
      end

    end


  end

end