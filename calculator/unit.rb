# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__) , 'context.rb') unless defined?(ILanguage::Calculator::Context)

module ILanguage
  module Calculator
    class EvalData
      attr :name,true
      def initialize(name , hash_or_bonus)
        @name = name
        case hash_or_bonus
        when Hash
          @hash = hash_or_bonus.dup
        when Integer
          @bonus = hash_or_bonus
        end
      end

      def bonus
        @bonus || @hash.values[0]
      end

      def is_string?
        false
      end

      def value(col=nil)
        col ? @hash[col] : @bonus
      end

      def [](col)
        self.value(col)
      end
      def to_hash
        @hash.dup
      end

      def keys
        @hash.keys
      end

      def has_key?(k)
        @hash.has_key?(k)
      end

      def delete(k)
        @hash.delete(k)
      end
      
      def source
        self.name
      end

      def self.create(name,hash)
        results = []
        ed = EvalData.new(name,hash)
        results << ed
        if ed.has_key?('全能力')
          all_abilities_hash = StandardAbilities.inject({}){|result,item|
            result[item]=hash['全能力']
            result
          }
          ed.delete('全能力')
          results << EvalData.new(name + '（全能力）',all_abilities_hash)
        end
        results.shift if ed.keys.size == 0
        results
      end
    end

    class Unit
      include ILanguage::Calculator::SituationContainer

      attr :name , true
      attr :character_no , true
      attr :passenger_type , true
      attr :children , true
      attr :unit_type , true
      attr :parent , true
      attr :context , false
      attr :num_of_members_cache , true
      attr :members_without_attendants_cache , true
      attr :cost_cache , false
      attr :tags , true
      attr :band , false

      attr_accessor :evaluator_filter , :node , :order

      attr_reader :total_cost_cache , :event_cost_cache
      attr_reader :optional_equipments
      
      def initialize(context)
        @context = context
        @children = []
        @unit_type = ['']
        @sp_find_cache = {}
        @cost_cache = {}
        @ace = nil
        @tags = []
        @band = []
        @evaluator_filter = nil
        @act_restrictions = []
        @optional_equipments = {}
        @members_without_attendants_cache = {}
        initialize_situations
      end


      def sp_include?(re)
        update_situation
        current_situation.sp_include_cache[re] ||= begin
          sp = specialties.find{|sp| sp.source =~ re && !sp.is_disabled? && sp.is_unit_type_match?}
          sp  ? true : false
        end
      end

      def reflesh
        update_situation
        current_situation.specialties = nil
        current_situation.evaluations = nil
        current_situation.action_names = nil
        current_situation.ability_bonuses = nil
        current_situation.judge_bonuses = nil
        current_situation.all_bonuses = nil
        current_situation.evaluators = nil
        #        @cost_cashe = {}
        @total_cost_cache = {}
        @event_cost_cache = nil
      end

      def reset_evals
        @evals = nil
      end
      
      #      def categories
      #        unless @categories
      #          categories_sp = self.all_idresses.select{|sp| sp.spec_type == 'カテゴリ'}
      #          @categories = categories_sp.collect{|sp|
      #            parse_array(sp['カテゴリ'])
      #          }.inject([]){|array,item| array | item.collect{|i|i.intern} }
      #        end
      #        @categories
      #      end

      def category_types
        unless @category_types
          categories_sp = self.all_idresses.collect{|name|
            lib = @context.create_library(name)
            lib && lib.categories
          }.flatten.compact

          @category_types = categories_sp.collect{|sp|
            sp['カテゴリ種別']
          }.inject([]){|array,item| array | item.collect{|i|i.intern} }

        end
        @category_types
      end


      def hops
        @hops ||= begin
          specs = specialties.select{|sp|sp.spec_type == '航路数'}.collect{|sp|sp['航路数'].to_i}
          if specs.size == 0
            nil
          else
            specs.min
          end
        end
      end

      def libraries
        update_situation
        current_situation.libraries ||= self.all_idresses.collect{|name|
          @context.create_library(name)
        }.flatten.compact
      end

      def placements
        update_situation
        current_situation.placements ||= self.libraries.collect{|lib|
          lib.placement
        }.flatten.compact.uniq
      end

      def initialize_copy(source)
        super
        @unit_type = source.unit_type.dup
        initialize_situations
        for s in source.situations.values
          s_clone = s.dup
          s_clone.reset_caches
          situations[s.name]=s_clone
          self.default_situation = s_clone if source.default_situation.name == s_clone.name
        end
        self.change_situation_to(source.current_situation_name)
        self
      end

      def idresses
        #do nothing. Must be override by inherited class
      end

      def add(child)
        case child.passenger_type
        when :band
          @band << child
        when :loaded
          #do nothing
        else
          @children << child
        end

        child.parent = self
        context.register(child)
        reflesh
      end

      def ancestors
        result = []
        target = self
        until target.nil?
          result << target
          target = target.parent
        end
        result
      end

      def remove(child)
        @children.delete(child)
        child.parent = nil
        @children.compact!
        reflesh
      end

      def is_attendant?
        self.passenger_type == :attendant || self.etc.any?{|sp|sp.source =~ /必要随伴者数/o}
      end

      include UnitTypeOwner

      def is_airplane?
        @is_airplane ||= begin
          klass = self.specialties.find{|sp| sp.spec_type =='カテゴリ'}
          klass ? (klass['カテゴリ種別'] == '乗り物' && klass['カテゴリ'] =~ /航空機/ ) : false
        end
      end

      def is_evadable?
        @evadable ||= self.sp_include?(/防御判定に敏捷(評価)?を使うことができる。/o)
      end

      #評価子の対象となるか否か？　（通常、常にRD値を評価合算に加えてよい。特殊な例においてはルールが上書きされ得る。兵員輸送車とか。
      def is_targetted?(ev)
        @evaluator_filter.nil? || @evaluator_filter.call(ev)
      end

      def change_situation_to(name)
        super
        for c in children
          c.change_situation_to(name)
        end
      end

      def evaluations
        update_situation
        current_situation.evaluations ||= all_idresses.collect{|name|
          ev = @context.create_evaluation(name,self)
          ev = nil if ev.is_not_found?
          ev
        }.compact
      end

      def ability_bonuses
        update_situation
        current_situation.ability_bonuses ||= specialties.select{|sp|
          sp.class <= BonusSpec &&
            !sp.is_disabled? &&
            sp.is_ability_bonus?
        }
      end

      def judge_bonuses
        update_situation
        current_situation.judge_bonuses ||= specialties.select{|sp|
          sp.class <= BonusSpec &&
            !sp.is_disabled? &&
            sp.is_judge_bonus?
        }
      end

      def specialties_for_inspect
        specialties.dup
      end

      def evals_keys
        @evals_keys ||= begin
          eval_obj = Evaluation.new(nil,self)
          eval_obj.evals_hash = {}
          for evaluation in self.evaluations.select{|ev| ev.keys.size > 0}
            eval_obj.add(evaluation)
          end
          eval_obj.keys
        end
      end

      def evals_org
        evals = Evaluation.new(nil,self)
        evals.evals_hash = {}
        #        StandardAbilities.each do |k| evals.evals_hash[k] = 0 end
        for evaluation in self.evaluations.select{|ev| ev.keys.size > 0}
          evals.add(evaluation)
        end
        apply_personal_bonuses(evals)
        evals
      end

      def evals
        update_situation
        current_situation.evals ||= evals_org
      end

      def evals_items
        result = self.evaluations.select{|ev| ev.keys.size > 0}
        result += EvalData.create('個人修正',@personal_bonuses) if @personal_bonuses
        result
      end

      def personal_bonus(key)
        return nil if @personal_bonuses.nil?
        value1 = @personal_bonuses[key]
        value2 = ['全評価' ,'全能力'].collect{|k|
          @personal_bonuses[k].to_i
        }.sum
        result = value1.to_i + value2
        return result == 0 ? nil : result
      end
      
      def apply_personal_bonuses(evaluation)
        if @personal_bonuses
          for key in StandardAbilities | evaluation.keys
            p_bonus = personal_bonus(key)
            evaluation.evals_hash[key] += p_bonus if p_bonus && evaluation.evals_hash.has_key?(key)
          end
        end
      end

      attr :personal_bonuses , true
      attr :optional_bonuses , true
      def personal_bonuses=(hash)
        @personal_bonuses = hash
      end


      def optional_bonuses=(hash)
        @optional_bonuses = hash
      end


      def select_bonuses(evaluator,arg=nil)
        related_sps = self.all_bonuses.select{|b|
          evaluator.match?(b)
        }
        return related_sps.select{|sp|
          (arg && sp.is_ability_match?(arg) && (evaluator.is_include_evaluate_items? ||!evaluator.is_available_sp?(sp) ) ) || sp.is_judge_bonus?
        }
      end
      
      def sub_evals(evaluator,items = sub_evals_items(evaluator))
        result = {}
        #自動成功、ないし自動失敗の場合
        args = evaluator.args(self)
        if auto = items.find{|sp|sp.class <= BonusSpec && sp.is_string?}
          args.each{|arg| result[arg] = auto.bonus}
          return  result
        end
        sum_bonus = 0
        items.each{|item| sum_bonus += item.bonus.to_i}
        for arg in args
          ev = evaluate(arg)
          result[arg] = ev + sum_bonus
        end
        result
      end

      def sub_evals_items(evaluator)
        related_sps = self.all_bonuses.select{|b|
          evaluator.match?(b) && (!b.is_tqr? || troop.is_tqr_qualified?(b,evaluator))
        }
        args = evaluator.args(self)
        result = related_sps.select{|sp|
          args.any?{|arg|
            ((sp.is_ability_match?(arg) && (evaluator.is_include_evaluate_items? || !evaluator.is_available_sp?(sp))) || sp.is_judge_bonus?)
          }
        }
        if @optional_bonuses
          hash = args.inject({}){|hash,arg|hash[arg]=0
            hash
          }
          apply_optional_bonuses(evaluator,hash)
          if hash.keys.size > 0
            value = hash.values[0]
            if value != 0
              optbonus = EvalData.new("行為修正:#{evaluator.action_name}#{sprintf('%+d',value)}",hash)
              result << optbonus
            end
          end
        end
        return result #, related_sps
      end

      def self_calculate(ev)
        update_situation
        current_situation.calculation_cache[ev] ||= begin
          calculation = ev.create_calculation(self)
          calculation.operands = []
          items = self.sub_evals_items(ev)
          arg_names = ev.args(self)
          for arg_name in arg_names
            arg = ev.create_calculation(self)
            if self.evals.has_key?(arg_name) || self.is_ace?
              arg.base_value = if ev.is_include_evaluate_items?
                self.evals[arg_name]
              else
                self.evaluate(arg_name)
              end

              arg.operands = items.select{|sp|
                !(sp.class <= Specialty) || sp.is_judge_bonus? ||
                  (sp.is_ability_bonus? && sp.is_ability_match?(arg_name))  }
              #                  (sp.is_ability_bonus? && sp.is_ability_match?(arg_name) && ev.match?(sp))  }
            elsif ev.is_action_bonus?
              arg.operands = self.all_bonuses.select{|sp|
                (sp.is_judge_bonus? && ev.match?(sp))
              }
            else
              arg.rd = 0
            end
            arg.arg_name = arg_name
            calculation.arguments[arg_name] = arg
          end
          calculation
        end
      end

      def calculate(ev)
        self_calculate(ev)
      end

      def self_bonus_caption
        "（個別修正）"
      end

      def apply_self_bonus(calc,evaluator)
        calc.operands = self.all_bonuses.select{|sp|
          flag = evaluator.match?(sp) && evaluator.sp_target(sp).object_id == self.object_id
          if evaluator.name =~ /同調/o
            flag && calc.is_unit_type_match?(sp)
          else
            flag && calc.is_unit_type_match?(sp) && sp.is_judge_bonus?
          end
        }
        opt_bonus = optional_bonus(evaluator)
        calc.operands <<  EvalData.new(self_bonus_caption, opt_bonus ) if opt_bonus

        calc
      end

      def optional_bonus(ev)
        return nil unless @optional_bonuses
        bonus_keys = @optional_bonuses.keys.select{|k| k == ev.name || k == ev.action_name || k == ev.class_name || ev.tags.include?(k)}
        all_bonus = @optional_bonuses.has_key?('全判定') ? @optional_bonuses['全判定'] : nil
        return nil if bonus_keys.size == 0 && all_bonus.nil?
        result = 0
        bonus_keys.each{|k| result += @optional_bonuses[k]}
        result += all_bonus.to_i
        result
      end

      def apply_optional_bonuses(evaluator,result)
        if @optional_bonuses
          value = optional_bonus(evaluator)
          return unless value
          for arg in (evaluator.args(self) + [evaluator.name]).uniq
            next unless result.has_key?(arg)
            result[arg] += value
          end
        end
        result
      end

      def evaluate(key)
        update_situation
        current_situation.evaluate_cache[key] ||= begin
          value = evals[key].to_i
          target_bonuses = evaluate_items()
          target_bonuses.each do |sp|
            if sp.is_applicable?(self,key)
              value += sp.bonus.to_i
              sp.use if sp.class <= BonusSpec
            end
          end
          value
        end
      end


      def is_evaluate_item?(sp)
        update_situation
        current_situation.is_evaluate_item_cache[sp] ||= begin
          self_keys = self.evals_keys
          self_keys.any?{|key| sp.is_applicable?(self,key)}
        end
      end

      def evaluate_items(ev = nil)
        current_situation.evaluate_items[ev && ev.cache_id] ||= self.all_bonuses.select{|sp|
          is_evaluate_item?(sp) && sp.owner.troop.is_tqr_qualified?(sp,ev)
        }
      end

      def specialties
        update_situation
        unless current_situation.specialties
          names = self.all_idresses

          current_situation.specialties = names.collect{|name|
            self_specs = @context.create_spec(self,name)
            if (category = self_specs.find{|spec|spec.spec_type =='カテゴリ'}) && category['カテゴリ種別']=='ＡＣＥ'
              @ace = true
              if regardings = self_specs.find{|spec|spec.spec_type=='みなし職業'}
                idresses_str = regardings['みなし職業']
                idresses = idresses_str.gsub(/(｛|｝|＜|＞|。)/,'').split('，')
                self_specs += idresses.collect{|regard_job|@context.create_spec(self,regard_job , true)}.flatten
              end
            else
              @ace = false if @ace.nil?
            end
            self_specs
          }.flatten
        end
        return current_situation.specialties
      end

      # @return [Boolean]
      def is_ace?
        if @ace.nil?
          specialties #to set @ace
        end
        @ace
      end

      def is_named_npc?
        self.placements.include?('銘入り猫士')
      end
      
      def find_spec(name_or_re)
        case name_or_re
        when Regexp
          self.specialties.find{|sp|sp.source =~ name_or_re}
        when String
          self.specialties.find{|sp| sp.name == name_or_re}
        end
      end

      def self_evaluators
        names = self.action_names
        evals = self.context.common_abilities_evaluators | names.collect{|action_name| self.context.find_evaluator_by_action_name(action_name)}.flatten
        evals
      end
      def evaluators
        update_situation
        unless current_situation.evaluators
          current_situation.evaluators = self_evaluators
        end
        current_situation.evaluators
      end

      def evaluator_include?(ev)
        evaluators.collect{|ev|ev.object_id}.include?(ev.object_id)
      end

      def self_action_names
        update_situation
        current_situation.self_action_names ||= begin
          result = specialties.collect{|sp|
            !sp.is_disabled? && sp.action_name && sp.is_unit_type_match? && sp.action_name != '' && sp.condition_match? ? sp.action_name : nil }.inject([]){|array,item|
            item ? array | [item] : array
          }
          result.delete_if{|action_name| self.act_restrictions.include?(action_name.to_sym)}
          result
        end
      end

      def action_names
        update_situation
        current_situation.action_names ||= self_action_names
      end

      def self_tqrs
        update_situation
        current_situation.self_tqrs ||= begin
          tqrs = specialties.collect{|sp|
            sp.is_disabled? ? nil : sp.tqr_id
          }.compact.uniq
          tqrs.uniq | tqr_appendance(tqrs)
        end
      end

      def tqrs
        update_situation
        current_situation.tqrs ||= self_tqrs
      end

      def non_action_bonus_tqrs
        update_situation
        current_situation.general_cache[:non_action_bonus_tqrs] ||= begin
          specs = (self.all_bonuses + self.etc).flatten
          result = specs.select{|sp|
            sp.source =~ /７５％制限(能力)?。/o
          }.collect{|sp|
            sp.tqr_id
          }

          result |= self.children.collect{|c|c.non_action_bonus_tqrs}.flatten.collect{|tqrid|tqrid.intern}

          result = result.select{|tqrid|
            self.tqrs.include?(tqrid)
          }
          result.collect{|tqrid|tqrid.to_s}
        end
      end

      def is_action_available?(action_name)
        self.action_names.include?(action_name)
      end

      def actions
        result = {}
        actions = specialties.select{|sp|
          sp.spec_type == '行為補正' || sp.spec_type == '行為'
        }
        for name in action_names()
          result[name]=actions.select{|a|a.action_name == name}
        end
        return result
      end

      def actions_flatten
        actions = specialties.select{|sp|
          sp.spec_type == '行為補正' || sp.spec_type == '行為'
        }
        return actions
      end

      def all_bonuses
        update_situation
        current_situation.all_bonuses ||= self.ability_bonuses + self.judge_bonuses
      end

      def all_bonuses_cache
        update_situation
        current_situation.all_bonuses_cache ||= begin
          bonuses = self.all_bonuses
          for c in children
            bonuses += c.all_bonuses_cache
          end
          bonuses
        end
      end

      def weapon_names
        update_situation
        current_situation.weapon_names ||= begin
          result = {}
          for b in self.all_bonuses
            if b.weapon_names && b.weapon_names.size > 0
              b.weapon_names.each{|name| result[name] = b }
            end
          end
          result.keys.compact.sort
        end
      end

      def weapon_classes
        update_situation
        current_situation.weapon_classes ||= begin
          if weapon_names
            result = []
            for name in weapon_names
              name =~ /(白兵|射撃)（/o
              result |= [$1]
            end
            result.sort
          else
            []
          end
        end
      end

      def inspect_gadgets
        gadgets = [@job_idress.inspect_idresses]
        gadgets += [@wd,@items].flatten.compact.collect{|item| item.name}
        gadgets.join('＋')
      end

      def job_idress=(ji)
        @job_idress = ji
        current_situation.specialties = nil
      end

      def etc
        update_situation
        current_situation.etc ||= specialties.select{|sp|
          sp.spec_type == 'その他定義'
        }
      end

      def general_cache(sym)
        update_situation
        current_situation.general_cache[sym] ||= yield
      end

      def act_restrictions
        general_cache(:act_restrictions) do
          sps = self.specialties.select{|sp|sp.class <= ActRestrictionSpec && !sp.is_disabled?}
          sps.collect{|sp|sp.act_restrictions}.flatten
        end
      end

      def initial_ar
        general_cache(:initial_ar) do
          sps = self.specialties.select{|sp|sp['特殊種別'] == 'アタックランク'  && !sp.is_disabled?}
          if sps.size > 0
            sps.collect{|sp|sp['ＡＲ']}.max || 10
          else
            10
          end
        end
      end

      def ar_modifiers
        general_cache(:ar_modifiers) do
          sps = self.specialties.select{|sp|sp.class  == ARModifierSpec  && !sp.is_disabled? && sp.is_available?}
        end
      end

      def select_cost_required_sp
        cost_required = self.specialties.select{|sp|
          sp.is_cost_required?
        }
        cost_required_set = Set.new(cost_required)

        children_items = (children + band).collect{|c|
          c.select_cost_required_sp.to_a
        }.flatten

        cost_required_set += children_items
        cost_required_set
      end
       
      def total_cost(with_modification = false)
        total = Hash.new{|h,k| h[k]=0}

        for sp in self.select_cost_required_sp
          yield(sp.owner,sp) if block_given?
          cost = sp.cost
          cost.keys.each{|k|
            value = sp.cost_value(k , with_modification)
            total[k] += value
          }
        end
        total
      end

      def total_cost_with_modification #(reffered_actions = :all)
        total_cost(true)
      end

      def event_cost_exempted?(sp)
        #（ACEである かつ 銘入りNPCでない） または この特殊はACEのみなし職業に由来するものである。
        self.is_ace? && !self.is_named_npc? || sp.is_for_ace?
      end


      def select_resource_modifications(sp , resource_type)
        self.resource_modifications.select{|rm|
          rm.resource_type == resource_type
        }.select{|rm|
          rm.is_applicable?(sp)
        }
      end

      
      def resource_reduction_coefft(sp,resource_type)
        #倍率が１倍未満のものだけを抽出している。
        resource_modification_coefficient(sp,resource_type , :reduction => true)
      end

      def resource_multiplier(sp,resource_type)
        #倍率が１倍を超えるものだけを抽出している。
        resource_modification_coefficient(sp,resource_type , :multiplier => true)
      end

      def resource_modification_coefficient(sp,resource_type , opt = {})
        #倍率にかかわらず抽出している。
        mods = select_resource_modifications(sp,resource_type)
        if opt[:reduction]
          mods = mods.select{|rm|rm.coefficient < 1.0}
        elsif opt[:multiplier]
          mods = mods.select{|rm|rm.coefficient > 1.0}
        end
        coefficient = mods.inject(1.0){|coefficient , mod_sp|
          coefficient * mod_sp.coefficient
        }
        (coefficient < 0.15 && coefficient > 0) ? 0.15 : coefficient
      end
      
      def rm_rate(sp , r_type)
        update_situation
        current_situation.general_cache[[sp , r_type]] ||= begin
          result = resource_multiplier(sp , r_type)
          unless r_type == '食料' && context.variable('猫・犬士食料消費の切り上げ')
            result *= resource_reduction_coefft(sp , r_type)
          end
          result 
        end
      end
      
      def cost_modification_expr(sp , resource_type)
        rms = select_resource_modifications(sp,resource_type)
        items = [ sprintf("%d" , sp.cost[resource_type]) ]
        items += rms.collect{|rm|rm.coefficient.to_s}
        items.join(' * ')
      end

      def npc_event_cost_hook(cost , sp)
        nil #do nothing
      end

      def event_cost(with_modification=false , &block)
        total = Hash.new{|h,k| h[k]=0}
        event_costs = specialties.select{|sp|
          sp.is_event_cost? && !sp.is_disabled? &&
            !self.event_cost_exempted?(sp)
        }
        for sp in event_costs
          next if sp.lib.idress_type == '定義' &&  !with_modification
          cost = sp.cost
          yield(sp.owner,sp) if block
          for r_type in cost.keys
            total[r_type] += sp.cost_value(r_type , with_modification)
          end
        end
        total
      end

      
      def event_cost_with_modification(&block)
        self.event_cost(true , &block)
      end

      def organization_cost_deprecated(resource_type , reffered_actions = :all)
        total = if resource_type == '食料'
          total = event_cost(true)[resource_type].to_f
          total = total.abs.ceil
          mods = self.troop.resource_modifications.select{|sp|sp.resource_type == '食料' && sp.coefficient < 1.0}
          coefficient = mods.inject(1.0){|value , sp|value * sp.coefficient}

          coefft = 0 < coefficient  && coefficient < 0.15 ? 0.15 : ( ((coefficient * 100).floor ).to_f / 100)
          total * coefft
        else
          event_cost_with_modification[resource_type].to_f
        end
        #        total += total_cost_with_modification(reffered_actions)[resource_type].to_f
        total += total_cost_with_modification[resource_type].to_f
        total.abs.floor
      end
      
      def organization_cost(resource_type , reffered_actions = :all)
        if self.context.variable('猫・犬士食料消費の切り上げ')
          return organization_cost_deprecated(resource_type,reffered_actions)
        end

        total = event_cost_with_modification[resource_type].to_f
        total += total_cost_with_modification[resource_type].to_f
        total = total.abs
        if resource_type == '食料' && (0 < total) && (total < 1)
          1
        else
          total.floor
        end
 
      end

      def collect_all_sp
        update_situation
        current_situation.general_cache[:collect_all_sp] ||= begin
          self.children.collect{|c|
            sp_list = c.specialties.dup
            if c.class <= ContainerUnit
              sp_list += c.children.collect{|pilot|pilot.specialties}.inject([]){|array,item|
                array | item
              }
            end
            sp_list.flatten
          }.flatten | self.specialties.flatten
        end
      end
      
      def collect_all_sp_for_view
        collect_all_sp
      end
      
      def to_s
        @name || '@name = nil'
      end

      def evals_str
        update_situation
        current_situation.general_cache[:evals_str] ||= begin
          self.context.default_abilities_evaluator.format(self)
        end
      end
      
      def defense_evals_str
        action_names = ['防御（通常）','防御（白兵距離）','防御（近距離）','防御（中距離）','防御（遠距離）']
        result = action_names.join('：')+"\n"
        result << action_names.collect{|ac|self.context.evaluators[ac].evaluate(self)}.join('：')
        result << "\n"
        result
      end

      def select_evaluator_by_action_name(name)
        evs = self.evaluators
        evs.select{|ev|
          !self.all_containments.include?(ev.name) && !ev.is_individualy? && if name.nil?
            ev.action_name.nil? && !CommonAbilities.include?(ev.class_name) && ev.class_name != '同調' && !ev.is_omitted?(self)
          else
            ev.action_name == name && (ev.args.size == 0 || ev.is_action_bonus? || !ev.is_omitted?(self))
          end
        }
      end

      def select_individual_evaluators
        self.evaluators.select{|ev|ev.is_individualy?}
      end

      def evaluation_names_order
        i = 0
        common_evs = self.evaluators.select{|ev| 
          CommonAbilities.include?(ev.class_name) && 
            ev.action_name.nil? && 
            !self.all_containments.include?(ev.name) &&
            !ev.is_omitted?(self)
        }

        action_evs = sorted_action_names().collect{|n| select_evaluator_by_action_name(n)}.flatten
        non_action_evs = select_evaluator_by_action_name(nil)
        individuals = select_individual_evaluators
        num = 0
        evs = (common_evs + action_evs + non_action_evs + individuals).select{|ev| !ev.is_omitted?(self)}
        evs.collect{|ev| ev.display_name}.inject({}){|hash , name|
          hash[name] = num
          num += 1
          hash
        }
      end



      def action_evals_tuples
        result = []
        for action_name in sorted_action_names()

          result += line = select_evaluator_by_action_name(action_name).collect{|ev|
            [ev.display_name , ev.evaluate(self) || next]
          }.compact
        end
        result
      end

      def individual_evals_tuples
        select_individual_evaluators.collect{|ev| [ev.display_name , ev.evaluate(self) || next]}.compact
      end

      def non_action_evals_tuples
        select_evaluator_by_action_name(nil).collect{|ev| [ev.display_name , ev.evaluate(self) || next] }.compact
      end

      def sorted_action_names
        update_situation
        current_situation.general_cache[:sorted_action_names] ||= begin
          i = 1
          action_names = self.evaluators.collect{|ev|ev.action_name}.compact.uniq
          action_names.sort_by{|name|[ActionNameOrder[name.to_sym] || 999999 , i += 1]}
        end
      end

      def action_evals_str
        update_situation
        current_situation.general_cache[:action_evals_str] ||= begin
          result = ''

          for action_name in sorted_action_names()
            line = ''
            ev = nil
            line = select_evaluator_by_action_name(action_name).collect{|ev|
              ev.format(self)
            }.join(' / ')
            line.strip!
            if ev && ev.is_for_optional_equips?
              ancestor_name = ev.ancestor.name
              if line !~ /#{ancestor_name}/
                line = "#{ancestor_name}：\n" + line
              end
            end
            result << line << "\n" if line.size > 0
          end

          line = ''
          for ev in select_evaluator_by_action_name(nil)
            line << ev.format(self)  + "\n"
          end

          result << line.strip  << "\n"

          for ev in select_individual_evaluators 
            result << ev.format(self) + "\n"
          end
          result
        end
      end

      def derived_evals_str
        return '' if self.context.abilities_evaluators.size == 0
        
        default_calc = self.context.default_abilities_evaluator.calculate(self)
        result = ''
        for ev in self.context.abilities_evaluators.reject{|ev|self.all_containments.include?(ev.name)}
          derived_calc = ev.calculate(self)
          next if default_calc == derived_calc
          
          diff = derived_calc.difference(default_calc).uniq
          result << if diff.size == 1
            "※#{ev.particular_situation_name}：全評価#{sprintf("%+d",diff[0])}\n"
          else
            ev.format(self , :ancestor => default_calc) + "\n"
          end
        end
        result
      end

      def common_abilities_evaluators
        result = []
        evs = self.evaluators.select{|ev| CommonAbilities.include?(ev.class_name) && ev.action_name.nil? && !self.all_containments.include?(ev.name)}
        for common in CommonAbilities
          result << evs.select{|ev| ev.class_name == common && !ev.is_omitted?(self) }
        end
        result
      end

      def common_abilities_evals_tuples
        update_situation
        current_situation.general_cache[:common_abilities_evals_tuples] ||= begin
          result = []
          common_abilities_evaluators.flatten.each do |ev|
            result << [ev.name , ev.evaluate(self)]
          end
          result
        end
      end
      
      def common_abilities_evals_str
        update_situation
        current_situation.general_cache[:common_abilities_evals_str] ||= begin
          result = []
          evs = self.evaluators.select{|ev| CommonAbilities.include?(ev.class_name) && ev.action_name.nil? && !self.all_containments.include?(ev.name)}
          for common in CommonAbilities
            result << evs.select{|ev|
              ev.class_name == common && !ev.is_omitted?(self)
            }.collect{|ev|
              ev.format(self)
              #            ev.name + "：" + ev.calculate(self).total.to_s
            }.join(' / ')
          end
          result.compact.join("\n").gsub("\n\n","\n").strip + "\n"
        end
      end


      def leaders_tuples
        synchro_orig = self.context.evaluators['同調']
        calc = synchro_orig.details(self)
        calc.all_leaves.collect{ |leader| ["同調：#{leader.unit.character_no + '_' if leader.unit.character_no}#{leader.unit.name}" , calc.total] }
      end

      def format_leaves(calc)
        result = ''
        calc.all_leaves.each do |leader|
          result += "#{leader.unit.character_no + '_' if leader.unit.character_no}#{leader.unit.name}：#{calc.total}\n"
        end
        result
      end

      def leaders_str
        update_situation
        current_situation.general_cache[:leaders_str] ||= begin

          title = "○同調#{self.class <= Troop ? '' : '（機内）'}\n"
          result = ''
          synchro_orig = self.context.evaluators['同調']
          cal_orig = synchro_orig.details(self)
          leaves_orig = format_leaves(cal_orig)
          result << leaves_orig << "\n"

          for synchro in self.evaluators.select{|ev|ev.name =~ /同調$/}
            calc = synchro.details(self)
            leaves = format_leaves(calc)
            if cal_orig.object_id != calc.object_id && leaves == leaves_orig
              next
            end
            result << "(#{synchro.particular_situation_name})" if synchro.particular_situation_name
            result << leaves
            result << "\n"
          end
          if result.strip == ''
            ''
          else
            title + result
          end
        end
      end


      def ar_tuple
        [["攻撃機会" , self.ar]]
      end
      def all_evals_tuples
        ar_tuple + evals_tuples + common_abilities_evals_tuples + action_evals_tuples + non_action_evals_tuples + individual_evals_tuples + leaders_tuples
#        individual_evals_tuples
      end

      def is_troop_member?
        @parent.nil? || (@parent.class <= Troop && @parent.parent.nil?)
      end

      def troop_member
        result = self
        until result.is_troop_member?
          result = result.parent
        end
        result
      end

      def troop
        troop = self
        troop = troop.parent until troop.parent.nil?
        troop
      end

      def is_tqr_qualified?(sp,ev=nil)
        true
      end
      
      def synchronizer(ev)
        ev.calculate(self)
      end

      def all_partial_conditions(situation_name = self.context.current_situation_name)
        change_situation_to(situation_name) if self.current_situation_name != situation_name
        current_situation.all_partial_conditions_cache ||= begin
          cache = super(situation_name)
          cache |= self.default_partial_conditions if defined?(self.default_partial_conditions)
          cache
        end
      end

      def resource_modifications
        update_situation
        current_situation.resource_modifications_cache ||= begin
          rm = self.specialties.select{|sp|sp.spec_type == '消費修正'}
          rm = if self.parent
            self.parent.resource_modifications + rm
          else
            rm
          end

          no_dupes = rm.select{|sp|sp.note =~ /同能力重複適用不可/o}
          no_dupes.each do |no_dupe|
            rm.delete_if{|sp|sp.name == no_dupe.name}
            rm << no_dupe
          end
          rm
        end
      end

      def evals_tuples
        StandardAbilities.inject([]) {|ary ,key| ary << [key , self.evaluate(key)] ; ary}
      end

      def ar
        (self.initial_ar + self.ar_modifiers.collect{|sp|sp['ＡＲ']}.sum ) || 10
      end

      def has_sp?(sp_name)
        !!self.specialties.find{|sp|sp.name == sp_name}
      end

    end
  end
end

