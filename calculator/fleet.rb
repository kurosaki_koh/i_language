# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator
    module Fleet
      def self.extended(mod)
        mod.instance_eval do
          extend ILanguage::Calculator::CrewDivisionFactory
        end
      end

      def formation
        :fleet
      end

      def evals
        update_situation
        current_situation.evals ||= begin
          non_general_evals = evals_org
          general_evals = crew_division.evals
          for col in StandardAbilities
            non_general_evals[col]=general_evals[col]
          end
          non_general_evals
        end
      end

      def change_formation_to(form)
        raise '艦船編成に艦船以外のユニットは混在できません。' unless form == :fleet
      end

      def calculate(ev)
        update_situation
        current_situation.calculation_cache[ev] ||= begin
          #艦船の評価以外はcrew_division へ移譲
          if self.evals_keys.include?(ev.class_name)
            result = super
            return result
          end

          result = if ev.class_name == '同調'
            crew_division.calculate(ev)
          elsif (!ev.is_standard && ev.action_name && crew_division.team_action_names.include?(ev.action_name)) || (ev.is_standard || ev.is_getted_off_only?)
            crew_division_getted_off.calculate(ev)
          else
            crew_division.calculate(ev)
          end
          if result && !result.is_string?
            apply_self_bonus(result,ev)
          end
          result
        ensure
          current_situation.calculation_cache[ev] = result
        end
      end

      def fleet_evals_str
        update_situation
        current_situation.general_cache[:fleet_evals_str] ||= begin
          result = ''
          manipulation = @context.evaluators['操縦（個別）']
          for c in self.children
            result << <<EOT
●#{c.name}
#{c.evals_str}
操縦：#{manipulation.evaluate(c)}

#{c.leaders_str}
EOT
          end
          result
        end
      end

      def crew_division
        result = super
        result.parent = nil
        result
      end

      def ar
        ar = children.collect{|c|c.ar.to_i || 10 }.min
        ar == 0 ? '不定' : ar
      end

      Troop.register_formation(:fleet , Fleet)
    end
  end
end