# coding: utf-8

require 'treetop'
require 'jiken/parser'
require File.join(File.dirname(__FILE__),'../parser/lib/jcode_util')
require File.join(File.dirname(__FILE__),'evcc_warnings.rb')
require File.join(File.dirname(__FILE__),'../parser/lib/token.rb')
require File.join(File.dirname(__FILE__),'../fcalc/fcalc_defs.rb')
require File.join(File.dirname(__FILE__),'evcc.rb')
#Treetop.load File.join(File.dirname(__FILE__),'evcc.treetop')

module Treetop
  module Runtime
    class SyntaxNode
      include Enumerable

      def each(&block)
        block.call(self)
        elements.each{|e| e.each(&block)} if  nonterminal?
      end

      def select_nodes(module_name)
        self.select{|e|
          if module_name.class == Module
            e.extension_modules.find{|m| m == module_name}
          else
            e.class == module_name
          end
        }
      end
      
      def select_literals(&block)
        if block
          self.select{|e| e.class == LiteralsDef}.collect(&block)
        else
          self.select{|e| e.class == LiteralsDef}.collect{|li| li.text_value}
        end
      end
    end
  end
end

module ILanguage
  module EVCC
    def parse_bonuses(bonuses_txt,source_hash={})
      source = sub_nums(bonuses_txt)
      bonuses = source.split(/＊|\*/)
      result = Hash.new{|hash,key|hash[key]=0}
      result.update(source_hash) if source_hash
      for str in bonuses
        if str =~  /(.+?)(＋|\+|－|\-)(\d+)/
          col = $1
          value = $3.to_i
          value = value * -1 if $2 =~ /^－|\-$/
          if col == '全評価'
            for col in Abilities
              result[col] += value
            end
          else
            col = '耐久力' if col == '耐久'
            result[col] += value
          end
        end
      end
      {}.update(result)
    end

    module RootDef
      def to_array
        @to_array ||= self.to_units
      end

      def to_units
        return [] unless defined?(self.unit)
        result = [unit.node]
        result += units.to_units if defined?(self.units)
        result
      end

      def variables
        to_array.select{|n|n.class <= VariableDef}
      end

      def troop_members
        to_array.select{|n|n.class <= UnitDef && !(n.class <= DivisionDef)}
      end

      def divisions
        to_array.select{|n|n.class <= DivisionDef}
      end

      def directives
        to_array.select{|n|n.class <= DirectiveDef}
      end

      def libraries
        to_array.select{|i|i.class <= LibraryDef}
      end

      def evaluator_definitions
        to_array.select{|i|i.class <= EvaluatorDef}
      end

      def statements
        to_array.select{|i|i.extension_modules.include?(ILanguage::FCalc::ExprNode)}
      end

      def resourcifies
        to_array.select{|i|i.class == ResourcifyBlockDef}
      end

      def specialties
        to_array.select{|i|i.class <= SpecialtyPatchDef}
      end

      def check_warning
        Warning.check(self)
      end
    end

    module StatementDef
    end

    class  UnitDef < Treetop::Runtime::SyntaxNode
    end


    module ItemsOwner
      def items
        self.select_nodes(ItemDef)
      end

      def item_names
        result = self.items.collect{|item|
          idresses_str = item.idresses.text_value
          if idresses_str =~ /(.+?)(\*|＊)/o
            item_name = item.idresses.name.text_value
            result = []
            item.idresses.number.times{ result << item_name}
            result
          else
            idresses_str.split(/＋|\+/)
          end
        }.flatten
        result.delete_if{|i| ['個人取得ＨＱ根拠ＵＲＬ','詳細'].include?(i) }
        result
      end

    end


    module Taggable
      def make_tags(node)
        str = node.tags.text_value
        str.tr!('()（）','')
        str.tr!('Ａ-Ｚ','A-Z')
        str.tr!('・' , '/')
        str.tr!('／' , '/')
        str.split('/')
      end

      def parse_tags(unit,node)
        unit.tags = self.make_tags(node)
        unit.passenger_type = case
        when unit.tags.include?('P')
          :pilot
        when unit.tags.include?('CP')
          :copilot
        when unit.tags.include?('OP')
          :operator
        when unit.tags.include?('騎')
          :rider
        when unit.tags.include?('同')
          :fellow
        when unit.tags.include?('随行')
          :attendant
        when unit.tags.include?('バンド')
          :band
        when unit.tags.include?('銃手')
          :gunner
          # 搭載関連はルールがはっきりするまで保留。
          #        when unit.tags.include?('搭載')
          #          :loaded
        else
          nil
        end
      end
    end

    class CharacterDef  < UnitDef
      include ItemsOwner
      include Taggable

      def p_part
        character_line.p_part
      end

      def e_part
        character_line.e_part
      end

      def name
        character_line.name
      end



      def idresses
        character_line.idresses
      end

      def character_no
        if character_line.character_no_part.text_value != ''
          character_line.character_no_part.character_no.text_value
        else
          nil
        end
      end

      def parse(context)
        soldier = Soldier.new(context)
        soldier.name = name.text_value
        idress_names = idresses.text_value.split(/＋|\+/)
        soldier.idresses = idress_names
        soldier.items = item_names

        bonuses_txt = p_part.text_value.strip.gsub(/(：|:)/,'')
        soldier.personal_bonuses = parse_bonuses bonuses_txt if bonuses_txt && bonuses_txt != ''

        options_txt = e_part.text_value.strip.gsub(/(：|:)/,'')
        soldier.optional_bonuses = parse_bonuses options_txt if options_txt && options_txt != ''
        soldier.character_no = character_no
        soldier.node = self
        parse_tags(soldier,character_line)

        soldier
      end
      
      def interpret(context)
        context.troop.change_formation_to(:troop) if context.troop
        soldier = parse(context)
        context.troop.add(soldier)
      end
    end

    class BonusDef  < Treetop::Runtime::SyntaxNode
    end

    class VehicleDef < UnitDef
      include ItemsOwner
      include Taggable

      def self.unit_class
        Vehicle
      end


      def self.formation_type
        :troop
      end

      def children
        select_nodes(CharacterDef)
      end

      def items
        container_items.select_nodes(ItemDef)
      end

      def parse(context)
        context.troop.change_formation_to(self.class.formation_type) if context.troop
        vehicle = self.class.unit_class.new(context)
        vehicle.idresses = idress_part.idresses.to_array.collect{|i|i.text_value} + item_names
        vehicle.name = name.text_value

        for option_node in self.select_nodes(VehicleOptionDef)
          vehicle.optional_equipments[option_node.idress_name] = option_node.number
        end

        bonuses_txt = p_part.text_value.strip.gsub(/(：|:)/,'')
        vehicle.personal_bonuses = parse_bonuses bonuses_txt if bonuses_txt && bonuses_txt != ''

        options_txt = e_part.text_value.strip.gsub(/(：|:)/,'')
        vehicle.optional_bonuses = parse_bonuses options_txt if options_txt && options_txt != ''
        parse_tags(vehicle,self)
        for c in children
          soldier = c.parse(context)
          vehicle.add(soldier)
        end
        vehicle.node = self
        vehicle
      end

      def interpret(context)
        vehicle = parse(context)
        context.troop.add(vehicle)
      end
    end

    class VesselDef < VehicleDef
      def self.unit_class
        Vessel
      end

      def self.formation_type
        :fleet
      end
      
      def interpret(context)
        vessel = parse(context)
        context.troop.add(vessel)
      end
    end

    class CareerDef < VehicleDef
      def self.unit_class
        Career
      end

      def interpret(context)
        unit = parse(context)
        context.troop.add(unit)
        unit
      end
    end


    class DivisionDef < UnitDef
      include ItemsOwner
      def member_names
        members.select_literals
      end

      def items
        division_items.select_nodes(ItemDef)
      end

      def DivisionDef.set_formation(division,context)
        children_classes = division.member_names.collect{|name|context.units[name]}.compact.collect{|u|u.class}
        if children_classes.all?{|c|c<= Vessel}
          division.change_formation_to(:fleet)
        elsif children_classes.all?{|c| c <= Soldier || c == Vehicle}
          division.change_formation_to(:troop)
        else
          raise "'#{division.name}'分隊の定義のおいて、艦船とそれ以外のユニットが混在しています。"
        end
      end

      def parse(context)
        division = Division.new(context)
        division.name = name.text_value
        raise "分隊と個々のユニットに同じ名前を付ける事はできません。（”#{division.name}”）" if context.units.has_key?(division.name)
        division.member_names = member_names
        TroopBonusDef.parse(self,division)
        division.idresses += item_names
        DivisionDef.set_formation(division,context)
        division.node = self
        division
      end

      def interpret(context)
        division = parse(context)
        context.troop.divisions << division
      end
    end


    module BonusesDef #<  Treetop::Runtime::SyntaxNode
      def bonuses
        select_nodes(BonusDef)
      end
    end

    class DirectiveDef < Treetop::Runtime::SyntaxNode
      def self.node_class
        self.name.split('::')[-1]
      end
    end

    class LiteralsDef < Treetop::Runtime::SyntaxNode
    end


    class TroopBonusDef < DirectiveDef
      def TroopBonusDef.parse(node,troop)
        idress_part = node.idress_part.text_value.gsub(/(：|:)/,'')
        troop.idresses = idress_part.split(/＋|\+/)

        bonuses_txt = node.p_part.text_value.strip.gsub(/(：|:)/,'')
        eval_bonus = parse_bonuses bonuses_txt if bonuses_txt && bonuses_txt != ''

        options_txt = node.e_part.text_value.strip.gsub(/(：|:)/,'')
        action_bonus  = parse_bonuses options_txt if options_txt && options_txt != ''

        troop.personal_bonuses = eval_bonus
        troop.optional_bonuses = action_bonus
      end
      
      def interpret(context )
        TroopBonusDef.parse(self,context.troop)
      end

    end

    module Targetable
      AllMember = '全隊員'.intern
      AllUnit = '全ユニット'.intern
      AllMemberDeprecated = '部隊全員'.intern
      def targets(context)
#        return [context] if targetable.text_value == ''
#        target_str = targetable.target.text_value

        target_str = targetable.text_value == '' ? '' : targetable.target.text_value.intern
        case target_str
        when AllMember 
          context.troop.children.collect{|c|
            case c
            when Soldier
              c
            when Vehicle
              c.children
            when Vessel
              c.children_origin
            else
              nil
            end
          }.flatten.compact
        when AllUnit
          context.troop.children
        when AllMemberDeprecated

          context.troop.children
        when ''
          division = context.troop.all_divisions.find{|div| div.name == context.situation_stack[-1]}
          division ? [division] : [context]
        else
          targetable.target.select_nodes(LiteralsDef).collect{|t|
            name = t.text_value
            unit = context.units[name] || context.tags[name]
            context.warnings << "ユニット名'#{name}'は存在しません" if unit.nil?
            unit
          }.flatten.compact
        end
      end
    end

    class SituationDef < DirectiveDef
      include Targetable
      def name
        @name ||= begin
          str = situation_name.text_value
          str = '本隊' if str == '【本隊】'
          str
        end
      end

      def interpret(context)
        #状況名が分隊名と同じかどうか
        not_regsiter_flag =  (unit = context.units[name]) &&  unit.class <= Troop

        if targetable.text_value != ''
          targets(context).each do |u|
            if u.class <= Troop
              not_regsiter_flag = true
              u.situation_names << name
            else
              context.warnings << "'#{u.name}'は分隊名、ないし\"本隊\"ではありません"
            end
          end
        end

        context.change_situation_for_register(self.name , not_regsiter_flag)
      end
    end

    class ParticularSituationDef < SituationDef
      def evaluation_names
        evaluation_names_array.select_literals
      end

      def interpret(context)
        context.derive_evaluators(self.name , self.evaluation_names)
      end
    end

    class ConditionDef < DirectiveDef
      include Targetable
      def interpret(context)
        content_text = '（' + content.text_value + '）'
        targets(context).each{|u|
          u.conditions << content_text
        }
      end
    end

    class PartialConditionDef < ConditionDef
      def interpret(context)
        p_cond = /#{content.text_value}/
        targets(context).each{|u|
          u.partial_conditions << p_cond
        }
      end
    end

    class DisableDef < DirectiveDef
      include Targetable
      def interpret(context)
        targets(context).each{|u|
          u.disabled_sps << sp_name.text_value
        }
      end
    end

    class IdressAppendantDef < DirectiveDef
      include Targetable
      def interpret(context)
        idress_parts = idress_part.text_value.gsub(/(：|:)/,'')
        idresses = idress_parts.split(/＋|\+/)

        targets(context).each{|u|
          u.idress_appendants.concat(idresses)
        }
      end
    end

    class ExceptionDef < DirectiveDef
      include Targetable
      def interpret(context)
        idress_parts = idress_part.text_value.gsub(/(：|:)/,'')
        idresses = idress_parts.split(/＋|\+/)

        targets(context).each{|u|
          u.idress_exceptions.concat(idresses)
        }
      end
    end

    class ContainmentDef < DirectiveDef
      include Targetable
      def evaluators
        evaluation_name.select_literals
      end
      def interpret(context)
        eval_names = evaluators
        targets(context).each{|u|
          u.containments.concat(eval_names)
        }
      end
    end

    class CostAdditionDef < DirectiveDef
      def division_names
        division_name.select_literals
      end
      
      def interpret(context)
        context.variables['コスト計上分隊'] |= self.division_names
      end
    end

    class BonusHQDef < DirectiveDef
      def hq_value
        parse_num(hq.text_value)
      end

      def interpret(context)
        sp_name_str = sp_name.text_value
        sp_name_str =~ /^(.+)の(.+)$/
        idress_name = $1
        idress = context.definitions[idress_name]

        sp_def = idress['特殊定義'].find{|sp|sp['定義'] =~ /＊#{sp_name_str}　＝/}
        raise "#{sp_name_str}:補正ＨＱで指定された特殊名称が存在しません。" if sp_def.nil?
        raise "#{sp_name_str}:指定された特殊は補正ないし行為補正ではありません" unless ['補正','行為補正'].include?(sp_def['特殊種別'])
        
        hq_hash = sp_def.dup
        hq_hash.update({
            "特殊種別" => "補正" ,
            "行為名" =>  "" ,
            "補正" => hq_value() ,
            "コスト" => {},
            "付記" => "",
            "名称" => sp_name_str + 'ＨＱボーナス'
          })
        targets_str = hq_hash['補正対象'].size > 1 ? '｛'+ hq_hash['補正対象'].join('，') + '｝' : hq_hash['補正対象'][0]
        source = "　　＊#{hq_hash['名称']}　＝　，#{hq_hash['兵科種別']}，#{hq_hash['発動条件']}，#{hq_hash['補正使用条件']}#{targets_str}、評価#{sprintf("%+d",hq_value())}。"
        source += self.text_value
        hq_hash['定義']=source
        idress['特殊定義'] << hq_hash
      end
    end

    class ResourceModificationHQDef < DirectiveDef
      def hq_value
        (expression.evaluate(@context).to_f * 100 ).floor * 0.01
      end

      def is_head_value_omitted?
        self.omitted.text_value != ''
      end

      def expression
        result = if is_head_value_omitted?
          @context.fcalc_parser.parse(sp_def['修正倍率'].to_s + ' * ' +  hq_eqpr.text_value).interpret
        else
          hq_eqpr.interpret(@context)
        end
        raise "＃！消費削減ＨＱにおいて正しい数式が指定されていません。（#{self.text_value}）" unless result.class <= ILanguage::FCalc::Functor
        result
      end

      def sp_def
        @sp_def ||= begin
          sp_name_str = sp_name.text_value
          sp_name_str =~ /^(.+)の(.+)$/
          @idress_name = $1
          idress = @context.definitions[@idress_name]

          raise "消費削減ＨＱで指定された特殊名称\"#{sp_name_str}\"は存在しません。" if idress.nil?

          sp_def = idress['特殊定義'].find{|sp|sp['定義'] =~ /＊#{sp_name_str}　＝/}
          raise "消費削減ＨＱで指定された特殊名称\"#{sp_name_str}\"は存在しません。" if sp_def.nil?
          raise "指定された特殊名称\"#{sp_name_str}\"は消費修正ではありません。" unless sp_def['特殊種別'] == '消費修正'
          sp_def
        end
      end

      def make_source(value)
        rate_str = case
        when value == 0
          '０にする。'
        when value < 1
          sprintf("%02d％に削減する。" , value * 100)
        when rate > 1
          sprintf("%d倍にする。" , value)
        else
          ''
        end
        old_source = sp_def['定義'].dup

        sprintf('　　＊%sの%s%s　＝　%s，%s%s消費を%s%s',
          @idress_name ,
          sp_def['修正対象'] ,
          sp_def['修正倍率'] >= 1 ? '追加消費' : '消費削減' ,
          [sp_def['兵科種別'],sp_def['発動条件']].join('，'),
          sp_def['対象条件']['条件'],
          sp_def['修正対象'],
          rate_str ,
          sp_def['付記'] + self.text_value
        )
      end
      
      def interpret(context)
        @context = context

        rate = self.hq_value
        sp_def['修正倍率'] = rate
        sp_def['定義'] = make_source(rate)
      end
    end

    module ItemDef

    end

    class VehicleOptionDef < Treetop::Runtime::SyntaxNode

      def idress_name
        name.text_value
      end
      
      def number
        sub_nums(numbers.text_value).to_i
      end
    end

    class FlexibleBonusDef < DirectiveDef
      include Targetable
      def parse(context = nil)
        @sp_name = sp_name.text_value
        @bonus_target = bonus_target.text_value
      end

      def interpret(context)
        self.parse(context)
        targets(context).each{|u|
          u.flexible_bonus[@sp_name] = @bonus_target
        }
      end
    end

    class OrganizationTypeDef < DirectiveDef

      def is_mixed?
        mixed.text_value != ''
      end
      
      def interpret(context)
        context.variables['編成種別'] = type_name.text_value

        context.variables['多兵科編成'] = true if is_mixed?
      end
    end
    
    class LibraryDef < Jiken::Parser::ILanguageDef::LibraryDef
      def node
        library
      end

      def to_array
        [node]
      end
    end

    class SpecialtyPatchDef < Treetop::Runtime::SyntaxNode
      def node
        specialty_definition
      end

      def library_name
        spname = sp_name.text_value
        array = spname.split('の')
        array.pop
        name = array.join('の')
        name.sub(/^＊/,'')
      end

      def sp_def_source
        sp_name.text_value + eq_sign.text_value + def_text.text_value
      end

      def interpret(context)
        nil
      end
    end

    class CommandResourceDefinitionDef < Treetop::Runtime::SyntaxNode

      def sp_name
        specialty_name_node.text_value
      end

      def cr_name
       cr_def.name.text_value
      end

      def cr_power
        parse_num(cr_def.power.text_value)
      end

      def cr_tag
        cr_def.tag.text_value
      end

      def command_resource_definition
        {name: cr_name ,
         power: cr_power ,
         tag: cr_tag
        }
      end

      def base_name
        @base_name ||= begin
          if self.sp_name =~ /^(.+?)(行為|補正)$/
            $1
          else
            false
          end
        end
      end

      def disable_sp(context )

        sps = if self.base_name
          %w"行為 補正".collect{|postfix| base_name + postfix}.select{|sp| @target.has_sp?(sp)}
        else
          [sp_name]
        end
        sps.each{|sp|
          context.disabled_sps <<  sp
          context.converted_specialty_names << sp
        }
      end

      def interpret(context)
        @target = context.troop.children.first
        if @target.has_sp?(self.sp_name)
          if context.converted_specialty_names.include?(self.sp_name)
            raise "関連する特殊（＜＊#{self.base_name}｛行為,補正,補正ＨＱ｝＞）を同時にコマンドリソース化することはできません。"
          end
          disable_sp(context )
          context.converted_command_resources << self.command_resource_definition
          context.converted_specialties.merge!( {self.sp_name => self.command_resource_definition} )
        else
          raise "ユニット：#{@target.name} は 特殊＜＊#{self.sp_name}＞を所持していません。"
        end
      end
    end

    class ResourcifyBlockDef < Treetop::Runtime::SyntaxNode

      def interpret(context)
        set = {}

        resourcify_definitions.each {|crd|
          if set.has_key?(crd.sp_name)
            raise "同名特殊（＜＊#{crd.sp_name}＞）を複数回コマンドリソース化することはできません。"
          end
          set[crd.sp_name] = true
          crd.interpret(context)
        }
      end

      def resourcify_definitions
        select_nodes(CommandResourceDefinitionDef)
      end
    end

    class EVCCParser < ExVCCFormatParser
      def self.is_line_comment?(line)
        if (line =~ /^(.*?)(?<!：)(#|＃)(?!(！|!)).*$/o)
          return $1
        elsif (line =~ /^((■|□|・|●|○).*$)/o)
          return ''
        else
          nil
        end
      end
      
      def self.remove_comment(source)
        result = []
        source.each_line{|line|
          if non_comment = is_line_comment?(line)
            if non_comment =~ /^$/
              next
            end
            line = non_comment + "\n"
          end

          line.gsub!(/\/\*(.*?)\*\//,'')
          line.gsub!(/ +\n/ , "\n")
          next if line =~ /^$/
          result << line
        }
        result = result.join
        return result
      end

      def parse(source)
        removed = EVCCParser.remove_comment(source)
        super(removed)
      end

      def failure_context(failure_line)
        failure_begin1 = [failure_line -3  , 0].max
        failure_begin2 = [failure_line -2  , 0].max

        lines = @input.split("\n")
        failure_end = [lines.size , failure_line + 2].min
        fls = failure_line_string
        target_line = sprintf("%d:%s",failure_line , fls && failure_line_string[0...failure_column])
        context = if failure_begin2 == failure_line - 1
          [target_line]
        else
          lines[failure_begin1 .. failure_begin2].collect{|str|
            line = sprintf("%d:%s" ,failure_begin1+=1 , str)
            line
          } << target_line
        end

        context.join("\n")
      end

      def jcolumn(string , column)
        return 0 if string.nil?
        result = 0
        index = 0
        string.each_char do |ch|
          result += 1
          index += ch.size
          return result if result >= column
        end
        result
      end

      def lines
        @lines ||= @input.split("\n")
      end
      def failure_line_string
        lines[failure_line-1]
      end

      def failure_reason
        return nil unless (tf = terminal_failures) && tf.size > 0
        (tf.size == 1 ?
            tf[0].expected_string :
            "文字列候補「#{tf.map{|f| (f.expected_string == "\n" ? '(改行)' : '\'' + f.expected_string + '\'')  }.uniq*', '}」があるべき箇所でエラーが発生しました。\n"
        ) +
          " #{failure_line}行目, #{jcolumn(failure_line_string , failure_column)}桁 (byte #{failure_index+1}) :\n" +
          "#{failure_context(failure_line)}"
      end
    end

    class EvaluatorDef  < Treetop::Runtime::SyntaxNode
      def parameters
        ev_parameters.select_literals
      end

      def bonus_target
        ev_bonus_target.bonus_target.text_value
      end

      def conditions
        if ev_bonus_conditions.conditions.text_value == 'なし'
          []
        else
          ev_bonus_conditions.select_literals{|node|/#{node.text_value}/}
        end
      end

      def class_name
        ev_class_name.text_value == '' ? nil : ev_class_name.class_name.text_value
      end

      def action_name
        ev_action_name.text_value == '' ? nil : ev_action_name.action_name.text_value
      end

      def has_target_filters?
        ev_target_filters.text_value != ''
      end

      def target_filters
        has_target_filters? ?  ev_target_filters.select_literals : nil
      end
      
      def has_target_exception_filters?
        ev_target_exception_filters.text_value != ''
      end
      
      def target_exception_filters
         has_target_exception_filters? ? ev_target_exception_filters.select_literals :  nil
      end

      def disabled_sps
        ev_disabled_sps.text_value == '' ? [] : ev_disabled_sps.disabled_sps.select_literals{|node|/#{node.text_value}/}
      end

      def append(context,ev)
        ev.represent_order = ev.order = context.evaluator_order += 100
        context.particular_evaluators << ev
        context.evaluators[ev.name] = ev
      end
      
      def interpret(context)
        ev = Evaluator.new
        name_str = self.name.text_value
        bonus_target_str = self.bonus_target
        conds = self.conditions
        disabled_sps = self.disabled_sps
        optional_hash = {:class_name => self.class_name , :action_name => self.action_name}
        Evaluator.initialize_values(ev , name_str , optional_hash , *parameters){|evl|
          evl.bonus_target = bonus_target_str
          evl.conditions = conds
          evl.disabled_sps = disabled_sps
        }
        
        if self.has_target_filters?
          ev.target_filters = Evaluator::TargetFilter.create_filters(self.target_filters , context)
        end
        
        if self.has_target_exception_filters?
          ev.target_exception_filters = Evaluator::TargetFilter.create_filters(self.target_exception_filters , context)
        end
        
        append(context , ev)
      end
    end

    class ExpressionEvaluatorDef  < EvaluatorDef
      def expr
        ev_expression.expr
      end

      def interpret(context)
        ev = ExpressionEvaluator.new
        name_str = self.name.text_value
        optional_hash = {:class_name => self.class_name , :action_name => self.action_name}
        expr_obj = expr.interpret(context)
        ExpressionEvaluator.initialize_values(ev , name_str , optional_hash ){|evl|
          evl.expr = expr_obj
        }

        append(context , ev)
      end
    end
  end


  
  class VariableDef  < Treetop::Runtime::SyntaxNode
    def interpret(context)
      vname = var_name.text_value
      value_str = value.text_value
      context.variables[vname]=value_str
    end
  end

  module Calculator
    include ILanguage::EVCC
    Abilities = ['体格','筋力','耐久力','外見','敏捷','器用','感覚','知識','幸運']
    include JcodeUtil

    SpecialtyParser = Jiken::Parser::ISpecialtiesParser.new
  end
end