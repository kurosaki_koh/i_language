# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator
    class Library
      attr :from_ace , true
      def initialize(hash)
        @lib = hash
        @from_ace = false
      end

      def placement
        sp = @lib['特殊定義'].find{|sp|sp['特殊種別']=='位置づけ'}
        sp && sp['位置づけ']
      end

      def categories
        @lib['特殊定義'].find{|sp|sp['特殊種別']=='カテゴリ'}
      end

      def is_ace?
        @is_ace ||= categories['カテゴリ種別'] == 'ＡＣＥ'
      end
      
      def is_race?
        %w!人 種族!.include?(@lib['種別'])
      end
      
      def is_wardress?
        @lib['種別'] == 'ウォードレス'
      end

      def is_container?
        @lib['種別'] == '乗り物'
      end

      def is_from_ace?
        @from_ace
      end

      def actions
        @actions ||= @lib['特殊定義'].select{|sp|sp['特殊種別']=='行為'}
      end

      def action_names
        @action_names ||= self.actions.collect{|sp|sp['行為名']}
      end

      def select_spec_by_type(type_name)
        @lib['特殊定義'].select{|sp|sp['特殊種別']==type_name}
      end

      def find_spec_by_type(type_name)
        @lib['特殊定義'].find{|sp|sp['特殊種別']==type_name}
      end

      def pilot_requirements
        @pilot_requirements ||= find_spec_by_type('必要パイロット数')
      end

      def copilot_requirements
        @copilot_requirements ||= find_spec_by_type('必要コパイロット数')
      end

      def name
        @lib['Ｌ：名']
      end
      
      def idress_type
        @lib['種別']
      end
    end
  end
end
