# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator
    class Soldier  < Unit
      attr :items, true
      
      def initialize(context)
        super
        @items = []
        @unit_type = Set.new(['' , '歩兵','歩兵武装'])
        @idresses = []
      end
      def items=(value)
        @items = value
        @evals = nil
        @specialties = nil
      end
      def idresses
        @idresses + @items
      end

      def idresses=(arg)
        @idresses = arg
        @evals = nil
        @specialties = nil
      end

     
      def toString
        to_s
      end


      #（整備行為ができない）ウォードレスを着用しているか？　（戦車兵用ウォードレス、植物型ウォードレスは該当しない）
      def is_wardressed?
        update_situation
        current_situation.general_cache[:wardressed] ||=
          self.placements.include?('ウォードレス')
      end

      InhibitActions = [:'整備行為']

      def act_restrictions
        update_situation
        current_situation.general_cache[:act_restrictions] ||= begin
          result = super.dup
          result |= InhibitActions if self.is_wardressed?
          result
        end
      end

      def self_action_names
        update_situation
        current_situation.self_action_names ||= begin
          result = super.dup
          result.delete_if{|name| act_restrictions.include?(name.intern)}
          result
        end
      end

      def self_tqrs
        update_situation
        current_situation.self_tqrs ||= begin
          result = super.dup
          result.delete_if{|name| act_restrictions.include?(name)}
          result
        end
      end

      def event_cost_exempted?(sp)
        self.is_ace? && !self.is_named_npc? &&  sp.lib.is_race?
      end

      def npc_event_cost_hook(cost , sp)
#        if is_npc? && sp.lib.categories['カテゴリ種別'] =~ /^(人|種族)$/o
        if is_npc? && sp.lib.is_race?
          cost['食料'] = cost['食料'].to_f / 2 if cost.has_key?('食料')
        end
      end

      def is_npc?
        cno = self.character_no
        result = cno && cno =~ /\d{2}\-[^0-9]{2}\d{3}\-[^0-9]{2}/
        result || self.is_named_npc?
      end

      def is_qualified_as_leader?
        @as_leader ||= !self.is_npc? || self.is_named_npc?
      end


      def default_partial_conditions
        if troop_member == self
          []
        else
          case self.passenger_type
          when :pilot
            [/^｛?パイロット.*として(搭乗している)?/]
          when :copilot
            [/コパイロット.*として(搭乗している)?/]
          else
            []
          end
        end
      end
    end
  end
end
