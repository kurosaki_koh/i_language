# -*- encoding: utf-8 -*-
module ILanguage
  module Calculator
    class Division < Troop
      attr :member_names

      def member_names=(names)
        @member_names = names.dup
      end

      def children
        unless @div_children
          @div_children = []
          member_names.collect{|name|
            unit = context.units[name]
            raise "#{self.name}分隊において、存在しないユニット名を指定しています：#{name}" if unit.nil?
            unit ? unit.dup : nil
          }.compact.each{|c|
              c.reflesh
              self.add(c)
          }
          @div_children = @children.dup
        end
        @div_children
      end
    end
  end
end