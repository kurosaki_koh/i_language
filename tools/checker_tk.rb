# -*- encoding: utf-8 -*-
require 'tk'

dir = File.dirname(__FILE__)
#require File.join(dir,'lib.rb')
require 'lib.rb'

include ILanguage

$KCODE = 'u'
Tk.encoding = 'utf-8'

$lib_parser = ILanguageLibraryParser.new
$sp_parser = ISpecialtiesParser.new

selected = TkVariable.new('')
$source = IDefinitionSource.new
$lib_ent = TkVariable.new('')
$lib_ent.trace('w' , proc{|var,em,op|
    update_list(var.value)
  })

def update_list(keyword)
  keys = []
  if keyword.nil? || keyword == ''
    keys = $source.key_list
  else
    keys = $source.key_list.select{|k| k =~ /#{keyword}/}
  end
  
  $lib_list.delete 0 , $lib_list.size-1
  keys.each{|k|
    $lib_list.insert 'end' , k
  }
  
end


$root_pane = TkPanedwindow.new{|f|
#  showhandle true
#  handlesize 10
  sashwidth 10
  pack(:side=>:left, :expand=>true, :fill=>:both, :pady=>2, :padx=>'2m')

  add left_frame = TkFrame.new(f){|root_pane|
    pack :side => :left , :fill => :both , :expand => true
    entry_frame = TkFrame.new(root_pane){
      pack :side => :top , :fill => :x 
    }
    label = TkLabel.new(entry_frame,:text => '検索：'){
      pack :side => :left , :fill => :none
    }
    
    $lib_entry = TkEntry.new(entry_frame){
      textvariable $lib_ent
      pack :side => :left , :fill => :x , :expand => true# , :expand => true
    }
    $lib_list = TkListbox.new(root_pane){
      width 30
      pack :side => :left , :fill => :both , :expand => true
    }
    $scr = TkScrollbar.new(root_pane){
      width 30
      pack :fill=>:y , :side => :left # , :expand => true
    }
    $lib_list.yscrollbar($scr)
    update_list(nil)
#    keys = source.key_list
#    keys.each{|k|
#      $lib_list.insert 'end' , k
#    }
  }
  paneconfigure left_frame , :minsize => 200
  add $right_pane = TkPanedwindow.new(nil, :orient=>:vertical){|f|
    sashwidth 10
    pack(:side=>:top, :expand=>true, :fill=>:both, :pady=>2, :padx=>'2m')
    add $def_text = TkText.new(f){
      pack :side => :top ,:fill => :both
    }
    add $parsed_text = TkText.new(f){
      pack :side => :top , :fill => :both ,:expand => true
    }
  }
  
}


def parse_sp(sp_node)
  sp_list = sp_node.select_nodes(SpecialtyDef)
#  result = sp_list.collect{|sp|sp.inspect_sp("% 20s:%s\n")}.join('')
  result = ''
  for sp in sp_list
    result += "#定義："+ sp.text_value + "\n"
    result += "特殊種別：#{sp.class.class_name}\n"
    hash = sp.to_hash
    max_length = sp.class.property_names.collect{|name|name.size/3}.max
    for key in sp.class.property_names
      title = key + ('　' * (max_length - (key.size / 3)))
      result +=  "#{title}: #{hash[key]}\n"
    end
    result += "\n"
  end
  result
end
def parse(text)
  lib_node = $lib_parser.parse(text).to_a[0]
  if lib_node
    sp_node = $sp_parser.parse(lib_node.specialty_text)
    if sp_node
      return parse_sp(sp_node) 
    else
      return "＃Ｌ：構文エラー\n"+$sp_parser.failure_reason
    end
  else
    return "＃特殊構文エラー\n"+$lib_parser.failure_reason
  end
end

$lib_list.bind('<ListboxSelect>',proc{
    idx = $lib_list.curselection[0]
    key = $lib_list.value[idx]
    text = $source.read_definition(key)
    text.gsub!("\r","")
    $def_text.value = text
    $parsed_text.value = parse(text)
  })

Tk.mainloop