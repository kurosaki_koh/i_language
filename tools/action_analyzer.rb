# -*- encoding: utf-8 -*-
#require File.join(File.dirname(__FILE__),'idef_sqlite.rb')
#require File.join(File.dirname(__FILE__),'../icalc/unit.rb')
#require File.join(File.dirname(__FILE__),'../icalc/evaluator.rb')

$: << File.dirname(__FILE__)
require 'idef_db'
$: << File.join(File.dirname(__FILE__),'../icalc')

require 'unit.rb'
require 'evaluator.rb'
require 'evcc_defs'

require 'rubygems'
require 'treetop'
include EVCC

parent_dir = File.dirname(File.dirname(__FILE__))
#Treetop.load(File.join(parent_dir ,'icalc/evcc'))
require 'evcc'

require 'tk'

$idress_entry = TkVariable.new
include ILanguage::Calculator
Definitions.set_activerecord(IDefinition)

Definitions['ＨＱ']={'評価' => StandardAbilities.inject({}){|hash,c|hash[c]=1 ; hash} ,'特殊定義' => {}}
Definitions['ＳＨＱ']={'評価' => StandardAbilities.inject({}){|hash,c|hash[c]=2 ; hash} ,'特殊定義' => {}}
for col in StandardAbilities
  Definitions['ＨＱ'+col] = {'評価' => {col => 1},'特殊定義' => {}}
  Definitions['ＳＨＱ'+col] = {'評価' => {col => 2},'特殊定義' => {}}
end



Abilities = ['体格','筋力','耐久力','外見','敏捷','器用','感覚','知識','幸運']
def evals_string(unit)

  hash = {}
  result_str = ''
  cols = Abilities.dup

  for col in  cols
    hash[col]=unit.evaluate(col)
    result_str += "#{col}:#{hash[col]}　"
    hash.delete(col)
  end

  for key in hash.keys
    next if key.nil? || key == ''
    result_str += "#{key}:#{hash[key]}　"
  end
  result_str
end

def parse_soldier_line(text)
  text = text.gsub(/(；|;)$/,'')
  name , idresses ,bonuses_txt ,options_txt= text.strip.split(/：|\:/)
  items = idresses.strip.split(/＋|\+/)
  soldier = Soldier.new
  soldier.name = name
  soldier.items = items
  soldier.personal_bonuses = parse_bonuses(bonuses_txt.strip) if bonuses_txt && bonuses_txt != ''
  soldier.optional_bonuses = parse_bonuses(options_txt.strip) if options_txt && options_txt != ''
  soldier
end

def format_row(name,hash,cols = Abilities)
  return '' if hash.nil?
  result = "|#{name}|"
  result << Abilities.collect{|c|c ? hash[c].to_s : ''}.join('|')
  result << "|\n"
  result
end

def inspect_pilot_evals_table(unit,sums)
  result = ''
  pilot_cols = [nil,nil,nil,nil,nil,'器用','感覚','知識','幸運']
  for child in unit.children
    result << format_row(child.name,child.evals,pilot_cols)
  end
  max_values = ['器用','感覚','知識','幸運'].inject({}){|hash,col|
     hash[col] = unit.children.collect{|c|c.evals[col]}.max
#     puts "#{col} : #{unit.children.collect{|c| c.evals[col]}.max}"
     sums[col] += hash[col]
     hash
  }
  result << format_row('最大値',max_values , pilot_cols)
  result
end

def inspect_evals_table_as_wiki(child)
    sums = Hash.new{|h,k|h[k]=0}
    result = "#region\n||#{Abilities.join('|')}|\n"
    for idress in child.idresses
      i_def = Definitions[idress]
      next if i_def.nil? || i_def['評価'].keys.all?{|k|Abilities.all?{|a| a  != k}}
      name = idress + (i_def.nil? ? "(登録なし)" : '')
      result << format_row(name , i_def['評価'])
      sum_rows(sums,i_def['評価'])
    end
#puts "ietaw:#{child.class}"
    if child.class <= Vehicle
      result << inspect_pilot_evals_table(child,sums)
    end
    for sp in child.bonuses
      if child.basic_bonus?(sp)
#        result << "#{sp.inspect}"
        sp['定義'] =~ /＊(.+?)　＝　/
        sp_name = $1
        hash = {}
        Abilities.each{|c|
          hash[c] = sp['補正'] if child.basic_bonus?(sp,c)
          }
        result << format_row(sp_name , hash)
        sum_rows(sums,hash)
      end
    end
    pb = child.personal_bonuses
    if pb && pb.keys.size > 0

      result << format_row('個人修正' , pb)
      sum_rows(sums,pb)
    end
    result << format_row('合計',sums)
    result << "#endregion\n"
    result
end

def sum_rows(sums,hash)
  return if hash.nil?
  for col in Abilities
    sums[col]+= hash[col].to_i
  end
end

def inspect_specialties(unit)
  unit.specialties.collect{|sp|sp['定義']}.join("\n")
end
def inspect_children(unit)
  result = "#region\n"
  for child in unit.children

    result << "#{child.name}："
    result << Abilities.collect{|c| "#{c}:#{v = child.evaluate(c)}(RD:#{to_rd(v)}) "}.join + "\n"
    result << inspect_evals_table_as_wiki(child)
    result << "#region\n"
    result << child.specialties.collect{|sp|sp['定義']}.join("\n")
    result << "\n#endregion\n"
  end
  result + "#endregion\n\n"
end

def actions_text(unit)
    actions = unit.actions
    result = ''
    sp_text = ''
    action_names = unit.action_names  + ['防御','防御（白兵距離）','防御（近距離）','防御（中距離）','防御（遠距離）','偵察']
    action_names << '追跡行為（幸運）' if action_names.find{|a| a == '追跡行為'}
    for action_name in action_names
      next if action_name.nil? || action_name == ''
      sum_bonus = 0
      sum_cost = Hash.new{|hash,key|hash[key]=0}

      sps = actions[action_name]
#      puts "#vehicle_sp.sps:#{sps.inspect},#{unit.name},#{action_name},#{unit.specialties.inspect}"
      sps ||= unit.bonuses.select{|sp|
        unit.bonus_available?(sp) && sp['補正対象'].find{|item| item == '防御'}
        }
      for sp in sps
        sp_text <<  "  補正：#{sp['補正']} コスト：#{sp['コスト'].inspect}  定義：(#{sp['定義']})\n"
        sum_bonus += sp['補正'].to_i
        sp['コスト'].keys.each{|k| sum_cost[k]+=sp['コスト'][k]}
      end
      personals = ''
      rds = Hash.new{|h,k|h[k]=0}
      for child in unit.children
        personals += "#{child.name}：#{action_name}："
        evalor = Evaluators[action_name]
        sp_texts = ''
        if evalor
          args = evalor.args
        filtered_sp  = child.bonuses.select{|sp|
            evalor.match?(sp,child.conditions) &&
              !(child.basic_bonus?(sp,args))
        }
 #         filtered_sp.each{|sp|evalor.match?(sp , child.conditions) && puts("#{child.name}:#{sp['定義']}")}
          
          args_txt = ''
          sp_texts = filtered_sp.collect{|sp|sp['定義']}.join("\n")
          for arg in args
            value = child.evaluate(arg) #evals[arg]
            args_txt += " #{arg}#{value}"
            bonus = 0
            if filtered_sp.size > 0
              value = child.evaluate(arg)
              for sp in filtered_sp
                if ( evalor.match?(sp,child.conditions,[arg]) && !child.basic_bonus?(sp,arg))
                  args_txt += "+#{sp['補正']}"
                  bonus += sp['補正']
                end
              end
            end
            if child.optional_bonuses
              vals = []
              key = child.optional_bonuses.keys.find{|k|k.to_s + '行為' == evalor.name}
              vals << child.optional_bonuses[key] if key

              vals << child.optional_bonuses['全判定'] if child.optional_bonuses.has_key? '全判定'
              bonus += vals.sum
              args_txt += "+#{vals.join('+')} " if vals.size > 0
            end
            args_txt += "=#{value + bonus}" if bonus > 0
            rd = to_rd(value + bonus)
            args_txt += "(RD:#{rd}) "
            rds[arg]+=rd
          end 
          personals += args_txt.strip + "\n" 
          if sp_texts.size > 0
            personals += "#region\n#{sp_texts.strip}\n#endregion"
          end
        end
        personals += "\n"
      end
      evaluator = Evaluator[action_name]
      eval_str = evaluator ? unit.accept_evaluator(evaluator) : '-'
      result<< "■#{action_name} 評価：#{eval_str}\n#region\n" # 補正合計：#{sum_bonus} コスト合計：#{sum_cost.inspect}\n#region\n"
      result << personals
      ev_sum = 0
      if evaluator
        result += rds.collect{|k,v|
          ev = ILanguage::Calculator::to_eval(v)
          ev_sum += ev
          "#{k}{RD:#{v} 評価:#{ev}}"
        }.join(" , ")
        result += " 結果：#{(ev_sum / evaluator.args.size).to_i}\n"
      end
      result += "#endregion\n"
   end
   result + "\n"+sp_text +"\n"
end

def bonuses_text(soldier)
  spbonuses= soldier.bonuses
  bsps = spbonuses.select{|sp|sp['特殊種別']=='補正'}
  result = ''
  for bsp in bsps
    result << "  補正：#{bsp['補正']} コスト：#{bsp['コスト'].inspect} 補正対象:#{bsp['補正対象'].inspect} 前提条件:#{bsp['前提条件:']} 定義：#{bsp['定義']}\n"
  end
  result
end

def calc_cost(troop)
  
end

def analyze
  unit = Troop.new
  source = $troop_entry.value.strip
  parser = ExVCCFormatParser.new
  root = parser.parse(source)
  conditions = nil
  if root
    unit = create_troop(root)
    cond_str = $conditions_entry.value.strip
    if cond_str != ''
      conditions = cond_str.split(',').collect{|c| '（' + c + '）'}
      unit.conditions.concat(conditions.dup)
    end
#    comments_flag = false
#    for u in root.to_array
#      if u.class == CharacterDef
#        comments_flag = false
#        soldier = parse_soldier(u)
#        unit.children << soldier
#        soldier.conditions.concat(conditions.dup)
##        puts soldier.conditions.inspect
#      elsif u.class == VehicleDef
#        comments_flag = false
#        vehicle = parse_vehicle(u)
#        vehicle.conditions.concat(conditions.dup)
#        for c in vehicle.children
#          c.conditions.concat(conditions.dup)
#        end
#        unit.children << vehicle
#      elsif u.class == ConditionDef
##        puts "#condition"
#        unless comments_flag
#          unit.conditions.clear
#        end
#        comments_flag = true
#        unit.conditions << '（' + u.content.text_value + '）'
#        conditions = unit.conditions.dup
##        puts conditions.inspect
#      end
#    end
    result = actions_text(unit)
    evals_str = evals_string(unit) + "\n"+ inspect_children(unit)
    sps = "#region\n"
    costs = unit.total_cost{|u,sp|
      sps << u.name + ':' + sp['定義']+ "\n"
#      puts sp['定義']
    }
    costs_str = "\n部隊特殊消費："+costs.inspect + "\n"+sps + "\n#endregion"
    $result.value = source + "\n----\n" + evals_str + "\n" +  result + bonuses_text(unit) + costs_str
  else
    $result.value = parser.failure_reason
  end
end


msfont = TkFont.new(['ＭＳ明朝',14,[]])
top = TkFrame.new{|top|
  pack :side=>:top , :fill => :x #, :expand => true

  TkFrame.new(top){|v_frame|
    pack :side => :top , :fill => :x

    TkLabel.new(v_frame){
      text '補正条件'
      font msfont
      pack :side => :left
    }

    $conditions_entry = TkEntry.new(v_frame){
      font msfont
      pack :side => :left , :fill => :x , :expand => true
    }
    button = TkButton.new(v_frame){
      text '分析'
      font msfont
      command {
        analyze
      }
      pack :side=>:left
    }
  }

  TkFrame.new(top){|t_frame|
    pack :side => :top
    TkLabel.new(top){
      text '部隊'
      font msfont
      pack :side=>:left
    }
    $troop_entry = TkText.new(top){
      font msfont
  #    textvariable $idress_entry
      pack :side=>:left ,:fill => :both  , :expand => true
    }

  }

}
$result = TkText.new{
  font msfont
  pack :side=>:top , :fill => :both , :expand => true
}

Tk.mainloop