# -*- encoding: utf-8 -*-
require 'kconv'
$LOAD_PATH.unshift File.dirname(__FILE__)
require 'idef_db'
#IdefSqlite::init_sqlite_db()

idefs = IDefinition.find(:all)
for idef in idefs
  doc = idef.compiled_hash
  spdefs = doc['特殊定義'] if doc

  for etc in spdefs.select{|spdef|spdef['特殊種別']=='その他定義'}
    puts etc['定義']
    puts etc['コスト'].inspect if etc['コスト'] != {}
  end
end

counts = Hash.new{|h,k|h[k]=0}

for idef in idefs
  doc = idef.compiled_hash
  spdefs = doc['特殊定義'] if doc
  for spdef in spdefs
    if spdef['特殊種別'].nil?
      puts spdef.inspect
    end
    counts[spdef['特殊種別']]+=1
  end
end

sum = 0
for key in counts.keys.sort
  puts "#{key}:#{counts[key]}"
  sum += counts[key]
end

puts "合計：#{sum}"