# -*- encoding: utf-8 -*-
require File.join(File.dirname(__FILE__),'jcode_util')

module ILanguageDef

  class ContentsRoot < Treetop::Runtime::SyntaxNode
    def to_a
      elements
    end
    def node
      elements.find{|e| e.node_type == :library}
    end
  end

  class CommentNode < Treetop::Runtime::SyntaxNode
    def node_type
      :comment
    end
  end

  class LibraryDef < Treetop::Runtime::SyntaxNode
    def name
      name_str.text_value
    end

    def long_name
      contents.elements.find{|e|e.node_type == :name}.name.text_value
    end

    def content
      contents.text_value
    end

    def specialty_text
      specialty = contents.elements.find{|e|e.node_type == :specialty_def }
      specialty ? specialty.text_value : nil
    end

    def point
      node = contents.elements.find{|e|e.node_type == :point}
      node ? node.text_value : nil
    end

    def object_type
      contents.elements.find{|e|e.node_type == :name}.idress_class.class_name.text_value
    end

    def evaluation
    eval_def =  contents.elements.find{|e|e.node_type == :evaluation}
      eval_def ? eval_def.evaluate : nil
    end

    def next_idresses
      node  = contents.elements.find{|e| e.node_type == :next_idresses}
      node ? node.next_idresses : nil
    end

    def environment
      node  = contents.elements.find{|e| e.node_type == :environment}
      node ? node.definition.text_value : nil
    end

    def point
      node  = contents.elements.find{|e| e.node_type == :point}
      node ? node.definition.text_value : nil
    end

    def node_type
      :library
    end

    def to_hash

      result = {  'Ｌ：名' => name ,
                  '原文' => self.text_value ,
                  '名称' => long_name ,
                  '種別' => object_type ,
                  '要点' => self.point ,
                  '周辺環境' => self.environment ,
                  '評価' => self.evaluation ,
                  '特殊定義' => [] ,
                  '→次のアイドレス' => self.next_idresses}
      return result
    end
  end
  class Evaluation < Treetop::Runtime::SyntaxNode 
    include JcodeUtil
    def node_type 
      :evaluation
    end

    def evaluate
      result = {}
      def_str = sub_nums(definition.text_value)
      evals = def_str.split('，')
      for str in evals
        str.gsub!('＋','')
        str.gsub!('+','')
        str =~ /(.+?)(\-?\d+)/
        result[$1]=$2.to_i
      end
      return result
    end
  end

  class NameNode < Treetop::Runtime::SyntaxNode
    def node_type
      :name
    end
  end

  class NextIdress < Treetop::Runtime::SyntaxNode
    def node_type
      :next_idresses
    end

    def next_idresses
      defs = definition.text_value
      names = defs.split('，')
      return names
    end
  end

  class EnvironmentNode < Treetop::Runtime::SyntaxNode
    def node_type
      :environment
    end

    def environment
      definition.text_value
    end
  end

  class SpecialtyNode < Treetop::Runtime::SyntaxNode
    def node_type
      :specialty_def
    end
  end

  class PointNode < Treetop::Runtime::SyntaxNode
    def node_type
      :point
    end
  end


end