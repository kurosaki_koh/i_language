# coding : utf-8

require File.join(File.dirname(__FILE__),'jcode_util')

module Treetop
  module Runtime
    class SyntaxNode
      include Enumerable
      
      def each(&block)
        block.call(self)
        elements.each{|e| e.each(&block)} if  nonterminal?
      end
      
      def select_nodes(module_name)
        self.select{|e|
          if module_name.class == Module
            e.extension_modules.find{|m| m == module_name}
          else
            e.class == module_name
          end
        }
      end
    end
  end
end

module SpecialtyDef
  include JcodeUtil
  class SpecialtyNode < Treetop::Runtime::SyntaxNode 
    @@class_name = {}
    @@property_names = {}
    
    def self.class_name(name=nil)
      @@class_name[self] = name if name
      return @@class_name[self]
    end
    
    def self.property_names(*args)
      if args.size > 0
        array = args.flatten
        @@property_names[self] = array.dup
      end
      return @@property_names[self]
    end
    def inspect_sp(format = "%s:%s\n")
      result = ''
      result += "#定義："+ self.text_value + "\n"
      result += "特殊種別：#{self.class.class_name}\n"
      hash = self.to_hash
      max_length = self.class.property_names.collect{|name|name.size}.max
      for key in self.class.property_names
        result +=  sprintf(format ,key,hash[key].inspect)
      end
      result += "\n"
      result
    end
    
    def to_hash
       {'特殊種別' => self.class.class_name , '定義' => text_value}
    end
  end
  
  def parse_cost(text)
    text = sub_nums(text)
    result = {}
    while text =~ /((燃料|資源|食料|生物資源|資金|マイル|犬士／猫士)((\+|\-)[0-9]+))/
      result[$2]=$3.to_i
      text.sub!(/((燃料|資源|食料|生物資源|資金|マイル|犬士／猫士)(\+|\-)[0-9]+)/,'')
    end
    return result
  end

  module CommonDefinitionAccessor
    PropertyNames = ['行為名','兵科種別','発動条件']
    def action_name
      common_definition.action_name
    end
    
    def unit_class
      common_definition.unit_class
    end
    
    def invocation
      common_definition.invocation
    end
    
    def common_definition_to_hash
      {
       '行為名' => action_name.text_value ,
       '兵科種別' => unit_class.text_value ,
       '発動条件' => invocation.text_value ,
      }
    end
  end

  module BonusAccessor
    PropertyNames = ['補正使用条件', '補正対象' ,'補正' , 'コスト' , '付記' ]

#    def judge_type
#      array
#    end
    
    def bonus
      parse_num(bonus_description.bonus.text_value)
    end
    
    def parsed_costs
      parse_cost(bonus_description.costs.text_value)
    end
   def bonus_to_hash
     unless bonus_value = bonus
       bonus_value = bonus_description.bonus.text_value
     end
     {
         '補正使用条件' => bonus_condition.text_value ,
         '補正対象' => judge_type.value ,#text_value ,
         '補正' => bonus_value ,
         'コスト' => parsed_costs,
         'コスト定義' => bonus_description.costs.text_value,
         '付記' => bonus_note.text_value.strip
     }
    end
#    
#    STUB_STR=''
#    def STUB_STR.text_value
#      ''
#    end
#    def judge_Type
#      STUB_STR
#    end
#    def bonus
#      STUB_STR
#    end
#    def bonus_note
#      STUB_STR
#    end
    
  end  
  class Category < SpecialtyNode
    class_name  'カテゴリ'
    property_names 'カテゴリ種別','カテゴリ'
    def category_class 
      dec_category.category_class
    end
    
    def to_hash
      super.update({'カテゴリ種別' => category_class.text_value , 'カテゴリ' => category.text_value})
    end
  end

  class PilotLicense < SpecialtyNode
    include CommonDefinitionAccessor
    class_name  'パイロット資格'
    property_names CommonDefinitionAccessor::PropertyNames , 'パイロット資格'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update({'パイロット資格' => licenses})
      result
    end
    
    def licenses
      array = vehicle_licenses.array
      if array.class == ArrayItem
        [array.text_value]
      elsif array.extension_modules.find{|item|item == SpecialtyDef::Array}
        array.items.collect{|i| i.text_value}
      end
    end
  end
  
  class CopilotLicense < PilotLicense
    class_name 'コパイロット資格'
    property_names CommonDefinitionAccessor::PropertyNames , 'コパイロット資格'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update({'コパイロット資格' => licenses})
      result
    end
  end
  
  class WearLicense < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '着用資格'
    property_names CommonDefinitionAccessor::PropertyNames , '着用資格'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update({'着用資格' => licenses})
      result
    end
    
    def licenses
      array = wear_licenses.array
#      if array.class == Array
      if array.extension_modules.find{|item|item == SpecialtyDef::Array}
        array.items.collect{|i| i.text_value}
      elsif array.class == ArrayItem
        array.value
      end
    end
  end
  
  class WearRestriction < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '使用制限'
    property_names '使用制限'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update({'使用制限' => value })
      result
    end
    
    def value
#      wear_restriction.restriction.text_value
      wear_restriction.text_value
    end
  end
  
  class Position < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '位置づけ'
    property_names '位置づけ'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update({'位置づけ' => value  })
      result
    end
    
    def value
      position.value
    end
  end

 class ARBonus < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '初期ＡＲ修正'
    property_names '対象','ＡＲ','修正使用条件' , '付記'
    def to_hash
      if defined?(ar_bonus_target.array)
        target = ar_bonus_target.array.value
      else
        target = []
      end
      result = super
      result.update(common_definition_to_hash)
      result['ＡＲ'] = parse_num(ar_bonus.text_value)
      result['対象'] = target
      result['修正使用条件'] = bonus_condition.text_value
      result['付記'] = note.text_value
      result
    end

  end

  class DeemedJob< SpecialtyNode
    include CommonDefinitionAccessor
    class_name 'みなし職業'
    property_names 'みなし職業'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update({'みなし職業' => line.text_value  })
      result
    end
  end

  class Action < SpecialtyNode
    class_name '行為'
    include CommonDefinitionAccessor
    property_names CommonDefinitionAccessor::PropertyNames , 'コスト','付記','コスト定義','評価名','評価'
    def to_hash
      result = super
      cost_val = (costs.text_value != 'なし') ? parsed_costs : nil
      cost_str = cost_val.nil? ? 'なし' : costs.text_value
      result.update(common_definition_to_hash)
      result['評価名']=eval_name
      result['評価']= evaluation
      result['コスト']=cost_val 
      result['コスト定義'] = cost_str
      result['付記']=note.text_value.strip
      result
    end

    def eval_name
      if has_action_evaluation?
        return action_evaluation_override.to_a[0].action_eval.eval_name.text_value
      else
        nil
      end
    end

    def evaluation
      if has_action_evaluation?
        return parse_num(action_evaluation_override.to_a[0].action_eval.numbers.text_value)
      else
        nil
      end
      
    end

    def has_action_evaluation?
      defined? action_evaluation_override.action_eval
    end
    def bonus
      return 0
    end
    
    def parsed_costs
      parse_cost(costs.text_value)
    end
  end

  class PilotEvalMultiply < SpecialtyNode
    class_name 'パイロット能力補正'
    include CommonDefinitionAccessor
    property_names '補正使用条件','倍率'

    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update({'倍率' => parse_num(numbers.text_value) , '補正使用条件' => bonus_condition.text_value })
    end
  end

  class Bonus < SpecialtyNode #< Treetop::Runtime::SyntaxNode
    class_name  '補正'
    include CommonDefinitionAccessor
    include BonusAccessor
    property_names CommonDefinitionAccessor::PropertyNames , BonusAccessor::PropertyNames

    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update(bonus_to_hash)
      result
    end
  end
  
  class ActionBonus < SpecialtyNode #< Treetop::Runtime::SyntaxNode
    class_name  '行為補正'
    include CommonDefinitionAccessor
    include BonusAccessor
    property_names CommonDefinitionAccessor::PropertyNames , BonusAccessor::PropertyNames
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update(bonus_to_hash)
      result
    end
   
  end
  
  class AttackRank < SpecialtyNode
    include CommonDefinitionAccessor
    class_name 'アタックランク'
    property_names 'ＡＲ' , '付記'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update({'ＡＲ' => parse_num(ar.text_value) , '付記' => note.text_value.strip})
    end
  end
  
  class Etc < SpecialtyNode
    class_name 'その他定義'
    property_names '定義','コスト'
    
    def to_hash
      result = super
      result.update({'コスト'=>parse_cost(line.text_value)})
    end
  end
  
  class EventCost < SpecialtyNode
    include CommonDefinitionAccessor
    class_name 'イベント時消費'
    property_names 'コスト','消費単位' , '付記'
    def to_hash
      result = super
      result['コスト'] = parse_cost(cost.text_value)
      result['消費単位'] = parenthesized_word.value.text_value
      result['付記'] = note.text_value
      result
    end
  end

  class PilotRequirement < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '必要パイロット数'
    property_names '人数' , '付記'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result['人数'] = parse_num(numbers.text_value)
      result['付記'] = note.text_value
      result
    end
  end
 
  class CopilotRequirement < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '必要コパイロット数'
    property_names '人数'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result['人数'] = parse_num(numbers.text_value)
      result['付記'] = note.text_value
      result
    end
  end

  class MaxPilotCapacity < SpecialtyNode
    include CommonDefinitionAccessor
    class_name 'パイロット最大搭乗数'
    property_names '人数' , '付記'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result['人数'] = parse_num(numbers.text_value)
      result['付記'] = note.text_value
      result
    end
  end


  class RiderRequirement < PilotRequirement
    class_name '騎乗者数'
    property_names '人数' , '付記'
  end

  class LicenceRequirement < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '搭乗資格'
    property_names '搭乗資格'
    def to_hash
      super.update({'搭乗資格' => line.text_value})
    end
  end
 
  class Hops < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '航路数'
    property_names '航路数'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update({ '航路数' => line.text_value})
    end
  end
 
  class ManmachineNum < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '人機数'
    property_names '人機数','付記'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update({ '人機数' => parse_num(numbers.text_value)})
      result['付記']= note.text_value
      result
    end
  end
 
  class Production < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '生産'
    property_names '生産','コスト'
    
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result['生産']= line.text_value
      result['コスト'] = parse_cost(line.text_value)
      result
    end
  end
 
  class EquipPoint < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '着用箇所'
    property_names '着用箇所'
    def to_hash
      super.update({ '着用箇所' => line.text_value})
    end
  end

  class Payload < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '輸送力'
    property_names '輸送力'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update({ '輸送力' => line.text_value})
    end
  end

  class PublicOfficial < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '出仕'
    property_names '出仕'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result.update({'出仕' => line.text_value})
    end
  end

  class ARConsumeReduction < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '移動時ＡＲ消費軽減'
    property_names 'ＡＲ消費軽減','使用条件'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result['ＡＲ消費軽減'] = parse_num(reduction.text_value)
      result['使用条件'] = bonus_condition.text_value
      result
    end
  end

  class OptionEquipment < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '基本オプション'
    property_names 'オプション名称','最大装備数','付記'
    def to_hash
      hash = super
      hash.update(common_definition_to_hash)
      hash['オプション名称']=option_equipment.option_name.text_value
      hash['最大装備数']=parse_num(option_equipment.max_number.text_value)
      hash['付記'] = note.text_value.strip
      hash
    end
  end

  class Passengers < SpecialtyNode
    include CommonDefinitionAccessor
    class_name '人員輸送'
    property_names '人数'
    def to_hash
      result = super
      result.update(common_definition_to_hash)
      result['人数'] = parse_num(numbers.text_value)
      result
    end
  end

#  class Array < Treetop::Runtime::SyntaxNode
  module Array #< Treetop::Runtime::SyntaxNode
    def to_hash
      {:value => value}
    end

    def items
      select_nodes(ArrayItem)
    end
    
    def value
      items.collect{|i| i.text_value}
    end
  end
  
  class ArrayItem < Treetop::Runtime::SyntaxNode
    def to_hash
      {:value => value}
    end
    
    def value
      text_value
    end
  end
end