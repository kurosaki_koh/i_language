# coding : utf-8

module JcodeUtil
  def sub_nums(str)
    text = str.dup
    text.tr!('０-９','0-9')
    text.gsub!('＋','+')
    text.gsub!('－','-')
    text.gsub!('．','.')
    return text
  end
  
  def parse_num(str)
    text = sub_nums(str)
    if text =~ /((\+|\-)?[0-9]+\.[0-9]+)/
      return $1.to_f
    elsif text =~ /((\+|\-)?[0-9]+)/
      return $1.to_i
    else
      return nil
    end
  end

  alias :parse_int  :parse_num
end
