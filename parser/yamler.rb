require 'idef_db'
require 'yaml'
#require 'yaml_waml'

#IdefSqlite::init_sqlite_db
option = ARGV.shift

records = IDefinition.find(:all)
db = {}
for rec in records
  db[rec[:name]]=rec.compiled_hash
end

if option == '--json'
  require 'rubygems'
  require 'json_builder'
  b = JsonBuilder.new
  STDOUT.puts b.build(db)
else
  YAML.dump(db,STDOUT)
end
