$KCODE='u'

require 'rubygems'
require 'active_record'
#p Dependencies.load_paths
#puts $LOAD_PATH.join("\n")

$: << File.dirname(__FILE__)
require 'lib.rb'
require 'i_definition.rb'
require 'yaml'
#require 'yaml_waml'
require 'uconv'

module ILanguage
  module UtilsConfig
    DefaultConfigFile = File.join(File.dirname(__FILE__),'idef_db.yml')
    @@configs = YAML.load_file(DefaultConfigFile)[:configs]
    def load_config(filename)
      @@configs = YAML.load_file(filename)[:configs]
    end
#    @@configs = {}
    @@configs[:db_config] = {
        :adapter => 'sqlite3',
        :dbfile => File.join(File.dirname(__FILE__),'idef.sqlite3') ,
        :encoding => 'UTF8'
      }

  end

  module IdefSqlite
    include ILanguage::UtilsConfig

    class InitialSchema < ActiveRecord::Migration
      def self.up
        create_table :i_definitions do |t|
          t.string :name
          t.string :object_type
          t.text :definition
          t.text :compiled_hash
          t.timestamps
        end
      end

      def self.down
        drop_table :i_definitions
      end
    end

    def self.init_sqlite_db(db_filename=nil)
      if db_filename =~ /^\.\//
        db_filename = File.join(File.dirname(__FILE__),db_filename) 
      end
      @@configs[:db_config][:dbfile] = db_filename if db_filename

      unless File.exist?(@@configs[:db_config][:dbfile])
        require 'sqlite3'
        db=SQLite3::Database.new(@@configs[:db_config][:dbfile])
      end

      ActiveRecord::Base.establish_connection(
        @@configs[:db_config]
      )

      unless IDefinition.table_exists?
        InitialSchema.migrate(:up)
      end
    end
    
    init_sqlite_db(@@configs[:db_file])
  end
end



