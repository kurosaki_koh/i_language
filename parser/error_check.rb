# -*- encoding: utf-8 -*-
require 'kconv'
$LOAD_PATH.unshift File.dirname(__FILE__)
require 'idef_db.rb'

DefinedKeywords = ['カテゴリ','位置づけ','着用制限','オプション','行為補正','行為','補正',
  'アタックランク','搭乗制限','搭乗資格','必要パイロット数','イベント時資源消費','イベント時食料消費',
  'イベント時燃料消費','生産','イベント時マイル消費','パイロット資格','コパイロット資格','みなし職業',
  '出仕','人機数','航路数','輸送力','必要コパイロット数','参加時食糧消費','着用箇所',
  'パイロット能力補正','初期ＡＲ修正','移動時ＡＲ消費軽減' ,
  '騎乗者数','人員輸送','基本オプション装備','パイロット最大搭乗数']

ContainedKeywords = ['（生産フェイズごとに）','輸送可能','（戦闘イベント参加時）',
  '消費'
  ]


$num_cost_error = 0
def cost_error(doc)
  return unless doc['特殊定義']
  for sp_node in doc['特殊定義']
    if sp_node['コスト定義']
      if sp_node['コスト定義'] != ''  && sp_node['コスト定義'] != 'なし' && sp_node['コスト定義'] !~ /^(、)?(燃料|資源|食料|生物資源)－/
        puts "コスト:#{sp_node['コスト定義']}\n定義：#{sp_node['定義']}\n\n"
        $num_cost_error += 1
      end
    end
  end
end

$num_etc_error = 0
def etc_error(doc)
    if doc['特殊定義']
      for sp_node in doc['特殊定義']
#       for key in sp_node.keys
#         puts "#{key},#{sp_node[key]}".tosjis
#       end
        if sp_node['特殊種別'] == 'その他定義'
          if DefinedKeywords.any?{|dk| sp_node['定義']=~ /#{dk}.?　＝　/}
            puts "＊#{sp_node['定義']}"
            $num_etc_error += 1
#          elsif ContainedKeywords.any?{|dk| sp_node['定義']=~ /#{dk}/}
#            puts "＊#{sp_node['定義']}"
#            $num_etc_error += 1
          end
        end
      end
    end
end

$num_note_error = 0
def fuel_in_note(doc)
  if doc['特殊定義']
    for sp_node in doc['特殊定義']
      if sp_node['付記'] =~ /燃料/ || sp_node['付記'] =~ /評価/
        puts "#{sp_node['定義']}"
        $num_note_error += 1
      end
    end
  end
end

#SourcePath = File.join(File.dirname(__FILE__),'../trunk/アイドレスデータ.txt'.tosjis)
include ILanguage::UtilsConfig
SourcePath = File.join(File.dirname(__FILE__),@@configs[:source].tosjis)
source_sjis = open(SourcePath,'r').read
source = Uconv.sjistou8(source_sjis).to_a

first_lines = source.grep(/^Ｌ：/)
EntireNames = first_lines.collect{|line|
  line =~ /^Ｌ：(.+?)　＝　/
  $1
}.compact

def correspondence(docs)
  for name in EntireNames
    unless IDefinition.find_by_name(name)
      puts name + " not compiled."
    end
  end
end

def count_name
  hash = Hash.new{|hash,key|
    hash[key]=0
  }

  for name in EntireNames
    hash[name] += 1
  end

  for key in hash.keys
    if hash[key] > 1
      puts "#{key}\n"
    end
  end
end

$num_contains_category = 0
def contains_category_check(idefs)
    puts "\n■Ｌ：名にカテゴリ名を含む\n"
	for idef in IDefinition.find(:all)
	  name = idef.name
	  if Categories.any?{|c| name =~ /（#{c}）/ }
		puts name
	  	$num_contains_category += 1
	  end
	end
	puts
end

$num_parse_failure = 0
def parse_failure(doc)
    if doc['エラー']
      puts '●名称：'+ doc['名称'].to_s
      puts doc['エラー']
      puts doc[:failure_reason]
      puts "定義：\n"+doc['定義']
      p doc
      $num_parse_failure += 1
    end
end
docs = []
puts "\n■構文エラーで解析できず\n\n"
$num_sp=0
#open('parse_result.yml','r')do |port|
#  YAML.load_documents(port) do |doc|
#    docs << doc
#    parse_failure(doc)
#    $num_sp += doc['特殊定義'].size if doc['特殊定義']
#  end
#end

#全カテゴリの取得
Categories = IDefinition.find_by_sql(<<SQL).collect{|i|i.object_type}
    select distinct object_type from i_definitions
SQL

idefs = IDefinition.find(:all)
for idef in idefs
  doc = idef.compiled_hash
  if doc
    doc['名称'] = idef.name unless doc['名称']
    docs << doc
    parse_failure(doc)
    $num_sp += doc['特殊定義'].size if doc['特殊定義']
  else
#    puts "compiled_hash is nil:#{idef.inspect}"
  end
end

puts "\n\n■「コスト」の値が想定外\n\n"
for doc in docs
  cost_error(doc)
end

puts "\n\n■「付記」の中に「燃料」「評価」が含まれる。\n"
for doc in docs
  fuel_in_note(doc)
end

puts "\n\n■Ｌ：名の重複\n"
count_name()

puts "\n\n■Ｌ：とコンパイル結果の不一致\n"
correspondence(docs)

puts "\n\n■「その他」分類されたものの、判定結果が怪しい\n\n"
for doc in docs
  etc_error(doc)
end

contains_category_check(idefs)


puts "\n\n■集計"
puts "・全アイドレス定義数：#{docs.size}"
puts "・・構文解析失敗：#{$num_parse_failure}"
puts "・特殊定義数合計：#{$num_sp}"
puts "・・「その他」誤分類：#{$num_etc_error}"
puts "・・「コスト」誤認：#{$num_cost_error}"
puts "・・Ｌ：名にカテゴリ記述：#{$num_contains_category}"

