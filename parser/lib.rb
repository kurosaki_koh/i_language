require 'rubygems'
require 'treetop'
require 'uconv'

dir = File.dirname(__FILE__)
$: << dir unless $:.find{|path| path == dir}

require 'lib/i_language_def.rb'
require 'lib/specialty_def.rb'
require 'lib/i_language_library.rb'
require 'lib/i_specialties.rb'

#Treetop.load(File.join(dir,'lib/i_language_library.treetop'))
#Treetop.load(File.join(dir,'lib/i_specialties.treetop'))

require 'i_definition_source.rb'


