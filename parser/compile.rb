dir = File.dirname(__FILE__)
require File.join(dir , 'compiler.rb')
require 'uconv'
require File.join(dir ,'../database')

$KCODE='u'

Ilanguage::IDefinitionDB::init_sqlite_db()

compiler = ILanguage::Compiler.new
compiler.parse_option(ARGV)
max = compiler.source.key_list.size
i = 1
compiler.compile_all{|name|
  puts Uconv.u8tosjis(sprintf("%s (%d/%d)",name , i  , max))
  i += 1
}
