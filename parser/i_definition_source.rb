# -*- encoding: utf-8 -*-
require 'uconv'
require 'kconv'
module ILanguage
  class IDefinitionSource
    source_path = File.join(File.dirname(__FILE__),'../source/アイドレスデータ.txt'.tosjis)
    ruby2exe_soruce_path = File.join(File.dirname(__FILE__),'../lib/アイドレスデータ.txt'.tosjis)
    if File.exist? source_path
      DefaultSourcePath = source_path
    elsif File.exist? ruby2exe_soruce_path
      DefaultSourcePath = ruby2exe_soruce_path
    else
      path = File.join(File.dirname(__FILE__),'../source/アイドレスデータ.txt')
      if File.exist?(path)
        DefaultSourcePath = path
      else
        DefaultSourcePath = path.tosjis
      end
    end
    
    attr :path,true
    attr :index,false

    LibraryParser = ILanguageLibraryParser.new
    SpecialtyParser = ISpecialtiesParser.new
    
    def initialize(path)
      @index = Hash.new{|hash,key|hash[key]={}}
      @path = path
      read_index if @path
    end

    def open
      unless @io
        if @path.class <= IO
          @io = @path
        else
          @io = File.open(@path,'r')
          @io.binmode
        end
      end
    end

    def close
      @io.close
      @io = nil
    end

    def read_until_next_lib
      result = ''
      while !@io.eof?
        pos = @io.pos
        line = Uconv.sjistou8(@io.readline)
        if line =~ /^Ｌ：(.+)　＝　/
          @io.pos = pos
          break
        end
        result += line
      end
      return result
    end

    def read_index(target=nil)
      open
      while !@io.eof?
        pos = @io.pos
        line = Uconv.sjistou8(@io.readline)

        case line
        when /^＃/
          next
        when /^Ｌ：(.+)　＝　/
          name = $1
          @index[name][:pos]=pos
          result = line + read_until_next_lib
          @index[name][:size]= (@io.pos - pos)
          return if target && target == name
        end
      end
    end

    def read_definition(name,with_comment = false)
      open
      @io.pos = @index[name][:pos]
      text = @io.read(@index[name][:size])
      text = Uconv.sjistou8(text)
      if text !~ /^Ｌ：#{name}/
        read_index(name)
        @io.pos = @index[name][:pos]
        text = @io.read(@index[name][:size])
      end
      text = text.gsub!("\r","")
      unless with_comment
        comment_checks = []
        text.each_line{|line| comment_checks << line}
        if comment_checks[-1] =~ /^＃/
          comment_checks.delete_at(-1)
          text = comment_checks.join
        end
      end

      return text
    end

    def parse_library_node(name)
      text =read_definition(name)
      lib_node = LibraryParser.parse(text) 
      if lib_node
         return lib_node
      else
        raise LibraryParser.failure_reason
      end
    end
    
    def parse_specialty_node(name)
      lib_node = parse_library_node(name).to_a[0]
      sp_node = SpecialtyParser.parse(lib_node.specialty_text)
      if sp_node
        return sp_node
      else
        raise SpecialtyParser.failure_reason
      end
    end
    
    def key_list
      @index.keys.sort{|a,b| @index[a][:pos] <=> @index[b][:pos]}
    end
    
    def [](key)
      return @index[key]
    end
  end
end