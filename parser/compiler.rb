# -*- encoding: utf-8 -*-
dir = File.dirname(__FILE__)

require 'rubygems'
require 'active_record'
require File.join(dir,'i_definition.rb')
require 'optparse'

require 'yaml'
#require 'yaml_waml'

#include ILanguage

module ILanguage

  class CompilerOptionParser
    OptParser = OptionParser.new{|opt|
        opt.on('--force-all'){
          @force_all = true
        }
        opt.on('--clear'){
          @clear = true
        }
        opt.on('--keyword KEYWORD',String){|keyword|
          @keyword = keyword
        }
        opt.on('-f FILENAME',String){|filename|
          @filename = filename
        }
        opt.on('--delete-orphan',String){
          @delete_orphan = true
        }
    }
  end
      
  class Compiler
    attr :source , false
    attr :filename , false
    attr :optparser,false

    attr :force_all ,false
    attr :clear , false
    attr :keyword , false
    
    def initialize(options)
      @options = options.dup
      @filename = @options[:filename] if @options[:filename]
    end
    def process_options
      IDefinition.update_all('compiled_hash = NULL') if @options[:force_all]

      IDefinition.delete_all if @options[:clear]
      if @options[:keyword]
        IDefinition.update_all('compiled_hash = NULL',["definition like ?",'%'+@options[:keyword].toutf8+'%'])
      end
    end

    def parse_option(argv)
      OptParser.parse(argv)
      process_options
      open_source(@filename)
      delete_orphan_record() if @options[:delete_orphan]
    end

    def open_source(filename = nil)
      @source = if filename
        @filename = filename
        IDefinitionSource.new(filename)
      else
        @filename = nil
        IDefinitionSource.new
      end
    end

    def delete_orphan_record
      key_list = @source.key_list.dup
      for rec in IDefinition.find(:all)
        print "#{rec.name}:"
        if key_list.find{|item| item == rec.name}
          key_list.delete(rec.name)
          puts "exist."
        else
          rec.destroy
          rec.save
          puts "not found."
        end
      end
    end

    def compile_all
      max = @source.key_list.size
      IDefinition.delete_all("name = ''")
      IDefinition.delete_all("name is NULL")
      i =1
      for name in @source.key_list
#        puts Uconv.u8tosjis(sprintf("%s (%d/%d)",name , i  , max))
        yield(name) if block_given?
        compile(name)
        i += 1
      end
    end

    def compile(name)
      idef = IDefinition.find_by_name(name)
      idef = IDefinition.new unless idef
      def_text = @source.read_definition(name)
      if idef.compiled_hash == nil || idef.compiled_hash['エラー'] || idef.definition != def_text

        idef.definition = def_text
        idef.save
        return idef
      else
        return nil
      end
    end
  end
end
